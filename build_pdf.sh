#!/bin/bash

### run pandoc to produce a latex file
### file are extracted from mldoc.yml

echo "File list:"
echo "   " `grep "\.md" mkdocs.yml | grep ":" | sed -f extract2.sed | sed -e s%index/index%index%`

pandoc \
    `grep "\.md" mkdocs.yml | grep ":" | sed -f extract2.sed  | sed -e s%index/index%index%` \
    pandoc.yaml \
    -s -o manual.html
       

echo DONE DONE
