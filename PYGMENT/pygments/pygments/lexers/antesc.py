# -*- coding: utf-8 -*-
"""
    pygments.lexers.antesc
    ~~~~~~~~~~~~~~~~~~~~~~

    Lexers for Antescofo augmented scores.

    :license: BSD, see LICENSE for details.
"""


import re

from pygments.lexer import RegexLexer, include, bygroups, default, words
from pygments.token import *
Literal

__all__ = ['AntescofoLexer']


class AntescofoLexer(RegexLexer):
    """
    Lexer for Antescofo augmented score
    """

    name = 'Antescofo'
    aliases = ['antesc', 'antescofo']
    filenames = ['*.antesc', '*.antesc.txt', '*.antescofo', '*.antescofo.txt', '*.asco', '*.asco.txt']
    mimetypes = ['text/x-antescofo']
    flags = re.MULTILINE | re.VERBOSE | re.IGNORECASE | re.UNICODE

    
    tokens = {
        
        'numbers': [
            (r'\d+[.]\d*', Number.Float),
            (r'(\d+)(\s*)[/](\s*)(\d+)', Number.Float), (r'(\s*)\.\d+', Number.Float),
            (r'(\s*)((\d+\.\d*)|(\.\d+|\d+))([eE]([+-]?)\d+)?',
             Number.Float), (r'\d+', Number.Integer)
        ],
        
        'eventattribute': [
            (words(('@fermata', '@hook', '@inlet', '@pizz', '@staccato', '@nosync' 'bpm',
                    'fermata', 'hook', 'jump', 'pizz', '@jump',
                    'nosync', 'rubato', 'staccato', 'transpose',
                    'variance'),
                   suffix=r'\b'), Keyword.Type)
        ],
        
        
        ## ur'' = raw unicode string, \u2080-\u208e = indice 0 à 9 et subscript + - = () \u1D62 subscript i
        ## u'' = unicode string, \u2080-\u208e = indice 0 à 9 et subscript + - = () \u1D62 subscript i
        ## the second is used with python3, the first works with python2
        
        'name': [
            (r'\@[a-zA-Z0-9_]+', Name.Function),
            (r'obj::[a-zA-Z0-9_]+', Name.Namespace),
            (r'antescofo::[a-zA-Z0-9_]+', Name.Builtin),
            (r'track::[a-zA-Z0-9_]+', Name.Constant),
            (r'faust::[a-zA-Z0-9_]+', Name.Constant),
            (r'dsp::[a-zA-Z0-9_]+', Name.Constant),
            (r'json::', Name.Constant),
            (r'pattern::[a-zA-Z0-9_]+', Name.Namespace),
            (r'\:\:[a-zA-Z0-9_]+', Name.Namespace),
            (r'\$[a-zA-Z0-9_]+', Name),
            (r'\.\.\.', Keyword.Pseudo),
            (r'(true)|(false)\b', Name.Builtin),
            (r"'\d+", Name.Builtin),
            (r'[a-zA-Z\#][a-zA-Z0-9\#\/~?!_$><.-]*', Name.Class),
            (u'[\u2080-\u2089\u1D62]+', Name.Class)
        ],
        
        'suitevent': [
            (r'[-]?[abcdefgABCDEFG]([xb#][xb#]?)?(\d+)([+-]\d+)?', Literal, 'suiteventbis'),
            (r'\->', Punctuation, 'suiteventbis'),
            (r'[(),-]', Punctuation, 'suiteventbis'),            
            (r'\'', Punctuation),            
            (r'\d+[.]\d*', Literal, 'suiteventbis'),
            (r'(\d+)([ \t]*)[/]([ \t]*)(\d+)', Literal, 'suiteventbis'),
            (r'\d+', Literal, 'suiteventbis'),
            (r'[ \t]+', Text),
            (r'\\\n', Text),  
            (r'\n', Text, '#pop'),
            (r'//(\n|(.*[^\\]\n))', Comment.Single, '#pop'),
            (r';.*\n', Comment.Single, '#pop')
        ],
        
        'suiteventbis': [
            (r'[-]?[abcdefgABCDEFG]([xb#][xb#]?)?(\d+)([+-]\d+)?', Literal),
            (r'\->', Punctuation),
            (r'[(),-]', Punctuation),            
            (r'\'', Punctuation),            
            (r'\d+[.]\d*', Literal),
            (r'(\d+)([ \t]*)[/]([ \t]*)(\d+)', Literal),
            (r'\d+', Literal),
            include('eventattribute'),
            (r'[a-zA-Z_\#\"][a-zA-Z0-9\#\/~?!_$><.-]*[\"]?', Name.Entity),
            include('name'),
            (r'[ \t]+', Text),
            (r'\\\n', Text),  
            (r'\n', Text, '#pop:2'),
            (r'//(\n|(.*[^\\]\n))', Comment.Single, '#pop:2'),
            (r';.*\n', Comment.Single, '#pop:2')
        ],
        
        # white space including comments
        'whitespace': [
            (r'[\…]|([*][*][a-zA-Z0-9\#\/~!_$.-]+[*][*])', Name.Other),
            (r'\n', Text),
            (r'\s+', Text),
            (r'\\\n', Text),  
            (r'//(\n|(.*[^\\]\n))', Comment.Single),
            (r';.*\n', Comment.Single),
            (r'/(\\\n)?[*](.|\n)*[*](\\\n)?/', Comment.Multiline),
            (r'\*/', Error)
        ],
        
        'attribute' : [
            (words(('@abort', '@action', '@ante', '@coef', '@conservative',
                    '@exclusive', '@grain', '@guard', '@immediate',  '@kill',
                    '@label', '@latency', '@loose', '@name', '@norec',
                    'rec_if_alive', '@override',
                    '@handling_int64', '@handling_double', '@handling_tab',
                    '@post', '@progressive', '@refractory', '@staticscope',
                    '@sync', '@target', '@tempo', '@tight', '@type'),
                   suffix=r'\b'), Name.Attribute),
            (r'_?[ \t]*:=', Operator),
            include('numbers')
        ],
        
        'lambda': [
            (r'\\', Operator.Word, 'lambdaarg')
        ],
        
        'lambdaarg': [
            include('whitespace'),
            (r'\$[a-zA-Z0-9_]+', Name),
            (r'[\,]', Punctuation),
            (r'\.[ \n\t]*\(', Operator.Word, 'lambdabody')
        ],
        
        'lambdabody': [
            include('whitespace'),
            (words(('@assert', '@alocal', 'abort', 'during', 'case', 'else', 'expr',
                    'forall', 'if', 'let', 'loop', 'map', 'nim', 'return', 
                    'switch', 'symb', 'tab', 'until', 'while', 'with'),
                   suffix=r'\b'), Keyword.Declaration),
            (r'\(', Punctuation,  'expression'),
            (r'\)', Operator.Word,  '#pop:2'),
            
            (r'[\{\[\]\}]', Punctuation),
            
            include('atomic'),
            include('lambda')
        ],
        
        'scope': [
            (r'[\(]', Punctuation, 'expression'),
            (r'[{\[\(]', Punctuation, 'action'),
            (r'[}\]\)]', Punctuation, '#pop')                        
        ],
        
        'expression': [
            include('whitespace'),
            (words(('@assert', '@alocal', 'abort', 'during', 'case', 'else', 'expr',
                    'forall', 'if', 'let', 'loop', 'map', 'nim', 'return', 
                    'switch', 'symb', 'tab', 'until', 'while', 'with'),
                   suffix=r'\b'), Keyword.Declaration),
            
            (r'[\(\{\[]', Punctuation, 'expression'),
            (r'[}\]\)]', Punctuation, '#pop'),
            
            include('lambda'),
            include('scope'),
            include('atomic')
        ],
        
        'atomic': [
            (r'@\!\=', Name.Builtin),
            (r'@\=\=', Name.Builtin),
            (r'@\&\&', Name.Builtin),
            (r'@\|\|', Name.Builtin),
            (r'@\+', Name.Builtin),
            (r'@\/', Name.Builtin),
            (r'@\%', Name.Builtin),
            (r'@\*', Name.Builtin),
            (r'@\>', Name.Builtin),
            (r'@\>\=', Name.Builtin),
            (r'@\<', Name.Builtin),
            (r'@\<\=', Name.Builtin),
            
            (words(('"linear"', '"linear_in"', '"linear_out"', '"linear_in_out"',
                    '"back"', '"back_in"', '"back_out"', '"back_in_out"', '"bounce"',
                    '"bounce_in"', '"bounce_out"', '"bounce_in_out"', '"cubic"',
                    '"cubic_in"', '"cubic_out"', '"cubic_in_out"', '"circ"',
                    '"circ_in"', '"circ_out"', '"circ_in_out"', '"elastic"',
                    '"elastic_in"', '"elastic_out"', '"elastic_in_out"', '"exp"',
                    '"exp_in"', '"exp_out"', '"exp_in_out"', '"quad"', '"quad_in"',
                    '"quad_out"', '"quad_in_out"', '"quart"', '"quart_in"', '"quart_out"',
                    '"quart_in_out"', '"quint"', '"quint_in"', '"quint_out"',
                    '"quint_in_out"', '"sine"', '"sine_in"', '"sine_out"', '"sine_in_out"',
                    '"poly3"')), String.Interpol),
            
            (words(('true', 'false'), suffix=r'\b'), Name.Builtin),
            
            (r'[<]undef[>]', Name.Builtin),
            
            (r'"', String, 'string'),
            
            (r'@UID\b', Keyword.Pseudo),
            (r'@LID\b', Keyword.Pseudo),
            
            include('attribute'),
            
            (words(('@assert', 'abort', 'action', 'and', 'at', 'before', 'bind', 'case',
                    'closefile', 'curve', 'do',  'during', 'else', 'expr',
                    'forall', 'gfwd', 'group', 'if', 'imap', 'in', 'kill', 'let', 'lfwd',
                    'loop', 'map', 'napro_trace', 'nim', 'noevent', 'of', 'off', 'on',
                    'openoutfile', 'oscoff', 'oscon', 'oscrecv', 'oscsend', 'osc_client',
                    'parfor', 'patch', 'port', 'return', 'start', 'state', 'event', 'note',
                    'stop', 'switch', 'symb', 'tab', 'tempo', 'until', 'value', 'whenever',
                    'where', 'while', 'with'),
                   suffix=r'\b'), Keyword.Reserved),
            
            (words(('$RT_TEMPO', '$SCORE_TEMPO', '$PITCH', '$DURATION', '$BEAT_POS',
                    '$LAST_EVENT_LABEL', '$NOW', '$RNOW', '$RCNOW', '$APPROX_RATIO',
                    '$ENERGY', '$JUMP_UPDATED', '$FIRST_DETECTED_EVENT', '$THISOBJ',
                    '$MYSELF'),
                   suffix=r'\b'), Name.Builtin.Pseudo),
            
            (r'[\,\.]', Punctuation),
            
            (r'(s|(ms))\b', Name.Decorator),
            (r'\#', Name.Decorator),
            
            (words(('@assert', '@command', '@date', '@dump', '@history_length',
                    '@is_undef', '@map_history','@map_history_date', 
                    '@map_history_rdate', '@plot', '@rdate', 
                    '@rplot', '@tab_history', '@tab_history_date', '@tab_history_rdate'),
                   suffix=r'\b'), Name.Builtin.Pseudo),
            
            (words(('nim', 'tab', 'map'), suffix=r'\b'), Keyword.Type),
            
            (words(('@Tracing', '@UnTracing', '@abs', '@acos', '@add_pair', '@aggregate',
                    '@align_breakpoints', '@approx', '@arch_darwin', '@arch_linux',
                    '@arch_windows', '@asin', '@at', '@atan', '@between',
                    '@bounded_integrate', '@bounded_integrate_inv', '@car', '@cdr', '@ceil',
                    '@clear', '@compose_map', '@concat', '@cons', '@copy', '@cos', '@cosh',
                    '@count', '@dim', '@domain', '@drop', '@dump', '@dumpvar', '@empty',
                    '@exp', '@explode', '@filter_max_t', '@filter_median_t',
                    '@filter_min_t', '@find', '@flatten', '@floor', '@gnuplot',
                    '@gshift_map', '@integrate', '@iota', '@is_bool', '@is_defined',
                    '@is_exec', '@is_fct', '@is_float', '@is_function', '@is_int',
                    '@is_integer_indexed', '@is_interpolatedmap', '@is_list', '@is_map',
                    '@is_nim', '@is_numeric', '@is_obj', '@is_prefix', '@is_proc',
                    '@is_string', '@is_subsequence', '@is_suffix', '@is_symbol', '@is_tab',
                    '@is_undef', '@is_vector', '@lace', '@last', '@linearize', '@listify',
                    '@loadvalue', '@loadvar', '@log', '@log10', '@log2',
                    '@make_duration_map', '@make_label_bpm', '@make_label_duration',
                    '@make_label_pitches', '@make_label_pos', '@make_score_map', '@map',
                    '@map_compose', '@map_concat', '@map_normalize', '@map_reverse',
                    '@map_val', '@mapval', '@max', '@max_key', '@max_val', '@member',
                    '@merge', '@min', '@min_key', '@min_val', '@normalise', '@normalize',
                    '@occurs', '@permute', '@pow', '@projection', '@push_back',
                    '@push_front', '@rand', '@rand_int', '@random', '@range', '@reduce',
                    '@remove', '@remove', '@remove_duplicate', '@replace', '@reshape',
                    '@resize', '@reverse', '@rnd_bernoulli', '@rnd_binomial',
                    '@rnd_exponential', '@rnd_gamma', '@rnd_geometric', '@rnd_normal',
                    '@rnd_poisson', '@rnd_uniform_float', '@rnd_uniform_int', '@rotate',
                    '@round', '@sample', '@savevalue', '@scan', '@scramble', '@select_map',
                    '@shape', '@shift_map', '@simplify_lang_v',
                    '@simplify_radial_distance_t', '@simplify_radial_distance_v', '@sin',
                    '@sinh', '@size', '@slice', '@sort', '@sputter', '@sqrt', '@string2fun',
                    '@string2proc', '@stutter', '@system', '@take', '@tan',
                    '@window_filter_t'),
                   suffix=r'\b'), Name.Builtin),
            
            (r'==>', Operator.Word),
            (r'\+=>', Operator.Word),
            (r'\:\:', Operator.Word),
            (r'[_]?[ \t]*:=', Operator), 
            (r'\+=', Operator), 
            (r'\*=', Operator), 
            (r'\-=', Operator), 
            (r'\/=', Operator), 
            (r'==', Operator),
            (r'[~!%^&*+=|?:<>/-]', Operator),
            
            include('name')
        ],
        
        'action': [
            include('whitespace'),
            (words(('@react', '@init', '@signal', '@whenever', '@local', '@global'), suffix=r'\b'),
             Keyword.Declaration),
            include('attribute'),
            (r'[{\[\(]', Punctuation, '#push'),
            (r'[}\]\)]', Punctuation, '#pop'),
            include('expression')
        ],
        
        'args': [
            include('whitespace'),
            (r'[()]', Text),
            (r'\$?[a-zA-Z0-9_]+', Name),
            (r'\,', Punctuation),
            (r'\{', Punctuation, ('#pop', 'action')),
            include('attribute')
        ],
        
        'root': [
            include('whitespace'),
            
            (words(('bpm', '@transpose', '@fermata', '@hook', '@inlet', '@pizz',
                    '@staccato', 'bpm', 'fermata', 'hook', 'jump', 'pizz', '@jump',
                    'top_level_groups_are_tight',
                    'top_level_groups_are_loose', 'bypass_temporeset', 'dummysilence',
                    'nosyncsection', 'pizzsection', 'rubato', 'staccato', 'transpose', 'variance'),
                   suffix=r'\b'), Keyword.Type),
            
            
            (r'@global\b', Keyword.Declaration),
            (r'@tempovar\b', Keyword.Declaration),
            (r'@insert\b', Keyword.Pseudo),
            
            (words(('chord', 'multi', 'note', 'event', 'trill'), suffix=r'\b'),
             Keyword.Type,
             'suitevent'),
            
            (r'(@fun_def)(\s+)(\@?[a-zA-Z_][a-zA-Z0-9_]*)(\s*)',
             bygroups(Keyword.Declaration, Text, Name.Function, Text), 'args'),
            
            (r'(@macro_def)(\s+)(\@?[a-zA-Z_][a-zA-Z0-9_]*)(\s*)',
             bygroups(Keyword.Declaration, Text, Name.Function, Text), 'args'),
            
            (r'(@proc_def)(\s+)((?:\:\:)?[a-zA-Z_][a-zA-Z0-9_]*)(\s*)',
             bygroups(Keyword.Declaration, Text, Name.Namespace, Text), 'args'),
            
            (r'(@method_def)(\s+)(\@?[a-zA-Z_][a-zA-Z0-9_]*)(\s*)',
             bygroups(Keyword.Declaration, Text, Name.Function, Text), 'args'),
            
            (r'(@obj_def)(\s+)((?:obj\:\:)?[a-zA-Z_][a-zA-Z0-9_]*)(\s*)',
             bygroups(Keyword.Declaration, Text, Name.Namespace, Text),'args'),
            
            (r'(@pattern_def)(\s+)((?:pattern\:\:)?[a-zA-Z_][a-zA-Z0-9_]*)(\s*\{)',
             bygroups(Keyword.Declaration, Text, Name.Namespace, Text), 'action'),
            
            (r'(@track_def)(\s+)((?:track\:\:)?[a-zA-Z_][a-zA-Z0-9_]*)(\s*\{)',
             bygroups(Keyword.Declaration, Text, Name.Constant, Text), 'action'),
            
            (r'(@eval_when_load)(\s*\{)',
             bygroups(Keyword.Declaration, Text), 'action'),
            
            (words(('@dsp_channel', '@dsp_cvar', '@dsp_inlet', '@dsp_link', '@dsp_outlet'),
                   suffix=r'\b'), Keyword.Declaration),
            
            (r'[{]', Punctuation, 'action'),
            (r'[}]', Error),
            include('expression'),
        ],
        
        'string': [
            (r'"', String, '#pop'),
            (r'\\([\\abfnrtv"\']|x[a-fA-F0-9]{2,4}|'r'u[a-fA-F0-9]{4}|U[a-fA-F0-9]{8}|[0-7]{1,3})',
             String.Escape),
            (r'[^\\"\n]+', String),  # all other characters
            (r'\\\n', String),       # line continuation
            (r'\\', String)          # stray backslash
        ]
    }
