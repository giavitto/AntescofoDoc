

<center>
![antescofo logo](Figures/logo_antescofo_couleur.png)
</center>


**Antescofo** is a coupling of a real-time listening machine with a
reactive and timed synchronous language. The language is used for
authoring of music pieces involving live musicians and computer
processes, and the real-time system assures its *correct* performance
and synchronization despite listening or performance errors.

These pages documents the language starting from version _0.5_.  Users
willing to practice the language are strongly invited to download
Antescofo and use the additional Max or PureData tutorials (with example
programs) that come with them for a sensible illustration of the
language.

Antescofo scores can be edited using Sublime or Atom editors, with a
dedicated mode, making possible to interact in real-time with a runing
Antescofo object via OSC messages. The score following can be monitored
using a Bach.roll object in Max. The dedicated [Ascograph][] editor and
visualizer is no longer maintened and is restricted to the handling of a
very restricted subset of the language.





### The documentation

- starts with the **[User Guide]** including the description of 
  the **[Antescofo Workflow]** which gives a bird view of the system,

- proceeds with the **[Reference Manual]** for detailed explanation,

- and refers to  by the description of **[Library Functions]** and an
  **[Antescofo How-To]**.


### Accessing the information

These documents are available as
[online web pages](https://discussion.forum.ircam.fr/c/antescofo). The
set of web pages for local use (when there is no internet connection) can
be downloaded as a zipped file
[AntescofoDocHTML.zip](http://support.ircam.fr/docs/Antescofo/AntescofoDocHTML.zip)
(it unzips into a directory named _AntescofoDocHTML_ and the file
_index.html_ in it can be opened in a browser). This file is also
bundled with the *Antescofo package*.  An older version of the documentation in
PDF form is [still
available](http://support.ircam.fr/docs/Antescofo/AntescofoReference.pdf).


Besides these documents, additional information on Antescofo can be
found:

- on the [Project home page](http://repmus.ircam.fr/antescofo)

- on IRCAM's users [Forum](https://discussion.forum.ircam.fr/c/antescofo)
  where you can find tutorials to download with bundles for MAX and
  PureData

- on the web site of the [MuTAnt team-project](http://repmus.ircam.fr/mutant)
  where you can find the scientific and technical publications on Antescofo

- on [Project Development Forge](https://antescofo.atlassian.net/secure/RapidBoard.jspa?rapidView=14)
  where you can post a bug report

- the page **[Release Notes]** at the end of the reference manual lists
  the changes between versions.


### Your feedback is important

Please, send your comments, typos, bugs reports, suggestions, [use
cases, hints, and tips](/Library/snipets/) on the Antescofo
[ForumUser](https://discussion.forum.ircam.fr/c/antescofo)
web pages. It will help us to improve the documentation and the
_Antescofo_ system.



{!references.ref!}
