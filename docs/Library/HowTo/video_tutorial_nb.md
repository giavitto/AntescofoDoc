<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}


# Tutorial Videos
<br>


nadir B. has produced [10 short
videos](https://www.youtube.com/playlist?list=PLJ-R9Hd3xi4D5x8du-yWZOy8lGNEdgn4a)
that provide a thoughtful and accessible introduction to the Antescofo
programming language:

1. [The_Very_Beginning](https://youtu.be/ODrl0c-M3dA)
2. [The Curve](https://youtu.be/eJylX99A9bA)
3. [Follow The Curve](https://youtu.be/96JPAUcquyA)
4. [To Wait Or Not To Wait](https://youtu.be/2jRKQ82Ta6Q)
5. [Relationships In A Curve: A Glimpse](https://youtu.be/o_QSeKxleDs)
6. [Relationships In A Curve: BPM/@Grain/The BPFs](https://youtu.be/80RppLGx2ug)
7. [Count And Follow: BPM/@Grain/BPFs/Steps](https://youtu.be/tc-CcIJ4pAY)
8. [What About Interpolation Type](https://youtu.be/LUCM9sywgQM)
9. [Back To The Beginning](https://youtu.be/7qFbqJTWmAE)
10. [Multiple Inputs And Outputs](https://youtu.be/xSiUWpQcx0o)


