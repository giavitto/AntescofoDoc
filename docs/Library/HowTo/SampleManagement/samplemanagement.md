<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




# Managing Sample Playback in Presence of Transport Commands

<br>

This _how-to_ details a possible approach to the proper management of
sample playback while using [transport commands].



## The problem with transport commands

During a rehearsal, it is essential to have the ability to start the
execution of the score at a given label. This can be done by using a
[transport command], for example _startfromlabel_.  In this case, the
execution of the score is started in [fastforward] mode up to the label
and then the normal execution is resumed.

This approach is adequate when the computation at the other end of the
messages send by _Antescofo_ are “stateless”. As a matter of fact,
messages are not sent during the fastforward mode. However, when the
computation has a state, we need to send the relevant messages to update
this state correctly, even in fastforward mode. This is the case for
instance for ‟on/off” message sent to a device. 
  
A possible solution is to use the transport commands
[antescofo::scrubtobeat] or [antescofo::scrubtolabel], as these commands
send all messages produced during the fast forward phase. However, this
solution is not always possible: the accumulation of messages sent
during the short duration of the fastforward mode can overload Max or PD
and lead to a crash.  to a crash. Antescofo limits the number of
messages sent in one logical moment (i.e. simultaneously) to avoid such
a crash. This limit can be increased (or decreased) by a call to the
[@set_max_message_sent_instantaneously] function.

But even if this approach is applicable, it does not achieve the correct
behavior in all cases. Consider a score that playbacks a sample by
sending messages to sfplay~ (in Max) through the receiver `:::antescofo
sample`, something like :

```antescofo

   sample open "/path/to/sample"
EVENT 1
   sample 1
   
EVENT 1
EVENT 1
EVENT 1
EVENT 1  Part2
EVENT 1
EVENT 1
EVENT 1
; ...
```

If the score is run with the command `:::antescofo
antescofo::startfromlabel Part2` the message `:::antescofo sample` are
not sent and there is no playback. With `:::antescofo
antescofo::scrubtolabel Part2` the sample is played but the timing is
not correct. When the event corresponding to `:::antescofo Part2` is
occurs, the sample is near the begining because the time used to achieve
the fastforward between the start of the score and the event labeled
`:::antescofo Part2` is negligible whilst in normal functioning, the
sample is 4 beats after its begining.

We propose below a solution that can be used to achieve the correct
behavior: when the computation resume at label `:::antescofo Part2`, the
sample must resume its playback 4 beats after its begining. 



## A Solution with message and event callbacks

The idea is to use [callback messages] and [event callbacks] to
‟intercept” the message directed to sfplay and to adjust them to achieve
the right behavior. 

We suppose that several sfplay~ are adressed through the receiver
`:::antescofo sampleN` where `:::antescofo N` is a number. The program
below can be **[downloaded
here](/Library/HowTo/SampleManagement/sfplay_control.asco.txt)**. 


This approach uses [callback messages], [event callbacks] and the
function [@send_message]. Refer also to [transport commands]. The
implementation is working because during the fastforward mode, the
`:::antescofo $NOW` variable is correctly updated, that is, it is
updated using the virtual time of the fastforward mode that flows as
fast as possible but in accordance with the progression in the score,
and not by the passing of the physical clockwall time. 


```antescofo
{!Library/HowTo/SampleManagement/sfplay_control.asco.txt!}
```



<br> 

<!-- FOOTNOTE -->

