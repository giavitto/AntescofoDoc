<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}


# A trajectory Score Library (by nadir B.)
<br>


[Trajectory Score
Library](https://github.com/nadirB/Trajectory_Score_Library) is an
Antescofo library of scripts, converting mathematical parametric
functions into curves in order to control the
[Spat5](http://forumnet.ircam.fr/fr/produit/spat/) sources
trajectories. However, the data produced by the trajectories can be used
for different purposes like synthesis control, audiovisuals, etc.

These scripts are written in the Antescofo language. The language
provides a convenient way of creating and transmitting automation
parameters through the Open Sound Control protocol. Trajectory Score
Library aims to use Max or Pure Data as a unified real-time environment
to unite different computer music processes. Bindings with
[Spat5](http://forumnet.ircam.fr/fr/produit/spat/) and
[SpatGris3](http://gris.musique.umontreal.ca/) are available.


- The Max an PD patches examples and the Antescofo code can be accessed as
a [GitHub project](https://github.com/nadirB/Trajectory_Score_Library)
and the releases can be downloaded as a [zip
file](https://github.com/nadirB/Trajectory_Score_Library/releases).


- [A paper presenting the Trajectory Score
Library](https://creaa.unistra.fr/websites/gream/Activites/Colloque_JIM_2020_-_Pre-actes_-_BABOURI_Nadir.pdf)
is available.


<br>
<center>
![Trajectory Score Library by Nadir Babouri](https://github.com/nadirB/Trajectory_Score_Library/raw/master/Trajectory_Score_Library_Max_Demo.png){: style="width:90%" }
</center>
