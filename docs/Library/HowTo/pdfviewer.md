<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




# Controlling a PDF Reader

<br>

You can achieve "page turning" in a PDF score, using a PDF reader that
get UDP/OSC messages. For instance, you can add a label to the event
where you want a page turning and specifies these labels in a map that
associates the label to the right page number in the PDF:


```antescofo
    ; define the events and the associated pages
    $pages := MAP {
        "section1" -> 1,
        "measure 12" -> 2,
        ; ...
    }
    
    ; define an OSC receiver used by the PDF viewer
    ; here on the same host and port 23654
    oscsend pdfviewer localhost : 23654
    
    ; a whenever will trigger the OSC message each time a label is reached 
    whenever ($LAST_EVENT_LABEL)
    {
       if ($pages.is_defined($LAST_EVENT_LABEL))
       {
	       ; the arguments of the message depends of the PDF reader that is used
           pdfviewer page ($pages($LAST_EVENT_LABEL))
       }
    }
  
    // Score 

    NOTE C4 1 section1
    
    ; ...

    NOTE C2 1 "measure 12"
    ; ...
```

This approach has the advantage to leave the score simple and to group
the mechanism in one place and the PDF reader can be on another machine.


As an alternative solution, you can use AppleScript and a PDF reader
that accept ActionScript command. This is the case for
[Skim](https://skim-app.sourceforge.io/). The commands accepted by Skim
are described here: [Scripts for Skim](https://sourceforge.net/p/skim-app/wiki/AppleScript/).

Create a script using “Script Editor.app” with the following lines and
save it in your ‘Downloads’ directory under the name ‘gotoskim.scpt’ :

```
on run argv
	set pageNum to (item 1 of argv) as integer
	tell application "Skim"
		set view settings of document 1 to {display mode:single page}
		tell document 1 to go to page pageNum
	end tell
end run
```

Then if you have an Antescofo score as below:

```antescofo
  $scorefile := "/Users/USER/Downloads/score.pdf"
  $script := "/Users/USER/Downloads/gotoskim.scpt"
  
  _ := @system("open -a skim " + $scorefile)


  NOTE C4 5
    _ := @system("osascript " + $script + " " + 10)

  NOTE C2 5
    _ := @system("osascript " + $script + " " + 20)

  NOTE D4 5
    _ := @system("osascript " + $script + " " + 30)
    print end
```

assuming that you replace `USER` by your actual user name in the score,
then the execution will open the ‘score.pdf’ file and changes the page
in the Skim viewer as the events occur.

The function [@system] executes a shell command:

   -  The first one before the first musical event launch skim (assuming
      that the Skim.app is installed on your computer).

   - The following commands launches the Apple Script that interacts
     with Skim to change the current page. The page to set is specified
     by the number in the @system() argument.The expression simply built
     the string to pass to the function @system (the + operators
     concatenates strings and convert its subsequent arguments into
     string if needed).

You can combine the previous methods that link a label with a page
number and this mechanism to avoid to pollute our score with explicit
turning commands. For additional commands, take a look at the Skim wiki.

Obviously, this is done on the same machine, but it is possible to
[launch remotely an Apple
Script](https://stackoverflow.com/questions/4620339/remote-applescript-events).



<br>


<!-- FOOTNOTE -->

