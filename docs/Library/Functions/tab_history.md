<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@tab_history(variable)
```
This is a special form. It returns a tab of the values of
the variable in argument.

See also [@map_history], [@tab_history_date] and [@tab_history_rdate].


{!Library/Functions/functions.ref!}
