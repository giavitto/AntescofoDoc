<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_fastforward()
```

this predicate returns `:::antescofo true` if the program is runing in
_fastforward mode_, _e.g._, as in the initial phase of a `:::antescofo
antescofo::starfrom...` or `:::antescofo antescofo::playfrom...`
commands. See paragraph [Controlling the Execution
Flow](/Reference/atomic_command#controlling-the-execution-flow).

This predicate can be used to avoid or to perform a specific computation
in this mode.

<br>

The fastforward mode is used to reach a location in the score where the
normal execution resumes. In fastforward mode, all computation are done,
except the final sending of messages, which is inhibited. In addition,
if the keyword [antescofo_is_in_rehearsal_mode] appears in the score,
the local actions are not triggered.

In fastforward mode, the time is spent in _virtual time_, meaning that
the clock goes as fast as possible. See [Transport, tempo computation,
and others practicalities](/Reference/tempo_transport)




See also
[@is_fastforward],
[@is_following_on],
[@is_paused],
[@is_playing_on],
[@is_running],
and [@is_stopped].


{!Library/Functions/functions.ref!}

