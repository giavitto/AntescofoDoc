<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@window_filter(nim, coef:tab, pos:numeric)
```
build a new nim by processing each component independently.

Each component is the result of a smoothing process of the breakpoints.
Ecah breakpoint of the new nim is computed by the dot product of `coef`
with a sequence of `y` of the same length as `coef` where position `pos`
corresponds to the current breakpoint.



For example,
```antescofo
       @window_filter(nim, [2], 0)
```
build a nim by
scaling the image of `:::antescofo nim` by 2.
```antescofo
       @window_filter(nim, [0.1, 0.2, 0.4, 0.2, 0.1], 2)
```       
is a moving weighted average with
symmetric weights around `y`. 


See also [@filter_max_t], [@filter_median_t] and [@filter_min_t].


{!Library/Functions/functions.ref!}


