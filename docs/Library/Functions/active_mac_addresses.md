<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@active_mac_addresses()
```
on OSX, retruns a tab where each entries is the mac address of an active network
interface. The mac address is described as a tab with 6 integers. 

```antescofo
	@active_mac_addresses() 
	--> TAB[TAB[61, 166, 245, 159, 174, 122], 
	        TAB[73, 163, 93, 249, 25, 0], 
			TAB[72, 163, 91, 249, 25, 3], 
			TAB[53, 146, 17, 40, 69, 61], 
			TAB[56, 146, 17, 40, 69, 63], 
			TAB[61, 166, 245, 180, 166, 42]]
```


See also
{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/system_functions.list!}


{!Library/Functions/functions.ref!}


