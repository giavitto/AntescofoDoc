<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi_isNoteOn(tab)
```

the predicate returns true if the command is a note on with a nbon-zero
velocity.  See [@midi_read].


See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


