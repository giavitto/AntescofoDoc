<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@directory_read(path:string)
@directory_read(path:string, level:int)
@directory_read(path:string, ["ext1", "ext2", ...]:tab)
@directory_read(path:string, level:int, ["ext1", "ext2", ...]:tab)
```

The first version of this command returns a MAP (dictionary) where the
keys are the names of the file located in the directory specifeid by
`:::antescofo path`. The subdirectory do not appears in the map. The
value associated to a key is the key itself.


If `:::antescofo level` is specified, the listing is extended to the
subdirectories up to `:::antescofo level` of nestings. A sub-directory is
described by a key (a string specifying its name) and a map describing
its content.


If `:::antescofo ["ext1", "ext2", ...]` is specified, only the files
with theses extensions are listed.


See also 
{!Library/Functions/system_functions.list!}

{!Library/Functions/functions.ref!}
