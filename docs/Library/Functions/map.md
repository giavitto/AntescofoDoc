<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@map(f:function, t:tab)
```
returns a tab such that element _i_ is the result of applying `f` to element
`t[i]`. Note that the computation is equivalent to
```antescofo
       [f($x) | $x in t]
```
See also [@scan] and [@reduce] for other tab-morphisms.


See also
{!Library/Functions/tab_functions.list!}

Note that function [@map] is relatd to `tab`, not to `map`: see
[@map_compose] and [@map_concat] for `map` related functions.



{!Library/Functions/functions.ref!}
