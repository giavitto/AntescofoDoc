<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midicent2hz(pitch:numeric)
@midicent2hz(pitch:string)
@midicent2hz(pitch:symbol)

@midicent2hz(reference:numeric,  pitch:numeric)
@midicent2hz(reference:numeric,  pitch:string)
@midicent2hz(reference:numeric,  pitch:symbol)
```

The version with one argument converts a midi note expressed in
_midicent_ or in midi pitch notation into a frequency. For example,
`6900` (the coding in midicent of the standard pitch A440) is converted
in `440`Hz.  See [midi
tuning](https://en.wikipedia.org/wiki/MIDI_Tuning_Standard) and function
[@midicent2hz], [@hz2midi], [@hz2symb] and [@symb2midicent].

The version with two arguments specifies explicitly the reference (by
default 440Hz). 

When a midi note is expressed symbolically (through a string or a
symbol), the midi numerical coding of a pitch in midicents or in midi
note is irrelevant and the function returns the same value as
[@midi2hz].


See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


