<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@normalize(tab)
@normalize(tab, min:numeric, max:numeric)
```
returns a new tab with the elements normalized between `min` and `max`. If
they are omitted, they are assumed to be 0 and 1.

If an element of the tab is not a numeric, `undef` is returned. 

See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}
