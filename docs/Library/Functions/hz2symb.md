<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@hz2symb(numeric)
```
convert a frequency (expressed in Hz) into a string in [Scientific pitch
notation](https://en.wikipedia.org/wiki/Scientific_pitch_notation)
representing the pitch. The _scientific pitch notation_ is the notation
used in the specification of [pitch in events]. 
For example, `440` is converted in `:::antescofo "A4"`.

**NOTE**: the microtonal alteration, as in `:::antescofo A#4+50` are not
  currently supported. 

See [midi tuning](https://en.wikipedia.org/wiki/MIDI_Tuning_Standard)
and functions [@midicent2hz], [@hz2midi], [@hz2symb] and
[@symb2midicent].




See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


