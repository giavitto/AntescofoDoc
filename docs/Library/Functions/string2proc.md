<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@string2proc(s:string)
```
returns the proc whose name is given in argument. The initial `::` in
the proc identifier can be omitted. Useful to convert directly a string
received through OSC or through a message into a process that can be
instantiated.

For example, assuming that is set in the Max environment to a tab of two
elements, the first being a process identifier and the second an integer,
then the code
```antescofo
       whenever($channel)
       {
            :: (@string2proc($channel[0])) ($channel[1])
       }
```          
will react to the assignment to `$channel` by calling the corresponding processes
with the specified integer.


<br>
See also [@string2fun], [@string2obj].

See also
{!Library/Functions/dataexchange_functions.list!}



{!Library/Functions/functions.ref!}
