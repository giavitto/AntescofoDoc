<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@char_is_alnum(string)
@char_is_alpha(string)
@char_is_ascii(string)
@char_is_blank(string)
@char_is_cntrl(string)
@char_is_digit(string)
@char_is_graph(string)
@char_is_lower(string)
@char_is_print(string)
@char_is_punct(string)
@char_is_space(string)
@char_is_upper(string)
@char_is_xdigit(string)
```

The predicates `:::antescofo @char_is_xxx(s)` returns true if all
characters in `s` are in the class `xxx`. These classes are defined as
follows:

- `alnum`  Any alphanumeric character
- `alpha`  Any alphabetic character
- `ascii`  Anay character between 0 and octal 0177 inclusive.
- `blank`  Any whitespace character that is not a line separator
- `cntrl`  Any control character
- `digit`  Any decimal digit
- `graph`  Any graphical character    
- `lower`  Any lowercase character    
- `print`  Any printable character    
- `punct`  Any punctuation character  
- `space`  Any whitespace character   
- `upper`  Any uppercase character
- `xdigit` Any hexadecimal digit character










See also 
{!Library/Functions/string_functions.list!}

{!Library/Functions/functions.ref!}

