<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@listify(map)
```
returns the range of its argument as a list, *i.e.* the returned map is
obtained by replacing the keys in the arguments by consecutive integers
starting from `1`.



See also 
{!Library/Functions/map_functions.list!}


{!Library/Functions/functions.ref!}

