<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@json_string(s:string)
@json_string(v:value)
```

If the argument of `:::antescofo @json_string` is a string, the string
is supposed to contain the json represnetation of a value. Then the
function call returns an Antescofo value (or undef if the string is not
a valid json format). 

For the orther arguents, the function converts the Antescofo value into
a string containing the translation into json of the value.

This function can be used to send and receive Antescofo value in json
format through OSC or Max/PD messages. Cf. sect. [Defining a value in Json
format](/Reference/exp_value/index.html#defining-a-value-in-json-format)



See also
{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/functions.ref!}

