<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@cons(v:value, t:tab)
```
build a new tab by prepending `v` to `t`.


See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}
