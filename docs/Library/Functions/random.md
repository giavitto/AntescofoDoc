<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@random()
```
[@random] is an impure function returning a random floating point number
between 0 and 1 (included). This is not a pure function because two
successive calls are likely to return different results.

The resolution of this random number generator is `1/(2^31 - 1)`, which
means that the minimal step between two numbers in the images of this
function is `1/(2^31 - 1)`.

[@random] is similar to [@rand] but relies on a different algorithm to
generate the random numbers. See also [@rand_int].

See also
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}


