<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@sin(numeric)
```
computes the sine of its argument (measured in radians).


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}


