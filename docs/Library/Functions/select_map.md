<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@select_map(m:map, f:funct)
```
build a new map containing the entries of `m` which satisfy
predicate `f`. This predicate takes two arguments: the key and the value. For instance
```antescofo
       @fun_def pred($key, $value) { return ($key > "b") && ($value < 4) }
       $m := MAP{ ("a", 1), ("b", 2), ("c", 3), ("d", 4) }
       $mm := @select_map($m, @pred)
       print $mm
```
displays `MAP{ ("c", 3) }`




       
See also
{!Library/Functions/map_functions.list!}

{!Library/Functions/functions.ref!}

