<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_obj(value)
```
the predicate returns true if its argument is an object.

Objects are implemented using processes so if a value `x` satisfies
`@is_obj`, then it satisfies `@is_exec` (but all execs are not objs).



See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
