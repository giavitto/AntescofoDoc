<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rnd_binomial(t:int, p:float)
```
returns a random generator that produces integers according to a
*binomial discrete distribution* P of parameters `t`and p`:

         P(i, | t, p) = \binom{t}{i} . p^i . (1 - p)^(t-i),  i ≥ 0.


See [@rnd_bernoulli] for a description of _random generators_.


See also 
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}
