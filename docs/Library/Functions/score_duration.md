<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@score_duration()
```

returns the total duration in beats of the musical events in the score.


See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
