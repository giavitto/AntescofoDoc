<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}


```antescofo
@exe_child_of(son:exe, ancestor:exe)
@exe_child_of(son:exe, ancestor:proc)
@exe_child_of(son:exe, ancestor:string)
```

returns `:::antescofo true` if `:::antescofo son` is a descendent of
`:::antescofo ancestor`. Otherwise a `:::antescofo false` value is
returned.

The first argument must be a live exe.
The second argument specifies the ancestor:

- by an exe;

- or by a proc value, meaning that `:::antescofo son` has been launched
  directly or indirectly from one instance of the proc;

- or by a string which is the label of a compound action of which one of
  the instances has launched `:::antescofo son`.

An [<undef>] is returned in case of problem (the second argument is a
dead exe or is not a valid proc value).

An exe is considered as a child of itself. Thus,
```antescofo
      @exe_child_of(ex, ex)
```
returns true if `:::antescofo ex`is a live exe. 

The child relation between the `:::antescofo son` and the `:::antescofo
ancestor` is not necessarily direct: a process $P$ that is spanned by a
loop triggered by a whenever in a group $G$ is a child of $G$ (in
pedantic terms, the child relation is the reflexive-transitive closure
of the relation defined by the function [@exe_parent]).


See also [@exe_parent], the special variables `:::antescofo $MYSELF` and
`:::antescofo $THISOBJ`.

See also {!Library/Functions/system_functions.list!}

