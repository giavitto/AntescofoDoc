<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@log(numeric)
```
computes the value of the natural logarithm of its argument.


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}
