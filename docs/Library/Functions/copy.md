<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@copy(value)
```
returns a fresh copy of the argument. For data structure like `map` or
`tab`, the copy is a deep copy: elements of the data structure are also
copied.


{!Library/Functions/functions.ref!}
