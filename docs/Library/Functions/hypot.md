<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@remainder(x:numeric, y:numeric) ; listable
```





See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}
