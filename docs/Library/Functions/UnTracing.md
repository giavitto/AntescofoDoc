<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@UnTracing()
@UnTracing(x:function, ...)
@UnTracing(x:string, ...)
@UnTracing(x:tab, ...)
```
Calling [@UnTracing] stops the trace the calls of the functions specified
by the arguments. If specified functions are not traced, there is no effect.

Without arguments, all traced functions stop to be traced.


See also [@Tracing] and 
{!Library/Functions/system_functions.list!}


{!Library/Functions/functions.ref!}


