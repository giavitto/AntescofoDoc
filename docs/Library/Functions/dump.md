<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@dump(file:string, variable_1, ... variable_p)
```

is a special form: the arguments `variable_i` are restricted to be
variables. Calling this special form store the values of the variables
in the file whose path is `file`. This function is a special form,
so it cannot be curryfied.

The stored value can be restored using the function [@loadvar].  The
dump file produced by [@dump] is in a human readable format and
corresponds to a fragment of the _Antescofo_ grammar.

The returned value is `true` if the value of the variable have been
saved, `false` elsewhere (_e.g._ if `file` cannot be created). The
process of saving the values is done asynchronuously in a dedicated
thread, so the run-time computation are not perturbed.

The dump file can be produced during one program execution and can be
read in another program execution. This mechanism can be used to manage
**presets**.


<br>

Note that this special form expands into the ordinary function
[@dumpvar]. The same comments apply. 


See also [@dumpvar]

See
{!Library/Functions/dataexchange_functions.list!}



{!Library/Functions/functions.ref!}

