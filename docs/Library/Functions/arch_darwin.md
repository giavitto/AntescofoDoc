<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@arch_darwin()
```
this predicate returns `true` if the underlying host is Mac OSX and
`false` elsewhere.

See also [@arch_linux] and [@arch_windows]

{!Library/Functions/functions.ref!}

