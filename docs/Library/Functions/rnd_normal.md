<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rnd_normal(μ:float, σ:float)
```

returns a random generator that produces
floating-point values according to a *normal distribution* P:

        P(x | μ, σ) = 1/(σ \sqrt{2 π})  e^{- (x - μ)^2/(2 σ^2)}



See [@rnd_bernoulli] for a description of _random generators_.


See also 
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}
