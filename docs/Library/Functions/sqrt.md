<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@sqrt(x:numeric)
```
computes the non-negative square root of `x`.

See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}
