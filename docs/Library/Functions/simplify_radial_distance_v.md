<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@simplify_radial_distance_v(nim, tol:numeric)
```
builds a new simpler nim by aggregating consecutive breakpoints whose
images are wihin a distance of `tol`.

In the simplification process, each component is handled separatively
and a breakpoint is assimilated to a 2-dimensional point with coordinate
(_x, y_). This is the case for all nim simplification functions that end
with a *_v*.

It reduces successive vertices that are clustered too closely to a
single vertex, called a key. The resulting keys form the simplified
polyline. This process is illustrated below:


![the radial distance simplification algorithm on a nim](../../Figures/simplify_radial_distance_v.png)


See also [@align_breakpoints], [@sample] and the nim simplification
functions: [@simplify_radial_distance_t], [@simplify_radial_distance_v],
[@simplify_lang_v], [@filter_median_t], [@filter_min_t],
[@filter_max_t], [@window_filter_t]


See also {!Library/Functions/nim_functions.list!}

{!Library/Functions/functions.ref!}

