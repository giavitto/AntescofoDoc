<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@string2fun(s:string)
```
returns the function whose name is given in argument. The initial `@` in
the function name can be omitted. This is useful to convert directly a
string received through OSC or through messages into a function that
can be applied.

<br>
See also [@string2obj], [@string2proc].

See also
{!Library/Functions/dataexchange_functions.list!}



{!Library/Functions/functions.ref!}
