<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rnd_exponential(λ:float)
```
returns a random generator that produces floats $x$ according to an
*exponential distribution* $P$ of parameter λ:

        P(x | λ) = λ e^(-λ x),    x > 0.



See [@rnd_bernoulli] for a description of _random generators_.


See also 
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}
