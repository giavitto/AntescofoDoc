<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@drop(t:tab, n:numeric)
@drop(t:string, n:numeric)
@drop(t:tab, x:tab)
@drop(t:string, x:tab)
```

`@drop(t:tab, n:numeric)` build a new tab which is `t` with its first
`n` elements dropped if `n > 0`, and with its last `n` elements dropped
if `n < 0`.

`@drop(t:string, n:numeric)` build a new string which is `t` with its first
`n` characters dropped if `n > 0`, and with its last `n` characters dropped
if `n < 0`.


`@drop(t:tab, x:tab)` returns the tab formed by the elements of `t`
whose indices are not element of `x`.

`@drop(t:string, x:tab)` returns the string formed by the elements of `t`
whose indices are not element of `x`.


See also _lisp like_ functions: [@car], [@cdr], [@concat],
[@cons], [@empty], [@drop], [@take].

See also 
{!Library/Functions/tab_functions.list!}

See also
{!Library/Functions/string_functions.list!}


