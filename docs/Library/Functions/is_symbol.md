<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_symbol(value)
```
the predicate returns true if its argument is a symbol. Symbvol appears
as Max/PD identifiers.



See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
