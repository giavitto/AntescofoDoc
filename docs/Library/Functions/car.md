<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@car(tab)
```
returns the first element of tab if is not empty, else an empty tab.

Some functions handle `tab` as _lisp lists_: [@car], [@cdr], [@concat],
[@cons], [@empty], [@drop], [@take].

See also 
{!Library/Functions/tab_functions.list!}


{!Library/Functions/functions.ref!}

