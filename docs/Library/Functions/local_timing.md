<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
    @local_tempo()
    @local_tempo(exe)

    @local_position()
    @local_position(exe)

    @logical_date()
    
    @seconds_in_beats_from_now(d:numeric)
    @seconds_in_beats_from_now(exe, d:numeric)
    
    @beats_in_seconds_from_now(d:numeric)
    @beats_in_seconds_from_now(exe, d:numeric)

```

These functions give information about the local timing in the current
scope (for the version without the exe argument) or in the temporal
scope of the exe given in the arguments (that is, local timing of a
given execution of a compound action denoted by its exe). 

These are impur functions: the retourned value depends of the scope of
execution. 

Notice that the value of the variables `:::antescofo $RT_TEMPO` and
`:::antescofo $RNOW` corresponds to the value of [@local_tempo()] and
[@local_beat_position] in the default top-level scope (which is the
musician temporal scope).

[@logical_date] returns a logical date, a positive integer, which
roughly corresponds to the number of logical instants handled so far. A
logical instant correspond to the handling of events and action that
occur simultaneously. This information is sometimes more precise than
the date given by the `:::antescofo $NOW` variable: two events may be
distinguished by their logical date but can share the same (wall clock)
datation. Notice that two different events may still share the same
logical date. Computations that share the same logical date are executed
sequentially and atomically from the point of view of the access of
variables.

Function `:::antescofo @seconds_in_beats_from_now(d)` converts in beat
of the current temporal scope the duration `d` expressed in seconds. 

Function  `:::antescofo @beats_in_seconds_from_now(d)` converts in
second the duration `d`expressed in the relative time of the current
temporal scope.

The version with an addition exe argument refer to the temporal scope of
the exe argument. 


**Example:** the execution of the following code

```antescofo

@fun_def pr($id)
{
  print \
	  $id "now=" $NOW \
	  "rnow=" $RNOW "lbp=" (@local_beat_position()) \
	  "rt_tempo=" $RT_TEMPO "lt=" (@local_tempo())
}

Group G60
{
    @pr("a")
  1 @pr("b")
  1 @pr("c")
}


Group G120 @tempo := 120
{
    @pr("aa")
  1 @pr("bb")
  1 @pr("cc")
}

```

produces the trace:

```
	a  now= 0.0 rnow= 0.0 lbp= 0.0 rt_tempo= 60.0 lt= 60.0
	aa now= 0.0 rnow= 0.0 lbp= 0.0 rt_tempo= 60.0 lt= 120.0
	b  now= 1.0 rnow= 1.0 lbp= 1.0 rt_tempo= 60.0 lt= 60
	bb now= 0.5 rnow= 0.5 lbp= 1.0 rt_tempo= 60.0 lt= 120
	c  now= 2.0 rnow= 2.0 lbp= 2.0 rt_tempo= 60.0 lt= 60
	cc now= 1.0 rnow= 1.0 lbp= 2.0 rt_tempo= 60.0 lt= 120
```



<br>
See also 
{!Library/Functions/system_functions.list!}



