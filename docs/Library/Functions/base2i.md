<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@base2i(b:num, n:string)
```
convert the string `:::antescofo n` into an integer, assuming that the
string express a number in base `:::antescofo b`. For instance

```antescofo
	@base2i(16, "1AE3")   ; returns 6883
	@base2i(2, "1001")    ; returns 4097
```


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}


