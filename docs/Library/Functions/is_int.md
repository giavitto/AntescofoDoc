<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_int(value)
```
the predicate returns true if its argument is a signed
integer. Integeres are represented using C type `long`. The number of
bits depends of the _Antescofo_ variants and may differes between the 32
and the 64 bits version.



See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
