<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@aggregate(n1:nim, n2:nim, …)
```
aggregates the arguments into a vectorial nim. The
number of components of the result is the sum of the number of
components of each arguments. This function admits a variable number of
argument and cannot be curryfied.



See also [@projection]

See also
{!Library/Functions/nim_functions.list!}

{!Library/Functions/functions.ref!}

