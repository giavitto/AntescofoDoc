<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@add_pair(dico:map, key:value, val:value)
```
add a new entry to a dictionary. If an entry with key `key` already
exists, value `val` replaces the old value. The dictionnary `dico` is
updated _in place_. The function returns its first argument.


See also [@insert]


{!Library/Functions/functions.ref!}
