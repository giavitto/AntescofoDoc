<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@hz2midicent(numeric)
```
convert a frequency (expressed in Hz) into a midi note expressed in
midicent. For example, `440` is converted in `6900`.  See [midi
tuning](https://en.wikipedia.org/wiki/MIDI_Tuning_Standard) and functions
[@midicent2hz], [@hz2midi], [@hz2symb] and [@symb2midicent].



See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


