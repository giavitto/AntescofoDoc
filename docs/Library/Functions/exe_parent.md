<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@exe_parent(exe)
```

returns the parent exe of the argument, or `:::antescofo <undef>` if
there is no parent or if the parent is dead.

See also [@exe_parent], the special variables `:::antescofo $MYSELF` and
`:::antescofo $THISOBJ`.

See also {!Library/Functions/system_functions.list!}


