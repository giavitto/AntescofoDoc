<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@max_val(tab)
@max_val(map)
@max_val(nim)
```
This overloaded functions returns the maximal element in the tab if it
is a tab, and the maximal element in the range if the argument is a map
or a nim.

If the argument is empty, a `undef` value is returned. 

See [@max], [@range].


See also
{!Library/Functions/tab_functions.list!}

See also
{!Library/Functions/map_functions.list!}

See also
{!Library/Functions/nim_functions.list!}


{!Library/Functions/functions.ref!}
