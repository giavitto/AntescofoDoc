<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}



```antescofo
@insert(t:tab, i:numeric, v:value)
@insert(m:map, k:val, v:value)
```
[@insert] is an impure overloaded function. 

`@insert(t, i, v)` inserts “in place” the value `v` into the tab `t`
after the index `i` (tab's elements are indexed starting with `0`).
If `i` is negative, the insertion take place in front of the tab.
If `i >= @size(t)` the insertion takes place at the end of the tab.

`@insert(m, k, v)` inserts “in place” a new entry with value `v` under
key `k` in the map `m`. If the entry already exists, the current value
is replaced by `v`. See [@add_pair]


**Notice** that the form is also used to include a file at
parsing time. See section [file structure].




