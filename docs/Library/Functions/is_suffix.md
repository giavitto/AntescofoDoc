<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_suffix(s1:string, s2:string)
@is_suffix(s1:string, s2:string, cmp:fct)
@is_suffix(t1:tab, t2:tab)
@is_suffix(t1:tab, t2:tab, cmp:fct)
```
[@is_suffix] is an overloaded function. See also the functions
[@is_subsequence] and [@is_prefix].


-----------------------------------------
```antescofo
@is_suffix(s1:string, s2:string)
```
the predicate returns true if string `s1` is a suffix of string `s2`.


-----------------------------------------
```antescofo
@is_suffix(s1:string, s2:string, cmp:fct)
```
the predicate returns true if string `s1` is a suffix of string `s2`
where the characters are compared with the function `cmp`. The
characters are passed to the function `cmp`q as strings of length one.


-----------------------------------------
```antescofo
@is_suffix(t1:tab, t2:tab)
```
the predicate returns true if the sequence of elements of `t1` is a
suffix of the sequence of element of `t2`.



-----------------------------------------
```antescofo
@is_suffix(t1:tab, t2:tab, cmp:fct)
```
same as the previous version but the function `cmp` is used to test the
equality between the elements, instead of the usual comparison between
values.  For example:
```antescofo
       @fun_def cmp($x, $y) { $x < $y }
       @is_suffix([1, 2], [5, 6, 7], @cmp) ->true
```
`true` is returned because `@cmp(1, 6)` and  `@cmp(2, 7)` hold. 



--------------------------------------------------

See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}




(t1:tab, t2:tab, cmp:fct): same as the previous version but the function
is used to test the equality between the elements, instead of the usual
comparison between values. For example:

                @fun_def cmp($x, $y) { $x < $y }
                @is_suffix([1, 2], [5, 6, 7], @cmp) ->true
          

is returned because and hold.
