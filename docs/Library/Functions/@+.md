<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}


```antescofo
@+(value, value) ; listable
```

`@+` is the prefix form of the infix binary operator `+`

<div class="codehilite"><pre>
<span class="o">@+</span><span class="p">(</span><span class="nc">x</span><span class="p">,</span> <span class="nc">y</span><span class="p">)</span> &equiv; <span class="c1">x + y</span>
</pre></div>


The functional form of the operator is useful as an argument of a
high-order function as in `@reduce(@+, v)` which sums up all the
elements of the tab `v`.

The addition of an `int` and a `float` returns a `float`.

The addition of two `string` corresponds to the concatenation of the arguments.

The addition of a `string` and any other value convert this value into its
string representation before the concatenation.

See also [@-], [@*], [@/], [@%]



