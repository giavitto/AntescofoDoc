<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@shift_map(m:map, n:numeric)
```

build a new map containing the entries of `m` that have a integer key
with `n` added. For example:
```antescofo
       $m := MAP{ (1, 1), ("a", 1), (1.0, 1)
                  (2, 2), ("b", 2), (2.0, 2) }
       print (@shift_map($m, 11))
```
displays `MAP{ (12, 1), (13, 2) }`.


See also {!Library/Functions/map_functions.list!}

{!Library/Functions/functions.ref!}

