<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@hz2midi(numeric)
```
convert a frequency (expressed in Hz) into a midi note. For example,
`440` is converted in `69`.  See [midi
tuning](https://en.wikipedia.org/wiki/MIDI_Tuning_Standard) and functions
[@midicent2hz], [@hz2midicent], [@hz2symb] and [@symb2midicent].



See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


