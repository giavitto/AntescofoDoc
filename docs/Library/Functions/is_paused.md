<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_paused()
```
this predicate returns `:::antescofo true` in between the two time
operations mode.

There are two time operation mode: _normal_, which represents the
passing of the physical time, and the _virtual_ where the clock goes as
fast as possible (see [@is_fastforward]).

In the curse of a `::antescofo antescofo::startfrom... XXX` command, the
program is first run in fastforward mode, using the virtual time. When
the location `::antescofo XXX` is reached in thescore, the time is
paused and the switch to the normal time is done only when an event is
detected by the listening machine.

The predicate [@is_paused] is always false in an _antescofo_
program. But it can be true in a script (script can be used in the
_Antescofo_ standalone version to execute a sequence of antescofo
command).

See also
[@is_fastforward],
[@is_following_on],
[@is_paused],
[@is_playing_on],
[@is_running],
and [@is_stopped].


{!Library/Functions/functions.ref!}

