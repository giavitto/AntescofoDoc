<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@round(numeric)
```
returns the integral value nearest to its argument rounding
half-way cases away from zero, regardless of the current rounding
direction.


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}

