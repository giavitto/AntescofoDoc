<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@atan2(x:numeric, y:numeric) ; listable
```

The atan2() function computes the principal value of the arc tangent of
y/x, using the signs of both arguments to determine the quadrant of the
return value.


The atan2() function is used mostly to convert from rectangular $(x,y)$
to polar $(r, \theta)$ coordinates that must satisfy $x = r \cos(\theta)$
and $y = r \sin(\theta)$.  In general, conversions to polar coordinates
should be computed thus:

```aantescofo
        $r := @sqrt($x*$x + $y*$y)
        $theta := @atan2($y, $x).
```



{!Library/Functions/math_functions.list!}
{!Library/Functions/functions.ref!}


