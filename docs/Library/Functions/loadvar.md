<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@loadvar(*file*:string)
```
read a file produced by a call to [@dump] (or [@dumpvar]) and set the
value of the corresponding variables.

The basic use of is to recover values of global variables that have been
previously saved with a [@dump] command. If the loaded variable is not defined
at the calling point, a new global variable is implicitly defined by [@loadvar].
If the variable with the same name exist at the calling point, either
global or local, this variable will be set with the saved value.

To be more precise, when [@dump] is called, the name of the variables in the
arguments list are stored as well as the corresponding values in `file`.
The variables in the argument list can be global or local variable.

When [@loadvar] is called, it is called in some scope `sc`. Each
identifier in the dump file is searched in the current scope `sc`. If
not found, the englobing scope is looked up, and the process is iterated
until a variable is found or until reaching the global scope. If no
global variable with the same identifier is found, a new global variable with this
identifier is created. The value associated to the identifier is used to
set the selected variable.

Beware that [@loadvar] does not trigger the `whenever`.

*Nota Bene:* because [@loadvar] can be called in a context which differs
from the context of the call of [@dump], there is no reason that the ’same’
variables will be set.

Here is an example:
```antescofo
       @global $a, $b, $c
       $a := 1
       $b := 2
       $c := 3
       $ret := @dump("/tmp/dump1", $a, $b, $c)
       $a := 0
       $b := 0
       $c := 0
       Group G1
       {
          @local $b
          $b := 22
          Group G2
          {
             @local $c
             $c := 33
             $ret := @loadvar("/tmp/dump1")
             print $a $b $c  ; print 1 2 3
          }
          print $a $b $c
          ; print 1 2 0
          ; because the variable $c set by @loadvar is in G1
       }
       print $a $b $c ; print 1 0 0
       
       $ret := @loadvar("/tmp/dump1")
       print $a $b $c ; print 1 2 3
```
In this example, the values of the global variables `$a`, `$b` and `$c`
are saved by the [@dump] special form in file `/tmp/dump1`. The
[@loadvar] is done in a context where a local variables `$b`and `$c`
hidde the global ones and these local variable (with global variable
`$a`) will be affected by the [@loadvar] command.



<br>

See
{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/functions.ref!}

