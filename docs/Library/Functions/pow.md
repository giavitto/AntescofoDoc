<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@pow(x:numeric, y:numeric) ; listable
```
computes `x` raised to the power `y`.




See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}
