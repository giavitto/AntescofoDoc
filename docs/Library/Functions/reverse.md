<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@reverse(tab)
@reverse(string)
```
returns a new tab or a new string where the elements (characters) are
given in the reverse order.

See also 
{!Library/Functions/tab_functions.list!}

See also 
{!Library/Functions/string_functions.list!}

{!Library/Functions/functions.ref!}

