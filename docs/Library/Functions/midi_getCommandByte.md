<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi_getCommandByte(tab)
```
The argument is a tab representing a midi message, see function
[@midi_read]. The [@midi_getCommandByte] extracts the first element of
the tab. For the seven channel related midi commands, this corresponds
to the encoding of a command plus a channel, cf. [@midi_read]


See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


