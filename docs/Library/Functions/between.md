<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@between(a:numeric, x:numeric, b:numeric) ; listable
```

This function admits an infix  special
syntax and can be written

```antescofo
	(x in a .. b)
```
the parenthesis are _mandatory_. This predicate is true if

```antescofo
	(a < x) && (x < b)
```

If one argument is a `tab`, each scalar
argument `u` is extended into a `tab` whose all elements are equal to `u`
and the predicate returns true if it hold point-wise for all elements of
the tabs. For example:

```antescofo
	([1, 2] in 0 .. 3)
```
          
returns true because `0 < 1 < 3`and `1 < 2 < 3`.


{!Library/Functions/math_functions.list!}
{!Library/Functions/functions.ref!}


