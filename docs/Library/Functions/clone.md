<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@clone(v:any)
```
returns a deep copy of the argument. For immutable values (e.g., scalar
values), the argument is simply returned. For [string], [tab] and [map], a copy of
the argument is build and returned. The elements of the data structure
are also cloned (recursively).


See also
{!Library/Functions/string_functions.list!}
{!Library/Functions/tab_functions.list!}
{!Library/Functions/map_functions.list!}


{!Library/Functions/functions.ref!}


