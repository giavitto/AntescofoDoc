<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@iota(n:numeric)
```
returns `[ $x | $x in n]` that is, a tab listing the integers from to `0` to `n`
excluded.

