<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi_isPitchbend(tab)
```
the predicate returns true if the tab represents a Pitchbend command.
See [@midi_read].


See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


