<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@current_event()
```
returns an integer which represents the rank of the current musical
event in the score, that is,which is also the value of 
`:::antescofo $CURRENT_EVENT_INDEX`. 

Before the occurence of the first event, the function returns 0. 

The rank of an event in the score does not corresponds to the index of
the event in the tab returned by functions like [@make_duration_tab] or
[@make_pitch_tab] because grace note does not appears in such tab.

System variable `:::antescofo $CURRENT_EVENT_INDEX` is updated like
`:::antescofo $BEAT_POS, _i.e._ when a new event is detected or
notified, but, contrary to `:::antescofo $BEAT_POS`, its value is
different for each event.

{!Library/Functions/functions.ref!}

