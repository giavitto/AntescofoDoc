<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@max_patch_path()
```

returns the path absolue of the patch that contains the antescofo object that evaluate this function. 

See also [@pwd], [@current_load_directory], [@directory_read], [@user_directory] and [directory_read]



