<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rand_int(int)
```
returns a random integer between 0 and its argument (excluded).  This is
not a pure function because two calls with the same argument are likely
to return different results.


See also
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}


