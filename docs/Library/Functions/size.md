<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@size(x:value)
```

If `x` is a scalar value, the function returns a strictly negative integer
related to the type of the argument (that is, two scalar values of the
same type gives the same result).

If `x` is a map or a tab, the function returns
the number of elements in its argument (which is a positive integer).

If it is a nim, it returns the number of breakpoints of the nim (which
is not the dimension of the nim). Note that a nim with zero breakpoints
is the result of a wrong definition.


See also [@shape] and
{!Library/Functions/predicates_functions.list!}


{!Library/Functions/functions.ref!}
