<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@min(value, value)
```
return the minimal value of its two arguments.

Values in _Antescofo_ are totally ordered. The order between two
elements of different types is implementation dependent. However, the
order on numeric is as expected (numeric ordering: the integers are
embedded in the decimals). For two argument of the same type, the
ordering is as expected (lexicographic ordering for string, and tab,
_etc._).

See [@max], [@min_key], [@max_key], [@min_val], [@max_val], [@sort].


{!Library/Functions/functions.ref!}

