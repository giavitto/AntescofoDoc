<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@median(tab)
```
computes the _median_ of the tab's element. The median of a list of
numbers can be found by arranging all the numbers from lowest value to
highest value and picking the middle one. For example, the median of
`[11, 5, 3, 3, 9]` is `5`.



See also
{!Library/Functions/tab_functions.list!}



{!Library/Functions/functions.ref!}

