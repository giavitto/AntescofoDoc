<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@dumpvar(file:string, id_1:string, v_1:value, ..., id_p:string, v_p:value)
```
saves in the dump file  `file` the value `v_i` under the name `id_i`.

The dump file `file` can be used by the function [@loadvar] to define
and set or to reset the variables with identifier `id_i`. The format used
in `file` is a text format corresponding to the _Antescofo_ grammar.

The number of arguments is variable, so this function cannot be
curryfied.

Dumping the values of the variables is done in a separate thread, so the
“main” computation is not perturbed. However, it means that the file is
created asynchronously with the function call and when the function
returns, the file may not be completed.


See also
{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/functions.ref!}

