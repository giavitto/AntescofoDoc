<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi_getMetaType(tab)
```
returns the meta-message type for the MidiMessage. If the message is not
a meta message, then returns -1.  See the [standard midi file
format](http://improv.sapp.org/doc/misc/MidiFileFormat.html).

The first byte of a meta message is a characteristic flag. The second
byte specify the kind of meta message. So this function simply returns
the second elemnt of the tab, if the first is the meta flag.




See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


