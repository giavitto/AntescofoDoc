<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rotate(t:tab, n:int)
```
build a new tab which contains the elements of `t`
circularly shifted by `n`. If `n` is positive the element are right shifted,
else they are left shifted.
```antescofo
       @rotate([1, 2, 3, 4], -1) == [2, 3, 4, 1]
       @rotate([1, 2, 3, 4], 1) == [4, 1, 2, 3] 
```

See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}



