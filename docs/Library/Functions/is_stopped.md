<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_stopped()
```
this predicate returns `:::antescofo true` if the _Antescofo_ system is
idle, waiting for a command or for an input message.

This predicate always returns true in a program. But it can be false if
used in an expression evaluated using a `::antescofo
antescofo::playstring` command.



See also
[@is_fastforward],
[@is_following_on],
[@is_paused],
[@is_playing_on],
[@is_running],
and [@is_stopped].


{!Library/Functions/functions.ref!}

