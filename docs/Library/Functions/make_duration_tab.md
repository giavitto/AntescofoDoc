<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_duration_tab()
@make_duration_tab(start:numeric)
@make_duration_tab(start:numeric, stop:numeric)
```

returns a tab whose $i$th element is the duration in beats the $i$th
musical event in the score.

Called with no arguments, the events considered are all the events in
the score.  With a `start`, only the events whose position in beats is
greater than `start` are considered. If a `stop` is specified, all
events must have a position in beats between `start` and `stop`. Grace
events do not appear in the tab.


Examples:
```antescofo
       NOTE D6 1    
       NOTE C7 0    
       NOTE D6 1/2  
       NOTE C7 1/5
       CHORD (D1 A7 Eb7) 1/8 
       Trill (CC6 D7 A7) 1/8 event6
```
With this score, `@make_duration_tab` returns:
```antescofo
       TAB[1.0, 0.5, 0.2, 0.125, 0.125]
```


Function [@make_bpm_tab] can be used to get the information necessary to
translate the relative duration in absolute duration.


See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
