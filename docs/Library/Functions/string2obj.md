<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@string2obj(s:string)
```
returns the object definition whose name is given in argument. The
initial `obj::` in the obj name can be omitted. This is useful to
convert directly a string received through OSC or through messages into
an object that can be instantiated.


<br>

See also [@string2fun], [@string2proc].

See also
{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/functions.ref!}
