<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@scale_x(nim, numeric)
@scale_y(nim, numeric)
```

These function multiply the x or the y coordinates by the second
argument: [@scale_x] streches/compresses the x-coordinates while
[@scale_y] streches/compresses the y-coordinates. Each function leave
the other coordinate unchanged. The result is a new nim. 


See also
{!Library/Functions/nim_functions.list!}

{!Library/Functions/functions.ref!}
