<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@dim(t:tab)
@dim(n:nim)
```
if the argument is a tab `t`, the call returns the dimension of `t`,
_i.e._ the maximal number of nested tabs. If the argument is a nim `n`,
the call returns the number of components of the nim, _i.e._ the nimber
of elements in the tab returned by the application of the nim.

In either case, the returned value is an integer strictly greater than
0. If the argument is not a tab nor a nim, the dimension is `0`.
