<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}




```antescofo
@configuration_arch()
```
returns 32 if the underlying architecture handles 32 bits addresses or 64
if it returns 64 bits addresses. 

<br>
```antescofo
@configuration_audiosample()
```
returns the size of the audio sample used by the listening machine. 


<br>
```antescofo
@configuration_compiler_name()
```
returns a string describing the version of the C++ compiler used to
compile antescofo:
`:::antescofo "clang++"` or
`:::antescofo "g++"`.

<br>
```antescofo
@configuration_compiler_version()
```
returns a string describing the version of the C++ language used to compile
Antescofo:
`:::antescofo "C++98"`,
`:::antescofo "C++11"`,
`:::antescofo "C++14"`,
`:::antescofo "C++17"`,
`:::antescofo "C++2a"`.



<br>
```antescofo
@configuration_debug()
```
returns true if the current Antescofo version is a debug version. 


<br>
```antescofo
@configuration_faust()
```
returns true if Antescofo embeeds the [Faust](https://faust.grame.fr/) extension. 



<br>
```antescofo
@configuration_host()
```
returns a string describing the current host: `:::antescofo "MAX/msp"`,
`:::antescofo "PD"`, `:::antescofo "standalone offline"`, `:::Antescofo
"standalone real-time"`, `:::antescofo "antescofo framework"`. 



<br>
```antescofo
@configuration_odeint()
```
returns true if Antescofo handles differntial curve and the ode solver
function familly.


<br>
```antescofo
@configuration_release()
```
returns true if Antescofo is a release version. 



```antescofo
@configuration_target_architecture()
```
returns a string describing the underlying operating system:
`:::antescofo "mac os"`, 
`:::antescofo "linux"`,
`:::antescofo "windows"`,
`:::antescofo "ios"`,
`:::antescofo "ios simul"`,
`:::antescofo "android"`,
`:::antescofo "android simul"`.





See also
{!Library/Functions/system_functions.list!}



{!Library/Functions/functions.ref!}


