<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@gshift_map(a:map, f:function)
```
(where `f` can be a map, a nim or an intentional function),
builds a new map `b` such that
```antescofo
       b(f(x)) = a(x)
```



See also 
{!Library/Functions/map_functions.list!}

{!Library/Functions/functions.ref!}
