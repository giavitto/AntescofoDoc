<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@mapval(a:map, b:function)
```
returns the composition of function `a` and `c`, that is, a map `c` such
that
```antescofo
       c(x) == b(a(x))
```
See also [@map_compose] and [@merge].

See also
{!Library/Functions/map_functions.list!}


{!Library/Functions/functions.ref!}
