<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@sinh(numeric)
```
computes the hyperbolic sine of its argument.


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}




