<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_function(value)
```
the predicate returns true if its argument is a `map`, a `nim`, an
intentional function (defined using `@fun_def`, a method, a signal or a
partial application of such functions.

If a value satisfies this predicates, it can be applied to (zero or
more) arguments to achieve a function call.


See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
