<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@history_length(variable)
```
This is a special form: the argument must be a variable (it cannot be a
general expression).  It returns the maximal length of the history of
the variable, *i.e.* the number of update that are
recorded.


