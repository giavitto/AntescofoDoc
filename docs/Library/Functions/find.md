<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@find(t:tab, f:function)
@find(m:map, f:function)
@find(s:string, f:function)
```
returns the index of the first element of `t`, `m` or `s` that satisfies the
predicate `f`.

The predicate is a binary function taking the index or the
key as the first argument and the associated value for the second
argument.

The undef value (for maps) or the integer `-1` (for tab and string) is
returned if there is no pair satisfying the predicate.


See also [@count], [@member] and [@occurs].

Example:
```antescofo
       @fun_def p($i, $v) { return ($i > 1) && ($v % 2 == 0) }
       @find([1, 2, 3, 4], @p)  ; returns 0
```
[@find] returns the first index greather than one such that the
corresponding element is a multiple of two.

```antescofo
       @fun_def p($i, $v) { return ($i > 1) && ($v >= "b") }
       @find("abcdefg", @p)  ; returns 2
```
the index `2` is returned because the `"c"` is the first character
greather or equal to `"b"` such that its position is strictly greather
than one (character numbering starts at zero).





