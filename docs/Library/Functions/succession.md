<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@succession(t:tab, f:fun)
```

Computes the tab of the results `:::antescofo f(t[i], t[i+1])`. For instance

```antescofo
     [3, 2, 1].succession(@-) == [1, 1]

     @fun_def @g($x, $y) { return $x - 2*$y }
     [3, 2, 1].succession(@f) == [-1, 0]
```



See also
{!Library/Functions/tab_functions.list!}


{!Library/Functions/functions.ref!}


