<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@xml_string(s:string)
```

The string argument is supposed to contain an XML text and the function
returns an Antescofo value encoding the XML text. The `:::antescofo
<undef>` value is returned if the string is not a well formed XML text.

The encoding of a XML text into an Antescofo value is described in [@xml_read].

See also
{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/functions.ref!}

