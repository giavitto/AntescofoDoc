<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@push_front(tab, value)
```
add the second argument at the beginning of its first argument. The
first argument, modified by side-effect, is the returned value.

Usually, [@push_front] is sightly more efficient thant [@push_back].



See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

