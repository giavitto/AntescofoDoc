<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rnd_bernoulli(p:float)
```
returns a boolean random generator with a probability `p` to have
a `true` value. For example
```antescofo
       $bernoulli60 := @rnd_bernouilli(0.6)
       $t := [ $bernoulli60() | (1000) ] 
```
produces a tab of 1000 random boolean values with a probability of 0.6
to be true.

#### Random Generators

The members of the `@rnd_distribution` family return a random generator
in the form of an _impure function_ `f` _taking no argument_. Each time `f`
is called, the value of a random variable following the `distribution`
distribution is returned. The arguments of the `@rnd_distribution` are
the parameters of the distribution. Two successive calls to
`@rnd_distribution` returns two different random generators for the same
distribution, that is, generators with _unrelated seeds_.



See also 
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}
