<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi_isMeta(tab)
```

the predicate returns true if the tab represents a is a Meta message
(when the command byte is 0xff).  See [@midi_read].


See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


