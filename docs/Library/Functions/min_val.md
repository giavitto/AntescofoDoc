<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@min_val(tab)
@min_val(map)
@min_val(nim)
```
This overloaded functions returns the minimal element in the tab if it
is a tab, and the minimal element in the range if the argument is a map
or a nim.

If the argument is empty, a `undef` value is returned. 

See [@min] and [@range].


See also
{!Library/Functions/tab_functions.list!}

See also
{!Library/Functions/map_functions.list!}

See also
{!Library/Functions/nim_functions.list!}


{!Library/Functions/functions.ref!}

