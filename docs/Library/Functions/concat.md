<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@concat(tab, tab)
```
returns a new tab made by the concatenation of the two tab arguments.


See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}
