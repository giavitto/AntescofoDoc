<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_label_pos()
@make_label_pos(start:numeric)
@make_label_pos(start:numeric, stop:numeric)
```
this function returns a map whose keys are the labels of the events and
the value, the position _in beats_ of the events.  Events with no label
do not appear in the map, as well as grace notes.

Called with no arguments, the events considered are all the events in
the score.  With a `start`, only the events whose position in beats is
greater than `start` are considered. If a `stop` is specified, all events
must have a position in beats between `start` and `stop`.


Examples:
```antescofo
       NOTE D6 1    event1
       NOTE C7 0    event2
       NOTE D6 1/2  event3
       NOTE C7 1/5
       CHORD (D1 A7 Eb7) 1/8 event5
       TRILL (CC6 D7 A7) 1/8 event6
```
with this score, `@make_label_pos()` returns:
```antescofo
       MAP{ ("event1", 0.0), ("event3", 1.0), ("event5", 1.7), ("event6", 1.825) }
```



See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
