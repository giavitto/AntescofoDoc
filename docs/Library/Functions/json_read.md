<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@json_read(file:string)
```
parse a json file into an Antescofo value. Cf. sect. [Defining a value
in Json
format](/Reference/exp_value/index.html#defining-a-value-in-json-format)

See also
{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/functions.ref!}

