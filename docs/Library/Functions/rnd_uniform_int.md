<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rnd_uniform_float(a:int, b:int)
```
returns a generator giving int values according to a *uniform
distribution* P:

        P(x | a, b) = 1/(b - a + 1),   a  ≤ x ≤  b



See [@rnd_bernoulli] for a description of _random generators_.

See also 
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}

