<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_bool(value)
```
the predicate returns true if its argument is a boolean value.


See also
{!Library/Functions/predicates_functions.list!}


{!Library/Functions/functions.ref!}

