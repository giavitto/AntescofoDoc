<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_duration_map()
@make_duration_map(start:numeric)
@make_duration_map(start:numeric, stop:numeric)
```
returns a map where the duration (in beat) of the _i_ th event of the
score, is associated to _i_ (the keys of the map are the **ranks** of
the events). Called with no arguments, the events considered are all the
events in the score.

With `start`, only the events whose position _in beat_ is greater than
`start` are considered.

If a `stop` is specified, all events must have a position _in beat_
between `start` and `stop`.

_Nota Bene_: The numbering of musical events starts at `1`. Grace notes,
_i.e._ musical event with a duration of `0`, do not appear in the map
and does not count in the ranking.


For example
```antescofo
       NOTE C7 0 mes2
       NOTE D6 1/2
       NOTE C7 1/5
       NOTE Eb7 2/5
       NOTE G#6 1/2
       CHORD (D1 A7) 1/8 mes2_2
       NOTE C2 1/8
```
with this score, `@make_duration_map()` will return:
```antescofo
       MAP{ (1, 0.5), (2, 0.2), (3, 0.4), (4, 0.5), (5, 0.125), (6, 0.125) }
```
Notice the grace note `C4`which does not appear in the map. 


See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
