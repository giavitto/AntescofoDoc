<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@to_num(numeric)
@to_num(string)
@to_num(tab)
```

converts its argument into a numeric value (integer or float, following the
most appropriate). 

If the argument is a number, this number is the value returned. 
If the argument is a string representing an arithmetic expression, the
function returns the result of this expression. 
If the argument is a tab, the function applies
implicitly to each element of the tab.  Value `:::antescofo <undef>` is
returned in the other case.

The string may contains parenthetised arithmetic expressions with
addition, substraction, multiplication and division between integers and
floating point values. Floating point values can be written in decimal
form or in engineering notation (like `:::antescofo +6.3e-12`).

If the expression is not a well formed parenthetised arithmetic
expressions, [not-a-number](https://en.wikipedia.org/wiki/NaN) (NaN) is
returned.  If a division by 0 occurs during the course of the
computation, the infinity value is returned and processed subsquently in
the rest of the expression. The calculation rules are the rules using
for [ieee floating point](https://en.wikipedia.org/wiki/IEEE_754)
expressions.

If the string does not represents an arithmetic expression, the NaN
value is also returned returned. This value can be tested using the
predicate [@is_nan()]. Beware that not-a-number values are floating
point values but their comparaison returns always false (they must be
tested using [@is_nan()]).


See also [@parse] and 
{!Library/Functions/string_functions.list!}


{!Library/Functions/functions.ref!}


