<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@map_history_date(variable)
```
This is a special form: the argument must be a variable identifier. It
returns a map where the keys are integers and the values are the date in
physical times of the successive assignements to the variable. Integer
`0` corresponds to the current value, `1` to the previous value,
_etc_. Variable's history has a bounded length that can be specified
using a `@local`or a`@global` declaration.

See also
[@tab_history], [@map_history] and [@map_history_rdate].


See also
{!Library/Functions/system_functions.list!}

{!Library/Functions/functions.ref!}
