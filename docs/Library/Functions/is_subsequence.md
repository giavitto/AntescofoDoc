<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_subsequence(s1:string, s2:string)
@is_subsequence(s1:string, s2:string, cmp:fct)
@is_subsequence(t1:tab, t2:tab)
@is_subsequence(t1:tab, t2:tab, cmp:fct)
```
[@is_subsequence] is an overloaded function. See also the functions
[@is_suffix] and [@is_prefix].


-----------------------------------------
```antescofo
@is_subsequence(s1:string, s2:string)
```
the function returns the index of the first occurrence of string `s1` in
string `s2`. A negative value is returned if `s1` does not occurs in
`s2`.


-----------------------------------------
```antescofo
@is_subsequence(s1:string, s2:string, cmp:fct)
```
same as above but the argument `cmp` is used to compare the characters
of the strings (represented as strings of only one element).


-----------------------------------------
```antescofo
@is_subsequence(t1:tab, t2:tab)
```
the predicate returns the index of the first occurrence of the elements
of `t1` as a sub-sequence of the elements of `t2`. A negative value is
returned if `t2` does not appear as a subsequence of tab `t2`. For
example
```antescofo
              @is_subsequence([], [1, 2, 3]) -> 0
       @is_subsequence([1, 2, 3], [1, 2, 3]) -> 0
             @is_subsequence([1], [1, 2, 3]) -> 0
             @is_subsequence([2], [1, 2, 3]) -> 1
             @is_subsequence([3], [1, 2, 3]) -> 2
       @is_subsequence([1, 2], [0, 1, 2, 3]) -> 1
```


-----------------------------------------
```antescofo
@is_subsequence(t1:tab, t2:tab, cmp:fct)
```
same as the version above but the function `cmp` is
used to compare the elements of the tabs.


--------------------------------------------------

See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}

