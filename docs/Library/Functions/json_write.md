<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@json_write(file:string, v:value)
```
write the json representation of value `v` into the file `file`. Only
bool, string, numeric, map, tab and their nestings can be translated
into Json, cf. sect. [Defining a value in Json
format](/Reference/exp_value/index.html#defining-a-value-in-json-format)

See also
{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/functions.ref!}

