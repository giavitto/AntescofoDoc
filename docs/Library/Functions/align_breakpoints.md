<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@align_breakpoints(nim)
```
builds a new nim with linear interpolation type whose _homogeneous_
breakpoints are the breakpoints of all components of the arguments. 

A vectorial nim is **homogeneous** if all its component have the same
breakpoints, _i.e._ breakpoints with the same abcisses.

See also [@align_breakpoints], [@sample] and the nim simplification
functions: [@simplify_radial_distance_t], [@simplify_radial_distance_v],
[@simplify_lang_v], [@filter_median_t], [@filter_min_t],
[@filter_max_t], [@window_filter_t]



In the figure below, the diagram at the top left shows a vectorial nim with two components:

- the effect of `@sample` is pictured at top right,

- the effect of `@align_breakpoints` is sketched at bottom left,

- and the effect of `@linearize` is illustrated at bottom right. 

![the effect of @sample, @align_breakpoints and @linearize on a nim](../../Figures/nimSampling.png)

{!Library/Functions/functions.ref!}

