<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@projection(nim, p:numeric)
```
extracts the `p` th component of a vectorial nim.


See also [@aggregate].

See also
{!Library/Functions/nim_functions.list!}



{!Library/Functions/functions.ref!}


