<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@reduce(f:function, t:tab)
@reduce(f:function, init:value, t:tab)
```

Function [@reduce] has two or three arguments, sso it cannot be curryfied.


#### Two arguments form

If `t` is empty, an undefined value is returned.

If `t` has only one element, this element is returned.

In the other case, the binary operation is used to combine all the
elements in `t` into a single value
```antescofo
       f(... f(f(t[0], t[1]), t[2]), ... t[n])
```

For example, if `t` is a vector of booleans, `@reduce(@||, t)` returns
the logical disjunction of the `t`’s elements.



#### Three arguments form

If `t` is empty, the `init` value is returned. In the other case, the
binary operation is used to combine all the elements in `t` into a
single value

```antescofo
       f(... f(f(init, t[0]), t[1]), ... t[n])
```

This form is more general than the previous one: the result of the
reduction function may be of a different type than the elements in the
tab.



