<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi_isNoteOff(tab)
```

the predicate returns true if the command is a note off (0x80) or if the
command is a note on 0 velocity (a 0 velocity means silence).  See
[@midi_read].


See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


