<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_bpm_map()
@make_bpm_map(start:numeric)
@make_bpm_map(start:numeric, stop:numeric)
```

returns a map where the BPM of the $i$th event of the score, is
associated to $i$ (the keys of the map are the **ranks** of the
events). Called with no arguments, the events considered are all the
events in the score.

With `start`, only the events whose position _in beat_ is greater than
`start` are considered.

If a `stop` is specified, all events must have a position _in beat_
between `start` and `stop`.

**Nota Bene:** The numbering of musical events starts at `1`. Grace
notes, _i.e._ musical event with a bpm of `0`, do not appear in the map
and does not count in the ranking.


For example
```antescofo
       NOTE D6 1    event1
       NOTE C7 0    event2
       NOTE D6 1/2  event3
       BPM 30
       NOTE C7 1/5
       CHORD (D1 A7 Eb7) 1/8 event5
       trill (CC6 D7 A7) 1/8 event6
```
with this score, `@make_bpm_map()` will return:
```antescofo
       MAP{ (1, 60.0), (2, 60.0), (3, 30.0), (4, 30.0), (5, 30.0) }
```
Notice the grace note `C7` which does not appear in the map. 


See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
