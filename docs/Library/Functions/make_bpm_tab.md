<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_bpm_tab()
@make_bpm_tab(start:numeric)
@make_bpm_tab(start:numeric, stop:numeric)
```

returns a tab whose $i$th element is the BPM of the $i$th musical event
in the score.

Called with no arguments, the events considered are all the events in
the score.  With a `start`, only the events whose position in beats is
greater than `start` are considered. If a `stop` is specified, all
events must have a position in beats between `start` and `stop`. Grace
events do not appear in the tab.


Examples:
```antescofo
       NOTE D6 1    
       NOTE C7 0
       NOTE D6 1/2
       BPM 30
       NOTE C7 1/5
       CHORD (D1 A7 Eb7) 1/8 
       Trill (CC6 D7 A7) 1/8 event6
```
With this score, `@make_bpm_tab` returns:
```antescofo
       TAB[60.0, 60.0, 30.0, 30.0, 30.0]
```

Function [@make_duration_tab] can be used to complete the bpm
information with the duration information for an event.


See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
