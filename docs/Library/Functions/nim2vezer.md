<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}



```antescofo
@nim2vezer(nim)
```
This function build a map encoding the nim argument. This map can bet
outputed as a JSON specification and latter read by [Vezér].

Vezér enables control and synchronisation of any MIDI, OSC or DMX
enabled environments - realtime video softwares, lighting rigs, etc. -
through the use of timeline based automations. Vezér offers a nice and
user-friendly interface to graphicaly edit breakpoint functions. The
function [@nim2vezer] can be used to input an Antescofo nim into the
Vezér graphical BPF editor through a JSON file.

In the reverse direction, the Vezér graphical editor can export a BPF as
an XML file. This XML file can be read using [@xml_read]. The resulting
data structure can be used to create the corresponding Antescofo nim.

Note that only a subset of Antescofo nim can be handled by Vezér: some
Antescofo interpolation type are not managed by Vezér, neither
discontinuous nim, etc.


The encoding of an XML text into an Antescofo value is described in
[@xml_read]. The encoding of a map in JSON is described in [@json_write]
and detailed in sect. [Defining a value in Json
format](/Reference/exp_value/index.html#defining-a-value-in-json-format). The
JSON format used to import a BPF into Vezér is described at page [import
from JSON](https://imimot.com/help/vezer/import-export/#json) and the
rendering of a Vezér BPF into XML at page [export
XML](https://imimot.com/help/vezer/import-export/#render-to-xml).


See also
{!Library/Functions/dataexchange_functions.list!}


