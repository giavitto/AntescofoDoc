<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@filter_min_t(nim, n:numeric)
```
build a new _smoother_ nim from the nim argument. The result is build by
replacing every image `y0` of a breakpoint `(x0, y0)` by the **minimum**
value of the `y` in a sequence of `2n + 1` breakpoints centered on
`(x0, y0)`.  The first `n` breakpoints and the last `n` breakpoints are
leaved untouched. The resulting nim has the same number of breakpoint as
the argument with the same `x`.


See also [@filter_max_t], [@filter_median_t], [@window_filter_t].


{!Library/Functions/functions.ref!}




