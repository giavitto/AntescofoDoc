<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_label_duration()
@make_label_duration(start:numeric)
@make_label_duration(start:numeric, stop:numeric)
```
returns a map associating to the label of an event, the duration of this
event. Events with no label do not appear in the map.

Called with no arguments, the events considered are all the events in
the score.  With a `start`, only the events whose position in beats is
greater than `start` are considered. If a `stop` is specified, all events
must have a position in beats between `start` and `stop`.



See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
