<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@remove_duplicate(t:tab)
```
keep only one occurrence of each element in `t`. Elements not removed
are kept in order and `t` is modified in place.



See also 
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

