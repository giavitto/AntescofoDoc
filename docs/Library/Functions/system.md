<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@system(cmd:string)
@system(cmd:tab)
```

The first version of this command sends the `:::antescofo cmd` argument
to the shell _sh_. The calling process waits for the shell to finish
executing the command, ignoring SIGINT and SIGQUIT, and blocking
SIGCHLD. A false boolean value is returned if an error occured (in this
case an error message is issued).

The second version of the command spawn a new process from the
executable file specified by `:::antescofo cmd[0]`.  

If the file specified by `:::antescofo cmd[0]` does not contains a slash
character, the file parameter is used to construct a pathname, with its
path prefix being obtained by a search of the path specified in the
environment by the _PATH variable_ (or of `/usr/bin:/bin` if this
variable does not exist). The resulting pathname either refers to an
executable object file, or a file of data for an interpreter.

At least `:::antescofo cmd[0]` must be present in the tab, and should
contain the file name of the program being spawned, as an absolute or
relative path to the program file to be launched.

The remaining elements in `::antescofo tab` are converted into strings to
construct the argument list to be made available to the new process.

For example

```antescofo
        @system("ls -l -R ..")
        @system(["ls", "-l", "-R", ".."])
```


See also 
{!Library/Functions/system_functions.list!}

{!Library/Functions/functions.ref!}
