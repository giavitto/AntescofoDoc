<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@number_active(string)
@number_active(proc)
@number_active()
```

returns the number of active (alive) instances of a process or an object
specified through its name (a string with or without the prefix `::` or
`obj::`) or through its `proc` value. With no argument returns the
number of concurrent runing processes.

See also [@active]


{!Library/Functions/functions.ref!}
