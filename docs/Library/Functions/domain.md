<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@domain(m:map)
```
returns a tab containing all the keys present in the map `m`.


See also [@range].

See also 
{!Library/Functions/map_functions.list!}

{!Library/Functions/functions.ref!}
