<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@count(tab, value)
@count(map, value)
@count(string, value)
```

computes the number of times the second argument appears in the first
argument. For a map, the second argument refers to a value stored in the
dictionary. For a string, the second argument must be a string and the
call returns the number of occurences of the first character in the
first string:
```antescofo
       @count("abacaa", "a")
```
returns `4`. 


See also [@find], [@member] and [@occurs].

{!Library/Functions/functions.ref!}

