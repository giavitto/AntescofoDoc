<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@replace(t:tab, find:value, rep:value)
```
returns a new tab in which a number of elements have been replaced by
another.

The argument `find` represents a sub-sequence to be
replaced: if it is not a tab, then all the occurrences of this value at
the top-level of `t` are replaced by `rep`:
```antescofo
       $t := [1, 2, 3, [2]]
       @replace($t, 2, 0) -> [1, 0, 3, [2]]
```          
If `find` is a tab, then the replacement is done on sub-sequence of `t`:
```antescofo
       @replace([1, 2, 3, 1, 2], [1, 2], 0) -> [0, 3, 0]
```          
Note that the replacement is done eagerly: the first occurrence found is
replaced and the replacement continue on the rest of the tab. Thus,
there is no ambiguity in case of overlapping sub-sequences, only the
first is replaced:
```antescofo
       @replace([1, 1, 1, 2], [1, 1], 0) -> [0, 1, 2]
```          
If the argument ``rep` is a tab, then it represents at sub-sequence to be
inserted in place of the occurrences of `find`. So, if the replacement is a
tab, it must be wrapped into a tab:
```antescofo
       @replace([1, 2, 3], 2, [4, 5]) -> [1, 4, 5, 3]
       @replace([1, 2, 3], 2, [[4, 5]]) -> [1, [4, 5], 3]
```



See also 
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}
