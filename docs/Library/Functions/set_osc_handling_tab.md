<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}




```antescofo
@set_osc_handling_tab(b:bool)
```

changes the handling of tabs in OSC messages. _Arrays_ are present only
in OSC _v1.1_ and not present in the initial protocol _v1.0_. Several
implementation ignore the array construction.

By default _Antescofo_ sends the elements of a tab as the successive
arguments of a message, without using the array facilities. A call to
`:::antescofo @set_osc_handling_tab(true)` switches the behavior to rely
on the array feature present in _v1.1_: the `[` and `]` markers are used
to wrap the sending of the tab elements. Calling the function with a
false value enables to switch to the _v1.0_ policy.


When receiving a message, array markers are always interpreted as tab
delimiters.


Beware that a message as a limited size that depends both on the sender
and receiver implementation limitation. The current _Antescofo_ raw
buffer size is 5096. Sending a tab or receiving an array must fit within
one message.

<br>


See the [Osc Messages] section in the reference manual and functions
[@set_osc_handling_double] and [@set_osc_handling_int64].

See also
{!Library/Functions/dataexchange_functions.list!}


