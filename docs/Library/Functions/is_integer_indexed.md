<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_integer_indexed(value)
```
the predicate returns true if its argument is a map whose domain is a
set of integers.



See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
