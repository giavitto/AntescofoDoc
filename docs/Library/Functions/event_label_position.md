<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@event_label_position(label:string)
```

returns the position in beat of an event specified through its label. 


see also: {!Library/Functions/score_functions.list!}

{!Library/Functions/functions.ref!}
