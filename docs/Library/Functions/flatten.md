<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@flatten(t:tab)
@flatten(t:tab, n:numeric)
```

`flatten(t)` returns a new tab where where the nesting structure of
`t` has been flattened. For example, 
```antescofo
       @flatten([[1, 2], [3], [[], [4, 5]]]) ->  [1, 2, 3, 4, 5]
```

`flatten(t, n)` returns a new tab where the first `n` levels of nesting
have been flattened. If `n == 0`, the function is the identity. If `n`
is strictly negative, it is equivalent to without the level argument.
```antescofo
       @flatten([1, [2, [3, 3], 2], [[[4, 4, 4]]]], 0)  -> [1, [2, [3, 3], 2], [[[4, 4, 4]]]]
       @flatten([1, [2, [3, 3], 2], [[[4, 4, 4]]]], 1)  -> [1, 2, [3, 3], 2, [[4, 4, 4]]]
       @flatten([1, [2, [3, 3], 2], [[[4, 4, 4]]]], 2)  -> [1, 2, 3, 3, 2, [4, 4, 4]]
       @flatten([1, [2, [3, 3], 2], [[[4, 4, 4]]]], 3)  -> [1, 2, 3, 3, 2, 4, 4, 4]
       @flatten([1, [2, [3, 3], 2], [[[4, 4, 4]]]], 4)  -> [1, 2, 3, 3, 2, 4, 4, 4]
       @flatten([1, [2, [3, 3], 2], [[[4, 4, 4]]]], -1) -> [1, 2, 3, 3, 2, 4, 4, 4]
```


See also some other _lisp like functions_: [@car], [@cdr], [@concat],
[@cons], [@empty], [@drop], [@take].

See also 
{!Library/Functions/tab_functions.list!}


{!Library/Functions/functions.ref!}


