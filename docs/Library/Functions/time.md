<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@time()               ; default: local time zone and current time
@time(localtimezone:bool)
@time(number_of_seconds_since_1970:numeric)
@time(number_of_seconds_since_1970:numeric, localtimezone:bool)
@time(localtimezone:bool, number_of_seconds_since_1970:numeric)
```

The function `:::antescofo @time` returns a map describing the date
given as argument. The date is given as the number of seconds elapsed
since since 0 hours, 0 minutes, 0 seconds, January 1, 1970 (UTC).

If there is no numeric arguments, the date used is the date at which the
function is called.

If a boolean argument is present and false, the date is interpreted
without time zone adjustment. Else, the date is interpreted in the
local time zone that is, the zone set for the computer running this
function.


The returned map contains the following entries which describe the date
in terms of years, weeks, day, etc. :

- `:::antescofo "date"` : its value is the date queried (in seconds
  since 1970), either given as an argument or the current date.

- `:::antescofo "year"` : its value is the year. Because the date is
  given in seconds by an integer, negative date can be used to describe
  dates after 1900.

- `:::antescofo "mon"` : its value is the month of the year (0 - 11)

- `:::antescofo "month"` : its value is the name of month of the year (a string)

- `:::antescofo "yday"` : its value is the day in the year (0 - 365)

- `:::antescofo "mday"` : its value is the day of the month (1 - 31)

- `:::antescofo "day"` : its value is the name of the day (a string)

- `:::antescofo "wday"` : its value is the day in the week (Sunday = 0)

- `:::antescofo "hour"` : its value is teh hour of day (0 - 23)

- `:::antescofo "min"` : its value is the minute (0 - 59)

- `:::antescofo "sec"` : its value is the seconds (0 - 60)

- `:::antescofo "summer_time"` : its value is non zero if summer time is in effect

- `:::antescofo "zone"` : its value is the abbreviation of timezone name (a string)

- `:::antescofo "gmt_offset"` : its value is the offset from UTC in seconds


<br>
See also {!Library/Functions/system_functions.list!}


