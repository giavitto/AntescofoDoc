<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_running()
```
this predicate returns `:::antescofo true` in normal functioning mode,
_i.e_, after an `::antescofo antescofo::start`.


See also
[@is_fastforward],
[@is_following_on],
[@is_paused],
[@is_playing_on],
[@is_running],
and [@is_stopped].


{!Library/Functions/functions.ref!}

