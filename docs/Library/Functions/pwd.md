<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@pwd()
```

returns the _current working directory_ as a string. The current working
directory is defined by the host system for the processus that executes
antescofo.

See also [@max_patch_path], [@current_load_directory], [@directory_read] and [@user_directory].

{!Library/Functions/functions.ref!}

