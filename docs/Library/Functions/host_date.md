<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@host_date()
```
returns a date related to the notion of date of the external
environment.

The date available as the value of the `::antescofo $NOW` variable is a
date related to the start of the program's execution and counted using
specific timers. The value of this variable is constant within a logical
instant. The date returned by this function is directly linked to the
date as known by the underlying host and is never constant (two calls
always gives different results).


{!Library/Functions/functions.ref!}

