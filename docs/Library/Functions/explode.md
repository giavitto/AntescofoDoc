<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@explode(s:string)
@explode(s:string, t:string)
@explode(s:string, t:tab of string)
@explode(s:string, t:tab of int)
@explode(s:string, t:funct)
```

Function `:::antescofo @explode` is used to split a string into a
sequence of contiguous substrings.

The first form (one argument) returns a tab containing the characters of
`s` (the characters are represented as string with only one
element). For example:

```antescofo
                      @explode("")  -> []
                   @explode("abc")  -> ["a", "b", "c"]
       @reduce(@+, @explode("abc")) -> "abc"
         @scan(@+, @explode("abc")) -> ["a", "ab", "abc"]
```          


The others forms (two arguments) use the second argument as a
specification of delimiters in the string `s` and split the string
accordingly in a sequence of substrings:

- if `t` is a string, the letter of the string are used as a delimiter,

- if `t` is a tab of strings, these strings are merged and the
  corresponding letters are used as delimiters,

- if `t` is a tab of integers, these integers are used as indices given
  the positions of teh delimiters (the tab must defines a sequence of
  strictly increasing indices),

- if `t` is a predicate (an unary function returning a value interpreted
  as a boolean: map, function, lambda, etc.), the predicate is applied
  to each letter of `s` to determine the delimiters.

The string `s` is split in a sequence of substrings that are a partition
of the first argument, _i.e._ the substring are non-overlaping,
contiguous and non-empty. Each substring begins by a delimiter and ends
before a delimiter, except for the first substring which is a prefix of
`s` that ends just before the first delimiters and the last substrings
that end with the end of `s`. If the resulting substring is empty, it is
removed from the result.

For instance :

```antescofo

     "".explode(["b"]) -> []
     "a".explode(["b"]) -> ["a"]
     "ab".explode(["b"]) -> ["a", "b"]
     "abaa".explode(["b"]) -> ["a", "baa"]
     "abbb".explode(["b"]) -> ["a", "b", "b", "b"]


     @fun_def compar($x, $y) { return $x == $y }
     $a := @compar("a")

     "".explode($a) -> []
     "a".explode($a) -> ["a"]
     "ab".explode($a) -> ["ab"]
     "abb".explode($a) -> ["abb"]
     "abba".explode($a) -> ["abb", "a"]

     "abcd".explode([0, 0]) -> ["abcd"]
     "abcd".explode([0, 1]) -> ["a", "bcd"]
     "abcd".explode([1, 2]) -> ["a", "b", "cd"]
     "abcd".explode([2, 2, 2]) -> ["ab", "cd"]

```

Remarks that `:::antescofo @explode(x) == @explode(x, \$a.(true))`.




See also 
{!Library/Functions/string_functions.list!}

{!Library/Functions/functions.ref!}
