<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@floor(x:numeric)
```
returns the largest integral value less than or equal to `x`


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}
