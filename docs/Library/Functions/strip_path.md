<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@strip_path(path:string)
```


returns a tab of two strings. The first denotes the parent directory of
the pathname pointed to by `path`.  Any trailing `/` characters are not
counted as part of the directory name.  If `path` is an empty string, or
contains no `/` characters, the returned string is `:::antescofo "."`,
signifying the current directory.


The second element of the returned tab is the last component from the
pathname pointed to by `path`, deleting any trailing `/` characters.  If
path consists entirely of `/` characters, the string `:::antescofo "/"` is
returned.  If `path` is the empty string, `:::antescofo "."` is returned.


```antescofo
       _ := @strip_path("/Users/Shared/Max 7/Examples/utilities/latency-test.maxpat") 
       -> [ "/Users/Shared/Max 7/Examples/utilities",  "latency-test.maxpat" ]
```          


See also
{!Library/Functions/system_functions.list!}
{!Library/Functions/string_functions.list!}



