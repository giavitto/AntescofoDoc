<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@erf(x:numeric) ; listable
```

The `:::antescofo @erf` function calculates the error function of $x$;
where

$erf(x) = 2/\sqrt{\pi}*integral from 0 to x of exp(-t*t) dt.$



{!Library/Functions/math_functions.list!}
{!Library/Functions/functions.ref!}


