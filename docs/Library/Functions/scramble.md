<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@scramble(t:tab)
@scramble(t:string)
```
builds a new tab or a new string where the elements `t` of have been scrambled (the
element are rearranged in a random order). The function is impure: two
calls to the same tab does not produce the same result.  The argument is
unchanged.


See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

