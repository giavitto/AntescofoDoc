<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@arch_linux()
```
this predicate returns `true` if the underlying host is a Linux system and
`false` elsewhere.

See also [@arch_darwin] and [@arch_windows]

{!Library/Functions/functions.ref!}

