<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_proc(value)
```
the predicate returns true if its argument is a process definition.
Notice that a `proc` refers to a processs definition and not to a runing
instance of this definition: it is not an `exec`



See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
