<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@cdr(tab)
```
if the argument is not empty, it returns a copy of it but deprived of its first
element, else it returns an empty tab.


Some functions handle `tab` as _lisp lists_: [@car], [@cdr], [@concat],
[@cons], [@empty], [@drop], [@take].

See also 
{!Library/Functions/tab_functions.list!}


{!Library/Functions/functions.ref!}

