<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rnd_geometric(p:int)
```
returns a random generator that produces integers following a *geometric
discrete distribution*:

        P(i | p) = p (1 - p)^i,    i  ≥ 0.

See [@rnd_bernoulli] for a description of _random generators_.


See also 
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}
