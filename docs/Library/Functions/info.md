<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@info(string or fun, ...)
```

Open in a browser the documentation page of the functions. Functions are
specified through their name in a string or directly with their
@-isdentifier. This function accepts several arguments. 


The online documentation is located at URL http://antescofo-doc.ircam.fr/




See also
{!Library/Functions/system_functions.list!}


{!Library/Functions/functions.ref!}


