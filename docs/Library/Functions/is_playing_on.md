<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_playing_on()
```
this predicate returns `:::antescofo true` if the detection of musical event is simulated. 

Musical event are simulated (opposed to detected by the listening
machine) in fastforward mode (see [@is_fastforward]) and also in play
mode. The play execution mode make a simulation of the normal
functionning of an _Antescofo_ program by providing a simulation of the
events to detect. This mode is usefull to debug a prgram or to use the
language as a kind of advanced sequencer.


See also
[@is_fastforward],
[@is_following_on],
[@is_paused],
[@is_playing_on],
[@is_running],
and [@is_stopped].


{!Library/Functions/functions.ref!}

