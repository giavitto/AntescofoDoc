<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@score_tempi()
```

returns a tab of couples (_i.e._ tab of size 2). The $i$th elements
correspond to the $i$th BPM changes in the score. The value of its
element is the tab [position, BPM].


For example
```antescofo
       NOTE D6 1    event1
       NOTE C7 0    event2
       BPM 120
       NOTE D6 2  event3
       NOTE C7 2
       CHORD (D1 A7 Eb7) 4 event5
       BPM 30
       trill (CC6 D7 A7) 2 event6
```
with this score, `@make_bpm_map()` will return:
```antescofo
       TAB[  TAB[0.0, 60.0],
             TAB[1.0, 120.0],
	     TAB[1.5, 30.0]  ]
```



See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
