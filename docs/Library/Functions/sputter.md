<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@sputter(t:tab, p:float, n:numeric)
```
is an impure function returning a new tab of length `n`. This tab is
filled as follows:

- The process starts with a current value initialized with the first
  element in `t`.

- Successively, for each element $e$ in the result, a random number $p'$
  between 0 and 1 is compared with `:::antescofo p`:

    - if $p'$ it is lower than `:::antescofo p`, then $c$ becomes the
      value of $e$ and the element next $e$ is processed.

    - If $p'$ it is greater than `:::antescofo p`, then $c$ takes the
      value of the next element in `t`, this new value becomes the value
      of $e$ and the element next $e$ is processed.



Not that this function is impure as it returns a different result for
each invocation. For example:

```antescofo
       @sputter([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 0.5, 16)
```

can return

```antescofo
        → [ 1, 1, 1, 1, 1, 2, 3, 4, 5, 6, 6, 6, 7, 8, 8, 9 ]
        → [ 1, 2, 3, 3, 4, 5, 6, 7, 8, 8, 9, 9, 9, 9, 10 ]
        → [ 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 4, 5, 5, 5 ]
```          




See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

