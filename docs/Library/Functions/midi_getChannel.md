<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi_getChannel(tab)
```
The argument is a tab representing a midi message, see function
[@midi_read]. The [@midi_getChannel] extracts from this tab the channel
of the message if the message corresponds to a channel specific command
(_i.e._, noteOn, noteOff, Aftertouch, Controller, PatchChange, Pressure
and Pitchbend, see [@midi_read]).

On a non-channel specific command, the result is undeterminate. 


See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


