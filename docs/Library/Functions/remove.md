<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@remove(t:tab, n:numeric)
@remove(s:string, n:numeric)
@remove(m:map, k:value)
```
[@remove] is an impure overloaded function. See also [@insert].


-------------------------------------
```antescofo
@remove(t:tab, n:numeric)
@remove(t:string, n:numeric)
```
removes the element (or the character) at index `n` in `t` (`t` is modified in place).

Note that building a new tab by removing elements satisfying some
property `P` is easy with a comprehension:
```antescofo
       [ $x | $x in t, P ]
```
which builds a new tab, leaving`t` untouched.



-------------------------------------
```antescofo
@remove(m:map, k:value)
```
removes the entry `k` in map (`m` is modified in place). Does nothing if
the key `k` is not present in map .


-----------------------------------

See also 
{!Library/Functions/tab_functions.list!}

See also 
{!Library/Functions/map_functions.list!}

{!Library/Functions/functions.ref!}
