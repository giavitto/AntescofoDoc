<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@permute(t:tab, n:numeric)
```

returns a new tab which contains the _n_ th permutations of the elements of
`t`. They are factorial _s_ permutations, where _s_  is the size of `t`.

The first permutation is numbered 0 and corresponds to the permutation
which rearranges the elements of `t` in an array `t_0` such that they are
sorted increasingly. The tab `t_0` is the smallest element amongst all
tab that can be done by rearranging the element of `t`. The first
permutation rearranges the elements of in a tab `t_1` such that
`t_0 < t_1` for the lexicographic order and such that any other
permutation gives an array `t_k` lexicographicaly greater than `t_0`
and `t_1`. _Etc._
The last permutation (factorial _s_ - 1) returns a tab where all
elements of are in decreasing order.

For example:
```antescofo 
       $t := [1, 2, 3]
       @permute($t, 0) == [1, 2, 3]
       @permute($t, 1) == [1, 3, 2]
       @permute($t, 2) == [2, 1, 3]
       @permute($t, 3) == [2, 3, 1]
       @permute($t, 4) == [3, 1, 2]
       @permute($t, 5) == [3, 2, 1]
```

See also [@sort].

See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}



