<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@resize(t:tab, int)
```
resizes its first argument and returns the results. The argument is
modified. If the second argument is smaller that the size of the first
argument, it effectively shrinks the first argument. If it is greater,
undefined values are used to extend the tab.

See also 
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

