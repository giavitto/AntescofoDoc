<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_prefix(s1:string, s2:string)
@is_prefix(s1:string, s2:string, cmp:fct)
@is_prefix(t1:tab, t2:tab)
@is_prefix(t1:tab, t2:tab, cmp:fct)
```
[@is_prefix] is an overloaded function. See also [@is_suffix] and
[@is_subsequence].


-----------------------------------------

```antescofo
@is_prefix(s1:string, s2:string)
```
returs true if string `s1`is a prefix of `s2`.


-----------------------------------------

```antescofo
@is_prefix(s1:string, s2:string, cmp:fct)
```
the predicate returns true if `s1` is a prefix of `s2` where the
characters are compared with the function `cmp` (taking two
arguments). The characters are passed to the function `cmp` as strings
of length one.


-----------------------------------------

```antescofo
@is_prefix(t1:tab, t2:tab)
```
the predicate returns true if `t1` is a prefix of `t2`, that is, if the
elements of `t1` are the initial elements of `t2`.


-----------------------------------------

```antescofo
@is_prefix(t1:tab, t2:tab, cmp:fct)
```
same as the previous version but the function is used to test the
equality between the elements, instead of the usual comparison between
values. For example:
```antescofo
       @fun_def cmp($x, $y) { $x > $y }
       @is_prefix([11, 22], [5, 6, 7], @cmp) -> true
```
`true` is returned because `@cmp(11, 6)` and `@cmp(22, 7)` hold.




-----------------------------------------

See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
