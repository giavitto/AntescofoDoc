<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@ceil(numeric)
```
This function returns the smallest integral value greater
than or equal to its argument.


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}

