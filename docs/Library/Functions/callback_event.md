<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
    @callback_stop(applicable value, bool)
    @callback_stop(<undef>, int)

    @callback_all_event(applicable value, bool)
    @callback_all_event(<undef>, int)

	@callback_next_event(applicable value, bool)
	@callback_next_event(<undef>, int)

    @callback_start_fastforward(applicable value, bool)
    @callback_start_fastforward(<undef>, int)

	@callback_stop_fastforward(applicable value, bool)
	@callback_stop_fastforward(<undef>, int)

    @callback_start_logical_instant(applicable value, bool)
	@callback_start_logical_instant(<undef>, int)


	@callback_stop_logical_instant(applicable value, bool)
	@callback_stop_logical_instant(<undef>, int)
```

See also [Event Callbacks] in the reference manual.

The function `:::antescofo @callback_xxxx` install (first form) or
uninstall (second form) an  handler which is called for the
internal event `:::antescofo xxx`.

The handler can be any applicable value (function, process, object with
an apply method, lambda expression) that takes two arguments. When
called, the relative date and the absolute date are given for the
arguments. 

The handler is run in the context of an ordinary antescofo
computation. If the handler is installed as persistent, then it will be
trigered by the occurence of the associated internal event until it is
removed. If the handler is not persistent, it is automatically removed
after its first activation.

Several handler can be installed for the same internal event. To remove
a specific handler, use teh integer returned by its installation. 

These functions can be called anytime and the installed handler is
trigered by the occurence of the next internal event. 

**Notice:**


- The _start\_logical\_instant_ handler is activated at the very
  begining of a logical instant, before any other activities, including
  activities that are trigered by the update of system variables like
  `:::antescofo $RNOW`. That is, if there is a [whenever] used to watch
  `:::antescofo $RNOW`, the handler is run with the right `:::antescofo
  $RNOW` value but before the execution of the whenever body.


- The _stop_ handler is executed when the stop command is invoked,
  ‟simultaneously” with the ongoing computations. Then, the actual
  stoping actions are undertaken, in the same instant: the 
  flow of time and the propagation of musical event (_e.g., the
  listening machine) are stoped (see [transport commands]) and all
  ongoing activities are killed.
  
- So, do not use processes for the _stop handler_: only the immediate
computation are carried. Any delayed computations spawn by the handler
will be killed by the processing of the stop command.

- The handler associated to the _stop\_fastforward_ event is run after
  the fastforward phase but before the normal time flow resumes.  If
  some actions need to be undertaken after the time has resumed, it can
  be achieved by a transient (_i.e._, non persistent) _nextevent_
  handler installed by the _stop\_fastforward_ handler.

<br> 

An example of the correct management of sample playback relying on
[event callbacks] and [callback messages] is given
[here](/Library/HowTo/SampleManagement/samplemanagement)




