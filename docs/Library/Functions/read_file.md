<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@read_file(file:string)
```
returns the content of _file_ as a string.


See also
{!Library/Functions/dataexchange_functions.list!}




