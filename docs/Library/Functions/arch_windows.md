<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@arch_windows()
```
this predicate returns `true` if the underlying host is a Windows system and
`false` elsewhere.

See also [@arch_darwin] and [@arch_linux]

{!Library/Functions/functions.ref!}

