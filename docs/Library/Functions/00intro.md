<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}



<center>
<h1>
<font color="red" style="font-size:150%;">
<i>Antescofo</i> Library
<br>
of Predefined Functions
</font>
</h1>

<img alt="header figure" src="/Figures/entete_library.png" width="85%">

</center>

<br>



*Antescofo* includes more than 360 predefined functions. They are
described mostly in the chapters [Expressions], [Scalar values] and
[Data structures]. For the reader's convenience, we give here a list of
these functions and there is a description page for each of them in the
next section.

In these following sections, the sequence of names after the function defines
the type of the arguments accepted by a function. For example
`:::antescofo numeric` is used when an argument must satisfy the
predicate `:::antescofo @is_numeric`, that is `:::antescofo @is_int` or
`:::antescofo @is_float`. In addition, we use the term `:::antescofo
value` when the function accepts any kind of argument.


In the rest of this page, we recall some useful features of _Antescofo_
functions (see chapter
[Functions](http://antescofo-doc.ircam.fr/Reference/9-functions) for a
more detailed treatment) and we list the predefined functions using
several categories in order to facilitate their identification according
to their uses.

<br>


## Listable Functions and Listable Predicates

When a function `f` is marked as **listable**, the function is extended
to accept [tab] arguments in addition to *scalar* arguments.  Usually,
the result of the application of `f` on a tab is the tab resulting on
the point-wise application of `f` to the scalar elements of the tab. But
for predicate, _i.e._ a function that returns a [bool], the result is
the predicate that returns true if the scalar version returns true on
all the elements of the tabs.

For example, `:::antescofo @abs` is a **listable function** on
`numeric`, so it can be applied to a tab of numerics. The result is the
tab of the absolute value of the elements of the tab argument.

Another example: the function `:::antescofo @approx` is a **listable
predicate** and `:::antescofo @approx(u, v)` returns true if
`:::antescofo @approx(u[i], v[i])` returns true for all elements
`:::antescofo i` of the tabs `:::antescofo u` and `:::antescofo v`.



## Overloaded functions

Some functions accept different kinds of values for the same
argument. For example [@insert] accepts tab or map as its first
argument. Such functions are said **overloaded**: they gather under the
same name several specialized version of the function.

A user-defined function can be overloaded: it recquires to check the
type of the argument value to dispatch to the specialized versions
(written elsewhere).

Overloading methods is more simple: the same identifier can be used to
name a method in different objects.


## Side-Effect

Most functions are **pure functions**, that is, they do not
modify their arguments and build a new value for the result.

In some cases, the function works by a side-effect, like `:::antescofo
@push_back` which modifies its argument *in place*. Such functions are
marked as **impure**. We also qualify functions that may return
different values when called with the same arguments as impure, even if
they do not produce a side-effect (for example, functions that return a
random number).



## Special forms

Special forms are syntactic constructs similar to function calls but that
are subject to some restriction or that behave differently of a function
call.

For example, boolean predicates are a special form of function
application because they do not always evaluate all their arguments
(they are _lazy_). For instance, the conjunction `:::antescofo &&`
evaluates its second argument only if the first is not `:::antescofo
false`.

Another example is [@plot], [@history_length] or [@dumpvar] that only
accepts a variable as an argument, not a general expression.

Special forms cannot be curryfied nor passed as an argument: they are not
ordinary functional values. Note, howevever that they can be wrapped in a
user-defined function which is an ordinary functional value.



## Infix function call

A function call is usually written in **prefix form**:
```antescofo
      @drop($t, 1)
      @scramble($t)
``` 

It is possible to write function calls in **infix form**, as
follows:

```antescofo
      $t.@drop(1)
      $t.@scramble()
```

The `@` character is optional when naming a function in
infix call, so we can also write:

```antescofo
      $t.drop(1)
      $t.scramble()
```

This syntax is the same as for a _method call_. The general form is:

```antescofo
      arg1 . @fct (arg2, arg3, ...)    ; or more simply
      arg1 . fct (arg2, arg3, ...)  
```

The `:::antescofo argi` are expressions. Notice that the infix call,
with or without the `@` in the function name, is not ambiguous with the
notation used to refer to a variable local `:::antescofo $x` in a
compound action from the exe of this action, `:::antescofo exe.$x`,
because `:::antescofo $x` cannot be the name of a function.

The infix notation is less general than the prefix notation. In
the prefix notation, the function can be given by an expression. For
example, functions can be stored into an array and then called following
the result of an expression:

```antescofo
      $t := [@f, @g]
      ; ...
      ($t[exp])()
```

will call `:::antescofo @f` or `:::antescofo @g` following the value
returned by the evaluation of `:::antescofo exp`. Only function name
(with or without `@`) are accepted in the infix notation.




## Named parameters and optional arguments in functions applications

The identifier of the arguments in a function definition can be used,
withoout the leading `$` as a name for an out-of-order argument passing
in function application. See section [Named parameters and out-of-order
application](/Reference/functions_apply#named-parameters-and-out-of-order-application)
(this also holds for processes and methods).

Arguments may have a default value, as explained in section [Optional
arguments and default argument's
value](/Reference/functions_apply/#optional-arguments-and-default-arguments-value). In
case of [partial
application](/Reference/functions_apply/#curried-functions), these
arguments take their default value if they are not explicitly specified
in the call.


<br>



<center>

<img alt="line divider" src="/Figures/line-divide-grayr.svg" width="45%">


# Listing by categories

</center>

<br>

Function names in square brackets are either reserved for internal use
or awaiting documentation.


<br>
## {!Library/Functions/math_functions.list!}

<br>
## {!Library/Functions/random_functions.list!}

<br>
## {!Library/Functions/tab_functions.list!}

in addition, see _listable_ operators.


<br>
## {!Library/Functions/listable_functions.list!}

<br>
## {!Library/Functions/nim_functions.list!}

<br>
## {!Library/Functions/map_functions.list!}

<br>
## {!Library/Functions/string_functions.list!}

For regular expressions, see [@r_compile] and also [@parse].


<br>
## {!Library/Functions/predicates_functions.list!}

<br>
## {!Library/Functions/score_functions.list!}

<br>
## {!Library/Functions/midi_functions.list!}

<br>
## {!Library/Functions/dataexchange_functions.list!}

<br>
## {!Library/Functions/system_functions.list!}


<br>
<br>


<center>

<img alt="line divider" src="/Figures/line-divide-grayr.svg" width="45%">



# Alphabetical Listing of _Antescofo_ Predefined Functions

</center>

<br>

Function names in square brackets are either reserved for internal use
or awaiting documentation. This listing is automatically produced by the
command [antescofo::libraryinfo]. 

<br>

{!Library/Functions/functions.list!}
