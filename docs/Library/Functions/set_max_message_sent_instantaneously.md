<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
    @set_max_message_sent_instantaneously(int)
```

This function can be used to set the limit of Max messages sent in one
logical instant.


There is a limit to the number of messages _Antescofo_ can send in the
same instant, because sending too many messages ‟in the same instant”
can crash Max. 

If you change this limit, you should probably also change accordingly
the Pool Throthle and the  Queue Throtle size in Max preferences
(section Scheduler).

Initially, this limit is set to 500 messages.

<br>
See also [callback messages] and
{!Library/Functions/system_functions.list!}



