<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@merge(map, map)
```
returns a new map which is the asymetric merge of the two argument maps.

The result of `@merge(a, b)` is a map `c` such that
`c(x) = a(x)` if `a(x)` is defined, and `b(x)` elsewhere.

Notice that `a(x)` is defined if `x` is a key in `a` but the value
`a(x)` may be the `undef` value.

See also [@map_compose] and [@mapval].


See also
{!Library/Functions/map_functions.list!}


{!Library/Functions/functions.ref!}
