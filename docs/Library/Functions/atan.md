<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@atan(numeric) ; listable
```

arc tangente


{!Library/Functions/math_functions.list!}
{!Library/Functions/functions.ref!}


