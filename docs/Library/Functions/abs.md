<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@abs(numeric) ; listable
```
absolute value


See also
{!Library/Functions/math_functions.list!}

{!Library/Functions/functions.ref!}

