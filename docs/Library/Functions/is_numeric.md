<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_numeric(value)
```
the predicate returns true if its argument is an integer or a floating point value.


See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
