<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@range(m:map)
```
returns the tab of the values present in the map. The order in
the tab is irrelevant.


See also [@domain]

See also
{!Library/Functions/map_functions.list!}

{!Library/Functions/functions.ref!}
