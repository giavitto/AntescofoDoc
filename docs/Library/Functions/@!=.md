<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}


```antescofo
@!=(value, value) ; listable
```

Prefix form of the infix relational operator. Same remarks as for [@<].


See also [@==], [@>], [@>=], [@<], [@<=]


{!Library/Functions/functions.ref!}
