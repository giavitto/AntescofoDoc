<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi2hz(pitch:numeric)
@midi2hz(pitch:string)
@midi2hz(pitch:symbol)

@midi2hz(reference:numeric,  pitch:numeric)
@midi2hz(reference:numeric,  pitch:string)
@midi2hz(reference:numeric,  pitch:symbol)
```
convert a midi note number into a frequency. The version with one
argument uses an explicit reference frequency of 440 Hz while the
version with two arguments allows an explicit specification of an
arbitrary base. 

For example, `69` (the midi encoding of the standard pitch A440) is
converted in `440`Hz by `::antescofo @midi2Hz(69)`.  See [midi
tuning](https://en.wikipedia.org/wiki/MIDI_Tuning_Standard) and
functions [@midicent2hz], [@hz2midi], [@hz2symb] and [@symb2midicent].


When a midi note is expressed symbolically (through a string or a
symbol), the midi numerical coding of a pitch in midicents or in midi
note is irrelevant and the function returns the same value as
[@midi2hz].


See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


