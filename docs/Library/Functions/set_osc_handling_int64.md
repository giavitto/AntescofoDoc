<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}


```antescofo
@set_osc_handling_int64(bool)
```

changes the handling of integers in OSC messages. _OSC integers_ are
only required to be 32 bits integers in OSC. Nevertheless, several
implementations proposes 64 bits integers.

By default _Antescofo_ sends integers as 32 bits integers. However,
Antescofo integers representation are 32 bits integers in the 32 bits
version of the `antescofo~` objects, and 64 bits integers in the 64 bits
version. So the default behavior may result in silent overflow when
sending integers with the 64 bits object.


A call to `:::antescofo @set_osc_handling_int64(true)` switches the
behavior to send 64 bits integers (irrespectively of the object
version). The drawback is that this feature is not implemented in all
OSC packages.

When receiving a message, 32 or 64 bits integers are always correctly
handled if the sender correctly handle 32 and 64 bits integers. Notice
that receiving a 64 bit integers with a 32 bits object may result in a
silent overflow.


Beware that a message as a limited size that depends both on the sender
and receiver implementation limitation. The current _Antescofo_ raw
buffer size is 5096. The use of 64 bits integers increase the size of a
message.


<br>

See the [Osc Messages] section in the reference manual and functions
[@set_osc_handling_double] and [@set_osc_handling_tab].

See also
{!Library/Functions/system_functions.list!}



