<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@user_directory()
```
returns a string which is the path of the home directory of the user that runs the program. 

See also [@pwd], [@directory_read], [@current_load_directory] and 

{!Library/Functions/functions.ref!}

