<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@log10(numeric)
```
computes the value of the logarithm of its argument to base 10.


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}
