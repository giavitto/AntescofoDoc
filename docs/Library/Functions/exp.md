<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@exp(x:numeric)
```
the base _e_ exponential of `x`.


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}
