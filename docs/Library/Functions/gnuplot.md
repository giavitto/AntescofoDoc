<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@gnuplot(data:tab)
@gnuplot(title:string, data:tab)
@gnuplot(time:tab, data:tab)
@gnuplot(title:string, time:tab, data:tab)
@gnuplot(title1:string, time1:tab, data1:tab, title2: string, time2:tab,
data2:tab, ...)
```
The function [@gnuplot] has a variable number of arguments and cannot be
curryfied. It admits five forms. See also [@plot] and [@rplot].


-------------------------------------------------------------------


```antescofo
@gnuplot(data:tab)
```
The function [@gnuplot] plots the elements in the `data` tab as a time
series. If `data` is a tab of numeric values, a simple curve is plotted:
an element `e` of index `i` gives a point of coordinate `(i, e)`.  If
`data` is a tab of tab (of `p` numeric values), `p` curves are plotted
on the same window.

Each [@gnuplot] invocation lead to a new window. The invocation is done
asynchrounously: when the function returns, the plot process is still
running for its completion.

Function [@gnuplot] returns `true` if the plot succeeded,
and `false` elsewhere.

To work, the [gnuplot] program must be installed on the system Cf.
[http://www.gnuplot.info](http://www.gnuplot.info) and must be visible
from the _Antescofo_ object (on a Mac system, it can be installed
through _fink_, _macport_ or _brew_; on linux, it can be installed
through the package management system). They are several ways to make
this command visible and the search of a [gnuplot] executable is done in
this order:

- set the global variable `$gnuplot_path` to the absolute
path of the [gnuplot] executable (in the form of a string);

- alternatively, set the environment variable `GNUPLOT` _of the shell_
used to launch the _Antescofo_ standalone or the Max/PD host of the
_Antescofo_ object, to the absolute path of the [gnuplot]
executable;

- alternatively make visible the [gnuplot] executable
visible from the shell used by the user shell to launch the
_Antescofo_ standalone or the Max/PD host of the _Antescofo_
 object (_e.g._ through the `PATH` _shell variable_).

The command is launched on a shell with the option
`-persistent` and the absolute path of the gnuplot command
file. 

The data are tabulated in a file `/tmp/tmp.antescofo.data.n`
(where _n_ is an integer) in a format suitable for [gnuplot].
The gnuplot commands used to plot the data are in the file
`/tmp/tmp.antescofo.gnuplot.n`.
These two files persists between two _Antescofo_ session and can then
be used to plot with other option.

The variable `:::antescofo $gnuplot_linestyle` can be used to change the
style of the lines used to conect the data points. The value must be as
string giving the gnuplot name of the choosen style. The default value is
`:::antescofo "linespoints"`. Other possible values are:
`:::antescofo "dots"`, 
`:::antescofo "points"`,
`:::antescofo "linespoints"`, 
`:::antescofo "impulses"`,
`:::antescofo "steps"`, 
`:::antescofo "fsteps"`,
`:::antescofo "histeps"`,
see [Gnuplot Plot style](http://lowrank.net/gnuplot/intro/style-e.html).

<br>

The [@gnuplot] function is overloaded and accepts a variety of
arguments described below. The [@gnuplot] function is used
internally by the special forms [@plot] and [@rplot].



-------------------------------------------------------------------


```antescofo
@gnuplot(title:string, data:tab)
```
same as the previous form, but the first argument is used as the label
of the plotted curve. If `data` is a tab of tab, (_e.g._  the
history of a tab valued variable), then the label of each curve takes
the form `title_i`.



-------------------------------------------------------------------

```antescofo
@gnuplot(time:tab, data:tab)
```
plots the points `time[i], data[i])`.  As for the previous form, `data`
can be a tab of tab (of numeric values). The `time` tab corresponds to
the `x` coordinates of the plot and must be a tab of numeric values.


-------------------------------------------------------------------

```antescofo
@gnuplot(title:string, time:tab, data:tab)
```
Same as the previous entry but the first argument is used as the label
of the curve(s).


-------------------------------------------------------------------

```antescofo
@gnuplot(title1:string, time1:tab, data1:tab, title2: string, time2:tab,
data2:tab, ...)
```
In this variant, several curves are plotted in the same window. One
curve is specified by 2 or 3 consecutive arguments. Three arguments are
used if the first considered argument is a string: in this case, this
argument is the label of the curve. The following argument is used as
the `x` coordinates and the next one as the `y` coordinates of the
plotted point. The tab arguments must be tab of numeric values (they
cannot be tab of tab).


<br>

See also [@plot], [@rplot].

See
{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/functions.ref!}

[gnuplot]: http://www.gnuplot.info