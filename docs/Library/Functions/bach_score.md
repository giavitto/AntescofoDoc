<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@bach_score()
@bach_score(normalize:bool)
```

This function returns a vector of strings. Each string decribes a muscal
event or a score annotation (_e.g._, A BPM specification). These strings
are suitable messages for the [bach.roll](http://www.bachproject.net/)
object in Max.

This function can be used to build in
[bach](http://www.bachproject.net/) a piano roll representation of the
Antescofo score. The current position (which is available as an outlet
of the Max or PD object or in the `:::antescofo $RNOW` or `:::antescofo
$RCNOW` variables) can be notified to _bach_, achieving the animation of
the partition during the play.

If the optional argument, a boolean, is true, it specifies that onsets
and durations have been normalized using the tempo specified in the
score (so they are specified in milliseconds in the result). Calling the
function without argument is equivalent to call it with a false argument
(_i.e._, onset and duration are given in milli-beats). 


For example, if the antescofo score is (notice the change in tempo in
the middle of the score):

```antescofo
BPM 72
NOTE A4 1/6  MES1
NOTE C#4 1/6
TRILL ((D4 A4)) 1 mes8
CHORD (A4 F#5) 1/2
BPM 60
NOTE A#5 0
CHORD (-A4 A5) 3/2
MULTI ((-A4 -A5) -> (-A4 G#5)) 1
NOTE A#6 7/2 syncM13
```

a call to `:::@bach_score()`or to `:::antescofo @bach_score(false)`
produces the following TAB:

```antescofo
   ["addmarker 0 () tempo (1/2 72)",
	"addchord (0 (6900 166.667 64) (name MES1) 0)",
	"addchord (166.7 (6100 166.667 64) 0)",
	"addchord (333.3 (6200 1000 64) (6900 1000 64) (name mes8) 0)",
	"addchord (1333.3 (6900 500 64) (7800 500 64) 0)",
	"addmarker 1833.3 () tempo (1/2 60)",
	"addchord (1833.3 (8200 0 64) 0)",
	"addchord (1833.3 (-6900 1500 64) (8100 1500 64) 0)",
	"addchord (3333.3 (6900 1000 64) (8000 1000 64) 0)",
	"addchord (4333.3 (9400 3500 64) (name syncM13) 0)"    ]
	
```

while a call to `:::antescofo @bach_score(true)` produces: 

```antescofo
   ["addmarker 0 () tempo (1/2 72)",
	"addchord (0 (6900 138.889 64) (name MES1) 0)",
	"addchord (138.889 (6100 138.889 64) 0)",
	"addchord (277.778 (6200 833.333 64) (6900 833.333 64) (name mes8) 0)",
	"addchord (1111.11 (6900 416.667 64) (7800 416.667 64) 0)",
	"addmarker 1527.78 () tempo (1/2 60)",
	"addchord (1527.78 (8200 0 64) 0)",
	"addchord (1527.78 (-6900 1500 64) (8100 1500 64) 0)",
	"addchord (3027.78 (6900 1000 64) (8000 1000 64) 0)",
	"addchord (4027.78 (9400 3500 64) (name syncM13) 0)"    ]
```


The antescofo~ object also understand the two commands 

 - `:::antescofo  antescofo::bachscore`  (or equivalenty the `bachscore`
message), and

 - `:::antescofo antescofo::bachscore_norm` (or equivalenty the
`bachscore_norm` message).

These commands takes an identifier `recv` as a mandatory argument. In
response of this command, the description of the score (as computed by
`:::antescofo @bach_score(false)` or `:::antescofo @bach_score(true)`) is
sent to receivers `recv`.



See also
{!Library/Functions/score_functions.list!}



{!Library/Functions/functions.ref!}

