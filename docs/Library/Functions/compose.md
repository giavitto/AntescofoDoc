<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@compose(nim1, nim2)
```
returns a new nim which is the composition  $nim1 \circy nim2$ that is
the nim $f$ such that $f(x) = nim1(nim2(x))$. 

Following the interpolation type used by the nim arguments, the
computation of the composition may require to compute a piecewise linear
approximation of these arguments. 


See also
{!Library/Functions/nim_functions.list!}

{!Library/Functions/functions.ref!}
