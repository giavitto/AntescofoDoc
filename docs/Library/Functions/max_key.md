<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@max_key(nim)
```
returns the coordinate `x_n` of the last breakpoint of the nim.  This
coordinate is the sum of `x_0` (the coordinate of the first breakpoint
of the nim, and of all intervals `d_i` of the breakpoints _i_. If the
nim is vectorial, `x_n` is a tab. 

See also [@min_key], [@min_val] and [@max_val].

<br>
<img  src="/Figures/nim_min_max.png" width=80% center>
<p>


See also
{!Library/Functions/nim_functions.list!}

{!Library/Functions/functions.ref!}
