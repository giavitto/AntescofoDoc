<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_fct(value)
```
the predicate returns true if its argument is an intentional function,
_i.e._ a predefined function, or a function defined using `@fun_def`, or
a partial application of such functions.


See also
{!Library/Functions/predicates_functions.list!}


{!Library/Functions/functions.ref!}
