<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@active(string)
@active(proc)
@active(exe)
@active()
```

returns in a `tab` the active (alive) `exe` of the instances of a
process or an object specified through its name (a string with or
without the prefix `::` or `obj::`) or through its `proc` value. If the
argument is an `exe`, it returns the empty tab if the exe is dead and a
singleton (with the argument) if it is alive. With no argument, it returns
all active `exe` in a `tab`.

See also [@number_active]


{!Library/Functions/functions.ref!}
