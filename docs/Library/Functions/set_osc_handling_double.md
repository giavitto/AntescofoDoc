<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}


```antescofo
@set_osc_handling_double(bool)
```

changes the handling of floating point values in OSC messages. _OSC
float_ are only required to be 32 bits. Nevertheless, several
implementations proposes 64 bits representation (aka _IEEE double_).


_Antescofo_ default behavior is to send floating point values as 32 bits
floats. However, _Antescofo_ floating point values are represented as
double. So the default behavior may result in silent overflow when
sending integers with the 64 bits object.


A call to `:::antescofo @set_osc_handling_double(true)` switches the
behavior to send double. The drawback is that this feature is not
implemented in all OSC packages.

When receiving a message, floats and double are always correctly handled
if the sender correctly handle them. Notice that receiving a double
never result in an overflow.


Beware that a message as a limited size that depends both on the sender
and receiver implementation limitation. The current _Antescofo_ raw
buffer size is 5096. The use of 64 bits integers increase the size of a
message.

<br>

See the [Osc Messages] section in the reference manual and functions
[@set_osc_handling_tab] and [@set_osc_handling_int64].


See also
{!Library/Functions/dataexchange_functions.list!}




