<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_defined(dico:map, k:value)
```
the predicate returns true if `k` is a key present in `dico`
Do not mismatch with the negation of the predicate [@is_undef].



See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}

