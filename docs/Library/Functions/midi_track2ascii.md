<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi_track2ascii(tab)
```

This function read a list of timestamped midi messages, as returned by
[@midi_read], and produces a tab where the seven previous commands are
given in a human readable way, with the channel is uncoupled from the
command name. This function can be used for debugging purposes.


For example, suppose that

```antescofo
       $t := @midi_read("bolero.mid")
```

returns the tab
```antescofo
    TAB[  [0.0,        TAB[255, 3, 6, 66, 111, 108, 101, 114, 111]],
          [0.0,        TAB[255, 81, 3, 11, 188, 206]],
          [0.0,        TAB[255, 88, 4, 4, 2, 24, 8]],
          [1.45032,    TAB[240, 65, 16, 66, 18, 64, 0, 127, 0, 65, 247]],
          [0.717147,   TAB[176, 91, 127]],
          [0.020032,   TAB[177, 91, 127]],
          [0.0400641,  TAB[178, 91, 127]],
          [0.0400641,  TAB[179, 91, 127]],
          [0.0400641,  TAB[180, 91, 127]],
          [0.00400641, TAB[201, 48]],
          [0.0,        TAB[153, 86, 64]],
          [0.00400641, TAB[137, 86, 64]],
          [0.0,        TAB[200, 32]],
          [0.0,        TAB[184, 10, 65]],
          [0.0,        TAB[152, 36, 52]],
          [0.0320512,  TAB[181, 91, 127]],
          [0.00801281, TAB[193, 60]],
          [0.0,        TAB[145, 67, 98]],
          [0.0320512,  TAB[182, 91, 127]],
          [0.0400641,  TAB[183, 91, 127]],
          [0.0360577,  TAB[129, 67, 98]],
          [0.00400641, TAB[184, 91, 127]],
          [0.0400641,  TAB[185, 91, 127]],
          [0.0400641,  TAB[186, 91, 127]],
          [0.0400641,  TAB[187, 91, 127]],
          [0.0400641,  TAB[188, 91, 127]],
          [0.0400641,  TAB[189, 91, 127]],
          [0.0120192,  TAB[145, 67, 98]],
          [0.0120192,  TAB[153, 86, 31]],
          [0.0,        TAB[190, 91, 127]],
          [0.0160256,  TAB[191, 91, 127]],
          [0.0520833,  TAB[129, 67, 98]],
          [0.0160256,  TAB[137, 86, 31]],
          [0.0360577,  TAB[145, 67, 98]],
          [0.0120192,  TAB[153, 86, 31]],
          [0.0240384,  TAB[136, 36, 52]],
          [0.020032,   TAB[129, 67, 98]],
          [0.0160256,  TAB[137, 86, 31]],
          [0.0360577,  TAB[145, 67, 98]],
          [0.0280448,  TAB[153, 86, 31]],
          [0.0440705,  TAB[129, 67, 98]]
    ]
```

Then 

```antescofo
    @midi_tarck2ascii($t)
```

returns

```antescofo
      TAB[  TAB[<<undef>>],
      	    TAB[<<undef>>],
	    TAB[<<undef>>],
	    TAB[<<undef>>],
	    TAB["Controller", 0, 91, 127],
	    TAB["Controller", 1, 91, 127],
	    TAB["Controller", 2, 91, 127],
	    TAB["Controller", 3, 91, 127],
	    TAB["Controller", 4, 91, 127],
	    TAB["PatchChange", 9, 48],
	    TAB["NoteOn", 9, "D6", 64],
	    TAB["NoteOff", 9, "D6"],
	    TAB["PatchChange", 8, 32],
	    TAB["Controller", 8, 10, 65],
	    TAB["NoteOn", 8, "C2", 52],
	    TAB["Controller", 5, 91, 127],
	    TAB["PatchChange", 1, 60],
	    TAB["NoteOn", 1, "G4", 98],
	    TAB["Controller", 6, 91, 127],
	    TAB["Controller", 7, 91, 127],
	    TAB["NoteOff", 1, "G4"],
	    TAB["Controller", 8, 91, 127],
	    TAB["Controller", 9, 91, 127],
	    TAB["Controller", 10, 91, 127],
	    TAB["Controller", 11, 91, 127],
	    TAB["Controller", 12, 91, 127],
	    TAB["Controller", 13, 91, 127],
	    TAB["NoteOn", 1, "G4", 98],
	    TAB["NoteOn", 9, "D6", 31],
	    TAB["Controller", 14, 91, 127],
	    TAB["Controller", 15, 91, 127],
	    TAB["NoteOff", 1, "G4"],
	    TAB["NoteOff", 9, "D6"],
	    TAB["NoteOn", 1, "G4", 98],
	    TAB["NoteOn", 9, "D6", 31],
	    TAB["NoteOff", 8, "C2"],
	    TAB["NoteOff", 1, "G4"],
	    TAB["NoteOff", 9, "D6"],
	    TAB["NoteOn", 1, "G4", 98],
	    TAB["NoteOn", 9, "D6", 31],
	    TAB["NoteOff", 1, "G4"]
	  ]
```

The `:::antescofo TAB[<<undef>>]` is a tab with ony one element,
`:::antescofo <<undef>>`. This corresponds to midi header associated
with the file and the track. The seven channel related commands (see
[@midiread]) are described with a tab whose first element is the command
name (as a string), the second element is the channel number, _etc._
Cf. the description of teh parameters of a command at [@midi_read].


The [@midi_track2ascii] is mainly used for printing partial information
in a human readable format.


<br>
<br>
See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}

