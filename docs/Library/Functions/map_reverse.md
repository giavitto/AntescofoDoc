<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@map_reverse(map) ; deprecated
```
**deprecated**


See also
{!Library/Functions/map_functions.list!}

{!Library/Functions/functions.ref!}



