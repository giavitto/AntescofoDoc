#!/bin/bash

for i in $(ls *.tex); do
    pandoc -t markdown -f latex -o `basename $i .tex`  $i
    
done
	  
