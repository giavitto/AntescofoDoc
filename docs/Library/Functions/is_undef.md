<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_undef(value)
```
the predicate returns true if its argument is the undefined value.
Do not mismatch with the negation of the predicate [@is_defined].


See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}

