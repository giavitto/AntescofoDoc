<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@print(val, ...)
```

writes argument values to the standard output (Max or PD console, stdout
for standalone, etc.). String values are printed without there quote. 

The returned value is the value of the last argument (or undef). 

see also: [@error]

{!Library/Functions/functions.ref!}
