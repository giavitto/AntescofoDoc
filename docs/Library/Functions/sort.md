<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@sort(t:tab)
@sort(t:tab, cmp:fct)
```
The [@sort] function is an impure overloaded function, with a variable
number of arguments (so it cannot be currifyed).

See also [@permute]


------------------------------------
```antescofo
@sort(t:tab)
```
sorts in-place the elements into ascending order using `<`.



------------------------------------
```antescofo
@sort(t:tab, cmp:fct)
```
sorts in-place the elements into ascending order.

The elements are compared using the function `cmp`. This function must
accept two elements of the tab as arguments, and returns a value
converted to a bool. The value returned indicates whether the element
passed as first argument is considered to go before the second.


See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

