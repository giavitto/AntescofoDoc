<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@shape(t:value)
```
returns 0 if is not an array, and else returns a tab of integers each
corresponding to the size of one of the dimensions of `t`. Notice that
the elements of an array are homogeneous, *i.e.* they have all exactly
the same dimension and the same shape.




       
See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

