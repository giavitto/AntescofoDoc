<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@take(t:tab, n:numeric)
@take(t:string, n:numeric)
@take(t:tab, x:tab)
@take(t:string, x:tab)
```
is a pure overloaded function.
See also functions [@cdr], [@drop] and [@slice].

-------------------------------------
```antescofo
@take(t, n:numeric)
```
builds a new tab or a new string (following the type of `t`) with the
`n` first elements of `t` if `n > 0` and the last `-n` elements of `t`
if `n` is negative. 



-------------------------------------
```antescofo
@take(t, x:tab)
```
gives the tab of elements or the string of characters in `t` whose indices are in tab `x`.
This is equivalent to
```antescofo
       [t[x[$i]] | $i in @size(x)]
```          

-------------------------------------


See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

