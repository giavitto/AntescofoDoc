<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_pitch_tab()
@make_pitch_tab(start:numeric)
@make_pitch_tab(start:numeric, stop:numeric)
```

returns a tab whose _i_ th element is the vector of pitches to the _i_
th musical event in the score.  A `NOTE` corresponds to a tab of size 1,
and a `CHORDS` with *n* pitches to a tab of size *n*. 
Grace notes are not counted.

Called with no arguments, the events considered are all the events in
the score.  With a `start`, only the events whose position in beats is
greater than `start` are considered. If a `stop` is specified, all events
must have a position in beats between `start` and `stop`.

Grace notes do not appear in the tab.


Examples:
```antescofo
       NOTE D6 1    
       NOTE C7 0    
       NOTE D6 1/2  
       NOTE C7 1/5
       CHORD (D1 A7 Eb7) 1/8 
       TRILL (CC6 D7 A7) 1/8 event6
```
With this score, `@make_pitch_tab` returns:
```antescofo
       TAB[ TAB[8600.0],
            TAB[8600.0],
            TAB[9600.0],
            TAB[2600.0, 0500.0, 9900.0],
            TAB[1206.0, 9800.0, 10500.0] ]
```


See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
