<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@clear(tab)
@clear(map)
```
clear all elements in the tab or in the map argument,
resulting in a vector or a dictionary of size zero.


See also
{!Library/Functions/tab_functions.list!}

See also
{!Library/Functions/map_functions.list!}


{!Library/Functions/functions.ref!}
