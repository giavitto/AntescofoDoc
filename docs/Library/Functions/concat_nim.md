<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@concat_nim(nim1, nim)
```
returns a new nim which is the extension of nim1 by nim2. The two
argument must have the same dimension. 


See also
{!Library/Functions/nim_functions.list!}

{!Library/Functions/functions.ref!}
