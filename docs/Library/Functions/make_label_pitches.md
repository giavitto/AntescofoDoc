<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_label_pitches()
@make_label_pitches(start:numeric)
@make_label_pitches(start:numeric, stop:numeric)
```
returns a map associating a vector of pitches to the label of an
event. A `NOTE` corresponds to a tab of size 1, and a `CHORDS` with *n*
pitches to a tab of size *n*. Events with no label do not appear in the
map, as well as grace notes. 


Called with no arguments, the events considered are all the events in
the score.  With a `start`, only the events whose position in beats is
greater than `start` are considered. If a `stop` is specified, all events
must have a position in beats between `start` and `stop`.

Examples:
```antescofo
       NOTE D6 1    event1
       NOTE C7 0    event2
       NOTE D6 1/2  event3
       NOTE C7 1/5
       CHORD (D1 A7 Eb7) 1/8 event5
       TRILL (CC6 D7 A7) 1/8 event6
```
with this score, `@make_label_pitches` returns:
```antescofo
       MAP{ ("event1", TAB[8600.0]),
            ("event3", TAB[8600.0]),
	    ("event5", TAB[2600.0, 10500.0, 9900.0]),
	    ("event6", TAB[1206.0, 9800.0, 10500.0])   }
```


See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
