<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@tan(x:numeric)
```
computes the tangent of `x` (measured in radians).


See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}
