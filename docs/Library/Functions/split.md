<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@split(s:string)
@split(s:string, sep:string)
@split(s:string, seps:tab)
```
The [@split] function splits a string into a tab.

You can specify the separator, default separator is any _space
characters_ (those include "\n" (carriage return, "\t" (tabulation), " "
(whitespace) and any locale white-space characters as defiend by `isspace`).

If the separator is specified by a string, this entire substring is
taken as a separator. 

If the separator is a tab, each element of this tab are a possible
separator.

The result is a tab of strings. The elements of the tab are substrings
of the string argument separated by the separator(s) (or the end of the
string).  An empty substrintg appears in the result if there is two
consecutive separators in the argument. 

Here are some examples:
```antescofo
//default separator
@split("abc def   ghi")	 ⇒ ["abc", "def", "", "", "ghi"]
@split("abc\ndef   ghi") ⇒ ["abc", "def", "", "", "ghi"] == 
@split("abc\tdef   ghi") ⇒ ["abc", "def", "", "", "ghi"] == 

@split("abc def   ghi ") ⇒ ["abc", "def", "", "", "ghi", ""] 

// one explicit separator (a white space)
@split("abc def   ghi", " ")  ⇒ ["abc", "def", "", "", "ghi"]
@split("abc\ndef   ghi", " ") ⇒ ["abc\ndef", "", "", "ghi"]
@split("abc\tdef   ghi", " " )⇒ ["abc\tdef", "", "", "ghi"]

// multi-characters separator (coma followed by a white space)
@split("abc, def, , , ghi", ", ")   ⇒ ["abc", "def", "", "", "ghi"] 
@split("abc, def, , , ghi,", ", ")  ⇒ ["abc", "def", "", "", "ghi,"]
@split("abc, def, , , ghi, ", ", ") ⇒ ["abc", "def", "", "", "ghi", ""]

// multiple separators
⇒["abc", "def", "", "", "ghi"] == @split("abc def   ghi", [" ", ",", "\n", "\t"])
⇒["abc", "def", "", "", "ghi"] == @split("abc\ndef   ghi",  [" ", ",", "\n", "\t"])
⇒["abc", "def", "", "", "ghi"] == @split("abc\tdef   ghi",  [" ", ",", "\n", "\t"])
⇒["abc", "def", "", "", "ghi", ""] == @split("abc def   ghi ",  [" ", ",", "\n", "\t"])
```


See also 
{!Library/Functions/tab_functions.list!}

See also 
{!Library/Functions/map_functions.list!}

{!Library/Functions/functions.ref!}
