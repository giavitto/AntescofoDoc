<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@xml_read(file:string)
```
parse an XML file into an Antescofo value. The argument of the function
is the path of the file. Note that an XML text can be written directly in
an Antescofo program after the keyword `:::antescofo XML::` and is implicitly
converted into an Antescofo value.

The encoding of an XML file into an Antescofo value is as follow :

- A valid XML text is described as a sequence of tags and is
  translated into a vector.

- Meta-data tags (_e.g._ `<?xml? ... ?>` and comments are ignored in the
  translation.

- Each remaining tag  is translated into a _triple_ (a vector of 3 elements)
  `:::antescofo [name, map, content]`:
   - The first element `:::antescofo name` is a string giving the name
     of the tag.
   - The second element `:::antescofo map` is a map describing the
     attributes of the tag. The map is empty if there is no attribute.
   - If the tag specified by the 3-uple has a content, this content is a
     sequence interleaving text and tags. The third elements is a vector
     representing this succession of elements. 

For instance,
```antescofo
$m := XML:: <a>
              <b>1</b>
              <c>3.14159</c>
            </a>
```
will build the following Antescofo value:
```antescofo
$m := [ "a",
        MAP{},
        [ ["b", MAP{}, [1]],
          ["c", MAP{}, [3.14159]]
        ]
      ]
```
A tag with content and attributes
```antescofo
$m := XML::<title size="big" font="times">Hello <strong>World</strong> ! </>
```
is equivalent to
```antescofo
$m := [ "title",
        MAP{ ("font", "times"), ("size", "big") },
        ["Hello ", ["strong", MAP{}, ["World"]], " ! "]
      ]
```


See also [@xml_string].

{!Library/Functions/dataexchange_functions.list!}


{!Library/Functions/functions.ref!}

