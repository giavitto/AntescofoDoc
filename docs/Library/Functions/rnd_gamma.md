<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rnd_gamma(α:float)
```

returns a random generator that produces floating-point values according
to a *gamma distribution* P:

       P(x | α) = x^(α-1) / Γ(α),    x ≥ 0.


See [@rnd_bernoulli] for a description of _random generators_.


See also 
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}
