<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@plot(variable_1, ..., variable_p)
```
is a special form (the arguments are restricted to be
variables).

Calling this special form plots the values stored in the history of the
variables as time series in **absolute time** using the [@gnuplot]
function.

<br>

See also [@rplot].

See
{!Library/Functions/dataexchange_functions.list!}

{!Library/Functions/functions.ref!}
