<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_obj_xxx(value)
```
where `xxx` is the name (without the prefix `obj::`) of an object defined through
`@obj_def`. This predicate is automatically generated with an object definition
and checks that a value represents an instance of `obj::xxx`.




See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}

