<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}



```antescofo
@specified_duration(string)
@specified_duration(exe)
@specified_duration(proc)
```

returns in the specified duration of the argument. The argument must
refer to a musical event (through its label) or to an action (through
its label, an exe or the name of a proc). The specified duration,
expressed in beats, is:

- for a musical event, the duration of this event
- zero for atomic actions, and for conditionnals and switch/case,
- the sum of the delays that appears in front of the top-level actions of a group,
- the specified duration of the loop body for a loop and a forall,
- the specified duration of the whenever body for a whenever,
- the specified duration of the proc body for a proc. 

Notice that, for a loop, the specified duration is not linked to its
period. Note also that the specified duration of an exe is the specified
duration of the compound action that instantiate into this exe. So, a
specified duration is a _static_ notion, refering to the score, not to
runtime values.


The specified duration can be computed (statically) only if the delays
of the involved actions are given statically (as constant) in beats. If
this is not the case (some delays are specified in seconds and others in
beats, or if delays are expressed by an expression), then
[@specified_duration] returns `:::antescofo -1`.


A common misunderstanding is to look for a “dynamic version” of the
notion of specified duration, for instance to take into account delays
expressed by an expression. Such thing cannot be computed in the general
case because the value taken by a delay expression in the futur cannot
be computed in the present (causality). If the objective is to trigger
some action at the end of a compound action, _cf._ [continuation
operators].


