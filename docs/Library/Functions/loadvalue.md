<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@loadvalue(*file*:string)
```
read a file produced by a call to the function [@savevalue] and returns
the value that was saved. If something goes wrong, an undefined value is
returned. See also function [@loadvar] for an example and the related
functions [@dumpvar] and [@dump].

A call to this function may take a noticeable time depending on the size
of the values to store in the dump file. While this time is usually
negligible, loading a tab of 10000 integers represents a file of size
about 60Kb and takes between 2ms and 3ms. This computational cost may
have a negative impact on the audio processing in heavy cases.  However,
the intended use of and functions is to restore a “preset” at isolated
places like the beginning of the score (see `eval_when_load` clauses) or
between musical sequences, a usage where this cost should have no
impact. Notice that saving a value or variables is done
*asynchronously* and does not disturb the “main” computation. See the
remarks of function [@dump].




<br>

See
{!Library/Functions/dataexchange_functions.list!}



{!Library/Functions/functions.ref!}

