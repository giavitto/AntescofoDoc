<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@slice(t:tab, n:numeric, m:numeric)
```
gives the elements of `t` of indices between `n` included up to `m`
excluded. If `n > m` the element are given in reverse order. So
```antescofo
       @slice(t, @size(t), 0) 
```      
is equivalent to
```antescofo
       @reverse(t)
```          

See also functions [@car], [@cdr], [@drop] and [@take].



See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

