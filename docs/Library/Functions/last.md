<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@last(tab)
@last(string)
```
returns the last element of a tab, or the last character of a string. If
the tab or the string is empty, it returns undef.

Some other functions handle `tab` or `string`as _lisp lists_: [@car], [@cdr], [@concat],
[@cons], [@empty], [@drop], [@take].

See also 
{!Library/Functions/tab_functions.list!}




