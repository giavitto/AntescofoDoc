<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@current_load_directory()
```
returns a string which is the directory part of the path used to load the program.

See also [@pwd], [@directory_read] and [@user_directory].

{!Library/Functions/functions.ref!}

