<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@member(tab, value)
@member(map, value)
@member(string, value)
```
returns true if the second argument is an element of the first. For a
map, the second arguments refers to a value stored in the
dictionary. For string, the value must be a character, _i.e._ a string
of size 1.

See also [@is_prefix], [@is_suffix], [@is_subsequence], [@find] and [@occurs].


See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
