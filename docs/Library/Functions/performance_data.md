<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@performance_data()
```

returns a tab, each element of which documents the occurrence of a
musical event from the start of the program.

Each event is described by a tab listing the following information:


0. _index_ : the index (rank) of the event in the score
   (or 0 if a score event is not meaningfull)
   
1. _date_: the date of the occurence of this event, expressed in seconds from
   the start of the program. This date is the value of the `:::antescofo
   $NOW` variable at the occurence of the event. 
   
2. _beat position_ : The date in beat of this event (i.e., its 'position'
   in the score).

3. _reset_ : true if this event specifies a new tempo value. This is the
   case for the first event following a `:::antescofo BPM`
   specifciation. This is also the case when the tempo is manually set
   (using the `:::antescofo antescofo::tempo` command): an entry not
   related to a score event is inserted in the performance data (the
   index is 0) and reset is true.
   
4. _tempo infered_ : True if the tempo has been infered. A new tempo
   value is not computed at the very start of the score, after a jump,
   after a silence, etc. 
   
5. _tempo_ : the value of the tempo at this event.

6. _$\kappa$_ : The current $\kappa$ value (a measure of the randomness of the
   interpretation), see [coupling strategies]. 
   This parameter is related to the certainty of the computed tempo: the
   greater its value, the greater the expectation that the current tempo
   $T$ will not vary. The value of $\kappa$ is inversely related to the
   width of a unimodal centered on $T$. The larger $\kappa$, the tighter
   the unimodal is on $T$. For $\kappa = 0$, all tempos in
   $[T/2, 2T]$ are equiprobable. Parameter $\kappa$ is used to compute
   the value of the coupling parameters $\eta_{\,p}$ and $\eta_{\,\phi}$
   in the [coupling strategy]. 

7. _$\eta_{\,p}$_ : The parameter $\eta_{\,p}$ used in the tempo
   inference. Its value is a function of $\kappa$ defined by the
   coupling strategy currently in use. It value is in $[0, 1]$. The
   smaller the value, the less the tempo is corrected. Cf. [coupling
   strategies].


7. _$\eta_{\,\phi}$_ : The parameter $\eta_{\,\phi}$ used in the tempo
   inference. Its value is a function of $\kappa$ defined by the
   coupling strategy currently in use.  It value is in $[0, 2]$. The
   smaller the value, the less the phase is corrected. Cf. [coupling
   strategies].

8. _$\phi$_ : this parameter is the mapping of the durantion of the
   previous event on the clock that is coupled with the musician. See
   the Large and Jones inference model in [The dynamics of attending:
   How people track time-varying
   events](https://doi.org/10.1037/0033-295X.106.1.119). The value
   belongs to $[-1/2, +1/2]$. A value of $0$ means _perfext synchrony_,
   i.e. the antescofo clock is fully synchronized with the musician
   'internal' clock. A deviation from 0 measures the approximation of
   the phase and the speed (tempo) of the musician.
   
9. _missed_ : True if the previous event is a missed event.

10. _duration_ : True if the previous event has a definite duration
    (this is not the case for silence, grace note, etc.). 
	
11. This element is reserved for internal use.

12. This element is reserved for internal use.


The information returned by `:::antescofo @performance_data()`can be
used to plot the evolution of the infered tempo. The following fragment
can be used to visualize the tempo and th instantaneous tempo. 

```antescofo
    $data := @performance_data()

    $t := [ $e[1]  | $e in $data]    // date (in seconds) of the events
    $tempo:= [ $e[5]  | $e in $data] // tempo at each event
	
	// duration of each event (except the last) shifted to the left
	// i.e., the first tab element is the duration of the second event
	$dur := [ $data[$i, 2] - $data[$i-1, 2]) | $i in 1 .. $data.size() ]
	
	// instantaneous tempo of each event, except the first
    $itempo := [ 60 * $dur[$i] / ($data[$i, 1] - $data[$i-1, 1]) | $i in 1 .. $data.size() ]
	 
	// date of each event in $itempo
    $tt := $t.drop(1)

     
    print plot3
    _ := @gnuplot("rtTempo", $t, $tempo, "iTempo", $tt, $itempo)
    _ := @gnuplot("kappa", $t, @min(10, $e[6]) | $e in $data])

```

A possible output on the Ravel example in the _Antescofo_ distribution
package is (open the image in another tab to have a full view):

<center>
![ravel default coupling strategy](/Figures/ravel_default.svg){: width="48%"; }
&nbsp;&nbsp;&nbsp;
![ravel default coupling strategy](/Figures/ravel_kappa.svg){: width="48%"; }
</center>

The $\kappa$ plot is clipped at $10$ to show better the small
fluctuations. 

{!Library/Functions/functions.ref!}
