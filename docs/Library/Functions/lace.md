<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@lace(t:tab, n:numeric)
```
builds a new tab whose elements are interlaced sequences of the
elements of the `t` subcollections, up to size `n`.
```antescofo
       @lace([[1, 2, 3], 6, ["foo", "bar"]], 9)
```
returns
```antescofo
       [1, 6, "foo", 2, 6, "bar", 3, 6, "foo"]
```
the first elements is taken in the first element of `t`, the second in
the second element of `t`, _etc._, in a cyclic way, until 9 elements
have been acquired. Scalar elements are extended to tab of constants and
element in a subcollection are taken modulo the size of the
subcollection.


See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

