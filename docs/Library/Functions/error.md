<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@error(val, ...)
```

writes the argument values to the standard output (Max or PD console,
stdout for standalone, etc.) in error mode (on Max, the message appears
in color). String values are printed without there quote.

The returned value is the value of the last argument (or undef). 

See [@print]

{!Library/Functions/functions.ref!}
