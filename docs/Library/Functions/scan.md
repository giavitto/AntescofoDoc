<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@scan(f:function, t:tab)
```
returns the tab of the partial reduction by `f` of the elements of `t`:
```antescofo
       [ t[0], f(t[0],t[1]), f(f(t[0], t[1]),t[2]), ...]
```
For example, the tab of the partial sums of the integers between
`0` (included) and `10` (excluded) is computed by the expression:
```antescofo
       @scan(@+, [$x | $x in (10)])  -> [0,1,3,6,10,15,21,28,36,45]
```

If `t` is empty, the results in `undef`. 

See also [@reduce] and [@map].

See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

