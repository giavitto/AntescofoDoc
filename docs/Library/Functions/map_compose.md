<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@map_compose(a:map, b:map)
```
returns the composition of functions `a` and `c`, that is, a map `c` such
that
```antescofo
       c(x) == b(a(x))
```
See also function [@mapval] to compose a map and a function and [@merge].

See also
{!Library/Functions/map_functions.list!}


{!Library/Functions/functions.ref!}
