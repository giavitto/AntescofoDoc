<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_label_bpm()
@make_label_bpm(start:numeric)
@make_label_bpm(start:numeric, stop:numeric)
```

returns a map associating the event labels to the BPM at this point in
the score.  Events with no label or with a zero duration (grace note) do
not appear in the map.

Called with no arguments, the events considered are all the events in
the score. With `start`, only the events whose position (in beats) is
greater than `start` are considered. If a `stop` is also specified, all
events must have a position between `start` and `stop`.

For example, with this score:

```antescofo
       NOTE D6 1    event1
       NOTE C7 0    event2
       BPM 120
       NOTE D6 2  event3
       NOTE C7 2
       CHORD (D1 A7 Eb7) 4 event5
       BPM 30
       trill (CC6 D7 A7) 2 event6
```

the call `:::antescofo @make_label_bpm()` returns

```antescofo
	MAP{ ("event1", 1.0), ("event3", 0.5), ("event5", 0.5), ("event6", 2.0) }
```

**BEWARE:** Contrary to the functions [@make_bpm_tab] and
[@make_bpm_tab] and despite the function name, the value associated to
the key in the returned map is not in BPM but is **second per beat**,
_i.e._ $\frac{60}{\text{bpm}}$. _This is expected to change._


See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
