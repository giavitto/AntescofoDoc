<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@reshape(t:tab, s:tab)
```
builds a tab of shape `s` with the element of tab `t`. These elements are
taken circularly one after the other. For instance
```antescofo
       @reshape([1, 2, 3, 4, 5, 6], [3, 2]) -> [ [1, 2], [3, 4], [5, 6] ]
```
the result has shape 3×2 and the elements are the elmeents taken in
`[1, 2, 3, 4, 5, 6]`.



See also 
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}
