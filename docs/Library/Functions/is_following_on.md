<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_following_on()
```
this predicate returns `:::antescofo true` if the listening module is
active, meaning that the system is processing an input audio stream to
detect musical events specified in the score. The listening module is
active in normal operation mode (launched by `::antescofo
antescofo::start`). The listening module is inhibited in play mode
 `::antescofo antescofo::play`) or in fastforward mode.


See also
[@is_fastforward],
[@is_following_on],
[@is_paused],
[@is_playing_on],
[@is_running],
and [@is_stopped].


{!Library/Functions/functions.ref!}

