<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rand(d:numeric)
```
[@rand] is an impure function returning a random number between 0 and
`:::antescofo d` (included).

[@rand] is similar to [@random] but rely on a different algorithm to
generate the random numbers.


See also
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}
