<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@savevalue(s:string, value)
```
argument `s` is interpreted as the path of a file where the value of the
second argument is saved. The format of the file is textual and
corresponds to the _Antescofo_ grammar. This value can then be read
using the function [@loadvalue].

<br>

See also functions [@dump], [@dumpvar] and [@loadvar].

See also
{!Library/Functions/dataexchange_functions.list!}



{!Library/Functions/functions.ref!}

