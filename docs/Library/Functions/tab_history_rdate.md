<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@tab_history_rdate(variable)
```
This is a special form. It returns a tab of the date in _relative time_
of the updates of the variable in argument.

See also [@map_history], [@tab_history] and [@tab_history_date].


{!Library/Functions/functions.ref!}


(*variable*) : This is a special form. It returns a tab See.
sect. \[sec:mapvariable\] page  and the functions.
