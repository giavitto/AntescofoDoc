<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@binary_search(t:tab, v:value)
```
returns the index of the first element that is equal to `v` in the
sorted tab `t`. The integer `-1` is returned if there is no such
element.

This function is faster than [@find] because the elements of `t` are
known to be increasing.


See also [@find], [@count], [@member] and [@occurs].

Example:
```antescofo
       @fun_def p($i, $v) { return ($i > 1) && ($v % 2 == 0) }
       @find([1, 2, 3, 4], @p)  ; returns 0
```
[@find] returns the first index greather than one such that the
corresponding element is a multiple of two.

```antescofo
       @fun_def p($i, $v) { return ($i > 1) && ($v >= "b") }
       @find("abcdefg", @p)  ; returns 2
```
the index `2` is returned because the `"c"` is the first character
greather or equal to `"b"` such that its position is strictly greather
than one (character numbering starts at zero).





