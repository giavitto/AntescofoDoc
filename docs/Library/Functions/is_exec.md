<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_exec(value)
```

the predicate returns true if the argument represents an *exec*, that
is, the instance of a compound action (_e.g._, the result of a
process call or an object instantiation).

The predicate returns true even if the compound action has finished its
computation. However, the *exec* itself used in a boolean condition
evaluates to true if the compound action is still running and false
elsewhere.

See also
{!Library/Functions/predicates_functions.list!}


{!Library/Functions/functions.ref!}
