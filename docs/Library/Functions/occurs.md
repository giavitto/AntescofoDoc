<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@occurs(tab, value)
@occurs(map, value)
@occurs(string, value)
```
returns the first index or the first key
whose value equals the second argument. For example
```antescofo
       @occurs(["a", "b", "c", "a", "b"], "b") -> 1
       @occurs("xyz", "z")  -> 2
       @occurs(map{ ("zero", 0), ("null", 0), ("void", 0) }, 0)  -> "null"
```          
In the last example, the answer `"null"` is returned because `{"null" < "void" < "zero"`.

See also [@count], [@find], [@is_prefix], [@is_subsequence],
[@is_suffix] and [@member].



See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}

