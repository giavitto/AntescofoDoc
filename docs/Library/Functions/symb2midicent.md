<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@symb2midicent(string)
@symb2midicent(symb)
```
convert a string expressing a pitch in [Scientific pitch
notation](https://en.wikipedia.org/wiki/Scientific_pitch_notation)
into its equivalent midicent encoding. The _scientific pitch notation_ is the notation
used in the specification of [pitch in events]. 
For example, `:::antescofo "A4"` is converted into '6900'. The function
also accept pitch specifications starting with a minus sign '-', like those used in
Antescofo musical event specification to denote the continuation of a
previous note. The minus sign is simply ignored in the translation. 

**NOTE**: the microtonal alteration, as in `:::antescofo A#4+50`, _are
supported_. 

See [midi tuning](https://en.wikipedia.org/wiki/MIDI_Tuning_Standard)
and functions [@midicent2hz], [@hz2midi], [@hz2symb] and
[@hz2symb].



See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


