<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@simplify_lang_v(nim, tol:numeric, n:numeric)
```
build a new simpler nim by aggregating breakpoints using a _Lang
polyline simplification algorithm_ with tolerance `tol` and sequence
size `n`.

In the simplification process, a breakpoint is assimilated to a _d_
dimensional point for a nim with dimension _d_ (in other words, the _x_
part of the breakpoint is not taken into account). This is the case for
all nim simplification functions that end with a *_t*.

The Lang simplification algorithm examines a sequence of such points of
a fixed length _n_ (_i.e._, _n_ successive breakpoints). The first and
last points of that sequence specify a segment. This segment is used to
calculate the perpendicular distance to each intermediate
breakpoints. If any calculated distance is larger than the specified
tolerance, the search region will be shrunk by excluding its last
point. This process will continue until all calculated distances fall
below the specified tolerance, or when there are no more intermediate
breakpoints. All intermediate brakpoints are removed and a new search
sequence is defined starting at the last point from old search
region. This process is illustrated below.


![the Lang polyline simplification algorithm on a nim](/Figures/simplify_Lang.png)


See also [@align_breakpoints], [@sample] and the nim simplification
functions: [@simplify_radial_distance_t], [@simplify_radial_distance_v],
[@simplify_lang_v], [@filter_median_t], [@filter_min_t],
[@filter_max_t], [@window_filter_t]




See also {!Library/Functions/nim_functions.list!}

{!Library/Functions/functions.ref!}

