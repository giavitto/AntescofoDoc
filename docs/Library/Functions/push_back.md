<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@push_back(tab, value)
@push_back(nim:NIM, d:numeric, y1:numeric)
@push_back(nim:NIM, d:numeric, y1:numeric, it:string)
@push_back(nim:NIM, y0:numeric, d:numeric, y1:numeric)
@push_back(nim:NIM, y0:numeric, d:numeric, y1:numeric, it:string)
```
[@push_back] is an impure overloaded function. See also [@push_front].


-------------------------------------------------------------
```antescofo
@push_back(tab, value)
```
add the second argument at the end of the first argument. The first
argument, modified by side-effect, is the returned value.

Usually, [@push_front] is sightly more efficient thant [@push_back].


-------------------------------------------------------------
```antescofo
@push_back(nim:NIM, d:numeric, y1:numeric, it:string)
```
add a breakpoint as the last breakpoint of `nim`.
The first argument, modified by side-effect, is the nim, which is
also the returned value.

The argument `d` specifies the length of the interpolation since the
previous breakpoint, `y1` is the final value attained at the end of the
breakpoint, and `it` is the interpolation type. The interpolation type
can be omitted: in this case, the interpolation is linear. The initial
value `y0` of the breakpoint is the `y1` value of the previous
breakpoint.


-------------------------------------------------------------
```antescofo
@push_back(nim:NIM, y0:numeric, d:numeric, y1:numeric, it:string)
```
similar to the previous function but `y0` is explicitly given, making
possible to specify discontinuous `nim`.


------------------------------------------------------------------

See also
{!Library/Functions/tab_functions.list!}

See also
{!Library/Functions/nim_functions.list!}

{!Library/Functions/functions.ref!}

