<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@erfc(x:numeric) ; listable
```

This function calculates the complementary error function of x; that is
`:::antescofo @erfc(x)` computes the difference of the error function
`:::antescofo @erf(x)` from $1.0$.  This is useful, since for large $x$
use of [@erfc] avoids loss of precision due to cancellation.


{!Library/Functions/math_functions.list!}
{!Library/Functions/functions.ref!}


