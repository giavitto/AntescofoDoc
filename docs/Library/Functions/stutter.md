<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@stutter(t:tab, n:numeric)
```
returns a new tab whose elements are repeated `n` times. 
```antescofo
       @stutter([1, 2, 3, 4, 5, 6], 2) 
       -> [ 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6 ]
```          


See also
{!Library/Functions/tab_functions.list!}

{!Library/Functions/functions.ref!}

