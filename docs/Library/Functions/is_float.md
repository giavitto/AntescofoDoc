<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@is_float(value)
```

the predicate returns true if its argument is a floating point value (a
decimal number). Floating point values correspond to _IEEE-754 double
precision values_ (the C type `double`). 



See also
{!Library/Functions/predicates_functions.list!}

{!Library/Functions/functions.ref!}
