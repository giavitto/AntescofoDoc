<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@midi_getCommand(tab)
```
The argument is a tab representing a midi message, see function
[@midi_read]. The [@midi_getCommand] extracts from this tab the command
of the message (a number between 0x80 and 0x8f), or returns -1 if the
message does not corresponds to a command.

See also
{!Library/Functions/midi_functions.list!}


{!Library/Functions/functions.ref!}


