<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@rnd_uniform_float(a:float, b:float)
```
returns a generator giving float values according to a *uniform
distribution* P:

        P(x | a, b) = 1/(b - a),   a  ≤ x < b



See [@rnd_bernoulli] for a description of _random generators_.

See also 
{!Library/Functions/random_functions.list!}

{!Library/Functions/functions.ref!}
