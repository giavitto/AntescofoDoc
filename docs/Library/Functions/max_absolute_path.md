<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@max_absolute_path(file:string)
```

returns the absolute path of _file_ in the directories looked up by M	x. If the file does not exist, `:::antescofo <undef>` is returned. 

See also [@pwd], [@current_load_directory], [@directory_read], [@user_directory] and [directory_read]



