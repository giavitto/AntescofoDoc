<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@empty(tab)
@empty(map)
```
returns true for an empty tab or an empty dictionary and false
elsewhere.

See also 
{!Library/Functions/tab_functions.list!}

See also 
{!Library/Functions/map_functions.list!}


{!Library/Functions/functions.ref!}

