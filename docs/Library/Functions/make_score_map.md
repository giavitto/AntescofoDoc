<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@make_score_map()
@make_score_map(start:numeric)
@make_score_map(start:numeric, stop:numeric)
```
returns a map where the keys are the rank _i_ of the musical events and
the value, the _position in beat_ of the _i_ th event. Called with no
arguments, the events considered are all the events in the score. With
`start`, only the events whose position in beats is greater than `start`
are considered. If a `stop` is specified, all events must have a
position between `start and `stop`.



See also
{!Library/Functions/score_functions.list!}


{!Library/Functions/functions.ref!}
