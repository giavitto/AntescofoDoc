<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@bit_shiftl(int, int)
@bit_shiftr(int, int)
@bit_or(int, int)
@bit_and(int, int)
```

Function `:::antescofo @bit_shiftl` sees its first argument (an integer)
as a series of 64 bits. The second argument is the number of positions
these bits will be shifted to the left. The last digits on the right are
filled with a zero. Please note that this is _not_ circular shift.

Function `:::antescofo @bit_shiftr` is similar but the shift is done to the right.

Function `:::antescofo @bit_and` and `:::antescofo @bit_or` see their
arguments as a series of 64 bits and they apply the corresponding
logical operation, bit-wise.

For example

```antescofo
   @bit_shiftl(123, 1) == 246
   @bit_shiftrl(256, 2) == 64
```

shifting an integer to the left by $n$ positions corresponds to a
multiplication by $2^n$ and accordingly, shifting to the rigth divides
by $2^n$.


Integer $1$ is represented by the sequence "...001" and integer $2$ by
"...010". Sot the results of the bit wise operations between $1$ and $2$
are:

```antescofo
   @bit_or(1, 2) == 3   
   @bit_and(1, 2) == 0
```



```antescofo

```



<br>

See also
{!Library/Functions/math_functions.list!}


{!Library/Functions/functions.ref!}


