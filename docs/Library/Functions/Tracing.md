<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@Tracing()
@Tracing(x:function, ...)
@Tracing(x:string, ...)
@Tracing(x:tab, ...)
```
Calling [@Tracing] starts to trace the calls to all functions specified
by the arguments. Functions to trace are given by their name (as a
string) or by their value (through their identifier) or by a tab
containing such values.

With no argument, all user-defined function are traced. 


See also [@UnTracing] and
{!Library/Functions/system_functions.list!}


{!Library/Functions/functions.ref!}
