<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
@min_key(nim)
```
returns the coordinate `x_0` of the initial breakpoint of the nim
argument. If the nim is vectorial, `x_0` is a tab.

See also [@max_key], [@min_val] and [@max_val].

 [@min_key], [@min_val] and [@max_val].

<br>
<img  src="/Figures/nim_min_max.png" width=80% center>
<p>


See also
{!Library/Functions/nim_functions.list!}

{!Library/Functions/functions.ref!}

