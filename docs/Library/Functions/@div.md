<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}


```antescofo
@/(numeric, numeric) ; listable
```
prefix form of the infix arithmetic operator `/`.
Coercions between numeric apply when needed.



See also [@+], [@-], [@*], [@%]


