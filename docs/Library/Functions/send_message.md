<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

```antescofo
    @send_message(receiver, arg1, arg2, ...)
```

This function takes at least two arguments and send a Max or PD
message. The receiver of the message is the first argument. The rest of
the arguments are used as the arguments of the message sent. 

<br>
See also [callback messages] and
{!Library/Functions/system_functions.list!}



