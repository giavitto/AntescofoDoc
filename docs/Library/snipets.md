<!--   -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}



# The rest of the story is yet to be written... _by you_

<br>

We want to complete the _Antescofo_ documentation with your
contributions.

Please, send your comments, typos, bugs reports, hints and suggestions
on the Antescofo
[ForumUser](https://discussion.forum.ircam.fr/c/antescofo)
web pages. It will help us to improve the documentation and the
_Antescofo_ system.


The documentation will evolve to include snippets of code, tutorials,
and howtos. Do not hesitate to send _your examples_.  Ideally, your
contribution must include a working patch with the needed audio files
and a description of your example in the forme of a `README.md` file
written in markdown format. The goal is to have an autonomous demo with
all the explanations needed to run the demo by the _Antescofo_ users.



## How-To

You will find in the following pages some useful examples that are given
to illustrate the interfacing of the system or its use for a specific
need. The code given here does not constitute a final and definitive
solution but illustrate a possible approach in a common problem:

- [Interfacing with Open Stage
  Control](/Library/HowTo/open_stage_control) interface _Antescofo_ with
  a generic, libre and modular OSC / MIDI controller in the style of
  touchOSC, Lemur or Mira. 
  
- [Managing Sample Playback in Presence of Transport
  Commands](/Library/HowTo/SampleManagement/samplemanagement/)
  illustrate how to handle the control of statefull external
  processes to be compatible with the transport command of _Antescofo_.

- [Controlling a PDF Reader](/Library/HowTo/pdfviewer)

- [Algorithmic Midi Follower](/Library/HowTo/midi_follower)

- [A simple spatial trajectory library](/Library/HowTo/spatialisation)
  used to control the trajectory of audio sources using parametric
  functions and an external spatial engine like the SPAT or SPATGris. 

- [Ploting the local position and local tempo of a
  group](/Library/HowTo/PlotLocalTempo/plotlocaltempo/)
  shows how to visualize the local timing in a group. 

<br>

... And we are awaiting your own libraries!



<br>

## External demos, tutorials and examples

The community has produced some examples, hands-on, and videos:

- [Tutorial videos](/Library/HowTo/video_tutorial_nb) 


<br>
<br>

![The hierophant was petrifying](../Figures/Walt_McDougall-The_hierophant_was_petrifying-background.jpg)
