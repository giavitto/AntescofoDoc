<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}



<center>
![Final image: experimental music](Figures/its_a_complex_world.gif){: width="55%";}
</center>


&nbsp;<br>
&nbsp;<br>

# Release Notes

&nbsp;<br>

These web pages documents the version 1.1 of the _Antescofo_
system. Some features may be not present if you use old version of the
system. The list below is an attempts to summarize the changes between
the versions.

It is important to note that:

- **versions upto 0.92** (march 2016) rely on the _old listening
  module_. <br> This listening module is doing a good job for midi input
  and handle silence well. However, the old listening module is
  sensitive wrt play mode and has difficulties with resonant instruments
  (like piano with pedal).

- **version starting from september 2016** (after 0.93 and for the
  official distributions, from 1.0) rely on the _new listening
  module_. <br> This improved listening machine is more robust with
  resonant instrument (including piano with pedal), it handles some play
  mode and has an improved handling of timing information. However, it
  may be sensitive to silence in some configuration and the handling of
  midi is less efficient (if this a concern, midi can be handled at the
  level of the _Antescofo_ program itself, see the [how-to
  section](/Library/snipets)).

- **version upto 1.0 included** are distributed as _fat object_ on OS X,
  meaning that they can be used with Max6 and Max7 runing in 32 bits
  mode, as well as 64 bits mode. <br> Starting with version 1.1, these
  objects are avalable only for 64 bits mode (Max8 does not support 32
  bits objects and Apple does not release any SDK compatible with 32
  bits starting with OS X 10.12. If you need to use a 32 bits compatible
  object, you have to use a version below 1.0.


Old versions of the [_Antescofo_ objects are archived
here](https://forum.ircam.fr/projects/releases/antescofo) for the
official distributions. [Older intermediate distributions (beta
versions)](https://forge.ircam.fr/p/antescofo/downloads/) are also
available. And here is [the antescofo online
converter](https://antescofo-converter.ircam.fr/).

&nbsp;<br>
&nbsp;<br>

## Version 1.1 (to be distributed through the Ircam Forum)

#### Features

- converter moved to **[https://antescofo-converter.ircam.fr/](https://antescofo-converter.ircam.fr/)**

- function/process/method call with named parameters

- function/process/method call with default argument value (optional
  arguments)

- arbitrary arguments on musical events 

- new commands `top_level_are_tight`, `antescofo_is_in_rehearsal_mode`,
  `antescofo::trace_mode_change`

- `@persistent` qualifier on global variables and osc send and receive commands

- methods in object can have their own attribute (used to have an abort-handler
  on object's methods)

- Lambda-expression

- remanent abort: abort handler of defunct groups are activated if a son
  of the group is still alive. This contrast with the previous behavior
  where abort handler are not activated on defunct groups. Attribute
  `@REC_if_alive` can be used on an abort command to achieve the
  previous behavior.


- OSC : new [osc_client]  command

- OSC : [oscrcv] may trigger a function or a process instead of setting a variable

- symbol evaluation: see [symbols]

- _experimental_:
    - experimental compilation and dynamic linking of antescofo user's
      functions. Only a subset of general expressions is handled.
    - dynamic linking of external objects
    - [differential curve]



#### Library
- regular expression:  [@r_compile], [@r_match], [@r_search], [@r_findall]

- extension of most tab functions to strings (viewed as tab of
  characters): [@car], [@last], [@cdr], [@drop], [@take], [@slice],
  [@scramble], [@stutter], [@rotate], [@sputter], [@permute], [@remove],
  [@remove_duplicate], [@replace], [@sort]

- new or extended string functions: [@explode], [@to_num],
  [@char_is_alnum], [@char_is_alpha], [@char_is_ascii],
  [@char_is_blank], [@char_is_cntrl], [@char_is_digit],
  [@char_is_graph], [@char_is_lower], [@char_is_print],
  [@char_is_punct], [@char_is_space], [@char_is_upper],
  [@char_is_xdigit]


- extension of [@min] and [@max] to tab and to an arbitrary number of elements.

- new system functions: [@time], [@max_absolute_path],
  [@configuration_compiler_version], [@configuration_arch],
  [@configuration_target_architecture], [@configuration_audiosample],
  [@configuration_faust], [@configuration_odeint]

- new function on tab: [@succession]

- fct: [@print], [@error], [@info]

- bit manipulation: [@bit_shiftl], [@bit_shiftr], [@bit_or], [@bit_and]

- knn functions: [@knn_create], [@knn_rebuild], [@knn_delete], [@knn_search], [@knn_rsearch],
     [@knn_scan], [@knn_rscan], [@knn_combine], [@knn_rcombine

- [@read_file]

- [@max_patch_path], [@pwd], [@user_directory], [@current_load_directory], [directory_read],
  [@max_absolute_path]

- [@symb2midicent]



#### Corrected bugs

- non uniform behavior between startfrom 0 and start.

- variable access in abort handler

- better management of time in delays

- better handling of negative periods in loops or negative grains in curve

- various bugs


&nbsp;<br>
&nbsp;<br>


## Version 1.0 (2018-03-06)

#### Features

- new listening machine

- improved time model (temporal scope], generalization opf the use of
  internal NIM to represents beat(time) and time(beat) at run-time:
     - arbitrary symbolic tempo curves (i.e. accelerando)
     - fully dynamic specifications
     - tempo limiters


- pitch alteration `NOTE B##4`

- improved documentation

- import of MusicXML score from web page [antescofo
  converter](http://forumnet-dev.ircam.fr/antescofo-converter/) at url
  `http://forumnet-dev.ircam.fr/antescofo-converter`

- new scheduler: reduced gig, improved precision over time, lesser footprint, deterministic

- extended OSC capabilities

- latency compensation: @latency

- curve as nim player (including vectorial non-aligned nim)

- spline type of interpolation in NIM and Curve

- qualitative specification of NIM

- actors (objects)

- new transport commands

- computed score jump

#### Functions

- extended syntax for function definitions (local variable, control structure, etc.)

- the standard library offers more than 250 predefined functions
    - extensive set of predefined NIM operations
    - reading & writing JSON file and MIDI file
    - reading XML file 
    - Antescofo score -> Bach representation




&nbsp;<br>
&nbsp;<br>


## Version 0.92 (2016-03-07)

#### Features

- old listening machine





&nbsp;<br>
&nbsp;<br>
&nbsp;<br>
 



