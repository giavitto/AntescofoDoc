<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}

## Interacting with MAX/PureData


When embedded in MAX, the systems appears as an `antescofo~` object that
can be used in a patch. This object presents a fixed interface through
its inlets and outlets[^1].

### Inlets

The main inlet is dedicated to the audio input. Antescofo’s default
observation mode is “audio” and based on pitch and can handle multiple
pitch scores (and audio). But it is also capable of handling other
inputs, such as control messages and user-defined audio features. To
tell Antescofo what to follow, you need to define the type of input
during object instantiation, after the operator `@inlets`. The following
hardcoded input types are recognized:

-  `KL` is the (default) audio observation module based on (multiple) pitch.

-  `HZ` refers to raw pitch input as control messages in the inlet (e.g.
    using fiddle\~ or yin\~ objects).

-  `MIDI` denotes to midi inputs.

You can also define your own inlets: by putting any other name after the
`@inlets` operator you are telling *Antescofo* that this inlet is
accepting a LIST. By naming this inlet later in your score you can
assign *Antescofo* to use the right inlet, using the `@inlet` to switch
the input stream.

### Outlets

By default, there are three Antescofo’s outlets:

-   `Main outlet` (for note index and messages),

-   `tempo` (BPM / Float),

-   `score label` (symbol) plus an additional BANG sent each time a new score
    is loaded.

Additional (predefined) outlets can be activated by naming them after
the `@outlets` operator. The following codenames are recognized :

- `ANTEIOI` Anticipated IOI duration in ms and in runtime relative to
    detected tempo

-   `BEATNUM` Cumulative score position in beats

-   `CERTAINTY` Antescofo’s live certainty during detections [0,1]

-   `ENDBANG` Bang when the last (non-silence) event in the score is detected

-   `MIDIOUT`

-   `MISSED`

-   `NOTENUM` MIDI pitch number or sequenced list for trills/chords

-   `SCORETEMPO` Current tempo in the original score (BPM)

-   `TDIST`

-   `TRACE`

-   `VELOCITY`



### Predefined Messages


The Antescofo object accepts predefined message sent to
antescofo-mess1. These messages correspond to the internal commands
described in section [internal command].





### The `setvar` command
See section [setvar] in the Reference Manual.


### Sending and Receiving OSC messages
See section [OSC message] in the Reference Manual. 





<!-- FOOTNOTE -->

[^1]: The tutorial patches in the Antescofo bundle, are good
illustrations of the points sketched here.