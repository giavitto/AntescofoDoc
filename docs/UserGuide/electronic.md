<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->


{!references.ref!}


# Beyond score following...

Most people think *Antescofo* is exclusively used for mixed music and
score following. That's partly true, but since the language exists and
has been developed, *Antescofo* is also a great tool for electronic
music.

## Antescofo as a sequencer

Speaking about time is very easy in *Antescofo*. An *Antescofo* score
can be seen like a programmable (textual) sequencer, where all the
actions are organised in time (if you click on the `Play` button,
*Antescofo* becomes a sequencer) that you specify in the score.

You can write an electronic score with the beat notation and decide
after to change the tempo. In this case you don't have to rewrite all
the durations. *Antescofo* does the translation. This allows you to
change the tempo (with `BPM`) in any place in your score.

As we have shown before, you can also work with different times in a
same moment with the `@tempo` attribute and write complex polyrythms.

With `macros` you can greatly simplify your score to focus on the
musical questions instead of the technical realization.


## Hierarchical scores

With the system of `:::antescofo GROUP`, you can write a hierarchical
score where the different voices are denoted by different groups. You
can also create `tracks` (see `:::antescofo @track_def`) to filter any
kind of musical object or focus on a particular voice.


## Open scores and installations

If you see the `process` and `whenever` constructions, you will see that
these features can be easily used for installations or for open
scores. You can use Antescofo without a “classical score”, without
`:::antescofo NOTES`, `:::antescofo CHORD`... It means that you can
program a reactive environment that “hears” sensors, audio
descriptors...

If you compose open scores, you can take a look to the `:::antescofo
@jump` keyword that is followed by a comma separated list of simple
identifiers referring to the label of an event in the score. This
attribute specifies that this event can be followed by several
continuations: the next event in the score, as well as the events listed
by the `:::antescofo @jump`.  So you can compose a non-linear score with
choice points leaved to the performer. You evn can modify during the
execution the list of allowed jumps, achieving a kind of “multi-graph”
score.

![Open score in "Tesla ou l'effet d'étrangeté", Sasha J. Blondeau](../Figures/JBLO_open.jpg)



## Beyond Max and Pure Data...

We have shown many uses of *Antescofo* in Max. However, with OSC, you
can use *Antescofo* as controller of any kind of software that supports
OSC. For example, SuperCollider and CSound are great *Antescofo* fellow
players ! In the two next examples, Max is a kind of "pipe" between
*Antescofo*, CSound or SuperCollider, IRCAM's Spat and audio
descriptors. But Antescofo sends also many messages via OSC without Max.


### A SuperCollider example

The figure below shows an environment created by José-Miguel Fernandez
for his pieces where you can see 3 different softwares (in the
background you can see the deprecated [AscoGraph] interface). At the
left front you can see Max using *Antescofo* and descriptors (who are
sended to `whenever` and `process`). At the right you can see a
SuperCollider interface where processes are generated “on the fly” and
displayed on the screen. In this example, Antescofo manages all the
control parts and sends the parameters needed for synthesis and effects
to SuperCollider. Fernandez uses many chaotic funcions included in
*Antescofo* and several of its data structures (_e.g._, `map` and
`tab`).

![Utilisation of Antescofo with SuperCollider, works by José-Miguel Fernandez](../Figures/JMF-example.png)

### A CSound example

In the next example, *Antescofo* is used to send a score to CSound and to generate table for CSound. Here, *Antescofo* can replace the ".sco" file for CSound or the "score part" of the ".csd" in CSound. All the CSound synthesis is generated in real time and controled by the object `antescofo~`. 

The spatialization is controled by *Antescofo* too with many `curves` and `process` that generate complex trajectory.



![Utilisation of Antescofo with CSound, works by Sasha Blondeau](../Figures/CSoundAntes.png)

### OSC

Many people have been using Antescofo message passing strategies as defined above to interact with processes living outside MAX/PD (such as CSound, SuperCollider, etc.). To make their life easier, Antescofo comes with a built-in OSC host. The OSC protocol3 can be used to interact with external processes using the UDP protocol. It can also be used to make two Antescofo objects interact within the same patch. Unlike MAX or PD messages, OSC messages can be sent and received at the level of the Antescofo program. The embedding of OSC in Antescofo is done through 4 primitives.

	oscsend name host : port msg_prefix

	oscrecv name port msg_prefix $v1 . . . $vn

see the [OSC message] chapter for more details.






<br>

## Be adventurous !

![DonQ](../Figures/don-quichotte.jpg)


<style> 
blockquote {
  background: #f9f9f9;
  border-left: 5px solid #ccc;
  margin: 3.5em 20px;
  padding: 0.5em 10px;
  quotes: "\201C""\201D""\2018""\2019";
}
</style>


<blockquote><i>
	In any case, the composer, during his exploratory journeys, is seen as both Columbus and Don Quixote - he lands on an unknown territory and/or falls off his horse and lands in an inglorious manner, and each time in any case where he did not think he would end up; it is only in this way that he experiences himself, that he transforms himself, that he comes to himself.</i>
	<br>
	<div align="right" style="margin-left: 30%;">
		LACHENMANN, Writings and interviews.
	</div>
</blockquote>

We have provided a brief overview of Antescofo, but a WORLD remains to
be discovered... and experimented with! In the [Reference Manual], you can
more precisely research the previously addressed issues. You can decide
to only take the examples given at the end of this documentation
(snippets, How-to...) and copy them in your own score... or you can
decide to roll up your sleeves and understand all the intricacies and
secrets of Antescofo language to try to be an “aware Antescofo
electronic composer!”  ;-)

If you continue the adventure, you will see how to create a complex process or how to create a reactive environment where Antescofo can hear many things and react as you had composed! You will see how to manipulate time in Antescofo and how to synchronize your machine with a musician in real, musical way! 

So... Try and fail, retry and be victorious, and don't forget that your feedback is important to us. Please, send your comments,
questions, bug reports, use cases, hints, tips *&* wishes using the
IRCAM Forum discussion group at:

<https://discussion.forum.ircam.fr/c/antescofo>
