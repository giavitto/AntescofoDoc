<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{! references.ref !}
{! Library/Functions/functions.ref !}

# _Antescofo_ Workflow

In this chapter, we will see how to work with *Antescofo*, from the
edition of the score to the interactions with Max, PureData or other
softwares through OSC.



## Editing the Score

The first step in the development of an Antescofo Application is
probably the authoring of the _augmented score_. An augmented score is a
text program and can be edited with any text editor (like *Sublime*,
*TextWrangler* or *Emacs* but _not_ with a text processor like
_Office/Word_).


Older versions of _Antescofo_ comes with a companion application:
[Ascograph].  [Ascograph] is a graphical tool that can be used to edit
and then to control a running instance of *Antescofo* through OSC
messages. This tools is now deprecated because it cannot handle the full
range of the language. [Ascograph] can still be used on very simple
Antescofo score but is usage is no longuer supported. Most of the
command available in [Ascograph] to drive a running _Antescofo_ instance
in Max are also available, e.g. from the _Sublime_ editor, using the
[dedicated
mode](https://discussion.forum.ircam.fr/t/antescofo-v0-92-and-sublimte-text-editor/2021). Take
also a look at function [@bach_score] which makes possible to export an
Antescofo score to
[Bach](https://cycling74.com/tools/bach-automated-composers-helper) in
order to visualize the piano roll of the musical events and to follow in
real-time the progression in the score. See the
[bachscograph](https://discussion.forum.ircam.fr/t/about-the-ascograph-the-antescofo-graphical-editor-category/20387/2)
patch proposed by Serge Lemouton.




### Importing Scores to *Antescofo* (import of Midi files and of MusicXML files)

Very often, the musical events to follow have already been specified
through a score editor (**Finale**, **Sibelius** ...), or exist as a
MIDI score.

It is possible to automatically import MIDI or MusicXML scores to
*Antescofo* format to spare the burden of rewriting the followed
part. This feature is available by drag and drop a MIDI or MusicXML file
into a dedicated web page:
[https://antescofo-converter.ircam.fr/](https://antescofo-converter.ircam.fr/)
A command line version of this converter exists but the online converter
must be the approach must be preferred approach.

For multiple instrument score, care should be taken to extract required
part in a separate MIDI or MusicXML file. The result is an Antescofo
score that can then be modified at will.

The conversion can be also produced by a standalone application
(termoinal mode) which offers additional options.

Users employing these features should pay attention to the following
points:


### Importing MIDI scores to *Antescofo*

The major problem with MIDI format is the absence of *grace notes*,
*trills*, and *glissandi*. Such events will be shown as a series of raw
event elements in the score.

Another major issue with MIDI import is the fact that in most cases,
timing of note-offs are not decoded correctly (based on where the MIDI
is coming from). Bad offset timing creates additional event with linked
pitches (negative notes) with short durations. To avoid this, we
recommend users to *quantize* their MIDI files using available software.
The Antescofo importer does not quantize durations.



### Importing MusicXML scores to *Antescofo* 

MusicXML is now the standard inter-exchange score format file between
various score editing and visualisation software. It includes high-level
vocabulary for events such as *trills*, *grace notes* and *glissandi*
which can be converted to equivalent events. However, decoding and
encoding MusicXML is not necessarily unique for the same score created
by different software!

The Antescofo MusicXML import is optimized for MusicXML exports from
*FINALE* software. Before converting MusicXML score to Antescofo, users
are invited to take into account the following points and to correct their
score accordingly, especially for complex contemporary music scores:

- Avoid using *Layers*: Merge all voices into one staff/voice before
    converting to MusicXML and dragging to the web converter. XML parsers
    sometimes generate errors and suppress some events when conflicts
    are detected between layers.

-   Avoid using *Graphical Elements* in score editors. For example,
    *Trills* can only be translated to if they are non-graphical.

-   If possible, avoid non-traditional note-heads in your editor to
    assure correct parsing for events.

-   Avoid *Hidden* elements in your scores (used mostly to create
    beautiful layouts) as they can lead to unwanted results
    during conversion. Verify that durations in your score correspond to
    what you see and that they are not defined as hidden in the score.

-   Verify your *Trill* elements after conversion as with some editors
    they can vary.

This feature is still experimental and we encourage users encountering
problems to contact us through the Online User Group.


![Antescofo Importer](../Figures/importer.png)




## Deprecated : Using _AscoGraph_


The information given [here](/UserGuide/deprecated_ascograph) is given only for those that are still able
to use ascograph on simple Antescofo score. [Ascograph] is no longer
supported.



## Using your own editor

An _Antescofo_ score is a simple text file and can be edited by any text
editor like Sublime, Emacs, Atom, etc.

Max users can also use the Antescofo Max Object <em>Autowatch</em>
attribute. This means that if the loaded score is modified elsewhere,
it'll be automatically reloaded in the Max object. Obviously, you'd want
to turn this attribute off during live concerts! Autowatch is currently
not available for PureData objects.

For the people that prefer use their own beloved text editor, we have
created some _syntax highlightings_ for **Sublime Text**, **TextWrangler**
and... **emacs** ! You have just to
[download them in our Forum](https://discussion.forum.ircam.fr/t/antescofo-useful-tutorials-on-this-forum). The [plugin]((https://discussion.forum.ircam.fr/t/antescofo-v0-92-and-sublimte-text-editor/2021) for **Sublime** offers also some keyboard shortcuts to control a runing _Antescofo_ instance (through OSC messages). 





## Styling your score

Antescofo scores can be pretty big and it is important to grasp the
elements at first glance. A syntax highlighter as third party packages
exists for Sublime, Atom and Emacs [look at our
Forum](https://discussion.forum.ircam.fr/t/antescofo-useful-tutorials-on-this-forum).

Antescofo scores can also be embedded in markdown (as illustrated by
this documentation) using the *Pygment* highlighter or in latex
using the *lstlisting* with a dedicated style.

The automatic indentation provided by editors like Sublime for Antescofo
score is basic. It is always possible to send a `printfwd` message to
the antescofo object. This command will write a file with an
automatically indented version of the score together with various
additional information (depending on the current verbosity). This file
can be loaded again in Antescofo. Notice however that macros are
expanded in this score and that the initial comments are not reported.

