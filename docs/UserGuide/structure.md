<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

# Structure of an Antescofo Score

## An Antescofo score is a text file

Antescofo is used for computer arts involving live interaction and
synchronisation between human and computerised actions. An Antescofo
score is a text file, a program in a dedicated language that fulfills
two purposes: it is used to specify real-time score following (detecting
the position and tempo of live musicians in a given musical score) and
to specify electronics action andtheir real-time triggering as written
by the artists.

An Antescofo score can be edited by any text editor. The users can find
syntax highlights for various text editor (sublime, atom, emacs) on our Antescofo
[ForumUser](https://discussion.forum.ircam.fr/t/antescofo-useful-tutorials-on-this-forum/1826). With
Max, the score file is monitored (file watch) so any changes through an
editor is reflected in the score loaded.
The _Sublime_ editor is especially recommended as the [syntax colouring](https://discussion.forum.ircam.fr/t/antescofo-v0-92-and-sublimte-text-editor/2021)
is the most complete and because it allows to send (via OSC messaging protocol)
a selected expression to Antescofo for an immediate evaluation.

There was a dedicated GUI which is now deprecated, [AscoGraph], that was
used to edit an Antescofo score and to interact with a running Antescofo
program. The mention of [Ascograph] still appears in some place in the
documentation, but the tool is not longer functional. For example, in
the video below, we can see Antescofo playing with a musician and
controlling both the electronic elements and the spatialization. The
musician's score (in a piano roll) and the electronic actions are both
visible in the [AscoGraph] window. Take a look to [this
post](https://discussion.forum.ircam.fr/t/about-the-ascograph-the-antescofo-graphical-editor-category/20387/2)
for replacement solutions.

<!-- <font color="red">(((REFAIRE UNE VIDEO)))</font> --> 

<center><iframe
src="https://player.vimeo.com/video/123960703" width="500" height="281"
frameborder="0" webkitallowfullscreen mozallowfullscreen
allowfullscreen></iframe></center>


<br>


## An interweaving of musical events and electronic actions

An Antescofo score describes both the human actions
to be recognized and the machine's reactions to environmental
input. A score thus has two main elements:

- **EVENTS** are elements to be recognized by the score follower or
machine listener, describing the dynamics of the outside
environment. They consist of `NOTE`, `CHORD`, `TRILL` and other elements
discussed in details in section [Event](events.md).

- **ACTIONS** are elements to be undertaken once corresponding event(s)
or conditions have been recognized. Actions in Antescofo extend the good-old
*qlist* object elements in MAX and PD with additional features which
will be described in this document.


The figure below shows a simple example from the Composer Tutorial on
Pierre Boulez’ “Anthèmes 2” (1997) for violin and live electronics as
it was seen in Ascograph (open the picture in a new tab for a larger view).

![Antescofo Score Excerpt showing basic events and actions](../Figures/Score-Intro-example1.png)


The left window shows a visual representation of Events and Actions,
whereas the right segment shows the raw text score. Events in the score
describe expected notes, trills and grace notes from the solo Violin,
and Actions specify messages to be sent upon recognition of each
event. In this example, we showcase actions for four real-time *pitch
shifter* (or harmoniser), whose general volume is controlled by the
`hr-out-db parameter`, and each shifter parameter separately controlled
by `hr1-p` to `hr4-p`.  The values for pitch shifters are in pitch-scale
factor. Their corresponding musical value is described in the text score
as comments (any text following a semi-colon ‘;’ is ignored in the
score).

This score shows basic use of actions and events. Red text in the
text-editor correspond to reserved keyword for Events. For details
regarding available events and their corresponding syntax, see section
[Event](events.md). In this example, actions are basic
message-passing to receivers in Max or Pd environments but with OSC,
Antescofo can interact with many other softwares like CSound,
Supercollider... Since they are isolated and discrete actions, we refer
to them as **Atomic Actions**. As will be shown later, actions in
Antescofo can use delays expressed in various time formats, and further
include dynamic (i.e. real-time evaluated) expressions, data-structures
and more. Details on action structures are discussed in section
[Actions](action.md).

The next figure shows a slightly more complex score corresponding to the
beginning of “Tesla ou l'effet d'étrangeté” (2014) by composer Sasha 
Blondeau for viola and live electronics as it was seen in Ascograph. The
graphical representation on the left is a visual interpretation of the
text score on the right. For easier viewing, messages are represented
here by white circles.

![The beginning of *Tesla* (2014) by Sasha J. Blondeau for Viola and Live electronics in *AscoGraph*](../Figures/Tesla1-JBLO.png)


In the previous screenshot, the score for human musician contains many
types of events (not all visible in text) with a mixture of discrete and
continuous compound actions as written by the composer. At the moment
you have to know that in Antescofo it exists 5 kinds of events :

- `:::antescofo NOTE` : in red in the piano roll
- `:::antescofo CHORD` : in green in the piano roll
- `:::antescofo TRILL` for classical trill or tremolo : in blue in the piano roll
- `:::antescofo MULTI` for glissandi (with one or more notes) : in yellow in the piano roll
- `:::antescofo EVENT` for symbolic events

*Tesla* makes use of **Compound Actions** which consist of parallel
groupings of **Atomic Actions**, as well as continuous actions
introduced by keyword `Curve`.  These elements correspond to different
electronic actions. We can therefore compose the electronic part in
parallel with the instrument part, together in an "augmented
score". Effects, synthesis, spatialization and others can be controlled
by Antescofo. Functions, Processes, Actors (objects) and Macros can be
used to abstract and reuse these controls.


We will see that with Antescofo, we can compose with many differents
kind of time. Sequential time for the atomic actions, continuous time
for `curves`, cyclic time for `loop` or "composed time" with `processus`
that will see in due course.




## The file structure of an Antescofo augmented score


A textual score, or program, is written in a single file and loaded from
there. The file itself can optionally include pointers to other score
files, using the `:::antescofo @insert` feature:

```antescofo
@insert macro.asco.txt
@insert "file name with white space must be quoted"
```

The keyword can be capitalized: `@INSERT` as any other predefined
`@`-keywords (_i.e._, identifiers that start with a `@` character). An
included file may includes other files. The `:::antescofo @insert`
command is often used to store definitions and initialisation of the
main Antescofo score. 

The `:::antescofo @insert_once` command is similar to `:::antescofo
@insert` except that the file is included only once in the current
score, when the directive is encountered the first time. The behavior
makes possible to include a library of primitives in a set of files
without the burden of taking care of their dependencies.

For the rest of this chapter, we will briefly introduce main elements in
the language. Details will be left for dedicated chapters in the
[reference manual].





<br>
<!-- FOOTNOTE -->



{!references.ref!}
