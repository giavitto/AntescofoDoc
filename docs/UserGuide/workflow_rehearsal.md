<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->



## Preparing the Performance, Rehearsals


### Controlling _Antescofo_ from _AscoGraph_

_AscoGraph_ is very useful in a concert context, when you have to
_follow the follower_ to control the following part of Antescofo and to
control what is launched in the Max patch.


The top of the Ascograph window has five buttons that correspond
respectively to the Antescofo function `previous event`, `stop`, `play`,
`start` and `next event` that you can launch also directly in your
patch.

The `play` button simply sequences from the beginning of the score to
the end, using given event timing and internal tempi, performing
actions wherever available.

The `Play string` function (Menu/Transport/Play string) plays
(evaluates) only the selected part of your score (restricted to a
sequence of actions).


When _Antescofo_ is in follower mode, and _AscoGraph_ is up, the current
position infered by the listening machine is outlined both in the piano
roll and in the _AscoGraph_ text editor.




### Moving in the Augmented Score

They are several commands (Max/PD messages that can be sent to
_Antescofo_) that are very useful in rehearsals. The idea is to be able
to take over the score deterministically (with correct electronics) if
you are rehearsing for example from "Measure10" (a label in score)
onwards and assuring that things are prepared (or not) once you start
from there as if you have performed everything before. Here is a list of
these commands:

#### Methods with no argument

- <strong>start</strong>: Sends initialization actions (before first
  event) and waits for follower

- <strong>play</strong>: Simulates the score (instrumental+electronics)
  from the beginning until the end or until STOP



#### Methods with label/symbol control

- <strong>startfromlabel</strong> <em>label</em> : Executes the score
  from current position to position corresponding to <em>$label</em> in
  accelerated more WITHOUT sending messages. Waits for follower (or user
  input) right before this position.

- <strong>scrubtolabel</strong> <em>label</em> : Executes the score from
  current position to position corresponding to <em>$label</em> in
  accelerated more WITH sending messages up to (and not including)
  $label. And waits for follower (or user input). 

- <strong>playfromlabel</strong> <em>label</em> : Executes the score
  from current position to position corresponding to <em>$label</em> in
  accelerated more WITHOUT sending messages, then PLAYs (simulates) the
  score from thereon.

- <strong>playtolabel</strong> <em>label</em> : PLAY (simulate) score
  from current position up to $label.

- <strong>gotolabel</strong> <em>label</em> : Position yourself on
  $label without doing anything else! (backward compatibility)

<em>NOTE:</em> Most above commands would use "current position" and not
"from the beginning". In order to reset, one would precede one of the
above with the STOP method.


#### Methods with float/beat-position control

Same as above, but taking a float value corresponding to cumulative
beat-position in score (useful for interaction via notation editors such
as AscoGraph and NoteAbility PRO). Namely:
<strong>startfrombeat</strong>, <strong>scrubtobeat</strong>,
<strong>playfrombeat</strong>, <strong>playtobeat</strong>, and
<strong>gotobeat</strong>



#### Non-standard use of score navigation command

Like the others commands understood by _Antescofo_, these navigation
messages can be issued from the _Antescofo_ score itself. They can be
used to develop open pieces, or to alter the flow of the performance on
purpose.








### Tuning the Listening Machine


In the default audio observation mode, you need to define the analysis
parameters which are window size, and hop size in sample
numbers. Antescofo automatically buffers audio and does the required
analysis. Your choice of window size and hopsize could alter the
performance and computation of the system! Default value is [2048
1024]. For example, when following piano music, you might want to set
window size to 4096 (for better spectral resolution). If you want more
exact timing, reduce the hopsize (and pay more CPU).

In your max/PD Patch, you can access to advanced performance controls
through the audio menu.



#### Calibration

The quality of audio input is of utmost importance for any recognition system and Antescofo is no exception! If the audio input is too low or corrupted, Antescofo can no longer correctly detect events.

Antescofo comes with a built-in Calibration mode to help make sure there is enough audio level for the recognition system to work. You can use it by sending the "calibrate 1" message to Antescofo, start sending audio to Antescofo (no need to "start") and watch the values sent from the left-most outlet with a prefix "calibration". 

Antescofo calibration values are always between 0.0 and 1.0 and can be interpreted as follows: Whenever there are "events" present in audio, these values should be relatively high (>0.75) and in the contrary (non-events such as silence, hall noise etc.) it should be relatively low (<0.5). It is best practice to test these conditions before any use. If these conditions do not meet, you have to tweak the input level (in Max or audio console) to Antescofo.

***It is highly preferable in practice to separate live processing input from that of Antescofo to keep separate level.***



#### Tuning

For pitch and polyphonic detection, Antescofo is tuned by default to
440.Hz (on A4). This can be changed with the message `tune $1`, where
`$1` is the new tuning.



#### Harmonic Analysis Control

When run in polyphonic mode (default mode) Antescofo analyzes realtime audio and matches it to score pitches using a predefined number of harmonics. This can be controlled using the `nofharm $1` message and is 10 by default. Use it if you are confident on what you're doing !

Some instruments have strange harmonic structures ! For example, clarinet usually has odd harmonics only. Or vibraphones (depending on the manufacturer) might produce only several non-arranged harmonics. If such cases are systematic (and you can observe it clearly on the instrument spectrum), you can tell Antescofo to systematically use a certain harmonic series (which also accepts floating points) with the message `harmlist`.
*We recommend you to be quite sure before changing this since it can severely affect behavior!*




### Dealing with Errors


Errors, either during parsing or during the execution of the *Antescofo*
score, are signaled on the MAX console.

The reporting of syntax errors includes a localization. This is
generally a line and column number in a file. If the error is raised
during the expansion of a macro, the file given is the name of the macro
and the line and column refers to the begining of the macro definition.
Then the location of the call site of the macro is given.


#### Printing the Parsed File

Using *Ascograph*, one has a visual representation of the parsed
Antescofo score along with the textual representation.

The result of the parsing of an *Antescofo* file can be listed using the
`printfwd` message or the `:::antescofo antescofo::printfwd` internal
command. This command opens a text editor. Following the verbosity, the
listing includes more or less information.




#### Tracing 

They are several alternative features that make it possible to trace a
running program.




##### The Outlet

If an *outlet* named `TRACE` is present, the trace of all event and action are
send on this outlet. The format of the trace is

          EVENT label ... 
          ACTION label ...



##### Verbosity

The verbosity can be adjusted to trace the events and the action. A
verbosity of $n$ includes all the messages triggered for a verbosity
$m < n$. A verbosity of:

-   1: prints the parsed files on the shell console, if any.

-   3: trace the parsing on the shell console. Beware that usually MAX
    is not launched from a shell console and the result, invisible,
    slowdown dramatically the parsing. At this level, all events and
    actions are traced on the MAX console when they are recognized
    of launched.

-   4: traces also all audio internals.


##### Tracing the Updates of a Variable

If one want to trace the updates of a variable `:::antescofo $v`, it is
enough to add a corresponding `:::antescofo whenever` at the begining of
the scope that defines `:::antescofo $v`:

```antescofo
      whenever ($v = $v) 
      {
              print Update "$v: " $v
      }
```

The condition may seems curious but is needed to avoid the case where
the value of `::antescofo $v` is interpreted as false (which will
prohibit the triggering of the body).


##### Tracing the Evaluation of Functions

Function calls can be traced, see the description of the functions
[@Tracing] and [@UnTracing].




{!Library/Functions/functions.ref!}
