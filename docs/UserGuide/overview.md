<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}



A Brief overview of Antescofo features
======================================

This section introduces some features that are very useful, especially for composers. If you are interested to learn more, take a look at the corresponding chapters in the *Antescofo reference*. Remember that everything having to do with control in Max or PD could certainly be replaced by an Antescofo Program. You will see that you can easily manage tabs, lists and some features that you need to control all the parameters of a concert patch.

A useful action : the curve
----------------------------
One of Antescofo's useful Keywords is the curve. Many composers use automations in their sequencers or use some "line" in Max. In Antescofo, you can write a large ensemble of curves (with many interpolation types) and create a score that controls all the effects, spatialization and synthesis with this curves.
All the receivers in your Max or PD patch can receive the updated variable controlled by a curve. 

The example below shows 3 simple curves with different types of
interpolation. A receiver named "print" will receive the variables
`:::antescofo $x`, `:::antescofo $y` and `:::antescofo $z`.

![An example of curve](../Figures/ex_User_curve.asco.png)



Make your life easier with macros !
-----------------------------------

Frequently, we use a same function that need some parameters in different moment of a piece. So you need to write many lines for a unique effect or synthesis. With a macro, you can denote that some parameters define a single musical entity. We think that is important to can choose between some writing style. Some different ways of thinking need different ways of writing !

The next example shows two ways to write a same thing. 

```antescofo
; EXAMPLE 1 : the good old Q-list style !
NOTE 60 1
        SPAT_REV 0.2
        SPAT_X 1.
        SPAT_Y 0.8

        AddSynt_Hn 8
        AddSynt_F0 888
        AddSynt_Rev 1.4
        AddSynt_Mod 0.1

NOTE 69 2
        SPAT_REV 0.8
        SPAT_X 0.
        SPAT_Y 1.2

        AddSynt_Hn 6
        AddSynt_F0 1857
        AddSynt_Rev 1.8
        AddSynt_Mod 0.

NOTE 63 4
        SPAT_REV 3.2
        SPAT_X 2.
        SPAT_Y 0.

        AddSynt_Hn 14
        AddSynt_F0 68.4
        AddSynt_Rev 2.3
        AddSynt_Mod 0.002

; EXAMPLE 2 : In another way....

@macro_def SPAT ($REV, $X, $Y)
        {
                SPAT_REV $REV
                SPAT_X $X
                SPAT_Y $Y
        }
@macro_def AddSynt ($Hn, $F0, $Rev, $Mod)
        {
                AddSynt_Hn $Hn
                AddSynt_F0 $F0
                AddSynt_Rev $Rev
                AddSynt_Mod $Mod
        }

NOTE 60 1
        @SPAT(0.2, 1., 0.8)
        @AddSynt(8, 888, 1.4, 0.1)

NOTE 69 2
        @SPAT(0.8, 0., 1.2)
        @AddSynt(6, 1857, 1.8, 0.)

NOTE 63 4
        @SPAT(3.2, 2., 0.)
        @AddSynt(14, 68.4, 2.3, 0.002)
```


Tour the loop
-------------

<!-- après "Steve Reich!" il y a un emoji invisible (clapping hands :-) -->
Sometimes, the situation calls for a loop (ask to Steve Reich! 👏). You
can use many loops and imbricated loops in Antescofo. For example, you
can include a curve in a loop and dynamically modify it while advancing
the the loop time.  There are many ways to end a loop. Guess what will
happen in the following examples:

```antescofo

loop ForEver 1 
     { print "Try again!" } ; an "infinite" loop...

3.5 abort ForEver ; ... that you have the power to finish !
    print "That's enough!" 

;----------------------------------------------------------
$cpt := 0 
loop L 1.5
     {
              $cpt := $cpt + 1 
          0.5 print a1
          0.5 print a2
     } until ($cpt >= 3)   ; A conditional end

;----------------------------------------------------------
; the same with an another type of abort
loop L 1.5
     {
              print a0
          0.5 print a1
          0.5 print a2
     } during [4.5]        ; A temporal constraint

```

<br>&nbsp;




Build your own world
--------------------

In *Antescofo*, you can create your own functions (see `:::antescofo
@fun_def`) if you frequently need to carry out the same task (to set a
diapason, for example). There are many features that help create musical
entities and facilitate electronic score writing.

### Why do you need data structures...

Data structures come in handy in many situations. Some effects and
syntheses involve a long list of parameters. In the *Data Structures*
chapter, you will see many ways to create and manipulate different kinds
of lists and data structures (see [map] and [tab]).

In the classical music notation, there are many symbols that each denote
an ensemble of parameters. These symbols permit the musician to focus on
the music and not on the parameters. You can have the same approach in
*Antescofo* if you use macros, processes and data structures. For
example, if you use a physical model for synthesis, it's very laborious
to enumerate all the parameters in your score. In this case, you can use
an ensemble of tabs as a “playing mode” library. So in your score, you
will just have to write the name of the tab and not the ensemble of
parameters. Like when you write *Sul ponticello* you don't have to
describe to the musicians how to play that !

<center>
![score excerpt](/Figures/exMDJ.png){: width="70%" }
</center>


The figure below shows a short library for a string physical model and a
process that permits interpolation between two “playing modes”. Don't
worry about understanding all the syntax (that's what the [reference
manual] is for) but remember that it's possible!

<center>
![An example of utilisation of Data Structures and processus](../Figures/ModeDeJeu.png){: width:"90%"}
</center>

You can use the same type of program to write a "spatialization" library
(see the Stroppa/Cont Library) where you can write a simple command in
the score that will make a complex movement in the space.


### In processes we trust

If you have to create complex processes that can heard some
extra-parameters of the score (like audio descriptors, patterns....),
*Antescofo* provides some dynamic features like the `:::antescofo
whenever` or the `processus` that permit to write a real musical entity
with some musical evolutions.

This process are different, in the way of thinking, of the classical
score. The process is a kind of "deamon" that can be launched when it's
needed and that can be aborted at the good moment. The process move in
parallel with the score but take account of the musician's tempo (see
`:::antescofo $RT_TEMPO`).

In the figure below, a musician can choose a path in an ensemble of
short extracts. Different zones are associated at this extracts. In the
score, some atomic actions are classicatly played with the score of
musician, in a sequencial way.  In parralel, a process is launched and
evolve depending on area where the musician is.  The process can be
seeing as an entity that evolve both with the musician and its own
independant evolutions.

![Process](../Figures/processEx.png)

Create your own process, macro and function library that you use in all
of your pieces.



### A conditional world

Sometimes we need to specify conditional actions. In *Antescofo*, the
constructions `:::antescofo if` and `:::antescofo switch` are made for
that. A conditional action is a compound action that performs different
actions depending on whether a programmer-specified boolean condition
evaluates to true or false.

You want launch a group of actions only if the musician plays at a
particular amplitude ? You have to use this kind of code :

```antescofo
     if ($musAmp >= 1.2)
     {
            synt_receiver bang
     }
     else
     {
            print "Hey! You're playing too softly!"
     }
```

But, note that this kind of `:::antescofo if` is evaluated when it is
launched. So... it is useful but you maybe have to watch at variable
during all the time of the performance. In this context, you need a
dynamic construction that look permanently the value of your
variable. You need the `:::antescofo whenever` construction! In the same
idea of before, if you want to know when your musician is playing too
loud, you can write something like this :

```Antescofo
     whenever ($musAmp >= 1.2)
     {
            synt_receiver bang
     }
```

When the `:::antescofo whenever` statement is launched, the variable
that it's given is permanently monitored and you will always know _when_
the `:::antescofo whenever`'s condition is true.

It's as easy as pie!



Become the time master
----------------------

In *Antescofo*, all the electronic actions are launched in the
musician's time. The internal variable `:::antescofo $RT_TEMPO` give the
tempo of the musician in real time. So, when you write your score, you
can be sure that it will be synchronize with the musician (if you
interested by the synchronization question, take a look at the chapter
[Synchronization Strategies]).

It's great ! But..., perhaps you would like to impose *your* time ! And,
maybe, you had written an electronic phrase that sound too steep and you
would like to introduce more softness in the “electronic phrasing”. You
can want to write an accelerando, but write the absolute time values is
so laborious....

<center>
![making an accelerando](/Figures/accel.png){: width="35%"}
</center>

So, in *Antescofo* language, all the `:::antescofo group`, [process],
`:::antescofo loop`, `:::antescofo curve` ... can be “time
controlled”. This means that for each group, you can impose a tempo
(BPM) or better: an evolution of tempo !

The attribute `:::antescofo @tempo` is made for that. If you see the
previous example, you can see that for two periods of time, the process
is like “time freezed” and you can see `::antescofo $tempo := 0` that is
a simple example where you can stop for a moment any instance if you put
its tempo at 0.


In the next example, a group `fusee` composed with atomic actions
`:::antescofo ::ASCOtoCS_points ...` is time controlled by the
variable `:::antescofo $tempfusee`. This variable is modified in time by
a curve that give a phrasing to the group.

<br>

<center>
![accelerando](/Figures/timeAccel.png){: width="75%"}
</center>


