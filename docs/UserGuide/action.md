<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}



Actions in Brief 
================

Think of *actions* as what Antescofo undertakes as a result of arriving at an
instant in time. In traditional practices of interactive music, actions
are *message passing* through *qlist* object in Max/Pd (or alternatively
message boxes or *COLL, PATTR* objects in MAX). Actions in Antescofo allow more
explicit organization of computer reactions over time and also with
regards to themselves. See section [message] for a detailed
description of the message passing mechanism.

Actions are divided into *atomic actions* performing an elementary
computation or simple message passing, and *compound actions*. Compound
actions group other actions allowing *polyphony*, *loops* and
*interpolated curves*. An action is triggered by the event or the action
that immediately precedes it.

In the new syntax, an action, either atomic or compound, starts with an
optional *delay*, as defined hereafter. The old syntax for compound
action, where the delay is after the keyword, is still recognized.

##### Action Attributes

Each action has some optional attributes which appear as a comma
separated list:

      atomic_action @att1, @att2 := value
      compound_action @att1, @att2 := value { ... }

In this example, `@att1` is an attribute limited to one keyword, and
`@att2` is an attribute that require a parameter. The parameter is given
after the optional sign :=.

Some attributes are specific to some kind of actions. There is however
one attribute that can be specified for all actions: *label*. It is
described in section [Action Label]. The attributes specific to a
given kind of action are described in the section dedicated to this kind
of action.


Delays
------

An optional specification of a *delay* `d` can be given before any action `a`.
This defines the amount of time between the previous event or the
previous action in the score and the computation of `a`. At the expiration
of the delay, we say that the action is *fired* (we use also the word
*triggered* or *launched*). Thus, the following sequence
```antescofo
          NOTE C3 2.0        
            d1 action1
            d2 action2
          NOTE D3 1.0
```
specifies that, in an ideal performance that adheres strictly to the
temporal constraint specified in the score, `action1` will be fired `d`1
after the recognition of the C note, and `action2` will be triggered
`d2` after the launching of `action1`.

A delay can be any expression. This expression is evaluated when the
preceding event is launched. That is, expression `d`2 is evaluated in
the logical instant where `action1` is computed. If the result is not a
number, an error is signaled.

##### Zero Delay

The absence of a delay is equivalent to a zero delay. A zero-delayed
action is launched synchronously with the preceding action or with the
recognition of its associated event. Synchronous actions are performed
in the same logical instant and last zero time, cf.
paragraph [Logical Instant].

##### Absolute and Relative Delay

A delay can be either absolute or relative. An absolute delay is
expressed in seconds or milliseconds and refers to wall
clock time or physical time. The qualifier `s` (respectively `ms`) is used to
denote an absolute delay:

```antescofo
                   a0
               1 s a1
         (2*$v) ms a2
```

Action `a1` occurs one second after `a0` and `a2` occurs (2*$v)
milliseconds after `a1`. If the qualifier `s` or `ms` is missing, the
delay is expressed _in beats_ and it is relative to the tempo of the
enclosing group (see section [local tempo]).


##### Evaluation of a Delay

In the previous example, the computed value of `a2`'s delay
may depend of the date of the computation (for instance, the
variable may be updated somewhere else in parallel). So, it is important
to know when the computation of a delay occurs: it takes place when the
previous action is launched, since the launching of this action is also
the start of the delay. And the delay of the first action in a group is
computed when the group is launched.

A second remark is that, once computed, the delay itself is not
reevaluated until its expiration. However, the delay can be expressed in
the relative tempo or relatively to a computed tempo and its mapping
into the physical time is reevaluated as needed, that is, when the tempo
changes.


##### Synchronization Strategies

Delays can be seen as temporal relationships between actions. There are
several ways, called *synchronization strategies*, to implement these
temporal relationships at runtime. For instance, assuming that in the
first example of this section `action2` actually occurs *after* the
occurrence of `NOTE D`, one may count a delay of *d1 + d2 - 2.0*
starting from `NOTE D` after launching `action2`. This approach will be
for instance more tightly coupled with the stream of musical events.
Synchronization strategies are discussed in section [synchronization strategies].


Label
-----

Labels are used to refer to actions. Like events, actions 
can be labeled with

-   a simple identifier,

-   a string,

-   an integer.

The labels of an action are specified using the `@name` keyword:
```antescofo
          ... @name := somelabel
          ... @name somelabel
``` 
One action can have several labels. Unlike with event labels, 
the $-identifier associated with an action label cannot be
used to refer to the relative position of this action in the score[^1].

Compound actions have an optional identifier (section [compound
action]).  This is a simple identifier and acts as a label for the
action without the burden to explicitly use the `:::antescofo @name`
attribute.

Action Execution
----------------

We write at the beginning of this chapter that *actions* are performed
when arriving at an instant in time. But the specification of this date
can take several forms. It can be

-   the occurrence of a musical event;

-   the occurrence of a logical event (see the `whenever` construction page  and
    the pattern specification at page [Whenever]);

-   the loading of the score (cf. the `@eval_when_load` construct at page [Eval when Load]);

-   the signal spanned by an `@abort` action (see abort handler at page [Abort]);

-   the sampling of a `curve` construct (page [Curve]);

-   the instance of an iterative construct (pages [Loop] and [ForAll]);

-   or the expiration of a delay starting with the triggering of
    another action.


[^1]: There is no useful notion of position of an action in the score
    because the same action may be fired several times (actions inside a
    loop body or a or associated to a whenever or a process).
