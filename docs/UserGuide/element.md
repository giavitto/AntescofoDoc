<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}


# Elements of an Antescofo Score

An _Antescofo_ program is a sequence of *events* and *actions*. Events, 
recognized by the listening machine, are described in detail in chapter
[Events]. Actions, outlined in chapter [Actions] are computations
triggered upon the occurrence of an event or of another action. Actions
can be dynamically parameterized by *expressions* and data structures,
evaluated in real-time and described in detail in the [Reference
Manual].


Elements of the language can be categorized into six groups that
correspond to various constructions permitted in the language:

- **Comments**: Any text starting by a semi-colon `:::antescofo ;` or
    `:::antescofo //` is considered a comment and ignored by parser
    until the end of line (inline comment).

    Block (multi-line) C-Style comments starting with
    `/*`and ending with `*/` are also allowed.

- **Keywords**: are reserved words that introduce either Event or Action
    constructions. Examples include `:::antescofo Note` (for events) and
    `:::antescofo Group` (for compound actions).

- **Simple identifiers**: denote Max or PD receivers and are also used
    to specify the label of a musical event or of an action.

- **@-identifiers**: are words that start with `@` character. They
  either introduce a new definition or denote predefined functions,
  user-defined functions, user-defined macros, action attributes, or
  event attributes.  The following @-identifiers are used to introduce
  new definitions: `:::antescofo @abort` &nbsp; `:::antescofo
  @broadcast` &nbsp; `:::antescofo @fun_def` &nbsp; `:::antescofo @init`
  &nbsp; `:::antescofo @macro_def` &nbsp; `:::antescofo @obj_def` &nbsp;
  `:::antescofo @pattern_def` &nbsp; `:::antescofo @proc_def` &nbsp;
  `:::antescofo @track_def` &nbsp; `:::antescofo @whenever`.


- **$-identifiers**: are words that start with `$` character. They
  correspond to user-defined variables or parameters in functions,
  processes, and object or macro definitions.

- **::-identifiers**: words starting with `:::antescofo ::`,
  `:::antescofo obj::`, `:::antescofo pattern::` or `:::antescofo
  track::` refer respectively to processes, objects (actors), patterns
  or tracks.


User defined score elements including macros, processes and functions
can only be employed after their definition in the score. We suggest
putting them at the beginning of the file or to put them in a separate file
using the `@insert` command. They will be discussed in proceeding chapters.


## Simple identifiers: keywords and message receivers


Simples identifiers denote *Antescofo* keywords. 
They are also used for the receivers of messages send to the host environment.

The Antecofo language comes with a list [Reserved
Keywords](/Reference/2-syntax.md#antescofo-keywords) for defining
elementary score structures. Reserved keywords are *case insensitive*.
They can be divided in two groups:

- **Event Keywords** including `:::antescofo NOTE`, `:::antescofo
  CHORD`, `:::antescofo TRILL` and `:::antescofo MULTI` introduce
  musical events (see chapter [Event in Antescofo](events.md))
  and are used to describe the music score to be recognised.

- **Action Keywords**, such as `GROUP`, `LOOP` and more, specify
  computations that can be instantaneous ([Atomic actions]) or
  *containers* for other actions that have a duration
  ([Compound actions]).

Here is an example:

```antescofo
        NOTE 60 1/2
            rcvr1 harm1 60 87 0.5
            rcvr2 ampSy 0.8 2
        NOTE 62 1
            rcvr1 harm1 87 78 1.5
            1/2 print HELLO
```

In the example above, `:::antescofo rcvr1 harm1 60 87 0.5` and
`:::antescofo rcvr2 ampSy 0.8 2` are actions that are hooked to event
`:::antescofo NOTE 60 1/2`, and `:::antescofo 1/2 print HELLO` denotes
an action (sending to a receiver `:::antescofo print` in max/pd) with a
delay of half-beat time. General syntax for atomic actions is described
in chapter [Atomic Actions].


Event keywords can not be nested inside Action blocks. Event keywords
are always defined at the top-level of the text score. Action keywords
and blocks can be nested as will be discussed later.

The example shows another use of a simple identifiers: `:::antescofo rcvr1`, `:::antescofo rcvr2`,
`:::antescofo harm1` `:::antescofo ampSy` and `:::antescofo print` are simple identifiers but are not 
reserved keywords. Here they refer to a receiver or a symbol in
Max/PD. Simple identifiers that are not reserved keywords are _case
sensitive_.
In case a score requires the user to employ a reserved keyword inside
a message, the user should wrap the keyword in quotes to avoid clash.



**REMARK: An Antescofo text score is interpreted from top to bottom.  In
this sense, *Event Sequence* commands such as `:::antescofo BPM` or
`:::antescofo variance` will affect they events that follow their
appearance.**

Example: The figure shows two simple scores. In the left score, the second tempo change to 90 BPM will be effective starting on the event with label `:::antescofo Measure2` and as a
consequence, the delay 1/2 for its corresponding action is launched
with 90 BPM. On the other hand, in the right score the tempo change
will affect the chord following that event onwards and consequently, the
action delay of 1/2 beat-time hooked on note C5 corresponds to a
score tempo of 60 BPM.


<html>
<div class="twocol">
<div>
```antescofo
	BPM 60
	NOTE C4 1.0 Measure
	CHORD (C4 E4) 2.0
	NOTE G4 1.0
	BPM 90
	NOTE C5 1.0 Measure2
	     1/2 print action1
	CHORD (C5 E5) 2.0 
	NOTE A4 1.0
```
</div>
<div>
```antescofo
BPM 60 
NOTE C4 1.0 Measure1 
CHORD (C4 E4) 2.0 
NOTE G4 1.0 
NOTE C5 1.0 Measure2 
     1/2 print action1 
BPM 90 
CHORD (C5 E5) 2.0 
NOTE A4 1.0 

```
 </div></div></html>



## @-identifiers: Functions, Macros, and Attributes

A word begining with a ‘@’ character is called a @-identifier. They have
five purposes in Antecofo language:

1. in the processing of a file, some commands directly affect the
    parsing of this file: the `:::antescofo @insert` command is used to
    insert another file, and the commands `:::antescofo @uid` and
    `:::antescofo @lid` are used to generate on-the-fly fresh
    identifiers;

2.  to introduce new definitions (functions, processes, tracks,
    patterns, etc.);

3.  to specify various attributes of an event or an action;

4.  to call internal functions that comes with Antecofo language as listed in
    chapter [Library Functions];

5.  and to call user-defined functions or macros.


Only `!` `?` `.` and `_` are allowed as special (non alphanumeric)
characters after the `@`.

Note that in the first three cases, @-identifiers are reserved
identifiers and thus are *case unsensitive*, that is `:::antescofo
@tight`, `:::antescofo @TiGhT` and `:::antescofo @TIGHT` are the same
keyword. Reserved @-identifiers are [listed
here](/Reference/2-syntax.md#-identifiers-functions-macros-and-attributes).


Users can define their own functions as shown in chapter
[Functions]. These @-identifiers are _case sensitive_. Predefined
functions are [listed
here](/Library/Functions/00intro.md#alphabetical-listing-of-antescofo-predefined-functions).


$-identifiers : Variables and Parameters
-----------

$-identifiers like `:::antescofo $id` or `:::antescofo $id_1` are simple
identifiers prefixed with a dollar sign.  Only `!` `?` `.` and `_` are
allowed as special characters after the `$`. $-identifier are used to
give a name to variables (see section
[variables](/Reference/6-expression.md#variables) and as
parameters for function, process and macro definition arguments. *They
are case-sensitive*.

The figure below shows a rewrite of the excerpt of Pierre Boulez’
“Anthèmes 2” [given
here](structure.md#an-interweaving-of-musical-events-and-electronic-actions)
using a simple macro and employing basic `@` and `$` identifiers. The
harmoniser command is here defined as a [macro] for convenience and
since it is being repeated through the same pattern. The content of the
`:::antescofo hr1-p` to `:::antescofo hr4-p` actions inside the Macro
use a mathematical expression using the internal function `:::antescofo
@pow` to convert semi-tones to pitch-scale factor. As a result the
Antescofo score is shorter and musically more readable. Variables passed
to the macro definitions are $-identifiers.

You can learn more on expressions and variables in
chapter [expression] onwards.

![Rewrite of Figure \[fig:a2-ex2\] using a Macro and expressions](../Figures/Score-Intro-example.png)



::-identifiers : Processes
-----------

::-identifiers like `::P` or `::q1` are simple identifier prefixed with two
semi-columns. ::-identifiers are used to give a name to processus (see
chapter [Process]).








