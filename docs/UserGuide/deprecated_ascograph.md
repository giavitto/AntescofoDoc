<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}



<br>


__The information given [below](#stillUsing) is only for those who are still able to use
AscoGraph on simple Antescofo score. Ascograph is no longer
supported. __



<br>


## _AscoGraph_ is deprecated

_AscoGraph_ is discontinued since 2016. It was based on graphic libraries that
has not survived the evolution of compilers and software.

The available version is the last version compiled in 2016. Even if the
binary is sometimes still executable (depending on the machine
architecture and the OS version), the syntax recognized by AscoGraph is
restricted to Antescofo 0.3. This means that scores that make use of new
features fail to be loaded by Ascograph. In any case, its use is
deprecated.



There are some tools that can be used to achieve the functionalities of
Ascograph.


### Edition
        
To translate a MusicXML score or a midi file into a list of musical
events in the Antescofo format, the **[translation MusicXML/Midi
$\rightarrow$
_antescofo_](http://forumnet-dev.ircam.fr/antescofo-converter/)**
available as a web service is working.
		
For editing an Antescofo score, you can use Sublime with a dedicated
theme. The theme enables the highlighting of Antescofo score
but also specific command with a keystroke. 

For example option-P send the selected area for evaluation to a running
instance of Antescofo. It is very useful for “live coding” or for the
work in studio. See : 

- [Antescofo v0.92+ and Sublimte Text Editor](https://discussion.forum.ircam.fr/t/antescofo-v0-92-and-sublimte-text-editor/2021)
- [Antescofo syntax color for Sublime Text 3](https://discussion.forum.ircam.fr/t/antescofo-syntax-color-for-sublime-text-3/1315)
			
			
Check also [useful tutorials in our
Forum](https://discussion.forum.ircam.fr/t/antescofo-useful-tutorials-on-this-forum/1826). 
			
			
			
### Edition and Automatic Filewatch in the Max Environment

Max users can also use the Antescofo Max Object <em>Autowatch</em>
attribute. This means that if the loaded score is modified elsewhere,
it'll be automatically reloaded in the Max object. Obviously, you'd want
to turn this attribute off during live concerts! Autowatch is currently
not available for PureData objects.




### Monitoring 

To control the score following during a performance, a possibility is to
export the score to a [bach.roll](https://www.bachproject.net/dl/)
object in the [bach computer-aided composition
library](https://www.bachproject.net/features/) using the [@bach_score]
function in the Antescofo library.

During the performance, the position produced by the listener can be
used to animate thae cursor in the bach.roll. See a [patch produced by
Serge Lemouton](https://github.com/slemouton/bachscograph/) and also this
post: [Ascograph for Windows is
ready?](https://discussion.forum.ircam.fr/t/ascograph-for-windows-is-ready/22563/6/)


&nbsp;<p>

<center>
<a name="stillUsing"></a>

<img alt="line divider" src="/Figures/line-divide-grayr.svg" width="45%">

</center>
&nbsp;<p>



## Using _AscoGraph_ 

_Ascograph_ is a graphical tool that can be used to edit and then to
control a running instance of Antescofo through OSC messages.

_Ascograph_ and _Antescofo_ are two independent applications but the
coupling between Ascograph and an Antescofo instance running in MAX
appears transparent for the user: a double-click on the Antescofo object
launches Ascograph, saving a file under Ascograph will reload the file
under the Antescofo object, loading a file under the Antescofo object
will open it under Ascograph, etc.

_Ascograph_ was available in the same bundle as Antescofo on the IRCAM
Forum. It was far from being stable but has proven useful for
authoring/visualizing complex scores and for monitoring your live
performances.


### The _Ascograph_ interface

As you can see below (open the screenshoot in another tab to have a
larger view), the Ascograph interface contains 3 parts :

-   the piano roll, where you can see the musician's score and the label at the top 
-   the action view, where you can see the graphical representation of the *electronic score* and where you can manually edit the curves.
-   the text editor, where you can edit the musican's score and the electronic score.

![AscoGraph Interface](../Figures/ascoDemo.png)




At the top of the ascograph window, there are five buttons that correspond
respectively to the Antescofo functions `previous event`, `stop`, `play`,
`start` and `next event` that you can launch directly in your
patch.

The `play` button simply sequences from the beginning of the score to
the end, using given event timing and internal tempi, and undertaking
actions wherever available.

The `Play string` function (Menu/Transport/Play string) only plays the
selected part of your score. This enables to test on the fly Antescofo
fragment (only actions are performed, musical events are ignored) and
even allows a limited form of live-coding.



### The App Menu 

The text Editor menu contains all the functions relevant to the text editor. Included is the display mode selector, where you can toggle between integrated, floating (default) and hidden. The View menu retains the functions pertaining to the visual editor.


### Color Scheme

_AscoGraph_ comes with a new color scheme for both the text and visual
editor. By default, we provide <em>dark backgrounds</em> for both. The
philosophy behind this design choice is the fact that many of us use
_AscoGraph_ during live concerts and bright backgrounds create too much
light on your screens which make things annoying for you and your
audience who are most of the time in the dark.

Many users however prefer working on white background while typing. You
can easily Import a new Color Scheme for AscoGraph's Text Editor from
the <em>Text editor Menu</em>. You can design your own color scheme by
doing an <em>Export Color Scheme</em> from the Text Editor menu, modify
the XML file and import it back. Users interested in classical white
background text editor can import [Larry Nelson's Color Scheme](https://discussion.forum.ircam.fr/t/ascograph-0-25-tips) in particular.


### Interaction Between Visual and Text Editors in _AscoGraph_

Clicking on a Musical Event or Action in the Visual Editor will bring
the text editor to the corresponding text. Conversely, Right/Ctrl-click
on an event in the text editor takes you to the corresponding place in
the visual editor.

Vertical mouse-wheel (or double-finger on pad) gesture allows you to
browse over the text-editor, as well as horizontal mouse-wheel
(double-finger) gesture on the visual editor.


### Shortcuts

Holding CMD + mouse scroll: zoom in/out (visual &amp; text editors) CMD
+ &lt;number&gt; switches the text editor between the open files (main
.asco score and any @INSERT-ed files). You can also switch using the
tabs at the top of the editor.




### Edit your curves

*Ascograph* is also very useful to visualize and edit the curves
 constructs.

You can Edit _Antescofo_ Curves easily from the Visual Editor. Applying
graphical changes will automatically create the corresponding text into
the right place. See Nadir B's very userful
[YouTube Tutorials](https://www.youtube.com/playlist?list=PLJ-R9Hd3xi4D5x8du-yWZOy8lGNEdgn4a).


To start, you can create a curve after a note with the menu
"create/actions/curve". After saving the score, if you see the action
view, below the piano roll, you can see your curve.

If you click on the arrow at the right of the curve band, the curve is
moved on the piano roll, to see the superpositions and you can add or
move the curve's points. You can also choose the type of the
interpolation between each points.


![curve edition with Ascograph](/Figures/curveEdit.png)




### AscoGraph Automatic Filewatch: Using Another Text Editor

Starting with _AscoGraph version 0.25_, there is an **Automatic
Filewatch** integrated into the AscoGraph editor. This means that you
can now use any of your favourite text editors in addition to
AscoGraph. The moment you save the Antescofo Score outside, it'll be
automatically reloaded (and visualized) in the _AscoGraph_ window without
any intervention (and the Antescofo object will reload the score
automatically in the Max environment). 


So you can use in parallel the Piano Roll and action view and still use
your favorite text editor.


The addition of the *Automatic Filewatch* feature just makes score
editing with AscoGraph more coherent with other editors as they all
integrate automatic filewatch as well.

Max users can also use the Antescofo Max Object <em>Autowatch</em>
attribute. This means that if the loaded score is modified elsewhere,
it'll be automatically reloaded in the Max object. Obviously, you'd want
to turn this attribute off during live concerts! Autowatch is currently
not available for PureData objects but upcoming.

For the people that prefer use their own beloved text editor, we have
created some _syntax highlightings_ for **Sublime Text**, **TextWrangler**
and... **emacs** ! You have just to
[download them in our Forum](https://discussion.forum.ircam.fr/t/antescofo-useful-tutorials-on-this-forum/1826).




<br>


<!-- FOOTNOTE -->



