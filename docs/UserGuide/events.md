<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}


# Events 

An event in Antescofo terminology refers to elements that define what
will probably happen outside your computers for real-time detection and
recognition. In regular usage, they describe the *music score* to be
played by the musician to follow. They are used by the listening machine
to detect position and tempo of the musician (along other inferred
parameters) which are by themselves used by the reactive and scheduling
machine of Antescofo to produce synchronized accompaniments.

The listening machine is in charge of real-time automatic alignment of
an audio stream played by one or more musicians, into a symbolic musical
score described by Events. The Antescofo listening machine is polyphonic
and constantly decodes the tempo of the live performer. This is achieved
through explicit time models inspired by cognitive models of musical
synchrony in the brain which provide both the tempo of the musician in
real-time and also the *anticipated* position of future events (used for
real-time scheduling).

This section describes *Events* and their syntax in Antescofo
language. In a regular workflow, they can come from pre-composed music
scores using *MusicXML* or *MIDI* import (see section [score
import]). They can also be composed directly into the Antescofo text
program.


## Event Specification


Events are detected by the listening machine in the audio stream. The
specification of an event starts by a keyword defining the kind of event
expected and some additional parameters:

```
        NOTE pitch duration [label]
        CHORD (pitch_list) duration [label]
        TRILL ((pitch_list)*) duration [label]
        MULTI ((pitch_list)*) duration [label]
        MULTI (pitch_list) -> (pitch_list)) duration [label]
```

Here the ’*’ is a metacharacter meaning “zero or more repetitions” of
the preceeding construction.  Elements between square brackets ’[’ and
’]’ are optional but parentheses are literal elements that do appear in
the code (they are not metacharacters).


`:::antescofo TRILL` and `:::antescofo MULTI` are examples of *compound
events* organizing a set of `:::antescofo NOTE`s in time. Thus they can
accept one or several *pitch_list*s. *pitch\_list*s in `:::antescofo
TRILL` and `:::antescofo MULTI` are distinguished by their surrounding
parentheses. See the next section for a more musically oriented
explanation.

Events specification can be optionally followed by some attributes as
discussed in the Event Attributes section below. Events must end by a
carriage return. In other word, you are allowed to define only one event
per line.

There is an additional kind of event

```antescofo
        EVENT d
```

also followed by a mandatory duration _d_, which correspond to a fake
event triggered manually by the “nextevent” button on the graphical
interface or by the “nextevent” message to the antescofo object in
MAx/PD.


Parameters for event specification are described below.


### Pitch

 *pitch* (used in `:::antescofo NOTE`) can take the following forms:

 - MIDI number (_e.g._  69 and 70),

 - MIDI cent number (e.g. 6900 and 7000),

 - Standard Pitch Name (e.g. A4 and A\#4).

 - For microtonal notations, one can use either MIDI cent
   (e.g. 6900) or Pitch Name standard and MIDI cent deviations using
    ’+’ or ’-’ (_e.g._ `:::antescofo NOTE A4+50` and `:::antescofo NOTE A#4+50`
    or `:::antescofo NOTE B4-50`).

```antescofo
   CHORD (A4+50 A#4+50 B4-50 Bx4-50 C##4+50 C##4-50)
     print OK
```

 - a minus sign `-` may precede the previous specification to
   specify that the current note is a continuation of a note with
   the same pitch in the preceding event:
```antescofo
                   CHORD (C4 D5)  1
                   CHORD (-C4 D3) 1/2
```

### Pitch_list

*Pitch_list* is a set containing one or more pitches (used to define
content of a `:::antescofo CHORD`). For example, the following line
defines a C-Major chord composed of `C4, E4, G4`:

```antescofo
                   CHORD ( C4 64 6700 )
```


### Duration

*Duration* is a mandatory specification for all events. The of duration
an event is specified _in beats_ either by an integer (1), the ratio of
two integers (4/3) or a float (1.0).


### Label

Optionally, users can define *labels* on events as a simple identifier
or as a _string_, useful for browsing inside the score and for
visualisation purposes. For example `:::antescofo measure1` is an
accepted label.  If you intend to use *space* or *mathematical symbols*
inside your string, you should surround them with quotations such as
`:::antescofo "measure 1"` or `:::antescofo "measure-1"`





## Events as Containers


Each event keyword in Antescofo in the above listing can be seen as a
*container* with specific behavior and given nominal durations. A
`:::antescofo NOTE` is a container of *one* pitch. A `CH0RD` contains a
vector of pitches. The figure below shows an example including simple
notes and chords written in _Antescofo_:

<div class="row">

  <div class="column" style="width:35%;">
      <img alt="chord notation" src="/Figures/chord_sample.png" width="70%">
  </div>

  <div  class="column" style="width: 60%;">
```antescofo
 BPM 60
 NOTE C4 1.0
 CHORD (D4 F4) 1.0
 NOTE 0  1.0  ; a silence
 NOTE G4 0.0  ; a grace note with duration zero
 NOTE F4 2.0
```
  </div>
</div>



The two additional keywords `:::antescofo TRILL` and `:::antescofo
MULTI` also define containers with specific extended behaviors:

<br>

### TRILL

Similar to trills in classical music, a `:::antescofo TRILL` is a
container of events either as atomic pitches or chords, where the
internal elements can happen in any specific order. Additionally,
internal events in a `:::antescofo TRILL` are not obliged to happen in
the environment. This way, can be additionally used to notate
improvisation boxes where musicians are free to choose elements. A
`:::antescofo TRILL` is considered as a global event with a nominal
relative duration. Figure below shows basic examples for Trill.



<div class="row">

  <div class="column" style="width:35%;">
    <img alt="trill notation" src="/Figures/Events-Trills.png" width="70%">
  </div>

<div  class="column" style="width: 60%;">
  <br>
```antescofo
 TRILL (A4 B4) 1.0
 NOTE 0  1.0     ; a silence
 TRILL ( (C5 E5) (D5 F5) ) 2.0
```
</div>
</div>
<br>



### MULTI

Similar to `:::antescofo TRILL`, a `:::antescofo MULTI` is a compound
event (that can contain notes, chords or event trills) but where the
*order* of actions are to be respected and decoded accordingly in the
listening machine. They can model continuous events such as
*glissando*. Additionally, a `:::antescofo MULTI` contents can be
trills. To achieve this, it suffices to insert a character after the
*pitch\_list* closure. The next example shows a glissandi between chords
written by `:::antescofo MULTI`.



<div class="row">
  <div class="column" style="width:35%;">
    <img alt="gliss notation" src="/Figures/gliss_chord.png" width="70%">
  </div>

<div  class="column" style="width: 60%;">
&nbsp;<br>
&nbsp;<br>
```antescofo
 MULTI ( (F4 C5) -> (D4 A4) ) 4.0 
```
</div>
</div>



### Compound Events

Events can be combined and correspond to specific music notations.  For
example, a classical *tremolo* can be notated as a `:::antescofo TRILL`
with one event (`:::antescofo note` or `:::antescofo chord`) inside. The
next figure shows a glissando whose internal elements are tremolo. In
this case, the *prime* `'` next to each chord group indicate that the
elements in side the `:::antescofo MULTI` are `:::antescofo TRILL`
instead of regular notes or chords.

<div class="row">
  <div class="column" style="width:35%;">
    <img alt="notation of a tremolo glissendo" src="/Figures/Events-MultiTrill.png" width="70%">
  </div>
  <div  class="column" style="width: 60%;">
  <br>
```antescofo
 MULTI ( (C5 G5)' -> (D4 F4)' ) 2.0
```
</div>
</div>


The figure below shows a typical polyphonic situation on piano where the
right-hand is playing a regular trill, and the left hand regular notes
and chords. In this case, the score is to be segmented at each event
onset as `:::antescofo TRILL` whose elements would become the trill
element plus the static notes or chords in the left-hand.

<div class="row">

  <div class="column" style="width:35%;">
    <img alt="mixed trill" src="/Figures/Events-MixedTrill.png" width="85%">
  </div>

  <div  class="column" style="width: 60%;">
```antescofo
 TRILL ( (A4  A2) (B4  A2) )        1/2
 TRILL ( (A4  D3) (B4  D3) )        1/2
 TRILL ( (A4  C3 E3) (B4  C3 E3) )  1/2
 TRILL ( (A4  D3) (B4  D3) )        1/2
 TRILL ( A4 B4 )    2.0
```
</div>
</div>
<br>



## Event Attributes 

Attributes in Antescofo are keywords following an `@` character after the
definition of the event. There are four kinds of event attributes and
they are all optional.

- The keyword `:::antescofo fermata` (or `:::antescofo @fermata`)
  specifies that this event has a *fermata* signature. A Fermata event
  can last longer and arriving and leaving it does not contribute to the
  tempo decoding of the performance.

- The keyword `:::antescofo pizz` (or `:::antescofo @pizz`) specifies
  that the event is a string *pizzicato*. This usually helps Score
  Follower stability.

- The keyword `:::antescofo hook` (or `:::antescofo @hook`) specifies
    that this event cannot be missed (the listening machine needs to
    wait the occurrence of this event and cannot presume that it can be
    missed).

- The keyword `:::antescofo jump` (or `:::antescofo @jump`) is followed
    by a comma separated list of simple identifiers referring to the
    label of an event in the score. This attribute specifies that the
    event can be followed by several continuations: the next event in
    the score, as well as the events listed by the `@jump`.

These attribute can be given in any order. For instance:

```antescofo
          Note D4 1 here  @fermata @jump l1, l2
```

defines an event labeled by `here` which is potentially
followed by the next event (in the file) or the events labeled by `l1`
or `l2` in the score. It has a *fermata* attribute. Note that

```antescofo
          Note D4 1 @jump l1, l2 here
```

corresponds to the same specification: `here` is not interpreted as the
argument of the jump but as a label for the event because there is no
comma after `l2`.

### Event Label

A simple identifier or a string or an integer acts as a label for this
event. There can be several such labels. If the label is a simple
identifier, its $-form can be used in a expression elsewhere in the score
to denote the time in beat of the onset of the event.


## BPM and the @modulate Attribute

A `:::antescofo BPM` command specifies the required tempo for the musical 
events that follow (up to the next `:::antescofo BPM` specification). As 
for the usual scores, this specification is a requirement that is followed 
more or less rigorously by the musician. The actual tempo followed by the 
musician is infered by the listening machine from the occurence of the 
musical events. This tempo infered in real-time is used for the scheduling 
of the electronic action. The `:::antescofo BPM` specification is an  
information used by the listening machine to start the inference of the 
actual tempo, but the two differs.


The `:::antescofo @modulate` attribute can be used on a `:::antescofo
BPM` specification, not on an event. It specifies that the tempo must be
modulated to the *pro rata* of the actual tempo of the performer. For
example, if a `:::antescofo BPM 60` is specified in the score, and the
actual tempo of the performance if 70, then an indication of
`:::antescofo BPM 80 @modulate` reset the tempo expected by the
listening machine to $80 \times \frac{70}{60} \simeq 93.3$.

