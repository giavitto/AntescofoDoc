<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




### String Value


String constants are written between quotes. To include a quote in a
string, the quote must be escaped:

```antescofo
          print "this is a string with a \" inside"
```

Others characters must be escaped in string: `\n` is for end of line (or
carriage-return), `\t` for tabulation, and `\\` for
backslash.

Characters in a string can be accessed as if it was a tab (see
sect. [tab]). Characters in a string are numbered starting from $0$, so:

```antescofo
          "abc"[1]  ➙  "b"
```

Note that the result is a string with only one character. There is no
specific type dedicated to the representation of just one character. The
function [@size] can be used to get the number of characters in a
string.


Notice also that strings are *immutable values*: contrary to tabs, it is
not possible to change a character within a string. A new string must be
constructed instead.


The operator `:::antescofo +` corresponds to string concatenation:

```antescofo
          $a := "abc" + "def"
          print $a
```

will output on the console `:::antescofo abcdef`. By extension, adding
any kind of value $v$ to a string concatenates the string representation
of $v$ to the string:

```antescofo
          $a := 33
          print ("abc" + $a)
```

will output `:::antescofo "abc33"`. The value of the variable
`:::antescofo $a` was automatically converted to type String before
being concatenated.

There are several
 {!Library/Functions/string_functions.list!}


The function [@explode] can be used to convert a string into a [tab]
(vector) of characters (represented as string of size one). This makes
it possible to use the functions defined for tabs. If a tab contains multiple
strings, they can be concatenated using the [@reduce] operator to build a
 new string:

```antescofo
     $t := @explode("123abc")
     @assert $t == ["1", "2", "3", "a", "b", "c"]
     $tt := @reduce (@+, $t)
     @assert $tt == "123abc"
```




<br>

<!-- FOOTNOTE -->


