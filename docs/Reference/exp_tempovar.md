<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}





## Temporal Variables

Starting version $0.8$, _Antescofo_ provides a feature to track the
progression of any kind of performance _P_ through the updates to a
variable.  The variable is used to infer the tempo of _P_ and any
sequence of actions can be synchronized with it, the same way a sequence
of actions can be synchronized with the musician.

Such variables are called **temporal variables**. They abstract the
“position” and the “speed” of _P_: each time _P_ progress, it updates
the associated variable, which corresponds to a predefined progression
in the position of _P_. The “tempo of the variable” is computed using
the same algorithm used by the listening machine to track the tempo of
the musician. 

Temporal variables are declared using the `:::antescofo @tempovar`
declaration:

```antescofo
          @tempovar $v(60, 1/2),  $w(45, 1) 
```

defines two variables `:::antescofo $v` and `:::antescofo $w`. Variable
`:::antescofo $v` has an initial tempo of $60$ BPM and periodic updates
of $1/2$ beats, whereas `:::antescofo $w` has an initial tempo of $45$
BPM and expected periodic updates of $1.0$ beat.

Temporal variables are regular variables that can be used in
expressions. The value of a temporal variable is the last assigned value
or undefined (as for an ordinary variable). In addition, a temporal
variable stores the following internal information that can be accessed
at any time:

- `:::antescofo $v.tempo` represents the internal tempo of `:::antescofo
  $v` as tracked by Large's algorithm. This attribute is initialized by
  the first parameter of the `:::antescofo @tempovar` declaration.

- `:::antescofo $v.position` represents current beat position of
  `:::antescofo $v`. This attribute is initialized to 0. 

- `:::antescofo $v.frequency` represents frequency (period) of
  `:::antescofo $v`. This attribute is initialized by the inverse of the
  second parameter of the `:::antescofo @tempovar` declaration.

- `:::antescofo $v.rnow` represents relative time of `:::antescofo $v`
  
Such internal attributes can be changed at any time like regular
variables. For example:

```antescofo
            let $v.tempo := 55  // change the current tempo of $v
```



### The `:::antescofo @sync` synchronization attribute

Using temporal variables, it is possible to define *synchronization
strategies* of groups, loops, proc, etc., based on the progression
recorded by a temporal variable, using the attribute `:::antescofo
@sync` as below:

```antescofo
        group
        @sync $v  
        @target[5s] 
        {
                ; actions...
        }
```

Temporal variables can be set by the environment, cf. [setvar], allowing for
easy tracking any kind of external processes and the synchronization on
it. Temporal variables can also be used to set synchronization
coordination schemes different than that of the human musician it follows.



### Comparing score following and temporal variables

The table below compares the features offered by the score following
(listening machine) and temporal variables:


| features | score following | temporal variable `:::antescofo $v`
:---------:|:---------------:|:------------------------------------:
event |musical event detected by the listening machine|update of  `:::antescofo $v`
elementary progression|an amount corresponding to the duration of the detected event|a fixed amount specified in the `:::antescofo @tempovar` declaration
position of last event|`:::antescofo $BEAT_POS`|`:::antescofo $v.position`
current position|`:::antescofo $RNOW`|`:::antescofo $v.RNOW`
progression speed|`:::antescofo $RT_TEMPO`|`:::antescofo $v.tempo`
progression speed estimation by|Large's algorithm|Large's algorithm
position is updatable|NO|YES
progression speed is updatable|NO|YES



_Nota Bene_:

- The value assigned to a temporal variable does not matter in the
  synchronization mechanism, nor in the position and progression speed
  tracking.

- Unlike a score, each event (update) of a temporal variable
  corresponds to a progression of the same quantity.

- Contrary to score following, the tracking parameter can be updated
  directly, directly impacting the progression of the sequence of
  actions synchronized with it.

<br>








<!-- FOOTNOTE -->

