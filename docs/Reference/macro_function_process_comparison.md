<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## Macro _vs._ Function _vs._ Process

How do you chose between macros, functions and processes?

These three mechanisms can be used to abstract a code fragment and to
parameterize it by argument provided when the code fragment is reused
(at a call point). However, these three mechanisms don't have the same
benefits, flexibilities or shortcomings.


The following figure compares the three mechanisms from several points of
view (click for access the table as a PDF document). See also the paragraph
[What to Choose Between Macro, Functions and Processes] for additional
advice.

[ ![macro, function and process comparison](/Figures/macro_function_process_comparison.png) ](/Figures/macro_function_process_comparison.pdf)