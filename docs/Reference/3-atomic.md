<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




# Atomic Actions 



[Actions] are divided into [Atomic Actions] which are performed
**instantaneously** and cannot be decomposed and actions that gather
others actions and that usually take time (described in next chapter
[Compound Actions]).



In this chapter, we focus on **atomic actions** that is, actions that
take no time to be performed. _Instantaneity_ is an idealization of the
actual computation, see chapter [Synchronization] for more detailed
explanation on this abstraction.

An **atomic action** corresponds to

- an interaction with the environment through: message passing to MAX/PD
  receivers, OSC messages, or file writing,

- an assignment,

- the termination of another action,

- an internal command,

- the launching of a process or the creation of an object, 

- checking an assertion.

[Callback messages] are a way to call a function or a process using the
syntax of a message, a syntactic commodity which does not introduce a
new kind of actions.


{!BNF_DIAGRAMS/atomic_actions.html!}

(Clicking on a box gives direct access to the corresponding documentation.) 