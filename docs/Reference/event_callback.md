<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## Internal Event Callbacks


[Event Callbacks] are a way to call a function or a process each time
some internal event occurs. These internal event cannot be handled by a
predicate so the usual [whenever] mechanism cannot be used to react to
these events. 

The internal events handled are the following:

- The begining or the end of a [logical instant]: a logical instant
  corresponds to some activity triggered somewhere. A logical instant
  gather all computations that must be done simultaneously. The
  reception of an OSC message, the recognition of a musical event, the
  expiration of a delay, the reception of a Max or PD message, etc.,
  give rise to a new logical instant. It is possible to trigger a
  function or a process at the very begining of a logical instant, or at
  its end. 
  
- The entry or the exit of the [fastforward mode].

- The occurence of the _next event_ (this event can be signaled by the
  listening machine or result of a `:::antescofo
  antescofo::nextevent`command).
  
- The occurence of a _musical event_ (for all musical events).

- The occurence of a `:::antescofo antescofo::stop` command).
  
  
The handler triggered by such internal events is installed using a call
to the following system functions:

  * [@callback_start_logical_instant]
  * [@callback_stop_logical_instant]
  * [@callback_start_fastforward]
  * [@callback_stop_fastforward]
  * [@callback_next_event]
  * [@callback_all_event]
  * [@callback_stop]


The handler is any applicable value that takes two parameters. When
called, the handler is called with two arguments: the relative time
(_i.e._, the value of `:::antescofo $RNOW` at the time of call), and the
abslute time (_i.e._, the value of `:::antescofo $NOW` at the time of
call).

The previous functions install a new handler to be triggered when an
internal event occurs. They all have the same signature

```antescofo
	@callback_xxx(handler, persistence)
```

where `:::antescofo handler` is an applicable value and `:::antescofo
persistence` is a boolean value that specifies if the handler is
triggered only one time (nd then the handler is no longer active) or if
the handler is used for all occurences of the `:::antescofo xxx`
internal event. 

Notice the installation of a persistent handler for the next event,
implements the same behavior as a persistent handler for all
events. Similarly, a non persistent handler for all events achieve the
same effect of a non persistent handler for the next event.


The installation functions return a non-zero integer `:::antescofo rank`
that can be used later to remove an handler, using the following
signature:

```antescofo
	@callback_xxx(<undef>, rank)
```

Several handlers can be installed for the same internal event. They are
triggered following their installation sequence (the rank returned by
the installation functions is related to the sequence of handlers for a
given internal event).

See additional information at [@callback_xxx](/Library/Functions/callback_event/)

<br>

<!-- FOOTNOTE -->


