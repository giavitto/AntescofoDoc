<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## Argument evaluation strategies

Programming languages through the years have introduced several strategies
to determine when and how to evaluate the argument of a routine call (by
a _routine_, we mean a sequence of code that can be called with
[parameters](https://en.wikipedia.org/wiki/Parameter_(computer_programming))
like functions, procedures, methods, processes, coroutines,
_etc._). When a routine is called, its code is run with the parameters
substituted by the actual arguments provided during the call.
What differs is the exact nature of the substitution.

**Call-by-value** replaces the parameters in the routine body with the
_value_ of the arguments. In other words, the expressions in arguments
are evaluated and this value is bound to the routine's parameters seen
as local variables of the routine. Thus, the callee cannot modify a value
referred to by the caller through its argument. This strategy is the most
common strategy, used in `C`, `Scheme`, _etc._

**Call-by-reference** replaces the parameters by a _reference_ to the
value of the arguments. The reference is handled transparently in the
routine body, where parameters are used like local variables. This
strategy allows a routine to alter a value refered by the caller. This
strategy is available in `Pascal`, `Fortran`, `C++` (using a reference
for the type of the parameter), _etc._

**Call-by-name** replaces the parameters in the routine body with the
expressions given as actual arguments. This strategy is not very common
in programming languages (Algol60 introduced it), but corresponds to the
macro mechanism, with the exception of the time of evaluation
(calling-by-name is interleaved with evaluation, while macros are textual
substitutions done before any evaluation, which may lead to differences
when a routine definition can be the product of the evaluation).

This categorization is blurred by the nature of the programming
languages: declarative (_e.g._ purely functional) or imperative, the type
system (if any; consider the `C++` approach where arguments are passed by
values, but a reference type exists), the representation of values (if
all values are ‟boxed〞and implemented as pointers to boxes, the call by
value cannot be distinguished from the call by reference), and additional
mechanisms (like [lazy
evaluation](https://en.wikipedia.org/wiki/Lazy_evaluation),
[memoization](https://en.wikipedia.org/wiki/Memoization) or [futures and
promises](https://en.wikipedia.org/wiki/Futures_and_promises)), _etc._




### _Antescofo_ Evaluation Strategy

_Antescofo_ implements call-by-value for functions, process and objects;
and _Antescofo_ macros implement textual substitution, which loosely
corresponds to call-by-name.


**However**, _nota bene_ that _Antescofo_ [data structures], _i.e._ tab,
map, nim and string, are implemented through a reference to an
underlying memory area. So, for instance, a tab may be shared between
the caller and the callee even if the call is by value. This ends up with
a behavior like call-by-reference for data structures. This behavior
"call-by-value where the value is a reference" is common: this is for
instance the behavior in `C`, `C++` or `Java`.



The following example illustrates this point. Consider the following
process and variable definitions:

<html>
<div class="twocol">
<div>
</html>

```antescofo
@proc_def ::P($a, $b)
{
      let $b := $b + 1
      let $a[0] := $b
}

$t := [3, 1, 2]
$x := 10

```

</div>
<div>
&nbsp;<br>
_the corresponding data layout:_

<html>
<div style="width:60%">
</html>

![data layout](/Figures/argument_passing_1.png)

</div>
</div>
</div>
</html>  


Then, the call

<html>
<div class="twocol">
<div>
```antescofo
::P($t, $x)
print $t 
print $x
```
</div>
<div>
&nbsp;<br>
_will produce:_

      [11, 2, 1]
      10
</div>
</div>
</html>  

The evaluation is sketched in the following diagram

![call by value 1](/Figures/argument_passing_2.png)


The value of `:::antescofo $t` is copied into the parameter
`:::antescofo $a` but this value is a reference to an underlyng memory
zone where the tab is actually stored. So, when the first element of the
tab is updated in the process, this is visible in the value referenced by
`:::antescofo $t`. On the other hand, `:::antescofo $x` still refers to
the value `:::antescofo 10` because this value is fully copied into
parameter `:::antescofo $b` and a subsequent change does not affect the
value refered by `:::antescofo $x`.



Consider now the call:

<html>
<div class="twocol">
<div>
```antescofo
::P($t + [10, 20, 30], $x+1)
print $t 
print $x
```
</div>
<div>
&nbsp;<br>
_it will produce:_

      [3, 2, 1]
      10
</div>
</div>
</html>  

The values referred to by `:::antescofo $t` and `:::antescofo $x` are
untouched by the evaluation of `:::antescofo ::P` which is sketched in
the following diagram:

![call by value 2](/Figures/argument_passing_3.png)

This time, the values in the caller are unaffected by the execution of
`:::antescofo ::P`.  As a matter of fact, expression `:::antescofo $t +
[10, 20, 30]` creates a new tab and it it this new tab that is mutated
by the element assignment in the process. The consequence is that the
value refered by `:::antescofo $t` remains unaltered.



As a final example, examine the following expression:

<html>
<div class="twocol">
<div>
```antescofo
::P(@sort($t), $x)
print $t 
print $x
```
</div>
<div>
&nbsp;<br>
_it will produce:_

      [11, 2, 3]
      10
</div>
</div>
</html>  


This time, the argument `:::antescofo @sort($t)` modifies the tab
referred by `:::antescofo $t` in place and returns its argument. So the tab
processed by `:::antescofo ::P` is also the tab referred by
`:::antescofo $t`.

![call by value 3](/Figures/argument_passing_4.png)



Notice that the previous examples involve processes but the same
behavior is achieved with functions. Nota bene that scalars are never
modified through a function or a process call.