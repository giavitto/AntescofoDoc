<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## Process Creation


A process is a group of actions that is triggered dynamically. A process
call takes the following form:

```antescofo
       :: exp (a₁, a₂, ...)
```

where `:::antescofo exp` is an expression which evaluates into a [proc]
and the `:::antescofo aᵢ` are expressions that are the parameters of the
process instantiation.

A process call is a compound action: the duration of a call is the
duration of the instantiated group[^1].


Synchronization attributes, which are inherited by the
instantiated group, may be specified. However, the attribute [@exclusive] has no effect. 


Refer to the chapter [Process] for an in-depth presentation.


## Object Creation

The remarks for process creation also apply for object creation. Please
refer to chapter [Object] for a thorough presentation on that topic.



<br>

<!-- FOOTNOTES -->

[^1]: The correct implementation is in development. For the moment, a
process call appears as an atomic action.

