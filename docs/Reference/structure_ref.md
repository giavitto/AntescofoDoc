<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




# Structure of an _Antescofo_ Program

An _Antescofo_ program, or **augmented score**, is a _sequence_ of:

- macros, processes, functions, tracks, pattern and object **definitions**

- musical **event** specifications

- **action** specifications

written in a file. This file can include other files (inclusions can be
nested arbitrarily) using the
[@insert](/Reference/file_structure/index.html) and
[@insert_once](/Reference/file_structure/index.html) commands, see
section [file structure]. File inclusion does not alter the organization
of an augmented score: the interpreter sees one single flat file
resulting from the textual inclusion of all included files before any
evaluation.

{!BNF_DIAGRAMS/augmented_score_1.html!}

Events are recognized by the listening machine (described in detail in
chapter [Event Specification]). Actions, outlined in chapter [Actions]
and detailed in [Atomic Actions] and [Compound Actions], are
computations triggered upon the occurrence of an event or of another
action. Actions can be dynamically parametrised by [Expressions] and
data structures, evaluated in real-time and described in detail in the
sections [Scalar Values] and [Data Structures].

[Comments](/Reference/2-syntax#Comments) may appear anywhere and are
simply ignored by the interpreter. 



## Definitions

Definitions may appear anywhere between events and actions. This is only
for the programmer convenience: they are handled when the score is
loaded and their appearance do not alter the sequence of events, the
sequence of actions or the interleaving of events and actions. When the
sequencing of actions and events must be considered, definitions are
simply abstracted away.

{!BNF_DIAGRAMS/definitions.html!}

There are nine kinds of entities that can be defined (see the above
diagram). An entity (function, process, _etc._) must be defined before
being used. So we suggest gathering all definitions at the begining of
the program, even if they can appear anywhere in the file. That said,
the position of a process definition in the file has an impact on its
priority. See section [Action Priority].


When definitions are abstracted away, the structure of an augmented
score is better viewed as a **first sequence of actions** followed by
**reactions** made of an event followed by a sequence of actions :

{!BNF_DIAGRAMS/augmented_score.html!}



## The First Sequence of Actions

Abstracting the definitions away, an augmented score starts by an
optional sequence of actions.  Actions in this section are evaluated at
the begining of the performance, as soon as the program is launched with
a `start` or a `play` command and before the recognition of the first
musical event.

An augmented score can consists of only this first sequence of
actions. In this case, there is no musical event to recognize at all,
and the actions correspond to a synchronous and temporized program (like
a sophisticated cue list). This program is launched by the command
`start` or `play`, may wait the elapsing of delays and can react to
changes in the external environments through

- [OSC messages],

- [Whenever] watching variables set by the environement using the
  [setvar] command

- [commands] send through the Max/PD interface to the _Antescofo object_.






## Reactions: Events Triggering a Sequence of Actions

A reaction is the specification of a musical event followed optionally
by a sequence of actions. This sequence of actions is triggered by the
recognition of the musical evant in the input stream (audio or midi).

Some elements categorized here as _events_ do not act as true musical
event to recognize in the input stream but as modifiers that affect the
state of the listening machine for the recognition of the following
'real' musical events:

- `:::antescofo BPM`

- `:::antescofo variance`

- `:::antescofo tempo`

- `:::antescofo transpose`

- `:::antescofo rubato` (experimental)

- `:::antescofo napro_trace` (deprecated)

See also the section
[Elements](/UserGuide/element##simple-identifiers-antescofo-keywords-and-reference-to-the-host-environment)
in the user guide.

These elements can be abstracted away when considering the sequence of
actions linked to an event.


Events are further described in the chapter [Event Specification]. 
Actions are further described in the chapter [Actions Specification]. 




## The Sequence of Reactions

Reactions appear in an augmented score in a _sequence_. The order in
this sequence is fixed by the order of textual apparition in the file.

The score followed by the listening machine corresponds to the sequence
of musical events extracted from the sequence of reactions. However, the
[@jump] attribute can be used on musical events to evade the strict
linear ordering of events and to specify arbitrary graph between
events. In this graph a branching between events corresponds to an open
score given the choice between several possible futures. See section
[open scores].



## An Example

Consider the following score (excerpt of an actual piece):

```antescofo
        BPM 65

        // some definition
        @proc_def ::CS_solo_points($x, $y, $z, $u, $v)  { /* ... */ }

        @global $tSolo, $tabFreq

        let $tSolo := 65
        let $tabFreq := [ /* ... */ ]

        // start of score 
        NOTE D1 1/8 
        NOTE C2 1/8
        NOTE Db2 1/8
        NOTE Ab1 1/8
        NOTE A2 1/2
             Curve tSolo @grain := 0.05s
             {   $tSolo
                 {          {  $RT_TEMPO     } @type "exp"
                        1/2 { ($RT_TEMPO+30) } @type "cubic_out"
                        2/4 { ($RT_TEMPO-20) }
                 }
             }
        BPM 68
             GROUP Solo @tempo := $tSolo
             {
                            ::CS_solo_points("i3",1/2,0.08,0.5,81)
                        1/8 ::CS_solo_points("i1",1/2,0.09,0.5,91)
             }

        NOTE 0 5/2
        NOTE G1 0
        NOTE B1 0
        NOTE D2 0
        NOTE C1 1/2
        NOTE 0 1/2
        NOTE C1 1/2 
        NOTE G2 1

        @fun_def @midi2hz($x)  {$diapason * @exp(($x-69.0) * @log(2.0)/12)}

             GROUP Solo2
             {
                            ASCOtoCS_SYNTH4 c (@midi2hz($tabFreq[0]))
                            ASCOtoCS_SYNTH4 c (@midi2hz($tabFreq[2]))
                            ASCOtoCS_SYNTH4 c (@midi2hz($tabFreq[4]))
             }

        NOTE 0 1/2
        NOTE G1 0 
        NOTE B1 0
        NOTE D2 0
```


With respect to the structure of the performance, the definition of
process `:::antescofo ::CS_solo_points` and of function `:::antescofo
@midi2hz` can be abstracted away. The definitions are mandatory but they
are processed when the file is loaded, not during the performance.

The [@global] clause is also a definition introducing global
variables. Global variables are implicitly defined, so here this
definition is used only for documentation purposes, to outline that two
global variables will be used in this score.

The two `:::antescofo BPM` specifications are not real sonic events:
they alter the recognition of the following musical events.

When we abstract these definitions and event modifiers away, the
resulting score is:

```antescofo
        let $tSolo := 65
        let $tabFreq := [ /* ... */ ]

        NOTE D1 1/8 
        NOTE C2 1/8
        NOTE Db2 1/8
        NOTE Ab1 1/8
        NOTE A2 1/2
             Curve tSolo @grain := 0.05s
             {   $tSolo
                 {          {  $RT_TEMPO     } @type "exp"
                        1/2 { ($RT_TEMPO+30) } @type "cubic_out"
                        2/4 { ($RT_TEMPO-20) }
                 }
             }
             GROUP Solo @tempo := $tSolo
             {
                            ::CS_solo_points("i3",1/2,0.08,0.5,81)
                        1/8 ::CS_solo_points("i1",1/2,0.09,0.5,91)
             }

        NOTE 0 5/2
        NOTE G1 0
        NOTE B1 0
        NOTE D2 0
        NOTE C1 1/2
        NOTE 0 1/2
        NOTE C1 1/2 
        NOTE G2 1
             GROUP Solo2
             {
                            ASCOtoCS_SYNTH4 c (@midi2hz($tabFreq[0]))
                            ASCOtoCS_SYNTH4 c (@midi2hz($tabFreq[2]))
                            ASCOtoCS_SYNTH4 c (@midi2hz($tabFreq[4]))
             }

        NOTE 0 1/2
        NOTE G1 0 
        NOTE B1 0
        NOTE D2 0
```

which is composed of a first sequence of actions


```antescofo
        let $tSolo := 65
        let $tabFreq := [ /* ... */ ]         
```

followed by 17 reactions. These reactions corresponds to the following
sequence of musical events looked in the audio stream by the listening
machine

```antescofo
        NOTE D1 1/8 
        NOTE C2 1/8
        NOTE Db2 1/8
        NOTE Ab1 1/8
        NOTE A2 1/2
        NOTE 0 5/2
        NOTE G1 0
        NOTE B1 0
        NOTE D2 0
        NOTE C1 1/2
        NOTE 0 1/2
        NOTE C1 1/2 
        NOTE G2 1
        NOTE 0 1/2
        NOTE G1 0 
        NOTE B1 0
        NOTE D2 0
```

Only two reactions have actions associated to it: `:::antescofo NOTE A2
1/2` and `:::antescofo NOTE G2`.  A sequence of two actions (the curve
`:::antescofo tSolo` and the group `:::antescofo Solo`) is associated to
`:::antescofo NOTE A2 1/2`. Only one action, the group `:::antescofo
Solo2` is associated to `:::antescofo NOTE G2 1`.  The groups are
compound actions that gather together several others actions. Here they
call processes or send MAX/PD messages.