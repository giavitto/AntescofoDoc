<!- -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*  -->

{!references.ref!}
{!Library/Functions/functions.ref!}





# Internal Commands


Internal commands correspond to the MAX or PD messages accepted by the
`antescofo~` object in a patch. The “internalization” of these messages
as primitive actions makes the control of the MAX or the PD
`antescofo~` object possible from within the score itself. For example, it is
possible to switch the follower on or off when a specific musical event
is reached.

Internal commands are named

```antescofo
       antescofo::xxx
```

where the suffix `xxx` is the head of the corresponding MAX/PD message
recognized by `antescofo~`. The exaustive list of internal commands is
given below together with their arguments.



<br>

## Transport command: Controlling the Execution Flow

We first we focus on a set of very important commands used to navigate
the score. They control the execution flow of an _Antescofo_ program
which is critical in reharseal or during the design phase, as they
evades the linearity of the score. See also the section [Transport,
tempo computation, and others
practicalities](/Reference/tempo_transport).

---

[antescofo::start] (no argument)
:   Sends initialization actions (before first event) and wait for follower. 

--- 

[antescofo::play]  (no argument)
:   Simulates the score (instrumental+electronics) from the beginning until
   the end or until stop.

---

[antescofo::startfromlabel] `string` 
[antescofo::startfrombeat] `float`
:   Stops the current execution (if needed) and then executes the score
   from current position to position specified by the argument (a label
   given by a string or a position in beats) in accelerated more WITHOUT
   sending messages BUT triggering all actions. Then waits for follower
   (or user input) right before this position.
 
---

[antescofo::gotolabel] `string`
[antescofo::gotobeat] `float`
:   Similar to _startfrom..._ but the actions from the begining of the
    score up to the target are not trigered.

---

[antescofo::scrubtolabel] `string`
[antescofo::scrubtobeat] `float`
:   Stops the current execution (if needed) and then executes in accelerated
    more the score from current position to position specified by the
    argument WITH sending messages up to (and not including) the
    specified position. Then waits for follower (or user input). The
    position is given by a lable (a string) or a position in beats.
 
---

[antescofo::resetfromlabel] `string`
[antescofo::resetfrombeat] `float`
:    Stops the current execution (if needed) and then executes in
    accelerated more the score from current position to position
    specified by the argument without sending messages up to (and not
    including) the specified position. Then all active action are killed
    before waiting for the event notified by the follower (or the user
    input). The position is given by a label (a string) or a position in
    beats.
 
---

[antescofo::playfromlabel] `string`
[antescofo::playfrombeat] `float`
:    Stops the current execution (if needed) and then executes the score from
    current position to position specified by the argument in
    accelerated more WITHOUT sending messages, then PLAYs (simulates)
    the score from thereon.

---

[antescofo::playtolabel] `string`
[antescofo::playtobeat] `float`
:    Play (simulate) score from current position up to position specified by
    the argument (there is no stop nor restart from the begining. 

--- 

[antescofo::visitlabel] `string`
:    Position the system on the position specified by the argument
    without doing anything else. The target event is notified and the
    event that follows is waited by the listening machine. Notice that
    this command does not imply an implicit stop nor a fastforward mode.

---

[antescofo::nextevent] (no argument)
:    forces the follower to wait for the first event that appears after
    the current position.  The actions between the current position and
    the next event are launched.

---

[antescofo::nextlabel] (no argument)
:    forces the follower to wait for the next event that has a label. The
    actions between the current position and the next event are
    launched. The infered tempo is kept unchanged.

---

[antescofo::nextlabeltempo] (no argument)
:    same as but the elapsed time is used to adjust the tempo.

---

[antescofo::before_nextlabel] (no argument)
:    force the progression
    of the score following up-to the next label, launching the actions
    between the current point and the next label, but still waiting
    its occurence.

---

[antescofo::previousevent] (no argument)
[antescofo::previouslabel] (no argument)
:    similar to _nextevent_ or _nextlabel_ but looking backward in the score.

---

[antescofo::nextaction] (no argument)
:    forces the follower to wait for the next event that has some
    associated actions. The actions triggered between the current
    position and the new one, are launched.


---


<br>

## List of other internal commands



------


[antescofo::actions] `string`
:    inhibits (string = `"off"`) or enables (string = `"on"`) the launch
    of actions on events recognition. This is different from muting a
    [track]: muting a track inhibit the sending of some Max/PD messages
    while this command inhibit the performance (execution) of all actions.

------

[antescofo::add_completion_string] `string`
 :    defines a new completion
    string to a running and connected [Ascograph] (Mac only).

------

[antescofo::analysis]  `int` `int`
:    specifies a new FFT windows length and a new hop size for the audio analysis.

------

[antescofo::ascographwidth_set] `int`
:    defines the width of the [Ascograph] window. Look also some
    parameters of the interface in the inspector of the _Antescofo_ MAX
    object.

------

[antescofo::ascographheight_set] `int`
:    defines the height of the [Ascograph] window. Look also some
    parameters of the interface in the inspector of the _Antescofo_ MAX
    object.

------

[antescofo::ascographxy_set] `int` `int`
:    defines the _x_ and _y_ position of the window. Look also some
    parameters of the interface in the inspector of the_Antescofo_ MAX
    object.

------

[antescofo::asco_trace] `int`
:    turn on (int=`1`) or off (int = `0`) the [Ascograph] tracking of the
    score position when is _Antescofo_ is running in following mode.

------

[antescofo::bachscore] `string`
:    send to the receivers recv a sequence of message tha descrive the
    current Anetscofo score. The format of the messages is understood by the 
    [bach.roll](http://www.bachproject.net/) object in Max which will build a
    piano roll representation of the score. 


------

[antescofo::bpmtolerance] `float`
:    reserved command.

------

[antescofo::bypass_temporeset] `"on"` _or_ `"off"` _or_ `int` (`0` or `1`)
:    When musical events are expected and they are not notified, the
    current tempo is set to zero after 8 not-received events, which
    stops all computations. The command `:::antescofo
    antescofo::bypass_temporeset "on"` disable this behavior, meaning
    that the current tempo keep its last value when no event are notified.

---

[antescofo::calibrate] `int`
:    turn calibration mode on (`1`) or off (`1`).

------

[antescofo::clear] (no argument)
:    clear all preloaded scores.

------

[antescofo::decodewindow] `int`
:    changes the length of the
    decoding window used in the inference of the position.

------

[antescofo::filewatchset] `string`
:    (Max only) watch the file whose path is given by the argument, to
    reload it when changed on disk (the file is suppose dto be the
    source of the current score).

------

[antescofo::gamma] `float`
:    change an internal parameter of the score following engine.

------

[antescofo::get_current_score] (no argument)
:    send to a running and connected the source of the current score.

------

[antescofo::get_patch_receivers] (no argument)
:    reserved command.

------

[antescofo::getlabels] (no argument)
:    send to the first outlet the list of events label. Correspond to the
    `get_cues` message accepted by the object.


------

[antescofo::harmlist] `float …` (a list of floats corresponding to
    a vector)
:      specify the list of harmonics used in the audio observation.

------

[antescofo::info] (no argument)
:    print on the console output various informations on the version, and
    the current status of the object. Usefull when reporting a problem.

------

[antescofo::killall] (no argument)
:    abort all running processes and actions.

------

[antescofo::mode] `int`
:    reserved command.

------

[antescofo::musician_tempo] `nim`
:  overhide the tempo specified in the score (using BPM statement) by a
  tempo curve defined by the NIM given in argument.

------

[antescofo::mute] `string`
:    mute (inhibits the sending) of the messages matched by a track.

------

[antescofo::nofharm] `int`
:    number of harmonics to compute for the audio analysis.

------

[antescofo::normin] `float`
:    set some internal parameter of the score following.

------

[antescofo::obsexp] `float`
:    set some internal parameter of the score following.

------

[antescofo::pedal] `int`
:    enables () or disables () the use of a resonnance model in the audio
    observation.

------

[antescofo::pedalcoeff] `float`
:    attenuation coefficient of the pedal model.

------

[antescofo::pedaltime] `float`
:    attenuation time of the pedal model.

------

[antescofo::piano] `int`
:    turn the follower in the piano mode (corresponding to a set of
    parameters adjusted to optimize piano observation).

------

[antescofo::playstring] `string`
:    interpret the string argument as an sequence of actions and perform
    it. This command is used by to evaluate a text region highlighted in
    the editor. It can be used to evaluate on-the-fly actions that have
    been dynamically generated in an improvisation scenario. Due to OSC
    limitation, the string size cannot be greather than 1000
    characters. But see next command.

------

[antescofo::playstring_append] `string`
:    this command is used to evaluate on-the-fly a string of size
    greather than 1000 characters.  The sequence of actions must be
    broken in a sequence of strings each of size less than 1000. Each of
    these pieces are processed in turn by this command, except for the
    last one which uses

------

[antescofo::preload] `string` `string`
:    preloads a score and store it under a name (the second argument) for
    latter use.

------

[antescofo::preventzigzag] `string`
:    allow or disallow zig-zag in the follower. In non-zig-zag mode, the
    default, the follower infer only increasing positions (except in the
    case of a jump). In zig-zag mode, the follower may revise a past
    inference. This ability does not impact the reactive engine : in any
    case, actions are performed without revision.

------

[antescofo::printfwd] (no argument)
:    output the formatted print of the current program in a new window.

------

[antescofo::printscore] (no argument)
:    output the formatted print of the current program in a new window.

------

[antescofo::read] `string`
:    loads the corresponding score from the file specified by the
    argument.

------

[antescofo::report] (no argument)
:    reserved command

------

[antescofo::reset_musician_tempo] 
:  reset the tempo specification in the score to the specification given
  by the BPM statement.

------

[antescofo::scrubtolabel] `string`
:    Executes the score from current position to the event specified by
    the argument, in accelerated more WITH sending messages. Waits for
    follower (or user input) right before this position.

------

[antescofo::scrubtobeat] `string`
:    Executes the score from current position to the position given by
    the argument, in accelerated more WITH sending messages. Waits for
    follower (or user input) right before this position.

------

[antescofo::setvar] `string` `numeric`
:    [antescofo::setvar] `string` `string`
:    [antescofo::setvar] `string` $a_1$ $a_2$ $\dots$
:    assign the value given by the second argument to the variable named
    by the first argument. If they are more than two arguments, the
    $a_i$ are packed into a tab. Using this command, the environment may
    notify _Antescofo_ some information. For instance, _Antescofo_ may
    react because the variable is in the logical condition of a
    [whenever]. See [external assignment].

------

[antescofo::score] `string`
:    loads the corresponding score (an alias of ).

------

[antescofo::static_analysis] (no argument)
:    reserved command

------

[antescofo::stop] (no argument)
:    stop the follower and abort the runing actions.

------

[antescofo::suivi] `int`
:    enables or disables the follower. Even if the follower is off,
    actions may run and can be spanned and interaction with the
    environment may happens through and .

------

[antescofo::tempo] `float`
:    specify an arbitrary tempo.

------

[antescofo::tempoinit] `int`
:    reserved command.

------

[antescofo::temposmoothness] `float`
:    adjust a parameter of the tempo inference algorithm.

------

[antescofo::tune] `float`
:    set the tuning base (default $440.0$Hz).

------

[antescofo::unmute] `string`
:    unmute (allows the sending) of the messages matched by a track.

------

[antescofo::variance] `float`
:    set the variance parameter of the inference algorithm.

------

[antescofo::verbosity] `int`
:    specify the level of system messages emitted during execution.

------

[antescofo::version] (no argument)
:    print the version of the object and various build information on the
    console.

------




## Score-level commands

For the sake of completness, we give here the commands that are not
expressed as atomic actions but appears as a keyword at the top-level of
a score. They usually cannot be triggered through dedicated a message
and do not follow the `:::antescofo antescofo::xxx` syntax.

There are described in section [Score Statments](/Reference/event_ref#score-statement)

- [antescofo_is_in_rehearsal_mode]

- [top_level_groups_are_tight]

- [top_level_groups_are_loose]

- [tempo] on, [tempo] off

- [atempo]

- [newtempo]

- [pizzsection]

- [pizz]

- [dummysilence]

- [nosyncsection]

- [bypass_temporeset] on, [bypass_temporeset] off




<br>

## Execution mode of an internal command

Initially (before September 2022), all internal commands were performed
immediatly, i.e.as soon as they were encountered in the score. This
behavior is called the _immediate execution mode_. It may cause some
subtle problem, especially for commands that divert the current
execution flow, like most of the transport command listed above.

Since version 1.1, internal commands can be performed following 

- the _immediate mode_ (previous behavior)

- or the _delayed mode_, which postpones the command to the end of the
  currrent logical instant.

The execution mode of a command can be enforced explicitly using the
two attributes `:::antescofo @immediate` (for the immediate mode) or
`:::antescofo @post` for the delayed mode. Example:

```antescofo
    antescofo::compil  "@fib" "int" "int"  @post
    antescofo::nextevent @immediate
```

Without explicit specification, the execution mode is selected taking
into account the execution context and the command itself:

- if the command is issued from a script[^script], the default behavior
  is the immediate mode, irrespectively of the command;

- if the command is coming from the usual execution flow of the score,
  then the execution mode depends of the command itself: most of
  [transport commands](/Reference/tempo_transport/#transport) are
  postponed, and the other are immediate.

Here is the list of internal commands that are postponed by default:


<div class="twocol">

<div>

```antescofo
// N.B.: this list is subject to change
antescofo::before_nextlabel
antescofo::clear
antescofo::gotobeat
antescofo::gotolabel
antescofo::jump
antescofo::musician_tempo
antescofo::musician_tempo_from_markers
antescofo::nextevent
antescofo::nexteventwithsil
antescofo::nextfwd
antescofo::nextlabel
antescofo::nextlabeltempo
antescofo::play
antescofo::playfrombeat
antescofo::playfromlabel
antescofo::playstring
```
</div>

<div>
```antescofo
antescofo::playtobeat
antescofo::playtolabel
antescofo::preload
antescofo::previousevent
antescofo::previouslabel
antescofo::reset_musician_tempo
antescofo::resetfrombeat
antescofo::resetfromlabel
antescofo::score
antescofo::scrubtobeat
antescofo::scrubtolabel
antescofo::start
antescofo::startfrombeat
antescofo::startfromlabel
antescofo::stop
antescofo::visitbeat
antescofo::visitlabel
```
</div>
</div>

Notice that when a command is received as a Max message or an OSC
message, the immediate and postponed distinction is no longer relevant
because the commands are performed in their own logical instant. Note
also that the execution mode specified by the attribute is always
acknowledged.



<center>

<p>&nbsp;</p>
<p>&nbsp;</p>

<img alt="line divider" src="/Figures/line-divide-grayr.svg" width="35%">


## Alphabetical Listing of _Antescofo_ Internal Command

</center>


<br>

Command names in square brackets are either reserved for internal use
or awaiting documentation. This listing is automatically produced by the
command [antescofo::libraryinfo]. 


{!Reference/internal_command.list!}







<!-- FOOTNOTE -->

<br>&nbsp;<br>

[^script]: _Scripts_ are an undocumented mechanism presents only in
standalone version of antescofo and used to drive an antescofo
execution by interleaving transport commands with other actions.





