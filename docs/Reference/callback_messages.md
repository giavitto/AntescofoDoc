<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## Simple Callback Messages


[Callback messages] are a way to call a function or a process using the
syntax of a [message], a syntactic commodity which does not introduce a
new kind of actions.

A `:::antescofo @message_def` construction is used to specify a receiver
and to associate to it a process or a function:

```antescofo
	@message_def recv1 := @cbk
```

The declaration is global: it can be appear anywhere in the score. Once
specified, each message with receiver `:::antescofo recv1` will call
function `:::antescofo @cbk` instead of being dispatched to the Max or
PD host (the default behavior).

Message's arguments becomes parameters of the function call:

- If there is to few arguments w.r.t. to the function parameters, the
  remaining parameters are set to `:::antescofo <undef>`.

- If there is to much arguments w.r.t. to the function parameters, all
  arguments in excess are gathered in a tab which becomes the last
  parameters.
  
For example, with `:::antescofo @cbk` defined as 

```antescofo
	@fun_def @cbk($x, $y, $z) { print "x=" $x "y=" $y "z=" $z }
```

the following messages gives:

```antescofo
	toto 1        ⟶  x= 1 y= <undef> z= <undef>
	toto 1 2      ⟶  x= 1 y= 2 z= <undef>
	toto 1 2 3    ⟶  x= 1 y= 2 z= 3
	toto 1 2 3 4  ⟶  x= 1 y= 2 z= TAB[3, 4]
```


<br>

### Callbacks

The right hand side of a `:::antescofo @message_def` specification must
be an _applicable value_: a process, a function, an object with a method
apply, a partially applied function, a nim, a tab or a map. However, to
produce a visible effect, sending a callback message must imply some
side-effect, which means that only process and (partially applied)
function are relevant. For instance, a function may send Max messages.

The right hand side must be a closed expression or the @-name of a
function.


### Possibles Uses

This feature has been introduced to ease the transitionning of Max
implemented functionnalities to Antescofo implementations. Using this
features, messages sent to Max can be rerouted to an Antescofo process
achieving the same behavior, without altering the main Antescofo
program.

Another possibel use is to trap any control message to a sampler, to
adapt its behavior during the [fastforward
phase](/Reference/tempo_transport/#transport) of a transport command.



<br>

## Full Callback Messages

The full specification of callback messages brought additional
expressiveness. 

### Specification of receives through a regular expression

It is possible to define the receiver of a `:::antescofo @message_def`
using a regular expression[^1] rather than a symbol. The callback is then
defined for all receivers that match the regular expression. For example

```antescofo
	@message_def "t[ao]t[au]" := ::P
```

will call the process `:::antescofo ::P` instead of sending a message
for the four receivers that are matched by the regular expression:
`:::antescofo tata`,  `:::antescofo tatu`, `:::antescofo tota`, and
`:::antescofo totu`.


### Optional Specification of a Context

Messages can appear in different contexts which modulate its
interpretation:

1. the message can be _muted_, in which case is it not actually sent,

- the execution can be done in _fastforward mode_, which implies that
  the messages are not sent, 

- the receiver may correspond to an _osc receivers_,

- the receivers may correspond to an _output file_,

- or the receiver is really interpreted as a Max or PureData message
  that must be sent.

The `:::antescofo @message_def` construct accepts the specification of a
context after the specification of the receiver. When a context is
specified, the right hand side of the definition is substituted for the
message only in the indicated context. 

The contexts of a callback message are given as a bracketed list of
symbols: 

1. `:::antescofo muted` is used to indicate a callback to use when the
   message is muted (instead of not sending it)
   
* `:::antescofo fastforward` or `:::antescofo ffw` for message during a
  fastforward mode
  
* `:::antescofo osc` for receivers that denote an osc output command

* `:::antescofo outfile` or `:::antescofo out` for receivers that
  denotes output in a file

* `:::antescofo msg` or `:::antescofo message` for ordinary message sent
  to the host. 
  
  
  
### Possible Use: a better handling of transport function
  
During a rehearsal, it is essential to have the ability to start the
execution of the score at a given label. This can be done by using a
[transport command], for example _startfromlabel_.  In this case, the
execution of the score is started in fastforward mode up to the label
and then the normal execution is resumed.

This approach is adequate when the computation at the other end of the
messages send by _Antescofo_ are “stateless”. As a matter of fact,
messages are not sent during the fastforward mode. However, when the
computation has a state, we need to send the relevant messages to update
this state correctly, even in fastforward mode.
  
It can be achieved by definng a computation to trigger specifically
during the fastforward mode:

```antescofo
	@message_def toto [ffw] := @toto_in_ffw_mode
```

replace the “non sending” of the `:::antescofo toto` message during a
fastforward mode by a call to  the function `:::antescofo
@toto_in_ffw_mode`. 

This mechanism can be used to bypass the message sending inhibition in
the fastforward mode: it is enough to define a function `:::antescofo
@toto_in_ffw_mode` that actually send the message.

There is however a difficulty: the substituted function or process is
run also in fastforward mode and so cannot send itself a message. This
difficulty can be alleviated using the `:::antescofo @send_message()`
function which send a message irrespectively of a specific context. 

For example, with this definition

```antescofo
	@fun_def @toto_in_ffw_mode($receiver, $args) { @send_message($receiver, args) }
```

the previous callback bypasses the silencing of messages during the fastforward
mode to send specifically the `:::antescofo toto` messages. 



<br> 

An example of the correct management of sample playback relying on
[callback events] and [message callbacks] is given
[here](/Library/HowTo/SampleManagement/samplemanagement)


<br>

<!-- FOOTNOTE -->




[^1]: The syntax used to define the regular expression follows the posix
extended syntax as defined in IEEE Std 1003.2, see for instance [regular
expression on
Wikipedia](http://en.wikipedia.org/wiki/Regular_expression).
