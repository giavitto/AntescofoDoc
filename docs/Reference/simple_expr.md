<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}


## Simple Expressions


Simple expressions are divided into several categories


### Constant Values


#### Boolean Constants

{!BNF_DIAGRAMS/bool_expr.html!}


#### Numeric Constants

Integers, decimals and scientific notation can be used to define a
numeric constant.

{!BNF_DIAGRAMS/constant_expr.html!}



### Data Structure Definition

#### Tab Definition by Enumeration

{!BNF_DIAGRAMS/tabdef_expr.html!}


#### Tab Definition by Comprehension

{!BNF_DIAGRAMS/tabcomprehension_expr.html!}


#### Map Definition

{!BNF_DIAGRAMS/map_expr.html!}


#### Nim Definition

`cexp` means [closed expression]


**Continuous NIM**

{!BNF_DIAGRAMS/nim_expr.html!}

**Discontinuous NIM**

{!BNF_DIAGRAMS/dinim_expr.html!}



### Tab Access

{!BNF_DIAGRAMS/tab_expr.html!}



### Variables and Variables Management

{!BNF_DIAGRAMS/var_expr.html!}



### Infix Unary Expressions

{!BNF_DIAGRAMS/unary_expr.html!}



### Infix Binary Expressions

Arithmetic, relational and logical binary operators.

{!BNF_DIAGRAMS/binary_expr.html!}



### Conditional

{!BNF_DIAGRAMS/conditional_expr.html!}


### Infix Predicates

{!BNF_DIAGRAMS/predicate_expr.html!}



### Function Application and Process Call

{!BNF_DIAGRAMS/apply_expr.html!}



### Action As Expression


{!BNF_DIAGRAMS/action_expr.html!}




<br>


<center>
[Returns to chapter Expression](/Reference/6-expression#simple-expressions)
</centre>