<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}

## Activating OSC

If you want Antescofo to receive and send OSC messages, you should send it the Max/MSP or Puredata object the message `ascographcomm 1`.

Other messages can be useful:

- `incomingosc [1|0]`
- `IncmingOscPort [port]` port from which receiving OSC messages
- `ascographconf [host] [port]` host and port to send message to

## OSC messages sent and received by Antescofo

_Antescofo_ sends and receives OSC messages during its execution, in
addition to the user-defined [OSC messages].  You can use
them to monitor an Antescofo execution.

### Messages sent by _Antescofo_


All messages are prefixed with `/antescofo/`.

#### Messages for the Ascograph window

- `width w`
- `height h`
- `positionxy x y` where `x:int` and `y:int`
- `hidetoppanel`
- `showtoppanel`
- `patch_receivers strs` where `strs: string vector`. Used for autocompletion.
- `quit`

#### Messages to send the score from Antescofo by chunks

- `loadscore finalname` where `finalname: string`
- `preloadscore fname pname` where `fname: string` and `pname: string`
- `startpreloaded p_name` where `p_name: string`
- `clear_score nb_chunks` where `nb_chunks: int`
- `current_score_append n_chunk data` where `n_chunk:int` and `data: string`

#### Information about execution

- `stop`
- `event_beatpos pos` where `pos: int`
- `rnow v` where `v:double`
- `tempo v` where `v:double`
- `pitch v` where `v:double`

#### Trace messages (each time an action is fired)

They are built a follows:

`action_trace action_name type fathername now rnow message`

`action_name` is the label of the action.

Possible types are:

- `message`
- `abort`
- `abortof`
- `assignment`
- `local_assignment`
- `tab_assignment`
- `osc_recv`
- `osc_off`
- `osc_on`
- `open_file`
- `close_file`
- `internal_command`
- `conditional`
- `switch`
- `group`
- `loop`
- `for`
- `whenever`
- `curve`
- `DataCurve`
- `patch activation`

If the action has no parents, `fathername` is `Top_level`. Otherwise, it
is the label of the parent's action.  `message` is a value that depends
on the type of the action. For instance, for an assignement, it is the
assigned value.

### Messages received by _Antescofo_

_Antescofo_ processes OSC messages with internal commands, the same as
for MAX/MSP and Puredata[internal commands].
