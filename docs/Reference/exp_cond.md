<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




# Operators and Predefined Functions 


The main operators and predefined functions are outlined in the
following chapters together with the main data types involved. Here we
sketch some operators and functions that are not linked to a specific
type. The predefined functions are listed in annex [Library].


## Conditional Expression

An important operator is the conditional *à la C&nbsp;* :

```antescofo 
          (cond ? exp₁ : exp₂)
```

returns the value of the expression `:::antescofo exp₁` if the
expression `:::antescofo cond` evaluates to `:::antescofo true` and else
`:::antescofo exp2`. The parentheses are mandatory.

As usual, the conditional operator is a special function: it does not
evaluate all of its arguments. If `:::antescofo cond` is true, only
`:::antescofo exp₁` is evaluated, and similarly for false and
`:::antescofo exp₂`.

In the body of a function, a conditional can be written using the usual
syntax:

          if (cond) { exp₁ } else { exp₂ }

see chapter [Functions].

Beware not to confuse the [conditional action] and the conditionnal
expression presented here. The body of the former is a sequence of
actions; the latter, an expression. The former does not return a value,
contrary to the latter.


## Functions `:::antescofo @empty` and `:::antescofo @size`

The predicate [@empty] returns true if its argument is an empty [map],
[tab] or [string], and false otherwise.

Function [@size] accepts any kind of argument and returns:

- for aggregate values, the “size” of the arguments; that is, for a
    [map], the number of entries in the dictionary, for a [tab] the
    number of elements, for a [nim], the dimension of the nim and for a
    [string] the number of characters in the string.

- for scalar values, [@size] returns a strictly negative number. This
    negative number depends only on the type of the argument, not on the
    value of the argument.


<br>

## Alphabetical Listing of _Antescofo_ Predefined Functions

{!Library/Functions/functions.list!}





<!-- FOOTNOTE -->

