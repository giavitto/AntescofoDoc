<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}





## Assertion 

The action

```antescofo

    @assert expression
```

checks that the result of an expression is `true`. If not, a message
ginvin the location of the faulty assertion is emitted. If the argument
evaluates to true, the assertion does nothing.

This action is provided as a facility for debugging and testing,
especially with the standalone version of _Antescofo_. If the standalone
runs in strict mode, then the entire program is aborted three seconds
after the emission of the localization. Abort never happens in the Max
or PD version.

