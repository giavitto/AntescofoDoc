<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}



# NotionS of TIME in _Antescofo_
 
<br>

<div width="66%";>
  <center>
    <img alt="image of clocks and metronoms" src="/Figures/metronome-montre.png" width="66%">
  </center>
  <div align="right" style="margin-left: 45%;">
    <i>Real musical time is only a place of exchange and coincidence between
    an infinite number of different times.</i>
    <br>
    Gérard Grisey
  </div>
</div>
<br>



_Antescofo_ claims to be a _strongly timed_[^StronglyTimed]
computer music programming language, which means that:

- time is a first class entity in the language, not a side-effect of the
  computations performance, making time itself both controllable and
  explicitly computable;

- _when_ a computation occurs is precisely specified and
   formalized[^theseJose] guaranteeing behavior predictability,
   temporal determinism and temporal precision;

- the behavior of an _Antescofo_ program is free from underlying
  hardware timing and nondeterministic scheduling in the operating
  system.

Several notions of time are at work in _Antescofo_.
<!-- -->
The composer defines _potential temporal relationships_ between
musical entities in an augmented score.
<!-- -->
These relationships are expressed through the temporal relationships
between _Antescofo_ actions and between actions and musical events.
<!-- -->
During the performance, the potential temporal relationships specified
by the composer become _actual_ relationships through the realization of musical events
by the musician and the computation of electronic actions by the
computer.
<!-- -->
A unique feature of _Antescofo_ is that the composer is able to specify
some constraints between the potential and the actual temporal
relationships.


<br>

<center>
![several layer of time in Antescofo](/Figures/several_time.png){: width="85%"}
<br>
<small>
Several “layers of time” at work in _Antescofo_. What depends of the
composer/human performer is in blue; what depends of the computer is in
gray. Composers can express constraints between the potential timing expressed in the score and the actual timing of the performance. These constraints apply to the actual timing of the electronic actions.
</small>
</center>


<br>

This chapter presents the rich model of time provided by the DSL and
the constraints that can be expressed between the potential and the
actual timing. The two following pages do not regard specific technical
or syntactic aspects of _Antescofo_, but they do contain essential ideas
about the language's use of time:

- [the manufacturing of time] elaborates on the temporal notions that
  organize the computations of actions;

- [the fabric of time] elaborates on the relationships between the
  potential timing of musical events expressed in the score, the actual
  timing of performer's events and the actual timing of the actions
  during the performance.




<br>

<!-- FOOTNOTE -->

[^StronglyTimed]: [ChucK] has introduced the term _strongly timed_ to
qualify programing languages that handle time explicitly as a first
class entity. This is the case in _Impromptu_, where performative time
is also considered in the context of _Live Coding_, refer to the article
[Programming with time : cyber-physical programming with
Impromptu](http://eprints.qut.edu.au/55712/1/sorensen_ow_2010.pdf). The
real-time systems community has long advocated for the inclusion of time
in the domain of discourse and not to consider it as an optimization
problem or as a quality of service problem. See for example [Motivating
Time as a First Class
Entity](http://repository.upenn.edu/cis_reports/288) (1987). The
reference [Computing Needs
Time](http://digitalassets.lib.berkeley.edu/techreports/ucb/text/EECS-2009-30.pdf)
is a documented presentation of the problematic.



[^theseJose]: Cf. for instance [José Echeveste's PhD
thesis](https://tel.archives-ouvertes.fr/tel-01196248/)

