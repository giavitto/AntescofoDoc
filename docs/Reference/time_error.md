<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}


# Missed Event Errors Strategies

<center>
![fragment of a clock mechanism](/Figures/entete_error.png){: width="65%";}
</center>



Some but not all of the errors during the performance (such as events
missed by the performer, events that weren't detected by the listening
maching, or events that the listening machine mistakenly recognizes) are
handled directly by the listening modules. The critical safety of the
accompaniment part is reduced to the handling of missed events (whether
missed by the listening module or human performer):

- In some automatic accompaniment situations,
one might want to dismiss associated actions to a missed event if the
scope of those actions does not bypass that of the current event at
stake.

- On the contrary, in many live electronic situations such actions
might be initialized for future actions to come.

It is the responsibility of the composer to select the right behavior by
attributing relevant *scopes* to accompaniment phrases and to specify,
using an attribute, the corresponding handling of missed events.

An action is said to be **local** if it should be dismissed in the absence
of its triggering event during live performance; and accordingly it is
**global** if it should be launched in priority and immediately if the
system recognizes the absence of its triggering event during live
performance.

Once again, the choice of an action being local or global is given to
the discretion of the composer or arranger, through the specification of
the `:::antescofo @local` or `:::antescofo @global` attribute. By
default (_i.e._ without explicit specification), the actions at
top-level are tagged `:::antescofo @global` and error behavior are
inherited from the enclosing group.





## Combining Synchronization and Error Handling.

The combination of the synchronization attributes and error handling
attributes for a group of accompaniment actions gives rise to four
distinct situations. Figure below attempts to showcase these
four situations for a simple hypothetical performance setup.

Each combination corresponds to a musical situation encountered in
authoring of mixed interactive pieces:

- [@local] and [@loose]: A block that is both local and loose correspond
    to a musical entity with some sense of rhythmic independence with
    regards to synchrony to its counterpart instrumental event, and
    strictly reactive to its triggering event onset (thus dismissed in
    the absence of its triggering event).

- [@local] and [@tight]: Strict synchrony of inside actions whenever
    there’s a spatial correspondence between events and actions in the
    score. However, actions within the strict vicinity of a missing event
    are dismissed.  This case corresponds to an ideal concerto-like
    accompaniment system.

- [@global] and [@tight]:: Strict synchrony of corresponding actions and
    events while no action is to be dismissed in any circumstance. This
    situation corresponds to a strong musical identity that is strictly
    tied to the performance events.

- [@global] and [@loose]: An important musical entity with no strict
    timing in regards to synchrony. Such an identity is similar to integral
    musical phrases that have strict starting points with *rubato* type
    progressions (free endings).

The Antescofo behavior during an error case is shown in figure below.
In this example, the score is assumed to specify four consecutive
performer events ($e_1$ to $e_4$) with associated actions gathered in a
group. Each action is aligned with an event in the score. The four
groups correspond to the four possible combinations of two possible
synchronization strategies with the two possible error handling
attributes. This diagram illustrates the system behavior in case event
$e_1$ is missed and the rest of events detected without tempo
change. Note that $e_1$ is detected as missed (in real-time) once of
course $e_2$ is reported. The signaling of the missing $e_1$ is denoted
by $\bar{e_1}$.


![error management](/Figures/erreurs.png)



To have a good understanding of the picture notice that:

-   An *action* ($a_i$), associated with a delay, can be an atomic
    action, a group, a loop or a curve.

-   The *triggers*, defining when an action is fired (*i.e.*, at an
    event detection, at another action firing, at a variable update…),
    are represented with plain arrows in the figure and detail mainly
    the schedule of the next action delay or the direct firing of
    an action. A black arrow signals a normal triggers whereas a red
    arrow is for the error case (*i.e.*, a missed, a too late or a too
    early event).




**Remarks:**

A sequence of actions following an event in an score corresponds to a
phantom group with attributes **@global** and **@loose**. In other
words, the two following scores are similar.

<html>
<div class="twocol">
<div>
</html>
```antescofo
       NOTE C3 2.0        
          d₁ action₁
          d₂ group G1
          {
              action₂
          }
       NOTE D2 1.0


```
<html>
&nbsp;<br>
&nbsp;<br>
</div>
<div>
</html>
```antescofo
         NOTE C3 2.0    
         Group @global @loose
         {
            d₁ action₁
            d₂ group G1
            {
               action₂
            }
         }
         NOTE D2 1.0
```
</div>
</div>


During a performance, even in case of errors, if an action has to be
launched it is fired at a date which is as close as possible to
the date specified in the score. This explains the behavior of a group
that is [@global] and [@loose] when its event trigger is recognized as
missed. In this case, the actions that are still in the future are
played at their “right” date, while the actions that should have been
triggered are launched immediately (as a tight group strategy).

In the previous example, we remark delay variations ($a_2$ is directly
fired for the @loose @global case and not 1.0 after $a_1$). This ’tight’
re-scheduling is important if the $a_2$ action has a delay of 1.10, the
action should effectively be fired at 0.10 beat after $a_1$ (next
figure):


<html>
<div class="twocol">
<div>
</html>
```antescofo
      NOTE C3 1.0 e1
        group G1 @global @loose
        {
                 a₁
            1.10 a₂
            1.0  a₃
            1.0  a₄
        }        
         
      NOTE D2 1.0 e2
      NOTE D2 1.0 e3
      NOTE D2 1.0 e4
```
<html>
</div>
<div>
&nbsp;<br>
<img alt="loose global" src="/Figures/loose_global.png">
&nbsp;<br>
&nbsp;<br>
</div>
</div>
</html>



<!-- FOOTNOTE -->

