<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->


{! references.ref !}
{! Library/Functions/functions.ref !}



# Lexical Elements of an Antescofo Score


Elements of the language can be categorised into six groups,
corresponding to various constructions permitted in the language:

- **Comments**: Any text starting by a semi-colon `:::antescofo ;` or
    `:::antescofo //` is considered as comment and ignored by parser
    until the end of line (inline comment).

    Block (multi-line) C-Style comments starting with `/*`
    and ending with `*/` are allowed.

- **Keywords**: are reserved words introducing either Events or Action
    constructions. Examples include `Note` (for events) and `Group` (for
    compound actions).

- **Simple identifiers**: denote Max or PD receivers. They are also used
    to specify the label of a musical event or the label of an action.

- **$-identifiers**: are words that start with `$` character. They
  correspond to user-defined variables or parameters in functions,
  processes, object or macro definitions.

- **::-identifiers:** words starting with `::`, `obj::`, `pattern::` or
  `track::` corresponding respectively to processes, objects, patterns
  or tracks.

- **@-identifiers**: are words starting with a `@`. They eiher
  introduce a new definition or denote predefined and user-defined
  functions, user-defined macros, or action or event attributes.


User defined score elements like macros, processes, objects, tracks and
functions can only be used after their definition in the score. We
suggest putting them at the beginning of the file or to put them in a
separate file using the `@insert` command. They will be discussed in
later chapters.


### Case-Sensitive and case-Unsensitive Identifiers

Identifiers are sometimes case-sensitive and sometimes case
unsensitive. The rule is simple: reserved keyword or predefined
identifiers are case unsensitive. User introduced identifiers and
predefined function names are case sensitive.



## Comments 


Block comments are in the C-style and cannot be nested:
```antescofo
          /*  comment split
              on several lines
          */
```          
Line-comments are in the C-style and also in the Lisp style:
```antescofo
          // comment until the end of the line 
          ; comment until the end of the line      
```

In this document, we use _railroad diagrams_ to give a more precise
description of the syntax. The railroad diagram specifying comment's
syntax is:

{!BNF_DIAGRAMS/bnf_comment.html!}

Comments are ignored by the interpreter but not suppressed in
normal processing. That is, they act usually as separators, _i.e._

```antescofo
      $x/**/y
```

is read as two consecutive tokens `$x`and `y`. However, during
macro-expansion comments are suppressed (not ignored) in the textual
expansion: in a macro-expansion, the previous fragment is read as only
one token `$xy` by the interpreter (see chapter [Macros]).




## Indentation


Tabulations are handled like white spaces. Columns are not meaningful so
you can indent program as you wish. **However**, some constructs must end
on the same line as their “head identifier”: event specification,
internal commands and *external actions* (like Max message or OSC
commands).

For example, the following fragment raises a parse error:
```Antescofo
          NOTE
          C4 0.5
          1.0s print
               "message to print"
```
(because the pitch and the duration of the note does not appear on the
same line as the keyword `NOTE` and because the argument of `print` is
not on the same line). But this one is correct:
```Antescofo
          Note C4 0.5 myLab
          Note C4 0.5 "some label with white space used to document the score"
          1.0s
          print "this is a Max message (to the print object)" 
          print "printed 1 seconds after the event Note C4..."
```
Note that the first 'print' message is indented after the specification
of its delay `1.0s` but ends on the same line as its “head identifier”,
achieving one of the customary indentations used for cue lists.


#### Splitting a line

A backslash before an end-of-line can be used to specify that the next
line must be considered as the continuation of the current line. It allows
for instance to split the list of the arguments of a message on several
physical rows:
```Antescofo
          print "this two" "messages" "are equivalent"
          print "this two" \
                "messages" \
                "are equivalent"
```



## _Reserved Keywords_

Reserved keywords can be divided in two groups:

- **Event Keywords** including `:::antescofo NOTE`, `:::antescofo
  CHORD`, `:::antescofo TRILL`, `:::antescofo BPM`, _etc._, introduce
  musical events (see chapter [Events]) and are used to describe the
  music score to be recognised.

- **Action Keywords**, such as `:::antescofo GROUP`, `:::antescofo LOOP`
  and more, specify computations that can be instantaneous ([Atomic
  actions]) or *containers* for other actions that have a duration
  ([Compound actions]).

The current list of reserved keywords is :

<html>
<blockquote>
<div class="threecol">
<!-- A -->
	<div class="kr">
	abort &nbsp;
	action &nbsp;
	and &nbsp;
	at &nbsp;
	</div>
<!-- B -->
	<div class="kr">
	before &nbsp;
	bind &nbsp;
	bpm &nbsp;
	</div>
<!-- C -->
	<div class="kr">
	case &nbsp;
     	chord &nbsp;
	closefile &nbsp;
	curve &nbsp;
	</div>
<!-- D -->
	<div class="kr">
	do &nbsp;
	during &nbsp;
	</div>
<!-- E -->
	<div class="kr">
	else &nbsp;
	event &nbsp;
	expr &nbsp;
	</div>
<!-- F -->
	<div class="kr">
	false &nbsp;
	forall &nbsp;
	</div>
<!-- G -->
	<div class="kr">
	gfwd &nbsp;
	group &nbsp;
	</div>
<!-- H -->
	<div class="kr">
	hook &nbsp;
	</div>
<!-- I -->
	<div class="kr">
	if &nbsp;
	imap &nbsp;
	in &nbsp;
	</div>
<!-- J -->
	<div class="kr">
	jump &nbsp;
	</div>
<!-- K -->
	<div class="kr">
	kill &nbsp;
	</div>
<!-- L -->
	<div class="kr">
	let &nbsp;
	lfwd &nbsp;
     	loop &nbsp;
	</div>
<!-- M -->
	<div class="kr">
	map &nbsp;
	ms &nbsp;
	multi &nbsp;
	</div>
<!-- N -->
	<div class="kr">
	napro_trace &nbsp;
	note &nbsp;
	</div>
<!-- O -->
	<div class="kr">
	of &nbsp;
	off &nbsp;
	on &nbsp;
	openoutfile &nbsp;
	<br>
	oscoff &nbsp;
	oscon &nbsp;
	oscrecv &nbsp;
	<br>
	oscsend &nbsp;
	</div>
<!-- P -->
	<div class="kr">
	parfor &nbsp;
	patch &nbsp;
	port &nbsp;
	</div>
<!-- S -->
	<div class="kr">
	s &nbsp;
	start &nbsp;
	state &nbsp;
	stop &nbsp;
	<br>
	switch &nbsp;
	symb &nbsp;
	</div>
<!-- T -->
	<div class="kr">
	tab &nbsp;
	tempo &nbsp;
	transpose &nbsp;
	trill &nbsp;
	true &nbsp;
	</div>
<!-- U -->
	<div class="kr">
	until &nbsp;
	</div>
<!-- V -->
	<div class="kr">
	value &nbsp;
	variance &nbsp;
	</div>
<!-- W -->
	<div class="kr">
	whenever &nbsp;
	where &nbsp;
	while &nbsp;
	with &nbsp;
	</div>
</blockquote>
</html>

Notice that event keywords always occur at the top-level of the text
score.  Reserved keywords are *case unsensitive*, that is,

<center>
`:::antescofo note` &nbsp;&nbsp;
`:::antescofo NOTE` &nbsp;&nbsp;
`:::antescofo Note` &nbsp;&nbsp;
`:::antescofo NoTe` &nbsp;&nbsp;
`:::antescofo notE`
</center>

refers to the same identifier. **However** simple identifiers which are
not reserved keywords are _case sensitive_.




## _Simple Identifiers_ : *Antescofo* keywords and references to the host environment


Simple identifiers are sequence of characters accepted by one of the
three following regular expressions and that are not reserved keywords:

         [[:alpha:]_#'!~\xc3\xe2\x80-\xbf][[:alnum:]_#'/.?!+~><\-\Xc3\xe2\x80-\xbf]*

         [0-9]+[[:alpha:]_'\xc3\xe2\x80-\xbf-]{2,}[[:alnum:]_#?'!~*/+.\-\xc3\xe2\x80-\xbf]*

         [0-9]+[a-rA-Rt-zT-Z]+

`[:alpha:]` represents an alphabetic character, `[:alnum:]` represents
an alphanumeric character, _i.e._ `[0-9a-zA-Z]` and the hexadecimal
range `\xc3\xe2\x80-\xbf` represents a raisonable subset of UTF-8
accentuated characters and printable symbols.

These identifiers are much more general than the identifiers
`[:alpha:][:alnum:]*` usually recognized in programming language. For
example they can start by a number or include a relational operator like
`<`, in order to represent the various Max or PD receivers found in
current patches (identifier starting with a number are common when
working with Max poly). It is however possible to encounter Max
receivers that are not caught by the above regular expressions. In this
case, just use a `string` instead of a simple identifier, for the
receiver. For instance

```antescofo
    "max receiver with white spaces" 1 2 3
```

refers to a receiver whose name includes white spaces.







## _$-identifiers_ : Variables


$-identifiers like `:::antescofo $id`, `:::antescofo $id_1` are simple
identifiers prefixed with a dollar sign.  Only `!` `?` `.` and `_` are
allowed as non-alphanumeric characters. $-identifiers are used to give a
name to variables and for function, process and macro definition
arguments. *They are case-sensitive*.

You can learn more on expressions and variables in
chapter [Expressions].




## _::-identifiers_ : Processes


::-identifiers like `:::antescofo ::P` or `:::antescofo ::q1` are simple
identifiers prefixed with two semicolons. ::-identifiers are used to
give a name to processus (see chapter [Processes]).

Other ::-identifiers have a prefix before the `::` and are used to give
a name to various entities:

- `:::antescofo obj::` identifies [Objects]

- `:::antescofo track::` identifies [Tracks]

- `:::antescofo pattern::` identifies Patterns]









## _@-identifiers_ : Functions, Macros, and Attributes


A word preceded immediately with a ‘@’ character is called a
@-identifier. Only `!` `?` `.` and `_` are allowed as non alphanumeric
characters after the `@`.

They have four purposes in Antecofo language. Note that in the first
three cases, @-identifiers are *case unsensitive*, that is `:::antescofo
@tight`, `:::antescofo @TiGhT` and `:::antescofo @TIGHT` are the same
keyword.


#### Parsing directives

Some commands affect directly the parsing of a file:

- the `:::antescofo @insert` command is used to insert another file,

- the `:::antescofo @insert_once` command is used to insert another file
  if it has not been already inserted;

- the command `:::antescofo @uid` generates on-the-fly a new (fresh)
  variable ($-identifier)

- the `:::antescofo @lid` command generate on-the-fly the las generated
  identifiers




#### Definitions

The following @-identifiers are used to introduce new definitions:
      
- `:::antescofo @abort` specifies an _abort handler_ linked to a
  compound action,

- `:::antescofo @broadcast` introduces a _broadcast method_ in an object
  definition,

- `:::antescofo @fun_def` defines a new _function_ or a function-method
  attached to an object,

- `:::antescofo @init` introduces a new _init section_ in an object
  definition,

- `:::antescofo @obj_def` specifies a new _object_ definition,

- `:::antescofo @pattern_def` declares a new _pattern_ to be used in a
  `:::antescofo whenever`

- `:::antescofo @proc_def` starts a _process_ or process-method
  definition

- `:::antescofo @track_def` declares a new _track_

- `:::antescofo @whenever` defines a _reaction_

- `:::antescofo @macro_def` is used to define new _macros_.



#### Events Attributes and Actions Attributes 

Here is a list of the reserved @-identifiers used to specify an attribute
of an action or an event. These @-identifers are _case unsensitive_. 
```antescofo
        @abort @action @ante
        
        @back @back_in @back_in_out @back_out @bounce
        @bounce_in @bounce_in_out @bounce_out
        
        @circ @circ_in @circ_in_out @circ_out @coef @command
        @conservative @cubic @cubic_in @cubic_in_out @cubic_out
        
        @date @dsp_channel @dsp_cvar @dsp_inlet @dsp_link
        @dsp_outlet @dump
        
        @elastic @elastic_in @elastic_in_out @elastic_out
        @eval_when_load @exclusive @exp @exp_in @exp_in_out @exp_out
        
        @fermata
        
        @global @grain @guard
        
        @hook @immediate
        
        @inlet @is_undef
        
        @jump
        
        @kill
        
        @label @latency @linear_in @linear_in_out @linear_out
        @local @loose
        
        @modulate
        
        @name @norec
        
        @pizz @plot @post @progressive
        
        
        @quad @quad_in @quad_in_out @quad_out @quart @quart_in
        @quart_in_out @quart_out @quint @quint_in @quint_in_out
        @quint_out
        
        @rdate @refractory @rplot
        
        @sine @sine_in @sine_in_out @sine_out
        @staccato @staticscope @sync
        
        @target @tempo @tempovar @tight
        @transpose @type
```


#### Naming Functions and Macros

@-identifiers are used to give a name to predefined functions,
user-defined functions or user-defined macros. These predefined or
user-introduced @-identifiers are _case sensitive_.






{!Library/Functions/functions.list!}

