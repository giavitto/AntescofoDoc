<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}



## Writing in a File


In the current _Antescofo_ version, it is only possible to write an
output file. The schema is similar to that of an OSC message: first, a
declaration opens and binds a file to a symbol. This symbol is then used
to write out in the file. Then the file is eventually closed. Here is a
typical example:

```antescofo
          openoutfile out "/tmp/tmp.txt" opt_int
          ; ...
          out "\n\tHello Wolrd\n\n"
          ; ...
          closefile out
```

The action `:::antescofo openoutfile` opens the file.  Currently, there
is only one possible mode to open a file: if it does not exist, it is
created. If it already exists, it is truncated to zero at opening.

After `:::antescofo openoutfile`, the symbol `:::antescofo out` (a
simple identifier) can be used to write in file `/tmp/tmp.txt` in a
syntax that mimic messages.  A ‟message” `:::antescofo out` is followed
by a list of closed expressions, as for OSC or MAX/PD messages. Special
characters in strings are interpreted as usual.

The optional integer `opt_int` at the end of the command `:::antescofo
openoutfile` is used to minimize the impact of the i/o on the
scheduling. It is interpreted as follows:

- If negative or null, the buffer associated to the file is
shrunk to zero and the outputs are always flushed immediately to the
file system.

- If positive, this number is used as a multiplier of the default
file buffer system size. Factors greater than one increase the size of the
buffer and thus reduce the number of effective i/o. The effect is
usually negligible.

If the i/o's interfere with the scheduling, consider using the host
environment to implement them (_i.e._ relying on Max or PD buffer to
minimize the impact on time sensitive resources)[^1]. 


The file is automatically closed at exit. Be aware that because of file
buffering, the content of the file may be not entirely written on disk
before closing it. If not explicitly closed, the file remains open
between program load, start and play command.

It is possible to save an _Antescofo_ value in a file to be read
somewhere else, or to dump the value of some variables to be restored
later (or in another program execution). See functions [@savevalue],
[@loadvalue], [@dump], [@dumpvar] and [@loadvar].




[^1]: Native implementation of threaded i/o with _future_ construct may
be considered in a future version of _Antescofo_ to minimize
interference with the real-time scheduling, but current applications do
not require such sophistication.