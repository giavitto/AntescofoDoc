<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}


## Grammar of object definitions


<div style="width:150%;">

{!BNF_DIAGRAMS/object_def.html!}

</div>