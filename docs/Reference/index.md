<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

<html>
<link href='https://fonts.googleapis.com/css?family=Amatic+SC:700' rel='stylesheet' type='text/css'>
</html>


<style type="text/css"><!-- #htmltagcloud{

/******************************************
 * CUSTOMIZE CLOUD CSS BELOW (optional)
 */
	font-size: 100%;
	width: auto;		/* auto or fixed width, e.g. 500px   */
	font-family: 'Amatic SC','lucida grande','trebuchet ms',arial,helvetica,sans-serif;
	margin:1em 1em 0 1em;
	border:0px;
	padding:2em; 
/******************************************
 * END CUSTOMIZE
 */

}#htmltagcloud{line-height:2.4em;word-spacing:normal;letter-spacing:normal;text-transform:none;text-align:justify;text-indent:0}#htmltagcloud a:link{text-decoration:none}#htmltagcloud a:visited{text-decoration:none}#htmltagcloud a:hover{color:white;background-color:#05f}#htmltagcloud a:active{color:white;background-color:#03d}.wrd{padding:0;position:relative}.wrd a{text-decoration:none}.tagcloud0{font-size:1.0em;color:#ACC1F3;z-index:10}.tagcloud0 a{color:#ACC1F3}.tagcloud1{font-size:1.4em;color:#ACC1F3;z-index:9}.tagcloud1 a{color:#ACC1F3}.tagcloud2{font-size:1.8em;color:#86A0DC;z-index:8}.tagcloud2 a{color:#86A0DC}.tagcloud3{font-size:2.2em;color:#86A0DC;z-index:7}.tagcloud3 a{color:#86A0DC}.tagcloud4{font-size:2.6em;color:#607EC5;z-index:6}.tagcloud4 a{color:#607EC5}.tagcloud5{font-size:3.0em;color:#607EC5;z-index:5}.tagcloud5 a{color:#607EC5}.tagcloud6{font-size:3.3em;color:#4C6DB9;z-index:4}.tagcloud6 a{color:#4C6DB9}.tagcloud7{font-size:3.6em;color:#395CAE;z-index:3}.tagcloud7 a{color:#395CAE}.tagcloud8{font-size:3.9em;color:#264CA2;z-index:2}.tagcloud8 a{color:#264CA2}.tagcloud9{font-size:4.2em;color:#133B97;z-index:1}.tagcloud9 a{color:#133B97}.tagcloud10{font-size:4.5em;color:#002A8B;z-index:0}.tagcloud10 a{color:#002A8B}.freq{font-size:10pt !important;color:#bbb}#credit{text-align:center;color:#333;margin-bottom:0.6em;font:0.7em 'lucida grande',trebuchet,'trebuchet ms',verdana,arial,helvetica,sans-serif}#credit a:link{color:#777;text-decoration:none}#credit a:visited{color:#777;text-decoration:none}#credit a:hover{color:white;background-color:#05f}#credit a:active{text-decoration:underline}// -->
</style>

<div id="htmltagcloud"> <span id="0" class="wrd tagcloud3"><a href="#tagcloud">abort</a></span> <span id="1" class="wrd tagcloud10"><a href="#tagcloud">action</a></span> <span id="2" class="wrd tagcloud8"><a href="#tagcloud">antescofo</a></span> <span id="3" class="wrd tagcloud6"><a href="#tagcloud">argument</a></span> <span id="4" class="wrd tagcloud2"><a href="#tagcloud">assignment</a></span> <span id="5" class="wrd tagcloud0"><a href="#tagcloud">attributes</a></span> <span id="6" class="wrd tagcloud0"><a href="#tagcloud">audio</a></span> <span id="7" class="wrd tagcloud0"><a href="#tagcloud">body</a></span> <span id="8" class="wrd tagcloud0"><a href="#tagcloud">breakpoints</a></span> <span id="9" class="wrd tagcloud0"><a href="#tagcloud">called</a></span> <span id="10" class="wrd tagcloud2"><a href="#tagcloud">case</a></span> <span id="11" class="wrd tagcloud0"><a href="#tagcloud">clause</a></span> <span id="12" class="wrd tagcloud0"><a href="#tagcloud">command</a></span> <span id="13" class="wrd tagcloud0"><a href="#tagcloud">compound</a></span> <span id="14" class="wrd tagcloud2"><a href="#tagcloud">computation</a></span> <span id="15" class="wrd tagcloud0"><a href="#tagcloud">condition</a></span> <span id="16" class="wrd tagcloud0"><a href="#tagcloud">current</a></span> <span id="17" class="wrd tagcloud3"><a href="#tagcloud">curve</a></span> <span id="18" class="wrd tagcloud2"><a href="#tagcloud">date</a></span> <span id="19" class="wrd tagcloud1"><a href="#tagcloud">def</a></span> <span id="20" class="wrd tagcloud0"><a href="#tagcloud">definition</a></span> <span id="21" class="wrd tagcloud2"><a href="#tagcloud">delay</a></span> <span id="22" class="wrd tagcloud0"><a href="#tagcloud">duration</a></span> <span id="23" class="wrd tagcloud5"><a href="#tagcloud">elements</a></span> <span id="24" class="wrd tagcloud0"><a href="#tagcloud">end</a></span> <span id="25" class="wrd tagcloud0"><a href="#tagcloud">evaluated</a></span> <span id="26" class="wrd tagcloud7"><a href="#tagcloud">event</a></span> <span id="27" class="wrd tagcloud3"><a href="#tagcloud">example</a></span> <span id="28" class="wrd tagcloud5"><a href="#tagcloud">expression</a></span> <span id="29" class="wrd tagcloud0"><a href="#tagcloud">figure</a></span> <span id="30" class="wrd tagcloud2"><a href="#tagcloud">file</a></span> <span id="31" class="wrd tagcloud2"><a href="#tagcloud">following</a></span> <span id="32" class="wrd tagcloud1"><a href="#tagcloud">form</a></span> <span id="33" class="wrd tagcloud8"><a href="#tagcloud">function</a></span> <span id="34" class="wrd tagcloud0"><a href="#tagcloud">given</a></span> <span id="35" class="wrd tagcloud4"><a href="#tagcloud">group</a></span> <span id="36" class="wrd tagcloud1"><a href="#tagcloud">history</a></span> <span id="37" class="wrd tagcloud0"><a href="#tagcloud">icmd</a></span> <span id="38" class="wrd tagcloud2"><a href="#tagcloud">identifier</a></span> <span id="39" class="wrd tagcloud2"><a href="#tagcloud">instance</a></span> <span id="40" class="wrd tagcloud0"><a href="#tagcloud">integer</a></span> <span id="41" class="wrd tagcloud0"><a href="#tagcloud">interpolation</a></span> <span id="42" class="wrd tagcloud0"><a href="#tagcloud">iteration</a></span> <span id="43" class="wrd tagcloud2"><a href="#tagcloud">label</a></span> <span id="44" class="wrd tagcloud1"><a href="#tagcloud">launched</a></span> <span id="45" class="wrd tagcloud1"><a href="#tagcloud">list</a></span> <span id="46" class="wrd tagcloud3"><a href="#tagcloud">local</a></span> <span id="47" class="wrd tagcloud3"><a href="#tagcloud">loop</a></span> <span id="48" class="wrd tagcloud1"><a href="#tagcloud">macro</a></span> <span id="49" class="wrd tagcloud7"><a href="#tagcloud">map</a></span> <span id="50" class="wrd tagcloud2"><a href="#tagcloud">max</a></span> <span id="51" class="wrd tagcloud2"><a href="#tagcloud">message</a></span> <span id="52" class="wrd tagcloud3"><a href="#tagcloud">name</a></span> <span id="53" class="wrd tagcloud4"><a href="#tagcloud">nim</a></span> <span id="54" class="wrd tagcloud4"><a href="#tagcloud">note</a></span> <span id="55" class="wrd tagcloud0"><a href="#tagcloud">number</a></span> <span id="56" class="wrd tagcloud2"><a href="#tagcloud">numeric</a></span> <span id="57" class="wrd tagcloud1"><a href="#tagcloud">object</a></span> <span id="58" class="wrd tagcloud0"><a href="#tagcloud">operator</a></span> <span id="59" class="wrd tagcloud1"><a href="#tagcloud">order</a></span> <span id="60" class="wrd tagcloud2"><a href="#tagcloud">pattern</a></span> <span id="61" class="wrd tagcloud1"><a href="#tagcloud">performed</a></span> <span id="62" class="wrd tagcloud1"><a href="#tagcloud">position</a></span> <span id="63" class="wrd tagcloud0"><a href="#tagcloud">predicate</a></span> <span id="64" class="wrd tagcloud3"><a href="#tagcloud">print</a></span> <span id="65" class="wrd tagcloud4"><a href="#tagcloud">process</a></span> <span id="66" class="wrd tagcloud4"><a href="#tagcloud">ref</a></span> <span id="67" class="wrd tagcloud1"><a href="#tagcloud">refers</a></span> <span id="68" class="wrd tagcloud0"><a href="#tagcloud">result</a></span> <span id="69" class="wrd tagcloud6"><a href="#tagcloud">returns</a></span> <span id="70" class="wrd tagcloud6"><a href="#tagcloud">score</a></span> <span id="71" class="wrd tagcloud4"><a href="#tagcloud">sec</a></span> <span id="72" class="wrd tagcloud0"><a href="#tagcloud">size</a></span> <span id="73" class="wrd tagcloud0"><a href="#tagcloud">specification</a></span> <span id="74" class="wrd tagcloud2"><a href="#tagcloud">specified</a></span>


<span id="75" class="wrd tagcloud3"><a href="#tagcloud">start</a></span> <span id="76" class="wrd tagcloud4"><a href="#tagcloud">string</a></span>

<span id="78" class="wrd tagcloud6"><a href="#tagcloud">synchronization</a></span> <span id="79" class="wrd tagcloud2"><a href="#tagcloud">systems</a></span> <span id="80" class="wrd tagcloud2"><a href="#tagcloud">target</a></span> <span id="81" class="wrd tagcloud7"><a href="#tagcloud">tempo</a></span> <span id="82" class="wrd tagcloud4"><a href="#tagcloud">temporal</a></span> <span id="83" class="wrd tagcloud1"><a href="#tagcloud">tight</a></span>

<span id="81" class="wrd tagcloud0"><a href="#tagcloud">trace</a></span> <span id="82" class="wrd tagcloud2"><a href="#tagcloud">true</a></span> <span id="83" class="wrd tagcloud0"><a href="#tagcloud">type</a></span> <span id="84" class="wrd tagcloud0"><a href="#tagcloud">updated</a></span> <span id="85" class="wrd tagcloud5"><a href="#tagcloud">used</a></span> <span id="86" class="wrd tagcloud8"><a href="#tagcloud">value</a></span> <span id="87" class="wrd tagcloud2"><a href="#tagcloud">var</a></span> <span id="88" class="wrd tagcloud8"><a href="#tagcloud">variable</a></span> <span id="89" class="wrd tagcloud3"><a href="#tagcloud">whenever</a></span> </div>



# Index

## A

<html><div class="twocol"></html>

[@abort]

[@abs]

[@acos]

[@active]

[@add_pair]

[@aggregate]

[@align_breakpoints]

[@ante]

[@approx]

[@arch_darwin]

[@arch_linux]

[@arch_windows]

[@asin]

[@atan]

[a strongly timed language]

[abort]

[abort handler]

[action]

[action as expression]

[action label]

[action priority]

[action specification]

[actor]

[alive]

[antescofo cookbook]

[antescofo distribution]

[antescofo workflow]

[antescofo::actions]

[antescofo::add_completion_string]

[antescofo::analysis]

[antescofo::asco_trace]

[antescofo::ascographheight_set]

[antescofo::ascographwidth_set]

[antescofo::ascographxy_set]

[antescofo::before_nextlabel]

[antescofo::bpmtolerance]

[antescofo::calibrate]

[antescofo::clear]

[antescofo::decodewindow]

[antescofo::filewatchset]

[antescofo::gamma]

[antescofo::get_current_score]

[antescofo::get_patch_receivers]

[antescofo::getlabels]

[antescofo::gotobeat]

[antescofo::gotolabel]

[antescofo::harmlist]

[antescofo::info]

[antescofo::killall]

[antescofo::mode]

[antescofo::mute]

[antescofo::nextaction]

[antescofo::nextevent]

[antescofo::nextlabel]

[antescofo::nextlabeltempo]

[antescofo::nofharm]

[antescofo::normin]

[antescofo::obsexp]

[antescofo::pedal]

[antescofo::pedalcoeff]

[antescofo::pedaltime]

[antescofo::piano]

[antescofo::play]

[antescofo::playfrombeat]

[antescofo::playfromlabel]

[antescofo::playstring]

[antescofo::playstring_append]

[antescofo::playtobeat]

[antescofo::playtolabel]

[antescofo::preload]

[antescofo::preventzigzag]

[antescofo::previousevent]

[antescofo::previouslabel]

[antescofo::printfwd]

[antescofo::printscore]

[antescofo::read]

[antescofo::report]

[antescofo::score]

[antescofo::scrubtobeat]

[antescofo::scrubtolabel]

[antescofo::setvar]

[antescofo::start]

[antescofo::startfrombeat]

[antescofo::startfromlabel]

[antescofo::static_analysis]

[antescofo::stop]

[antescofo::suivi]

[antescofo::tempo]

[antescofo::tempoinit]

[antescofo::temposmoothness]

[antescofo::tune]

[antescofo::unmute]

[antescofo::variance]

[antescofo::verbosity]

[antescofo::version]

[argument evaluation strategies]

[argument passing strategies]

[articulating time]

[ascograph]

[assignment]

[atomic action]

[atomicActionInExpression]

[attribute]

[auto delimited expressions]

[auto-delimited expression]

<html></div></html>


## B

<html><div class="twocol"></html>

[@between]

[@bounded_integrate]

[@bounded_integrate_inv]

[@broadcast]

[boolean]

<html></div></html>




## C

<html><div class="twocol"></html>

[@car]

[@cdr]

[@ceil]

[@clear]

[@concat]

[@cons]

[@conservative]

[@copy]

[@cos]

[@cosh]

[@count]

[chuck]

[closed expression]

[commands]

[compound action]

[conditional action]

[conditional expression]

[constant bpm expression]

[continuation]

[continuation operator]

[coroutine]

[curryfied functions]

[curve] 

[curve interpolation methods]

[curve playing a nim]

<html></div></html>



## D 

<html><div class="twocol"></html>

[@dim]

[@div]

[@domain]

[@drop]

[@dump]

[@dumpvar]

[data structures]

[dead]

[dealing with errors]

[delay]

[dot notation]

[during]

<html></div></html>




## E

<html><div class="twocol"></html>

[@empty]

[@exclusive]

[@exp]

[@explode]

[end clause]

[error]

[error handling strategy]

[error strategies]

[eval when load]

[evaluation]

[evaluation at load time]

[event specification]

[events]

[exe]

[exec]

[exec value]

[expression]

[extended expressions]

[extensional function]

[external assignment]

<html></div></html>




## F

<html><div class="twocol"></html>

[@filter_max_t]

[@filter_median_t]

[@filter_min_t]

[@find]

[@flatten]

[@floor]

[@fun_def]

[files layout]

[fig:Excerpt1]

[fig:Tesla]

[ForAll]

[ForumUser]

[function]

[function as value]

[functions library]

<html></div></html>






## G

<html><div class="twocol"></html>

[@global]

[@gnuplot]

[@gshift_map]

[grammar of object definition]

[Group]

<html></div></html>




## H

[@history_length]

[history of a variable]





## I

<html><div class="twocol"></html>

[@immediate]

[@init]

[@insert]

[@integrate]

[@iota]

[@is_bool]

[@is_defined]

[@is_exec]

[@is_fct]

[@is_float]

[@is_function]

[@is_int]

[@is_integer_indexed]

[@is_interpolatedmap]

[@is_list]

[@is_map]

[@is_nim]

[@is_numeric]

[@is_obj]

[@is_obj_xxx]

[@is_prefix]

[@is_proc]

[@is_string]

[@is_subsequence]

[@is_suffix]

[@is_symbol]

[@is_tab]

[@is_undef]

[@is_vector]

[if]

[impure predefined functions]

[infix Notation for Function Calls]

[int]

[intentional function]

[internal command]

[internal commands]

<html></div></html>




## J

[jump]



## K


## L

<html><div class="twocol"></html>

[@lace]

[@last]

[@latency]

[@linearize]

[@listify]

[@loadvalue]

[@loadvar]

[@log]

[@log10]

[@log2]

[@local]

[@loose]

[lexical elements]

[library Functions]

[library]

[local tempo]

[logical instant]

[loop]

<html></div></html>





## M

<html><div class="twocol"></html>

[@macro_def]

[@make_bpm_map]

[@make_bpm_tab]

[@make_duration_map]

[@make_duration_tab]

[@make_label_bpm]

[@make_label_duration]

[@make_label_pitches]

[@make_label_pos]

[@make_pitch_tab]

[@make_score_map]

[@map]

[@map_compose]

[@map_concat]

[@map_history]

[@map_history_date]

[@map_history_rdate]

[@map_normalize]

[@map_reverse]

[@mapval]

[@max]

[@max_key]

[@max_val]

[@median]

[@member]

[@merge]

[@midi_getChannel]

[@midi_getCommand]

[@midi_getCommandByte]

[@midi_getMetaType]

[@midi_isAftertouch]

[@midi_isController]

[@midi_isEndOfTrack]

[@midi_isMeta]

[@midi_isNote]

[@midi_isNoteOff]

[@midi_isNoteOn]

[@midi_isPatchChange]

[@midi_isPitchbend]

[@midi_isPressure]

[@midi_isTempo]

[@midi_read]

[@midi_track2ascii]

[@min]

[@min_key]

[@min_val]

[macro versus Function versus Process]

[macro, Function and Processus]

[macro]

[management of Time]

[map]

[max]

[message]

[methods]

[mutating a tab element]

<html></div></html>





## N

<html><div class="twocol"></html>

[@norec]

[@normalize]

[@number_active]

[Nim]

<html></div></html>




## O

<html><div class="twocol"></html>

[@obj_def]

[@occurs]

[obj]

[objects]

[object instantiation]

[open Scores and Dynamic jumps]

[OSC message]

[OSC messages]

[OSC protocol]

[OSCRECEIVE]

<html></div></html>





## P

<html><div class="twocol"></html>

[@pattern_def]

[@permute]

[@plot]

[@post]

[@pow]

[@proc_def]

[@progressive]

[@projection]

[@push_back]

[@push_front]

[pattern]

[patterns]

[pd]

[priority]

[proc]

[proc value]

[proc values]

[process call]

[process]

[processes]

[processus]

[procVariable]

[program Structure]

<html></div></html>






## Q

## R

<html><div class="twocol"></html>

[@rand]

[@rand_int]

[@random]

[@range]

[@reduce]

[@remove]

[@remove_duplicate]

[@replace]

[@reshape]

[@resize]

[@reverse]

[@rnd_bernoulli]

[@rnd_binomial]

[@rnd_exponential]

[@rnd_gamma]

[@rnd_geometric]

[@rnd_normal]

[@rnd_uniform_float]

[@rnd_uniform_int]

[@rotate]

[@round]

[@rplot]

[Reference Manual]

[reserved @-identifier]

[reserved keywords]

[return]

<html></div></html>





## S

<html><div class="twocol"></html>

[@sample]

[@savevalue]

[@scan]

[scoped variable]

[@score_tempi]

[@scramble]

[@select_map]

[@set_osc_handling_tab]

[@shape]

[@shift_map]

[@simplify_lang_v]

[@simplify_radial_distance_t]

[@simplify_radial_distance_v]

[@sin]

[@sinh]

[@size]

[@slice]

[@sort]

[@sputter]

[@sqrt]

[@string2fun]

[@string2obj]

[@string2proc]

[@stutter]

[@sync]

[@system]

[scalar values]

[Scheduling priorities]

[score import]

[setvar]

[side effect]

[simple expressions]

[splitting]

[string]

[superdense time]

[switch action]

[Switch]

[synchronization]

[synchrony hypothesis]

[Synchronization Strategies]

[system variables]

<html></div></html>




## T

<html><div class="twocol"></html>

[@tab_history]

[@tab_history_date]

[@tab_history_rdate]

[@take]

[@tan]

[@target]

[@tempo]

[@tempovar]

[@tight]

[@Tracing]

[@track_def]

[Tab]

[Temporal Pattern]

[Temporal Patterns]

[temporal scope]

[temporal variable]

[tempovar]

[the fabric of time]

[the manufacturing of time]

[three kinds of expressions]

[Tracing]

[Tracks]

<html></div></html>





## U

[@UnTracing]

[undef]

[until]

[user Guide]



## V

[value]

[variable]

[variable declaration]

[variables and notifications]

[vectorial curve]


## W

[@whenever]

[@window_filter_t]

[what to Choose Between Macro, Functions and Processes]

[Whenever]

[while]

## X
## Y
## Z





##Miscellaneous

<html><div class="twocol"></html>

[:=]

[+=]

[*=]

[-=]

[/=]


[@!=]

[@%]

[@&&]

[@*]

[@+]

[@-]

[@<]

[@<=]

[@==]

[@>]

[@>=]

[@||]

[@/]

<html></div></html>



