<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## Parallel Iterations


{!BNF_DIAGRAMS/forall.html!}




The [loop] construction iterates the triggering of a group (the loop
body): one body instance is triggered after the other, with a given
interval (the loop period). The action [ForAll] (for _parallel
iteration_) instantiates a group _in parallel_ for each element in an
iteration set. The simplest example is the iteration on the elements of
a [tab]:

```antescofo
          $t := tab [1, 2, 3]
          forall $x in $t
          {
               (3 - $x) print OK $x " at time" (3 - $x) " = (3 - " $x ")"
          }
```	  

will trigger in parallel a group for each element in the tab referred by
`:::antescofo $t`. For each group, the *iterator variable* `:::antescofo
$x` takes the value of its corresponding element in the tab. It is
implicitly a local variable, not visible outside the `:::antescofo
ForAll` body.

The result of this example is to `:::antescofo print` in sequence

          OK 3 at time 0 = (3 - 3)
          OK 2 at time 1 = (3 - 2)
          OK 1 at time 2 = (3 - 1)

<br>

The general form of a parallel iteration is:

```antescofo
          forall $var in expression
          {
                ; action sequence
          }
```	  

where `:::antescofo expression` evaluates to a `int`, a `tab` or  a `proc`:

- If the iteration set $n$ is an [int], the values of the iterator are
  the integers $0, ..., (n-1)\;$ if $n$ is positive, and $(n+1),
  (n+2), ..., 0\;$ if $n$ is negative.

- If the iteration set is a [tab], the values of the iterator are the
  tab's elements.

- If the iteration set is a [proc] or an [obj], the values of the
  iterator are the [exec] that correspond to the proc's or obj's
  instances.


Parallel iterations also accept a [map] for the iteration set. In this
case, the syntax introduces two variables to refers to the keys and the
values in the map:

```antescofo
          $m := map { (1, "one"), (2, "two"), (3, "three") }
          forall $k, $v in $m
          {
                print $k " => " $v
          }
```

will print:

          1 => one
          2 => two
          3 => three


There is also a parralel iteration expression allowed only in the
context of a function definition, see the section [extended
expressions].