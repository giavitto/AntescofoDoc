<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}

<html>
<center>
</html>

  ![magnetic harpsychord](/Figures/clavecin-magnetique.png){: width="60%"; }

<html>
</center>
</html>


# Additional Elements


In this chapter, we present some additional elements of the _Antescofo_
language that do not fit in the previous chapters:

- the [track mechanism] which allows the disabling of some messages during a
  program run;

- the
  [organization](/Reference/file_structure#writing-an-augmented-score-through-multiple-file)
  of a large score into several files;

- the [evaluation] that can be performed when a score is loaded;

- the [compilation] of functions;

- the use and the “evaluation” of [symbols];

- the substitution of an arbitrary computation to messages thank to
  [callback messages];
  
- and the association of _handlers_ to perform a computation when some
  internal event occurs through [event callbacks]. 

<br>
<br>

