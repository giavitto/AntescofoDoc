<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}





## IF and SWITCH: Conditional and Alternative 

<br>

### IF: Conditional Actions


{!BNF_DIAGRAMS/if.html!}


A conditional action is a construct that performs different actions
depending on whether a programmer-specified [boolean] condition evaluates
to true or false. A conditional action takes the form:

```antescofo
          if (boolean_expression)
          {
                ... actions launched if the condition evaluates to true ...
          }
```       

or

```antescofo
          if (boolean_expression)
          {
                ... actions launched if the condition evaluates to true ...
          }
          else
          { 
                ... actions launched if the condition evaluates to false ...
          }
```       

Like other actions, a conditional action can be prefixed by a delay.
Note that the actions in the *if* and in the *else* clause are evaluated
as if they are in a group. So, the delay of these actions does not impact
the timing of the actions which follow the conditional. For example

```antescofo
          if ($x) { 5 print HELLO }
          1 print DONE
```
          
will print `DONE` one beat after the start of the conditional
independently of the value of the condition.

The actions of the “true” (resp. of the “else”) parts of a condition are
members of an implicit group named `xxx_true_body` (resp.
`xxx_false_body`) where `xxx` is the label of the conditional
itself. The attribute of `:::antescofo if` are used for these groups.

There are also conditional expressions (sections [Conditional
Expression] and [Extended Expression
If](/Reference/9-functions/index.html#the-extended-expression-if)) that
share a similar syntax.


Any kind of _Antescofo_ value can be interpreted as a boolean value, see
sections [scalar values] and [data structures].

<br>


### SWITCH: Alternative Actions


*Alternative actions* extend conditional actions to handle several
alternatives. At most one of the alternative will be performed. They are
two forms of alternative actions, without and with selector, which
differ by the way the alternative to execute is chosen.


There are also alternatives expressions (section [switch
expression](/Reference/9-functions/index.html#the-extended-expression-switch))
that share a similar syntax in the context of function definitions.




#### Alternative Actions without Selectors

{!BNF_DIAGRAMS/switch.html!}


An alternative action without a selector is simply a sequence of cases
guarded by expressions. The guards are evaluated in the sequence order
and the action performed is the first case whose guard evaluates to a
true value. So :

```antescofo
          switch
          {
               case e₁: a₁  
               case e₂: a₂
               ; ...
          }
```

(where `:::antescofo e₁`, `:::antescofo e₂`... are arbitrary expressions
and `:::antescofo a₁`, `:::antescofo a₂`... are sequences of actions) can
be rewritten in:

```antescofo
          if ( e₁ )  { a₁  }
          else
          {
               switch
               {
                    case e₂: a₂
                    ; ...
               }
          }
```
          
If no guard is true, then no action is performed. Notice that several
actions can be associated to a `:::antescofo case` : they are launched
as a group.

Here is an example where the evaluation order matters: the idea is to
rank the value of the variable `:::antescofo $x`. The following code

```antescofo
          whenever ($PITCH)
          {
               switch
               {
                    case $PITCH < 80:
                         $octave := 1

                    case $PITCH < 92:
                         $octave := 2
                         
                    case $PITCH < 104:
                         $octave := 3
               } 
         }
```
         
uses a [whenever] to set the variable `:::antescofo $octave` to some
value each time `:::antescofo $PITCH` is updated for a value below
`:::antescofo 104`.

Note that the actions associated to a `:::antescofo case` are evaluated
as if they are in a group. So the eventual delay of these actions does
not impact the timing of the actions which follows the alternative. And
like other actions, an alternative action can be prefixed by a delay.




#### Alternative Action with a Selector

{!BNF_DIAGRAMS/switch2.html!}

In this form, a selector is evaluated and checked in turn against each
guard of the cases:

```antescofo
          switch (s)
          {
               case e₁: a₁  
               case e₂: a₂
               ; ...
          }
```

The evaluation proceeds as follows: the selector $s$ is evaluated and
then, the result is checked in turn with the result of the evaluation of
the $e_i$:

- If `:::antescofo eᵢ` evaluates to a function, this function is assumed
    to be a unary predicate and is applied to `:::antescofo s`. If the
    application returns a true value, the sequence of actions
    `:::antescofo aᵢ` is performed.

- If `:::antescofo eᵢ` is not a function, the values of `:::antescofo s`
    and `:::antescofo eᵢ` are compared with the operator `:::antescofo
    ==`. If it returns a true value, the sequence of actions
    `:::antescofo aᵢ` is performed.

The evaluation start with `:::antescofo e₁` and stops as soon as an
action is performed for one of the `:::antescofo eᵢ`. If no guard checks
true, no action is performed.

For example:

```antescofo
          switch ($x)
          {
               case 0: 
                  $zero := true
               case @size: 
                  $empty := false
                  $zero := false
          }
```

checks a variable `:::antescofo $x` and sets the variable `:::antescofo
$zero` to true if `:::antescofo $x == 0` or `:::antescofo 0.0` (because
`:::antescofo 0.0 == 0`). It then sets the variable `:::antescofo
$empty` and `:::antescofo $zero` to false if `:::antescofo $x` refers to
a non-empty tab or to a non-empty map (because function [@size] returns
an integer which is `:::antescofo 0` only if its argument is an empty
tab or an empty map).
