<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## Auto-Delimited Expressions

{!BNF_DIAGRAMS/closed_expr.html!}


Expressions appear everywhere to parameterize actions (and actions
may appear in expressions, cf. [Action As Expression]). This may cause
some syntax problems. For example, when writing:

```antescofo 
          print @f (1)
```

there is a possible ambiguity: it can be interpreted as the message with
two arguments (the function `:::antescofo @f` and the integer
`:::antescofo 1`) or it can be the message with only one argument (the
result of the application of function `:::antescofo @f` to the argument
`:::antescofo 1`). This kind of ambiguity appears in other places, as
for example in the specification of the list of breakpoints in a curve.

The cause of the ambiguity is that there is no separator between the
arguments of a messages. So we don’t know where the expression starting
by `:::antescofo @f` finishes.


The example here shows a more general problem which leads us to distinguish
a subset of expressions: *auto-delimited expressions* are meaningful
expressions that cannot be “extended” with what follows. Integers,
for example, are auto-delimited expressions. We can write

```antescofo
          print 1 (2)
```

without ambiguity: this is the message with two arguments because there
is no other possible interpretation. Variables are another example of
auto-delimited expressions.

Being auto-delimited is a sophisticated property involving the type of
the actual value of the expressions. So, the _Antescofo_ approach is to
accept a simple syntactic subset of expressions to avoid possible
ambiguities in the places where this is needed. This subset is defined
by the syntax diagrams given above.

For example:

```antescofo
     $x + 3  print BAD  // syntax error: x + 3 is not auto-delimited
    ($x + 3) print OK   // parenthetized expressions are always auto-delimited
```

To disambiguate our first example, we can also use parentheses:

```antescofo
     print (@f (1))  // is interpreted as the print of one argument: (@f(1))
     print @f (1)    // is interpreted as the print of one argument: (@f(1))
     print (@f) (1)  // is interpreted as the print of two arguments: @f and 1
```

The second form is interpreted as only one argument, because a
functional constant is useless as an argument in a message sent to the
environment. But the third form shows how to force the alternative
interpretation.

<br>

[Returns to chapter Expression](/Reference/6-expression#auto-delimited-expressions)


