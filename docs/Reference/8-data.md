<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## Data Structures

_Antescofo_ currently provides four kinds of data structures:

- a [string] is a *sequence of characters*,

- a [map] is a *dictionary* with arbitrary keys and values,

- a [tab] is a *vector* holding possibly heterogeneous values,

- a [nim] is an *interpolated function* defined by a sequence of
  breakpoints.


These data structures are described in the following sections.




<br>

<!-- FOOTNOTE -->



