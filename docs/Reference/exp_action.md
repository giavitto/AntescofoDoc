<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}





## Actions as Expressions


An action can be considered as an expression: when evaluated, its value
is an [exec]. This can be a useful way to bypass _Antescofo's_ syntax
constraints. To consider an action as an expression, the action must
usually be enclosed in an `:::antescofo EXPR { ... }` construct.

However, the main use of this construct is to get the exec of an action.
The action is fired when the expression is evaluated. The returned
*exec* refers to the running instance of the action and can be used to
kill this running instance or to access the local variables of the
action. An atomic action (with a 0-duration run) returns the special
exec `:::antescofo '0`.


### Simplified Syntax

A number of shortcuts can be used to simplify the writing:

The surrounding `:::antescofo EXPR { ... }` is optional in the case of a
    process call.

The surrounding `:::antescofo EXPR { ... }` is optional in the body of a
function, but only for messages and variable assignment, see
[AtomicActionInExpression].

The keyword `:::antescofo EXPR` is optional in the right hand side of an
assignment. For example:
      
```antescofo
          $x := EXPR { whenever (...) {...} }
```              

is equivalent to
    
```antescofo
          $x := { whenever (...) {...} }
```              




### Example

In the following example, a tab of 5 elements is created. Each element
refers to a running loop:

```antescofo
          $tab := [ EXPR{ Loop 1 { @local $u ... } } | (5) ]
```

Thus, one can kill the second instance with

```antescofo
          abort $tab[1]
```

and one can access the local variable of the third instance through the
dot notation:

```antescofo
          $uu := $x[2].$u
```

In this case, the use of the `:::antescofo EXPR { ... }` avoids the
definition of a process to encapsulate the loop.



<br>





<!-- FOOTNOTE -->

