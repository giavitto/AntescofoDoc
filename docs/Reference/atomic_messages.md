<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}





## Message passing to Max/PD

The simplest form of action in _Antescofo_ is sending some values to a
*receive* object in MAX or PD. This way, _Antescofo_ acts as a coordinator between
multiple tasks (machine listening and actions themselves) attempting to
deliver actions deterministically as they have been authored despite
changes from musicians or controllers. These actions are simply
equivalent to *message boxes* and their usage is similar to *cue list*
object in MAX/PD with the extension of the notion of
[Delay](/Reference/action_ref#delays).
They take the familiar form of:

          <optional-delay> <receiver-name> <message-content>

Since such actions are destined for interaction with external processes
(in MAX/PD), we refer to them as *external actions*. External actions
includes also sending an OSC message or writin data in some file, see
below.


### Message Receiver

A MAX/PD message starts by an optional delay followed by a symbol
refering to a MAX or PD receiver. This identifier must be a simple
identifier (that is, it cannot be a [reserved #-identifier] and must not
be a [reserved keywords]) refering to **receiver object** in MAX/PD.

Alternatively, it is always possible to use an arbitrary string: the
content of the string denotes the Max receiver.

For example, the following action attempts to send its message to a
receiver called “print” in MAX/PD whose patch might look like the figure
on its left:

<html>
<table style="border-style: none;">
  <Tr>
    <td>
    <div>
    ![simple print patch](/Figures/SimplePrintPatch.png)
    </div>
    </td>
    <td>
</html>
```antescofo
NOTE C4 1.0 
   print I will be printed upon recognition of C4
   0.5 print "I will be printed next, after 0.5 beats"
   print Comma, separated, message, as in MAX 
```
<html>
    </td>
  </tr>
</table>
</html>





### Message arguments


What follows a receiver is a comma-separated sequence of argument
list. An argument list is simply a sequence of closed-expressions,
simple identifiers and @-identifiers. _Antescofo_ follows the Max
convention: a message to the receivers is sent for each argument list,
that is

```antescofo
       print 1 2 3, 4 5 6, 7 8 9
```

is equivalent to the 3 messages

```antescofo
       print 1 2 3
       print 4 5 6
       print 7 8 9
```

### Message terminator

The specification of a message ends with a carriage-return (the end of
the line) or a closing brace. This is important because the arguments of
a message is a list of items, without separators, so a terminator is
needed.

Writing a message with a lot of arguments on one line can be
cumbersome. So a message can span several lines, but the intermediate
lines must end with a backslash `:::antescofo \` which voids the
following end of line.

For instance,

```antescofo
          $a := 1   ; This is an assignment! see below in this chapter
          print "the value of the variable a is " $a 
          print and here is \
                a (2 * $a) "nd message " \
                "specified on 3 lines (note the \\)"
```

will print

```
          the value of the variable a is 1
          and here is a 2nd message specified on 3 lines (note the \)
```

In the second message of the previous example, there are 7 arguments:
the first four are simple identifiers converted into the corresponding
symbols, the fifth argument is evaluated into an integer and the last
is a string. The backslash character has a special meaning and must be
“backslashed” to appear in the string, see section [String].



### Expressions in messages' arguments

Expressions are evaluated to give the arguments of the
message. To avoid ambiguities, an expression in a message must be a 
[closed expression], that is: a simple identifier, a scalar constant or an
expression between parentheses.

In the previous example, the first `print` has two arguments: a string
and a variable which evaluates to `1`. Each value is converted into the
appropriate MAX/PD values when the message is sent:

- a simple identifier is converted into a Max/PD symbol,

- a string is converted into a MAX/PD symbol

- an integer is converted into a long

- a float is converted into a float

- a boolean is converted into a long

- a tab of size _n_ is converted into _n_ arguments, one for each tab
  element

- other _Antescofo_ values (_e.g._ `map` or `nim` or functions,
  processes, _etc._) are converted into their string representation
  before being sent to Max/PD.


When a string is converted into a MAX/PD string, the delimiters (the
quotation marks `"`) do not appear. If one wants these delimiters, you have to
introduce it explicitly in the string, using an escaped quote :

          print "\"this string will appear quoted\""

prints the following to MAX/PD console

          "this string will appear quoted"




### Computing the receiver

Sometiimes it is necessary, or just handy, to specify the receiver of a
message as the result of a computation. In this case, the special
construct `@command` is used:

```antescofo
       @command(expression) argument_sequence
```

This action is performed in three steps: 

- first the expression in the `@command` is evaluated.

- Then the resulting value is interpreted as a string.

- At last this string is used as the Max/PD receiver of the message.


Examples of such computations often involve string concatenation, as in

```antescofo
       ForAll $num in (4)
       {
            @command("spat" + $num) ($param[$num))
       }
```

In this code, the [ForAll] construct iterates its body, which will send
the four messages:

```antescofo
       spat0 ($param[0])
       spat1 ($param[1])
       spat2 ($param[2])
       spat3 ($param[3])
```


