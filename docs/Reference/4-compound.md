<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}



# Compound Actions


Compound actions act as *containers* for others actions. They serve as
_temporal_ containers: their purpose is to organizes actions in time,
rather than in space (that is what data structures are for). They can
thus also be seen as _control structures_.


The actions “inside” a container (we also say “nested in” or refer to
“child actions”) are specified as a sequence of actions. Successive
actions in the sequence are linked with one of the three possible
sequencing operators. These operators are described in section
[continuation operators].

All child actions inherit some of the container's attributes.  The
nesting of containers creates a hierarchy which can be visualized as an
inclusion tree. The **father** of an action _A_ is its immediately
enclosing container _F_, if it exists, and _A_ is a **child** of _F_.


The nesting of actions can be explicit. This is the case for a
(sub-)group nested in a group (see below): the textual fragment of the
score that defines the sub-group is part of the text that defines
the enclosing group. But the nesting of action can also be
implicit. This is the case for the action launched by a process call:
they are “implicitly nested” in the caller.


We first present the [group] structure. It gathers several actions
logically within a same block that share common properties of tempo,
synchronization and error handling strategies.  The [group] is the
basic container: all other compound actions are variations on this
structure

- a [loop] is a sequential iteration of a group,

- a [curve] is also a sequential periodic iteration of a group,


- a [forall] is a parallel iteration of a group,


- an [if] or a [switch] is a choice between several groups to launch, at
  some point in time,


- a [whenever] is a conditional choice in time of the launching of a
  group.


All compound actions are guarded by an optional [end clause] that limits
its scope of execution. The [@exclusive] attribute may also be specified
to prevent the overlapping execution of the same action.


In a sequence of actions, two successive actions are related by a
[continuation operator]. 
 
### General Syntax of a Compound Action

{!BNF_DIAGRAMS/compound_actions.html!}


### Loop

<center>
![loop](/Figures/container_loop.png){: width="70%";}
</center>



### ForAll

<center>
![forall](/Figures/container_forall.png){: width="45%";}
</center>


### If

<center>
![if](/Figures/container_if.png){: width="75%"}
</center>




### Whenever

<center>
![whenever](/Figures/container_whenever.png){: width="80%"}
</center>




### Continuation operators

The three **[continuation operators]** link two successive actions in a
sequence of actions. It is not possible to specify an attribute for the
resulting actions, but it is easy to embed this sequence as the body of
a group and to specify the attributes for this group. 

<center>
![continuation](/Figures/container_continuation.png)
</center>

