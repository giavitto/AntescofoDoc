<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




# Functions


<center>
![image from a clock mechanism](/Figures/action_DeLongDrawing-2.png){: width="80%"}
</center>



If computerized actions in your score observe very repetitive conceptual
patterns similar to electronic leitmotifs, then you might want to
simplify your score by defining **Functions**, **Process**, **Macros**,
**Actors** and **Patterns**, which recognize those patterns through
user-defined entities and arguments.


*Functions* are described in this chapter, *processes* in the next one
and *macros* in chapter [macro]. Functions and processes are first class
values in the language: they can be elements in a tab or a map,
passed as arguments to a function or a process, _etc._ See paragraph
[Macro versus Function versus Process] for a comparison of the three
constructions. [Actors] and [Patterns] are not primitive entities: they are
internally rewritten respectively in process and nested whenever. 



Functions live in the domain of expressions and values:

- they are defined by an expression that specifies how the values
  provided as arguments in a function call are transformed into
  a(nother) value;

- they are themselves values (see section [Function As Value]);

- a function call can appear only inside an expression (as a
  sub-expression) or where an expression is expected (for example in the
  attributes of an action);

- and the call of a function takes “no time” (see sect. [synchrony hypothesis]).

These properties do not hold for macros nor processes (see page [Macro
versus Function versus Process] for a comparison of the three
constructs).


_Antescofo_ offers several kind of functions:

- [MAP] are *extensional functions*: they are qualified as extensional
   because they enumerate explicitly the image of each possible argument
   in the form of a `:::antescofo (key, value)` dictionary;

- [NIM] are also unary functions: the association between the argument
  and $y$ is taken in a limited set of possibilities constrained by the
  breakpoints;

- **[anonymous functions]** defined by a _lambda-expression_;

- **named functions** defined by the `:::antescofo @fun_def` construct
    at top-level, or within an [actor] definition.


<br>


#### Functional Values

All these values are _functional values_.  In this chapter, we will
focus on named and unamed functions. These functional values are
**intentional functions**, _i.e_ a functions $f$ defined by an arbitrary
rule (_i.e._ by an expression) that specify how an image $f(x)$ is
associated to an element $x$.


#### Predefined and User Functions

Some intentional functions are predefined and available in the initial
environment like the IEEE mathematical functions. See [Library] for a
description of more than 200 predefined functions.

Functions defined using the `:::antescofo @fun_def` or a
lambda-expressions are said _user defined_. There is no difference
between predefined intentional functions and user’s defined intentional
functions except that in a Boolean expression, a user’s defined
intentional function is evaluated to `true` and a predefined intentional
function is evaluated to `false`.

The definition of user's function are detailed in the next section
[functions definition].


#### Methods

Named function are defined at top-level _or_ in the body of an
[actor]. The latter ones are also called _methods_ (or functional
methods). Methods are described specifically in chapter [actor] but the
properties of named function apply equally on method even if the
examples in this chapter rely on top-level `:::antescofo @fun_def`
defined functions. 




#### Function Application

Named functions have _named parameters_ making possible to call the
function with out-of-order arguments. In addition, _default argument's
values_ can be defined. These two features make easier the use of
sophisticated functions implying a large number of arguments.

Function can also be partially applied, which is useful when developing
higher order function (_i.e._, functions taking other function as
arguments).

Function application is detailed in section [application of functions].


#### Compilation

From version 1.1, an _experimental feature_ offers the compilation of
functions. Compiled functions are more efficient in time and memory
resources. Compilation implies the typing of compiled
functions. Compilation is detailed in chapter [Functions Compilation].


<!-- FOOTNOTE -->
