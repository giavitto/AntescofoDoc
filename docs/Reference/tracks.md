<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}



## Tracks

A *track* refers to all actions that have a label of some form and to
the message whose head has some form. A track is defined using a
statement:

```antescofo
          @track_def track::T
          {
                print, "synth.*"
          }
```

refers to all actions that: (1) have a label or a label that matches
(*i.e.* any name that starts with the prefix ) *and* (2) all Max or PD
messages whose receivers satisfy the same constraints *and* (3) the
children of these actions (recursively).

More generally,

-   a track definition is a list of tokens separated by a comma;

-   a token is either a symbol (an identifier without double-quotes) or a
    string;

-   a symbol refers to labels or receivers equal to this symbol;

-   a string denotes a regular expressions[^1] (without the
    double quote) used to match a label or a receiver name;

-   an action belongs to a track if there is a symbol in the track
    equal to the label of the action or if there is a regular
    expression that matches the label;

-   in addition, a Max or PD message belongs to the track if the
    receiver's name fulfills the same constraint;

-   in addition, an action nested in a compound action belonging to the
    track also belongs to the track;

-   an action may belong to several tracks (or none);

-   there is a maximum of 32 definable tracks.

Tracks can be muted or unmuted:

```antescofo 
          antescofo::mute track::T
          antescofo::unmute track::T
```

A string can be also used for the name of the track:

```antescofo
          antescofo::mute "track::T"
          antescofo::unmute "track::T"
```

which means that the track to mute or unmute can be computed
dynamically:


```antescofo
          $tracks := ["track::T", "track::Harmo", "track::Synthe", "track::Reverb"]

          whenever ($track_to_mute == $track_to_mute)
          {
                antescofo::mute ($tracks[$track_to_mute])
          }
```


Tracks are muted/unmuted independently. An action is muted if it belongs
to a track that is muted, otherwise it is unmuted. A muted action has
the same behavior as an unmuted action **except** for messages: their
arguments are evaluated as usual but the final shipping to Max or PD is
inhibited. It is important to note that muting/unmuting a track has no
effect on the _Antescofo_ internal computations, only in the sending of
messages.

For example, to inhibit the sending of all messages, one can define the
track:

```antescofo 
          @track_def track::all { ".*" }
```

and mute it:

```antescofo 
          antescofo::mute track::all
```





<br>

<!-- FOOTNOTE -->



[^1]: The syntax used to define the regular expression follows the posix
extended syntax as defined in IEEE Std 1003.2, see for instance [regular
expression on
Wikipedia](http://en.wikipedia.org/wiki/Regular_expression).