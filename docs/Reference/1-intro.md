<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->
{!references.ref!}
{!Library/Functions/functions.ref!}

<html>
<center>
  <h1>
  <font color="red" style="font-size:150%;">
  <i>Antescofo</i> 1.1<br>
  Reference Manual
  </font>
  </h1>

  <h3>
  <font color="red">
  (September 2023)
  <br>
  </font>
  </h3>
</html>


 ![Frontispice metronom](/Figures/metronome.png){: style="width:40%" }

<html>
<p>&nbsp;</p>
</center>
</html>





# Organization of the Reference Manual 

This document is intended as a reference manual for the _Antescofo_
programming language. It lists the language constructs, and gives their
informal syntax and semantics through several chapters gathered in eight
parts:

## Syntax and score structure

- [Lexical elements] describes the lexical notation
  in an _Antescofo_ program.

- [Program Structure] discusses the organization of an augmented score
  and present the interleaving of definition, events and actions.



## Events

- [Event Specification] details the definition of musical events
  expected by the listening machine.



## Actions

[Actions] are the computations launched by the system in reaction to the
recognition of musical events or as the time passes. The introduction
presents the notion of action sequence, actions' attributes and delays
that are available for any kind of action. Then, the two kinds of
actions are introduced:

- [Atomic actions] take no time to be performed;

- [Compound actions] represent durative activities.


## Management of Time

The chapter [A strongly Timed Language] introduces the various notion of
time at work in _Antescofo_:

- [the manufacturing of time] elaborates the temporal notions that
  organize the computations of actions;

- [the fabric of time] elaborates on the relationships between the
  potential timing of musical events expressed in the score and the
  actual time of the actions during the performance.

These two sections are followed by technical discussion of
_Antescofo_ temporal features:

- [Action Priority] explains the ordering of action that must be
  executed in teh same instant.

- [Synchronization Strategies] details the constraints that can be
  specified between the actual timing of the performer and the timing of
  the electronic actions during the performance.

- [Error Strategies] presents the management of error from the
  action perspective.



## Expressions

[Expressions] are used to parameterize actions. They are build from
values and [variables], [conditional expression] and [functions].

Expressions are evaluated into values wich are either:

- [Scalar values] represent indecomposable values, or

- [Data structures] that provide several ways to organize the data to
  manage during a performance.




## Structure

The following chapters details the main mechanisms that can be used to
capitalize a piece of code and to re-use it:

- [Functions]
- [Processes]
- [Macros]
- [Actors]
- [Temporal Patterns]



## Additional Features

Finally, several additional features that does not fit in the previous
chapters are presented:

- [Tracks]
- [Files layout] of an _Antescofo_ augmented score
- [Evaluation at load time]





## Side-Notes

A few side-notes are used to present in detail some points of the
_Antescofo_ grammar and to dig deeper in some technical subjects:

- [Auto delimited expressions]
- [Simple expressions]
- a comparaison between [Macro, Function and Processus]
- [Argument evaluation strategies]
- the [grammar of object definition]
- [Workflow recap](/Reference/workflow/)
- [and the OSC internal messages](/Reference/osc_internals/)




<br>
## Other source of documentation


This [Reference Manual] is by no means a tutorial introduction to the
language. A good working knowledge of the _Antescofo_ application is
assumed:

- The reader may refer to the [User Guide] for a general presentation of
the system.

- This manual is completed by a  [Functions Library] that describes all
predefined functions in the _Antescofo_ library.

- The [Antescofo distribution] comes with several tutorial
patches for [Max] or [PD] as well as the augmented score of actual
pieces.


- The [Antescofo Cookbook] is a valuable source of code snippets and
_howto's_.

- And the [ForumUser] can be used to share information about
_Antescofo_.


<br>


[ ![graphical table of content of the reference manual](/Figures/Antescofo_Reference_Manual.png) ](/Figures/Antescofo_Reference_Manual.png)


<br>
<br>
