<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}



# Expressions


<center>
![details from the Paul Klee notebook](/Figures/klee_notebook.jpg){: width="65%";}
</center>
<div align="right" style="margin-left: 30%;"><small>
details from the <a href="http://www.kleegestaltungslehre.zpk.org/">Paul Klee notebook<a>
</small>
</div>

<br>


Expressions can be used to compute delay, period, local tempo,
breakpoints in specification, and arguments of internal commands and
external messages sent to the environment. Expressions are evaluated
into [values] and this evaluation is supposed to take no time.

In this chapter

- we compare [actions and expressions and we present the three kind of
  expressions](/Reference/6-expression#expressions-versus-actions) that
  exist in _Antescofo_;

- we introduce the [general notion of a value](/Reference/exp_value) (each
  specific kind of value will be discussed at length in the following
  chapters);

- we detail [the notion of a variable](/Reference/exp_variable);

- we explain the use of [variables to define a dynamic
  tempo](/Reference/exp_tempovar) (a tempo that evolves in time);

- we present the [conditional expressions](/Reference/exp_cond);

- and finally we explain [how to get the _exe_ value associated to each
  running compound action](/Reference/exp_action).




## Expressions _versus_ Actions

Actions and expressions belongs to two clearly separated worlds in
_Antescofo_:

- Expressions appear as parameters of actions and the evaluation of
  expressions is subordinated to the execution of actions.

- The evaluation of an expression does not last over time, thus the
  evaluation process can be more efficient than the execution of
  actions[^groupExecution].

This separation may appear sometimes somewhat artificial. For example,
the assignment of a variable is an action (because it can trigger
activities or alter synchronization or tempo information) but some
variables are only introduced to store intermediate values in the
computation of a complex expression. The [Loop] construct is an action
but it is useful to implement iterative expressions. A process
definition is a value (of type [proc]) and a handle to the current
execution of a compound action is also a value (an [exec]). _Etc._


To make the boundary between the two worlds more permeable, and to take
into account the usual syntax of Max or PD message (they have no
delimeters between arguments and the end of line is used as terminator),
the _Antescofo_ syntax distinguishes between three kinds of expressions. This
distinction is only useful for syntactic reasons: they are not allowed
to appear anywhere to make the parsing non-ambiguous but expressions
‟have the same rights” and are managed in the same way, irrespectively
of their kind.



## Three Kinds of Expressions

Expressions are categorized in three kinds of increasing generality,
each including the previous one:

- [Closed expressions][^closedExpression] are best called
   _auto-delimited expressions_. These expressions are allowed in the
   specification of a delay, a breakpoint in a curve, the value of an
   attribute, _etc._ See section [auto-delimited expressions] below.

- [Simple expressions] are the usual expressions allowed anywhere else,
  for example in the right hand side of an assignment, as the argument
  of a function or process call, _etc._

- [Extended expressions] are the expressions allowed in the body of a
  function. They enrich simple expressions with local variables,
  assignments, messages and instantaneous iterations.

<center>
![three kinds of expressions](/Figures/3kindOfExpressions.png){: width="65%";}
</center>


We stress again that these three categories exist only to make the
parsing of an augmented score non-ambiguous. Expressions have the same
status and are managed in the same way, irrespective of their
category. So it is possible to turn an expression of a more general
category into an equivalent expression of a less general one:

- an extended expression can be used in place of a simple expression,
  simply by calling a function whose body is specified by the extended
  expression (a function call is a simple expression).

- a simple expression can be used where a closed expression is expected,
  simply by putting it between parentheses. 




### Auto-Delimited Expressions

Closed expressions, also called auto-delimited expressions, are
expressions that are allowed in specific locations:

- the specification of a [delay],

- the arguments of a [message],

- the arguments of an [internal command],

- the list of breakpoints in a [curve],

- the parameters of a [NIM],

- the specification of an [attribute] value,

- the specification of a [while] or [until] clause.

If a simple or an extended expression is provided where an
auto-delimited expression is required, a syntax error is declared.

Numeric constants and strings are closed expressions, as well as map
definitions. Tab definitions are closed expressions but the keyword
`:::antescofo TAB` is mandatory. A variable is a closed expression too.

Notice that *every expression between parentheses is an auto-delimited
expression*. So, a rule of thumb is to put the expressions in the contexts
listed above between braces when the expression is more
complex than a scalar constant or a variable.

The section [syntax of auto-delimited
expressions](/Reference/auto_delimited) gives the full syntax of closed
expressions and explains the motivations of these syntactic constraints.



### Simple Expressions

Simple expressions include:

- closed and arithmetic expressions

- variables and constant values

- data structure definitions (tab, map, nim)

- function applications and process calls

- variable manipulation

and a combination thereof.  A simple expression between
parentheses is a closed expression (and a simple expression).

The grammar of simple expressions is defined in [simple expression
grammar](/Reference/simple_expr).



### Extended Expressions

Writing large expressions can be cumbersome and may involve the
repetition of common sub-expressions. Functions can be used to avoid the
repeated evaluations of common sub-expressions. In addition, the body of
a function is an _extended expression_, which is a sequence of simple
expressions enriched with local variables, messages, assignments and
loops.

Extended expressions enable a more concise and more readable
specification of expressions. They are described in the chapter
[Functions](/Reference/9-functions/index.html#extended-expressions).


<br>

-----------



_Next_, we introduce the [general notion of value](/Reference/exp_value).

You may also go directly to:

- [the notion of variable](/Reference/exp_variable);

- [temporal variables](/Reference/exp_tempovar);

- [conditional expressions](/Reference/exp_cond);

- [actions as expressions](/Reference/exp_action).


<br>


<!-- FOOTNOTE -->


[^groupExecution]: For instance, the implementation of a [Group] implies
a state to maintain an environment accessible by the group's childs, it
requires a scheduler to manage the delay, additional computation for the
management of synchronization and the translation of relative delays
into physical time, _etc._  Even if a compound action performs
instantaneously, its execution is a little more costly than its
corresponding expression.


[^closedExpression]: The term _closed expression_ usually refers to an
expression that contains no free variables. This is not the meaning used
here. A closed expressions refer here to expressions that can be put in
sequence without ambiguity. See paragraph [auto-delimited expressions].

