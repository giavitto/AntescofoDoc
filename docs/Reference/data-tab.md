<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




# Tables


_Tab_ values (tables) are used to define simple vectors and more. They
can be defined by giving the list of their elements:

{!BNF_DIAGRAMS/tabdef_expr.html!}

The `:::antescofo tab` keyword is case-insensitive and optional. For
example:

```antescofo
          $t := tab [0, 1, 2, 3]    ; or
          $t := [0, 1, 2, 3]        ; the `tab' keyword is optional
```

these statements assign a tab with 4 elements to the variable
`:::antescofo $t`.

Tables are compared in [lexicographical
order](https://en.wikipedia.org/wiki/Lexicographical_order). In a
boolean expression, an empty tab is false. Otherwise, it's true.


Elements of a tab can be accessed through the usual square bracket
`...[...]` notation.  Element indexing starts at $0$, so `:::antescofo
$t[n]` refers to the $(n+1)$th element of the tab refered by
`:::antescofo $t`. Notice that the arguments of the square bracket are
expressions[^tabAccess]. Elements of a tab can be any kind of value,
even a tab, which would create a multidimensional array. Additionally,
one tab can contain multiple different types of objects.

The [ForAll] action can be used to iterate through all of the elements in a
tab. A _tab comprehension_ can be used to build new tab by filtering
and mapping tab elements. There are also several predefined functions to
transform a tab.



## Multidimensional tab

Elements of a tab are arbitrary, so they can be other tabs. Nested
tabs can be used to represent matrices and multidimensional arrays. For
instance:

```antescofo
          [ [1, 2], [3, 4], [4, 5] ]
```

is a $3 \times 2$ matrix that can be interpreted as 3 lines and 2
columns. For example, if `:::antescofo $t` is a $3 \times 2$ matrix,
then the first element of the second line is accessed by the expression

```antescofo
          $t[1][0]  
          $t[1, 0]  ; equivalent form
```


The function [@dim] can be used to query the dimension of a tab, that
is, the maximal number of tab nesting found in the tab.

A multidimensionnal array is a _homogeneous_ tab: a tab of tab
elements is an multidimensional array (of dimension `:::antescofo 1`) and
a tab whose elements are multidimensionnal arrays of the same dimension
and size are also multidimensional arrays.

If a tab's argument is a multidimensional array, the function [@shape]
returns a tab of integers wherein the element $i$ represents the number of
elements in the $i$th dimension. For example

```antescofo
          @shape( [ [1, 2], [3, 4], [4, 5] ] )  →  [3, 2]
```

The function returns `:::antescofo 0` if the argument is not a
well-formed (dimensionally consistent) array. For example

```antescofo
          @shape( [1, 2, [3, 4]] )    →    0
```

Note that for this argument, [@shape] returns `::antescofo 0` because
the argument is a tab nested into a tab, but it is not an array because
the element of the top-level tab are not homogeneous. The tab

```antescofo
          [ [1, 2], [3, 4, 5] ]
```

also fails to be an array, despite that all elements are homogeneous,
because these elements are not of the same size.




##  Tab Comprehension

If a tab is specified by giving the list of its elements, the definition
is said *in extension*. The `:::antescofo tab [ ... ]` construction is
an expression that defines a tab in extension.


A tab can also be defined *[in
comprehension](https://en.wikipedia.org/wiki/List_comprehension)*. A tab
comprehension is an expression to build a tab from an existing tab or on
some iterators.  The general form of a tab comprehension is

```antescofo
          [ output_expression | $var in input_set , predicate ]

```

where `:::antescofo output_expression` is an expression, `:::antescofo
$var` is a variable identifier, `:::antescofo input_set` is an expression
evaluating to a tab or an integer or the construction `:::antescofo e₁
.. e₂ : e₃`, and `:::antescofo predicate` is a boolean expression. More
precisely:

<div style="width:110%;">

{!BNF_DIAGRAMS/tabcomprehension_expr.html!}

</div>

This construction follows the form of the mathematical [set-builder
notation](https://en.wikipedia.org/wiki/Set-builder_notation) (set
comprehension). For instance

```antescofo
          [ e | $x in t ]
```

generates a tab of the values of the output expression
`:::antescofo e` by running through the elements specified by the input
set `:::antescofo t`. If `:::antescofo t` is a tab, then takes all the
values in the tab. For example:

```antescofo
          [ 2*$x | $x in [1, 2, 3] ]   →  [2, 4, 6]
```

The input set `:::antescofo t` may also evaluate to a numeric value
$n$: in this case, take all the numeric values between $0$ and $n$
excluded by unitary steps:

```antescofo
          [ $x | $x in (2+3) ]     →   [0, 1, 2, 3, 4]
          [ $x | $x in (2 - 4) ]   →   [0, -1]
```

Note that the variable `:::antescofo $x` is a local variable visible
only in the tab comprehension: its name is not meaningful and could be any
variable identifier (but beware that it can mask a regular variable in
the output expression, in the input set or in the predicate).

The input set can be specified by a range giving the starting value, the
step and the maximum value:

```antescofo
          [ e | $x in start .. stop : step ]
```

The specification `:::antescofo start .. stop` specifies a range where
`:::antescofo start` is included and `:::antescofo stop` is excluded. 
If the specification of the step is not given, it value is $+1$ or $-1$
following the sign of `:::antescofo (stop - start)`. The specification
of `:::antescofo start` is also optional: in this case, the variable
will start from $0$. For example:

```antescofo
          [ $s[$i] + $t[$i]  | $i in @size($s) ]
```

creates a tab whose elements are the pointwise sums of `:::antescofo
$s` and `:::antescofo $t` (assuming that they have the same
size). Notice that expression `:::antescofo $i in (@size($s))`
enumerates the indices of `:::antescofo $s`. Expression

```antescofo
          [ @sin($t) | $t in -3.14 .. 3.14 : 0.1 ] 
```

generates a tab of 62 elements: $\sin(-3.14), \sin(-3.04), ..., \sin(3.04)$. 


Tab comprehension may specify a predicate to filter the members of the
input set:

```antescofo
           [$u | $u in 10, $x % 3 == 0]   →  [0, 3, 6, 9]
```

filters the multiple of $3$ in the interval $[0, 10)$. The expression
used as a predicate is given after a comma, at the end of the
comprehension.

Tab comprehensions are ordinary expressions, so they can be nested.
This can be used to create a tab of tabs. Such a data structure can be
used to make matrices:

```antescofo
          [ [$x + $y | $x in 1 .. 3] | $y in [10, 20, 30] ]
            →  [ [11, 12], [21, 22], [31, 32] ]
```


### More Examples of Tab Comprehension

Here are some additional examples of tab comprehensions to illustrate
the syntax:

```antescofo
          $z := [ 0 | (100) ]        ; builds a vector of 100 elements, all null
```

In this example, the iterator variable is absent. In this case, the
input set is constrained to be an expression between parentheses and
evaluating to either a number or a tab (it cannot be a range).


```antescofo
          $s := [ $i | $i in 40, $i % 2 == 0 ] ; lists the even numbers from 0 to 40
          $t := [ $i | $i in 40 : 2]           ; same as previous 
          $u := [ 2*$i | $i in (20) ]          ; same as previous

          ; equivalent to ($s + $t) assuming arguments of the same size
          [ $s[$i] + $t[$i] | $i in @size($t) ] 
          
          $m := [ [1, 2, 3], [4, 5, 6] ]        ; builds a matrix of 3x2 dimensions
          $m := [ [ @random() | (10) ] | (10) ] ; builds a random 10x10 matrix

          ; transpose of a matrix $m
          [ [$m[$j, $i] | $j in @size($m)] | $i in @size($m[0])]

          ; scalar product of two vectors $s and $t
          @reduce(@+, $s * $t)

          $v := [ @random() | (10) ] ; builds a vector of ten random numbers
          ; matrice*vector product
          [ @reduce(@+, $m[$i] * $v) | $i in @size($m) ]

          ; squaring a matrix $m, i.e. $m * $m
          [ [ @reduce(@+, $m[$i] * $m[$j]) | $i in @size($m[$j]) ] 
            | $j in @size($m) ]
```



## Mutating a tab's element

A tab is a **mutable** data structure : one can change an element within
this data structure. Although a similar syntax is used, changing one
element in a tab is an atomic action different from the assignment of a
variable. For example

```antescofo
          let $t[0] := 33
          $t[0] := 33   ; the 'let' is optional if the tab is denoted by a variable
```

changes the value of the first element of the tab referred by
`:::antescofo $t` to the value `:::antescofo 33`. The general syntax is:


{!BNF_DIAGRAMS/tab_assignment.html!}

Unless the tab is referred to by a variable,
the `:::antescofo let` keyword is mandatory. It is required when the
expression in the left hand side of the assignment is more complex than
a variable, a simple reference to an array element or a simple access to
a local variable of an *exec*. See sect. [Assignment].


Because the tab to mutate can be referred to by an arbitrary expression,
one may write something like:

```antescofo
          $t1 := [0, 0, 0]
          $t2 := [1, 1, 1]
          @fun_def @choose_a_tab() { (@rand(1.0) < 0.5 ? $t1 : $t2) } 
          let @choose_a_tab()[1] := 33
```

that will change the second element of a tab chosen randomly between
 `:::antescofo $t1` and `:::antescofo $t2`. Notice that:

```antescofo
          let @choose_a_tab() := [2, 2, 2]  ; invalid statement
```

raises a syntax error: this is neither a variable assignment nor the
update of a tab element (there are no indices to access such element).

Elements of nested tabs can be updated using the multi-index notation:

```antescofo
          $t := [ [0, 0], [1, 1], [2, 2] ]
          let $t[1,1] := 33
```

will change the tab referred by to `:::antescofo [[0, 0], [1, 33], [2,
2]]`. One can change an entire “column” using partial indices:

```antescofo
          $t := [ [0, 0], [1, 1], [2, 2] ]
          let $t[0] := [33, 33]
```

will produce `:::antescofo [[33, 33], [1, 1], [2, 2]]`. Nested tabs are
not homogeneous, so the value in the r.h.s. can be anything.



## Sharing and copying a tab

In the introduction of section [Values], we mention that a compound
value is refered through a handle (or pointer). So, the same compound
value can be shared between variables or shared between nested data
structures.

This can be illustrated by the following example:

```antescofo
          let $t := [0, 0, 0]
          $u := $t
          let $t[0] := 333
          print $u  ; will print [333, 0, 0]
```

After the assignment to `:::antescofo $t`, the value referenced by
`:::atescofo $u` has mutated, because the same tab is referenced by
`:::antescofo $t` and `:::antescofo $u`.

Assignment of a tab, and more generally a compound value, does not
imply the copy of the referenced value. The function [@copy] can be used to
create a fresh value:


```antescofo
          let $t := [0, 0, 0]
          $u := @copy($t)
          let $t[0] := 333
          print $u  ; will print [0, 0, 0]
```

The sharing of data structures is useful: the same tab can be referred to
from many different places in the score and one update is visible by all
the tab referrers. However, it may be troublesome. The following code is
intended to create a 3×3 null matrix `:::antescofo $m0`

```antescofo
   let $row := [0, 0, 0]
   let $m0 := [ $row, $row, $row ]
```

but this is not exactly the case. As a matter of fact, if we want to
turn `:::antescofo $m0` in the identity matrix using assignment:

```antescofo
   let $m0[0, 0] := 1
   let $m0[1, 1] := 1
   let $m0[2, 2] := 1
```

what we obtain is a a 3×3 matrix full of `:::antescofo 1`: because all
the row of `:::antescofo $m0` refers to the same tab. 





## Assignment _versus_ Mutating a tab's element


Changing the value of a tab's element _is not a variable assignment_:
the variable has not been “touched”, it is the value referred by the
variable that has mutated.

The difference between variable assignment and mutating one element in a
tab is more evident in the following example:

```antescofo
          let [0, 1, 2][1] := 33
```

where it is apparent that no variable at all is involved. The previous
expression is perfectly legal: it changes the second element of the tab
`:::antescofo [0, 1, 2]`. This change will have no effect on the rest of
the program because the mutated tab cannot be refered elsewhere but this
does not prevent the action to be performed.


_An important consequence_ is that mutating a tab elements does not
trigger a [whenever] even if a variable is involved in the assignment.
It is however very easy to trigger a [whenever] watching a variable
referring to a tab, after the update of an element: it is enough to
assign it to itself:

```antescofo
          $t := [1, 2, 3]
          whenever ($t[0] == 0) { ... }
          let $t[0] := 0 ; does not trigger the whenever
          $t := $t       ; the whenever is triggered       
```

We can mutate the first element of `:::antescofo $t` but this does not
trigger the [whenever]. To do so, we assign the variable to itself. As a
matter of fact, a [whenever] watches the assignment of a set of
variables , NOT the mutation of the values referred by these variables.




##  Listable Operators

Usual arithmetic and relational operators are **listable** (cf. other
listable functions in annex [Library]).

When an operator $op$ is marked as *listable*, the operator is extended
to accept tab arguments in addition to scalar arguments. Usually, the
result of the application of $op$ on tabs is the
point-wise application of $op$ to the scalar elements of the tab. But
for relational operators (predicates), the result is the predicate that
returns true if the scalar version returns true on all the elements of
the tabs. If the expression mixes scalar and tab, the scalars are
extended pointwise to produce the result. So, for instance:

```antescofo
          [1, 2, 3] + 10               →  [11, 12, 13]
          2 * [1, 2, 3]                →  [2, 4, 6]
          [1, 2, 3] + [10, 100, 1000]  →  [11, 102, 1003]
          [1, 2, 3] < [4, 5, 6]        →  true
          0 < [1, 2, 3]                →  true
          [1, 2, 3] < [0, 3, 4]        →  false
```



###  Tab manipulation

Several functions exist to manipulate tabs *intentionally*, *i.e.*,
without referring explicitly to the elements of the tab. We briefly
describe some of these functions. The [Library] exhaustively describes
all tab related functions (click on function name to access the page
dedicated to the function).

---
[@car]`:::antescofo (t)` 
:  returns the first element of `:::antescofo t`
     if is not empty, else it returns an empty tab.

---
[@cdr]`:::antescofo (t)` 
:  returns a new tab corresponding to
     `:::antescofo t` deprived of its first element. If `:::antescofo
     t` is empty, returns an empty tab.

---
[@clear]`:::antescofo (t)` 
:  shrinks the argument to a zero-sized tab
  (no more elements in `:::antescofo t`, which is modified in-place).

---
[@concat]`:::antescofo (t₁, t₂)` 
:   returns the concatenation
  `:::antescofo t₁` of and `:::antescofo t₂`.


---
[@cons]`:::antescofo (v, t)` 
:  returns a new tab made of `:::antescofo
  v` in front of `:::antescofo t`.

---
[@count]`:::antescofo (t, v)` 
:  returns the number of occurrences of
  `:::antescofo v` in the elements of `:::antescofo t`. Also works on
  string and maps.


---
[@dim]`:::antescofo (t)` 
:  returns the dimension of `:::antescofo t`,
  _i.e._ the maximal number of nesting in the elements of `:::antescofo
  t`. If `:::antescofo t` is not a tab, the dimension is 0. A ‟flat” tab
  (a vector) has dimension 1.


---
[@drop]`:::antescofo (t, n)` 
:  gives a copy of `:::antescofo t` with its
  first `:::antescofo n`, elements dropped if `:::antescofo n` is a
  positive integer, and with its last elements dropped if `:::antescofo
  n` is a negative integer. If `:::antescofo n` is a tab of integers,
  returns the tab formed by the elements of whose indices are not in
  `:::antescofo n`.


---
[@empty]`:::antescofo (t)` 
:  returns true if there is no element in
  `:::antescofo t`, and false elsewhere. Also works on maps.


---
[@find]`:::antescofo (t, f)` 
:  returns the index of the first element of
  that satisfies the predicate `:::antescofo f`.  The first argument of
  `:::antescofo f` is the index of the element and the second is the
  element itself. Works also on strings and maps.


---
[@flatten]`:::antescofo (t)` 
:  builds a new tab where the nesting
  structure of`:::antescofo t` has been flattened. For example,
  `:::antescofo @flatten ([[1, 2], [3], [[], [4, 5]]])` returns
  `:::antescofo [1, 2, 3, 4, 5, 6]`.


---
[@flatten]`:::antescofo (t, l)` 
: returns a new tab where `:::antescofo
  l` levels of nesting have been flattened. If `:::antescofo l == 0`,
  the function is the identity. If `:::antescofo l` is strictly
  negative, it is equivalent to [@flatten] without the level argument.


---
[@gnuplot]`:::antescofo (t)` 
:  plots the elements of the tab as a curve
  using the external command `gnuplot`. See the description [@gnuplot]
  in the index for further information and variations.


---
[@insert]`:::antescofo (t, i, v)` 
:  inserts “in place” the value into
  the tab after the index . If is negative, the insertion takes place in
  front of the tab. If $\leq$ the insertion takes place at the end of
  the tab. Notice that the function is overloaded and applies also on
  maps. The form is also used to include a file at parsing time.

---
[@is_prefix], [@is_suffix] and [@is_subsequence] 
:  operate on tabs as
  well as on strings. Cf. the description of these functions
  in [library].

---
[@lace]`:::antescofo (t, n)` 
:  returns a new tab whose elements are
  interlaced sequences of the elements of the `:::antescofo t`
  subcollections, up to size `:::antescofo n`. The argument is
  unchanged.  For example: `:::antescofo @lace([[1, 2, 3], 6, ["foo",
  "bar"]], 12)` returns `:::antescofo [ 1, 6, "foo", 2, 6, "bar", 3, 6,
  "foo", 1, 6, "bar" ]`.

      
---
[@map]`:::antescofo (t, f)` 
:  computes a new tab where the `:::antescofo
  i`_th_ element has the value `:::antescofo f(t[i])` .


---
[@max_val]`:::antescofo (t)` 
:   returns the maximum element among those
  of `:::antescofo t`.

---
[@member]`:::antescofo (t, v)` 
:  returns true if `:::antescofo v` is an
  element of `:::antescofo t`. Also works on string and map.


---
[@min_val]`:::antescofo (t)` 
:  returns the minimum among the
  elements of `:::antescofo t`.

---
[@normalize]`:::antescofo (t, min, max)` 
:  returns a new tab with the
  elements normalized between `:::antescofo min` and `:::antescofo
  max`. If `:::antescofo min` and `:::antescofo max` are omitted, they
  are assumed to be 0 and 1 respectively.


---
[@occurs]`:::antescofo (t, v)` 
: returns the first index whose value
  equals the second argument.  Also on string and map (the
  returned value is the corresponding key in a map).


---
[@permute]`:::antescofo (t, n)` 
: returns a new tab which contains the
  `:::antescofo n`_th_ permutation of the elements of `:::antescofo
  t`. They are $s!$ permutations, where $s$ is the size of `:::antescofo
  t` (and $!$ is the factorial function). The first permutation is numbered
  0 and corresponds to the permutation which rearranges the elements of
  `:::antescofo t` in an vector $t_0$ such that elements are sorted
  increasingly. The tab $t_0$ is the smallest element[^lexico] among all
  tab that can be done by rearranging the element of `:::antescofo
  t`. The first permutation rearranges the elements of `:::antescofo t`
  in a tab $t_1$ such that $t_0 < t_1$ for the lexicographic order and
  such that any other permutation gives a tab $t_k$ lexicographicaly
  greater than $t_0$ and $t_1$. _Etc_. The last permutation (numbered
  $s! - 1$) returns a tab where all elements of `:::antescofo t` are in
  decreasing order.


---
[@push_back]`:::antescofo (t, v)` 
: pushes `:::antescofo v` to the end
  of `:::antescofo t` and returns the updated tab (`:::antescofo t` is
  modified in place).


---
[@push_front]`:::antescofo (t, v)` 
: pushes `:::antescofo v` to the
  beginning of `:::antescofo t` and returns the updated tab
  (`:::antescofo t` is modified in place and the operation requires the
  reorganization of all elements).


---
[@reduce]`:::antescofo (t, f)` 
: computes `:::antescofo f(... f(f(t[0],
  t[1]), t[2]), ... t[s-1])` where `:::antescofo s` is the size of
  `:::antescofo t`. If `:::antescofo t` is empty, an undefined value is
  returned. If it has only one element, this element is returned. Otherwise,
  he binary operation `:::antescofo f` is used to combine
  all the elements in into a single value.  For example, `:::antescofo
  @reduce(@+, t)` returns the sum of the elements of `:::antescofo t`.


---
[@remove]`:::antescofo (t, i)` 
:   removes the element at index
  `:::antescofo i` in `:::antescofo t` (`:::antescofo t` is modified in
  place). This function is overloaded and also applies to maps.


---
[@remove_duplicate]`:::antescofo (t)`
:    keeps only one occurrence of
  each element in `:::antescofo t`. Elements not removed are kept in
  order and `:::antescofo t` is modified in place.


---
[@replace]`:::antescofo (t, find, rep)` 
:  returns a new tab in which a
  number of elements have been replaced by another. See full description
  at [@remove_duplicate] in [library].


---
[@reshape]`:::antescofo (t, s)` 
:  builds an array of shape `:::antescofo
  s` with the element of tab `:::antescofo t`. These elements are taken
  circularly one after the other. For instance `:::antescofo
  @reshape([1, 2, 3, 4, 5, 6], [3, 2])` returns `:::antescofo [ [1, 2],
  [3, 4], [5, 6] ]`.
      

---
[@resize]`:::antescofo (t, s)` 
:  increases or decreases the size of
  `:::antescofo t` to `:::antescofo s` elements. If `:::antescofo s` is
  greater than the size of `:::antescofo t`, then additional elements
  will be `:::antescofo <undef>`. This function returns a new tab.


---
[@reverse]`:::antescofo (t)` 
:  returns a new tab with the elements of in
  reverse order.


---
[@rotate]`:::antescofo (t, n)` 
:  builds a new array which contains the
  elements of `:::antescofo t` circularly shifted by `:::antescofo n`.
  If `:::antescofo n` is positive the elements are right-shifted, otherwise
  they are left-shifted.



---
[@scan]`:::antescofo (f, t)` 
:  returns the tab `:::antescofo [ t[0],
  f(t[0], t[1]), f(f(t[0], t[1]), t[2]), ... ]` of the partial result of
  the reduction (see [@reduce]). For example, the tab of the factorials
  up to 10 can be computed by:
  `:::antescofo @scan(@*, [$x : $x in 1 .. 10])`.
      


---
[@scramble]`:::antescofo (t)` 
:  returns a new tab where the elements
  of `:::antescofo t` have been scrambled (their order is randomized).
  The arguments are unchanged.



---
[@size]`:::antescofo (t)` 
:  returns the number of elements of
  `:::antescofo t`.



---
[@slice]`:::antescofo (t, n, m)` 
: gives the elements of `:::antescofo t`
  of indices between `:::antescofo n` included up to `:::antescofo m`
  excluded. If `:::antescofo n > m` the element are given in reverse
  order.


---
[@sort]`:::antescofo (t)` 
:  sorts in-place the elements into ascending
  order using `:::antescofo <`.


---
[@sort]`:::antescofo (t, cmp)` 
:  performs an in-place sort on the elements,
  putting them in ascending order. The elements are compared using the function
  `:::antescofo cmp`. This function must accept two elements of the tab
  as arguments and returns a value converted to bool. The value returned
  indicates whether the element passed as first argument is considered
  to go before the second.


---
[@sputter]`:::antescofo (t, p, n)` 
: returns a new tab of length
  `:::antescofo n`. This tab is filled as follows. The process starts
  with the first element in `:::antescofo t` as the _current_
  element. Successively for each element $e$ in the result, a random
  number $p'$ between 0 and 1 is compared with `:::antescofo p`: if it
  is lower, then the current element becomes the value of $e$. If $p'$
  is greater, the element after the current element becomes the new
  current element and this element becomes the value of $e$.


---
[@stutter]`:::antescofo (t, n)` 
:  returns a new tab whose elements are
  each elements of `:::antescofo t` repeated `:::antescofo n` times. The
  argument is unchanged.


---
[@take]`:::antescofo (t, n)` 
:  gives the first elements of `:::antescofo
  t` if `:::antescofo n` is a positive integer and the last elements of
  `:::antescofo t` if `:::antescofo n` is a negative integer. If
  `:::antescofo n` is a tab of indices, it gives the tab of elements
  whose indices are in `:::antescofo n`.


<br>

## Lists and Tabs

_Antescofo_’s tabs may be used to emulate lists

- [@car], [@cons], [@cdr], [@drop], [@last], [@map], [@slice] and
    [@take] are similar to well known functions that exist in Lisp.

-  [@concat] returns the concatenation (*append*) of two lists.

- Arithmetic operations on vectors are done pointwise, as in some Lisp
    variants.

In particular, the operators [@cons], [@car] and [@cdr] can be used to
destructure and to build a tab. They can be used to define recursive
functions on tabs in a manner similar to that of recursive functions on
lists. *However*, it builds a new tab unlike the operation *cdr* on list
in Lisp. A tab comprehension is often more convenient and usually more
efficient than a recursive definition.


<br>

The [Library] documents all 
{!Library/Functions/tab_functions.list!}





<br>

<!-- FOOTNOTE -->



[^tabAccess]: The arguments of the square brackets are expressions so one
can write, _e.g._ `:::antescofo (@f($x))[$n]` to access the value of the
`:::antescofo $n`th element of the tab returned by a function
`:::antescofo @f`. Parentheses are used to apply the brackets to the
value returned by `:::antescofo @f` instead on `:::antescofo $x`.

[^lexico]: _smallest_ for the `:::antescofo <` ordering. On tabs, this
ordering corresponds to the lexicographic ordering.

