<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}

&nbsp;<br>
&nbsp;<br>

# Acknowledgements and credits

&nbsp;<br>


The _Antescofo_ documentation was mainly written by [Jean-Louis
Giavitto](http://repmus.ircam.fr/giavitto) with some help from [Arshia
Cont](http://repmus.ircam.fr/arshia-cont), [Sasha J.
Blondeau](http://www.sasha-j-blondeau.com/) and [José
Echeveste](http://www.antescofo.com/). Sam Wiseman revised the spelling 
of the very first version of the manual during an internship. 
Pierre Donad-Bouillud listed the OSC messages exchanged between Antescofo 
and the deprecated Ascograph.

The documentation is written in markdown using
[mkdocs](https://www.mkdocs.org/) to generate a static set of HTML
pages. The [railroad-diagrams
library](https://github.com/tabatkins/railroad-diagrams) is used for
drawing the syntax diagrams. _Antescofo_ code is highlighted using
[Pygment](https://pygments.org/). The soruce of the documentation is
accessible on the [AntescofoDoc gitlab
project](https://forge-2.ircam.fr/giavitto/AntescofoDoc).

<br>

_Antescofo_ was born out of the collaboration between a researcher (Arshia
Cont), a composer (Marco Stroppa), and a saxophonist (Claude Delangle)
for the world premier of _… of Silence_ in late 2007. _Antescofo_ is
particularly grateful to composer Marco Stroppa, the main motivation
behind its existence and his continuous and generous intellectual
support. Since 2007, many composers and computer musicians have joined
the active camp to whom _Antescofo_ is always grateful: Pierre Boulez,
Philippe Manoury, Gilbert Nouno, Sivan Eldar, Larry Nelson, José
Miguel Fernandez, Sasha Blondeau, Yann Marez, Jason Freeman, Christopher
Trapani and others…

_Antescofo_ is also in debt to and heartily acknowledges the patience of
RIMs at IRCAM and elsewhere that have been in the front line in the use
of _Antescofo_ in actual performances:
Greg Beller,
José Echeveste,
José Miguel Fernandez,
Thomas Goepfer,
Carlo Laurenzi,
Serge Lemouton,
Benjamin Levi,
Grégoire Lorieux,
Benoit Meudic,
Augustin Muller,
Gilbert Nouno
and many others…

<br>

Over the years, the _Antescofo_ system has been developped by Arshia
Cont, Philippe Cuvillier, José Echeveste and Jean-Louis Giavitto.
Additional help on Ascograph by Grig Burloiu, Thomas Coffy and Robert
Piechaud.

The _Antescofo_ technology is mature enough to have led to the creation
of a start-up in 2017 by Arshia Cont, Philippe Cuvillier and José
Echeveste for the development of an automatic accompaniment app called
[Metronaut](https://www.antescofo.com/). They continue to support the
systems, along with [Jean-Louis Giavitto](http://repmus.ircam.fr/giavitto) 
which focuses on the language side at the
[IRCAM](https://www.ircam.fr/)/[STMS](https://www.stms-lab.fr/)
laboratory. The [antescofo~
object](https://forum.ircam.fr/projects/detail/antescofo/) is still
freely available and will remain so for artistic and research
projects. The language continues to evolve to fix bugs, optimize
performance and include new features both to respond to artists'
requests and to offer new creative tools.



<br>

The development of Antescofo has been made possible by support from
[Ircam](http://www.ircam.fr/), [CNRS](http://www.cnrs.fr/ins2i),
[Sorbonne Université](https://www.sorbonne-universite.fr/) (the new name
of _Université Pierre et Marie Curie_, Paris 6), the [STMS
lab](https://www.stms-lab.fr/) a joint laboratory under the patronage of
the previous institutions, [Inria project
MuTAnt](http://www.inria.fr/equipes/mutant), and the [ANR project
INEDIT](http://www.agence-nationale-recherche.fr/?Projet=ANR-12-CORD-0009).


<center>
![Final image: experimental music](/Figures/musique-savant.png){: width="45%"}
</center>




