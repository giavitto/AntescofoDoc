<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## Antescofo Workflow

_Antescofo_ programs strongly interact with the environment through the
listening machine, but also through messages send through the Max/PD
patch. These interactions happen during the performance, but also at
previous phases of the _Antescofo_ workflow, when editing or debugging
the score and during rehearsals. Messages understood by _Antescofo_ are
described in section [internal commands]. The User's guide also contains
three useful sections on:

- [Editing the score]

- [Interacting with MAX/PureData]

- and [Preparing the Performance, Rehearsals].
