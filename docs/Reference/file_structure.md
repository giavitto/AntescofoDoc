<!--    -*- coding: utf-8; ispell-dictionary: "american"; fill-column: 72 -*-     -->

{!references.ref!}
{!Library/Functions/functions.ref!}




## File Structure of an Antescofo Score

### Writing an augmented score through multiple files


A simple _Antescofo_ augmented score is written in a single text
file. However, for large scores, places where one wants to make
underlying organization explicit, or places some procedures or score
elements are reused in several pieces, an _Antescofo_ augmented
score may be specified in several scores.


There is always a _main_ file from which the additional files will be
loaded. The file loaded in Max/PD or used as the last argument in a
standalone command task will be read as the _main_ file.

From the _main_, other files are loaded using the `:::antescofo @insert` directive:

```antescofo
        @insert macro.asco.txt
        @insert "file name with white space must be quoted"
```

The `:::antescofo @insert` keyword can be capitalized (`@INSERT`) as can
any other predefined `@`-keyword (`:::antescofo @local`, `:::antescofo
@target`, _etc._). Beware not to confuse the `:::antescofo @insert`
_directive_ used to include a text file at score loading time and
the [@insert] _function_.


The `:::antescofo @insert` command is often used to store definitions
and initializations of the main Antescofo score in alternate files. So
several setups may coexist with the main score.


The `:::antescofo @insert_once` command is similar to `:::antescofo
@insert` directive except that the file is included only once in the
current score, when the directive is encountered the first time. This
behavior makes possible to include a library of primitives in set of
files without the burden to take care of the dependencies.


### Conditional parts

A directive
<html>
<div class="twocol">
<div>
<br>
```antescofo
          #if
             ; ...
          #endif
```
<br>
</div>
<div>
```antescofo
          #if
             ; ...
          #else
             ; ...
          #endif
```
</div>
</div>
</html>
is available to control conditionnal parsing. They delimit blocks of
program text that are parsed only if a specified condition is
true. These directives can be nested. The program text within the blocks
is arbitrary and may consist of musical event specification, function or
process definition, etc.

The condition is written after the `:::antescofo #if`. The condition is
statically evaluated during the parsing time. If the value of the
expression is not a constant (_e.g._ if it depends of a variable), an
error is raised. If the condition is false (or equivalent to false),
then all lines between the `:::antescofo #if` and the
`:::antescofo #endif` (if the first form is used) or the
`:::antescofo #else` (if the second form is used) are ignored.

This mechanism can be used to have several version of the same program
in the same file, for instance, to initialize different setups:

```antescofo

@macro_def @current_setup { "SCALA" }

; ...

#if @current_setup == "SCALA"
    ; initialization specific to Scala
#endif
```


The mechanism can also be used to include some functionality depending
of the presence or the absence of a function. For example, differential
curve are not implemented in all _Antescofo_ versions. This can be tested
by the existence of the function `:::antescofo @ode_system`:

```antescofo
#if @ode_system
    ; here we can use differential curve
#else
    ; here we cannot rely on differential curve
#endif
```


<br>

### Load and Preload Command


A file is loaded through a `load` message sent to the Antescofo object
(in Max or PD). The argument of this message is the path of the file to
load. The effect of this command is to abort the current computations
(if any) and to load the specified file which becomes the **current
score**. The previous score, if any, is simply thrown away with all of its
definitions: definitions are not shared between two score loads.


A file can also be preloaded using the command `preload file name`. The
`file`argument is the path of a file while `name`is a simple identifier
to refer to the score in future command.

---
**NOTA BENE**:
the use of the preload command is **deprecated**. For new pieces, it is better
to use file inclusion facilities and commands `:::antescofo
antescofo::visitlabel` or `:::antescofo antescofo::startfromlabel` to
move into the score.

---

Preloaded scores are used to defines functions, processes, actors, data
structures: the definitions in a preloaded score are added to the
currently known definitions. So they are in use for the next preloads
and for the (final) load. A preload does not define a current score. So,
the usual workflow is to preload a set of files with a final load.

Preloaded scores can also contain definitions of musical events to
follow. During the program execution, one can switch from the current
score to the score defined by a preloaded score using the `:::antescofo
start name` command with `name` refering to a preloaded score. A
`start` command without name simply starts the current score.

The `start name` command can be issued from the _Antescofo_ program
itself using the `:::antescofo antescofo::start` action. This will keep
the program state (variables values, processes, _etc._) but divert the
listening machine to follow the new specified score. 


