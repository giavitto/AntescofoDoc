# -*- coding: utf-8 -*-
"""
    pygments.styles.jlgclassic
    ~~~~~~~~~~~~~~~~~~~~~

    A highlighting style for Pygments/Antescofo

"""

from pygments.style import Style
from pygments.token import *


class JlgclassicStyle(Style):
    """
    The default Antescofo style
    """

    background_color = "#f0f0f2"
    default_style = ""

    styles = {
        Whitespace:                "#bbbbbb",
        Comment:                   "#408080",
        Comment.Single:            "noitalic #406060",
        Comment.Multiline:         "noitalic #402040",
        Comment.Preproc:           "noitalic",
        Comment.Special:           "noitalic bold",

        Literal:                   "#445588",

        Keyword:                   "bold #006699",
        Keyword.Pseudo:            "nobold",
        Keyword.Reserved:          "nobold",
        Keyword.Type:              "bold #bb0066",

        Operator:                  "#666666",
        Operator.Word:             "bold #AA22FF",

        Name.Builtin:              "#0F083F",
        Name.Builtin.Pseudo:       "#1522AA",
        Name.Function:             "nobold #C34E00",
        Name.Class:                "#000080",
        Name.Namespace:            "#cd0000",
        Name.Exception:            "bold #D2413A",
        Name.Variable:             "#B8860B",
        Name.Constant:             "#880000",
        Name.Label:                "#A0A000",
        Name.Entity:               "#fb660a bold",
        Name.Attribute:            "#BB4444",
        Name.Tag:                  "bold #008000",
        Name.Decorator:            "#AA22FF bold",
        Name.Other:                "italic #FF0000",

        String:                    "#BB4444",
        String.Doc:                "italic",
        String.Interpol:           "italic",
        String.Escape:             "bold #BB6622",
        String.Regex:              "#BB6688",
        String.Symbol:             "#B8860B",
        String.Other:              "#008000",
        Number:                    "#666666",

        Generic.Heading:           "bold #000080",
        Generic.Subheading:        "bold #800080",
        Generic.Deleted:           "#A00000",
        Generic.Inserted:          "#00A000",
        Generic.Error:             "#FF0000",
        Generic.Emph:              "italic",
        Generic.Strong:            "bold",
        Generic.Prompt:            "bold #000080",
        Generic.Output:            "#888",
        Generic.Traceback:         "#04D",

        Error:                     "border:#FF0000"
    }
