#!/bin/bash

### run pandoc to produce a latex file
### file are extracted from mldoc.yml

echo "File list:"
echo "   " `grep "\.md" mkdocs.yml | sed -f extract.sed`

pandoc \
    pandoc.yaml \
    `grep "\.md" mkdocs.yml |  sed -f extract.sed` \
    docs/references.ref \
    docs/Library/Functions/functions.ref \
    -markdown+markdown_include \
    --template antescofoLatextemplate.tex \
    --smart --normalize \
    -s -o manual.tex
       

rm -f LATEX/_manual.tex LATEX/manual.*
mv -f manual.tex LATEX/_manual.tex
cd LATEX

echo "Fix latex"
sed -f ../fix_latex_typo.sed _manual.tex > manual.tex

echo "PDFLATEX"
pdflatex manual.tex

echo DONE DONE
