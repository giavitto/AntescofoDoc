#!/bin/bash

### copy the entire directory HTML in FixHTML
###    rm -fr HTML ; cp -r ../HTML .
### 
### run this command in FixHTML directory
###     sh ../fix_html_for_pdf.sh
### 
### file are extracted from mldoc.yml
### html is fixed using a flex filter Build/fixHTML



SRC=`grep "\.md" ../mkdocs.yml | sed -f ../extract3.sed | sed -e s+index/index+index+g `

LST=`grep "\.md" ../mkdocs.yml | grep Library | sed -f ../extract2.sed`

NB=11

\rm -fr open_html.sh HTMLfixed

echo "#!/bin/bash\n\n" > open_html.sh

# mkdir HTMLfixed


for f in $SRC; do
    base=`basename $f`
    dir=`dirname $f`
    name=$dir/$NB"_"$base
    nf=$NB"_"$base
    echo "process file " $base " directory " $dir " in name " $name
    Build/fixHTML $name.pdf $nf < $f > $name
    ((NB++))
    case $f in
	*/Library/Functions/00intro*)
	    echo "open -F -a firefox " $name >>  open_html.sh
	    ;;
	*/Library/Functions/*)
	    ;;
	*)
	    echo "open -F -a firefox " $name >>  open_html.sh
	    ;;
    esac
done

echo "open -F -a firefox function_manual.html" >>  open_html.sh



rm -fr list.html
echo "<html>\n" > list.html
echo $LST >> list.html
echo "</html>" >> list.html


exit 0

echo
echo "Processing pandoc for function list"
pandoc \
    $LST \
    --smart --normalize \
    -s -o function_manual.html

       


echo DONE DONE
