
%{


/* C++
#include <stdlib.h>

#include <fstream>
#include <iostream>
#include <sstream>
*/

char* name = "default_path";
char* short_name = "default_name";

// #define yyFlexLexerOnce

// Return 1 if there is no more input
// inline int yyFlexLexer::yywrap() { return 1; }

%}


    // The scanner is in C++
    // %option c++

    // use with bison (use yylval)
    // no need : %option bison-bridge

    // To change the prefix "yy" for another one
    // change because there is a parser for Faust 


    // DO NOT USE these in-file option: they are fixed by the
    // xcode compilation command to produce the generated file
    // at the right place

    // The scanner is reading 8bit not 7bit
%option 8bit

    // The scanner is not interactif (we read in a file)
%option batch

/*
  // The lineno are handled by flex
  // %option yylineno

     // At the end of the file, we stop
    // %option noyywrap


      // %option prefix="fixHTML"


  // %option debug
  // %option verbose

  // %option warn

*/
  // To test  : expansion on the standard output
%option default


startMenu "<div class="\""navbar navbar-default navbar-fixed-top"

endMenu [ \t\n]+"</ul>"[ \t\n]+"</div>"[ \t\n]+"</div>"[ \t\n]+"</div>"



%%
%{
   yyset_debug(0); 
%}

{startMenu}(.*\n+)+{endMenu} {
      printf("<script type=\"text/javascript\">\n");
      printf("jsPrintSetup.setOption('orientation', jsPrintSetup.kPortraitOrientation);\n");
      printf("jsPrintSetup.setOption('marginTop', 15);\n");
      printf("jsPrintSetup.setOption('marginBottom', 15);\n");
      printf("jsPrintSetup.setOption('marginLeft', 20);\n");
      printf("jsPrintSetup.setOption('marginRight', 10);\n");
      printf("jsPrintSetup.setOption('headerStrLeft', '');\n");
      printf("jsPrintSetup.setOption('headerStrCenter', '');\n");
      printf("jsPrintSetup.setOption('headerStrRight', '');\n");
      printf("jsPrintSetup.setOption('footerStrLeft', '');\n");
      printf("jsPrintSetup.setOption('footerStrCenter', '');\n");
      printf("jsPrintSetup.setOption('footerStrRight', '');\n");
      // printf("jsPrintSetup.setSilentPrint(true);\n");

      // printf("jsPrintSetup.printToFile(true);\n");
      // printf("jsPrintSetup.toFileName('%s');\n", name);

	printf("jsPrintSetup.printWindow(window);\n");
      // printf("jsPrintSetup.setSilentPrint(false)\n");
      printf("</script>\n");
}


"<footer class=\"col-md-12"(.*\n*)*"</footer>" {}

"<title>".*"</title>" {
     printf("<title>%s</title>", short_name);
}

%%

int main (int argc, char **argv)
{
  if (argc > 1)
     name = argv[1];

  argc--;
  argv++;

  if (argc > 1)
     short_name = argv[1];


  yylex();
}