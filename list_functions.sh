#!/bin/bash

cd docs/Library/Functions

DD=`date "+%Y-%m-%d:%H:%M:%S"`
## mv functions.list  "old.$DD.functions.list"
rm -fr functions.ref functions.list

for f in $(ls @*md); do
    X=`basename "$f" .md`
    echo "[$X](/Library/Functions/$X/index.html)" >> functions.list
    echo " " >> functions.list
    echo "[$X]: /Library/Functions/$X/index.html" >> functions.ref
done

echo '[@/]:  /Library/Functions/@div/index.html' >> functions.ref

for f in $(ls [a-z]*md); do
    X=`basename "$f" .md`
    echo "[@$X](/Library/Functions/$X/index.html)" >> functions.list
    echo " " >> functions.list
    echo "[@$X]: /Library/Functions/$X/index.html" >> functions.ref
done

echo "new functions listing in docs/Library/Functions/functions.list"
echo "new functions link listed in docs/Library/Functions/functions.ref"

