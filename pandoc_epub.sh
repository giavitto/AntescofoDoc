#!/bin/bash

### run pandoc to produce a latex file
### file are extracted from mldoc.yml

echo "File list:"
echo "   " `grep "\.md" mkdocs.yml | grep ":" | sed -f extract2.sed`

pandoc \
    pandoc.yaml \
    `grep "\.md" mkdocs.yml | grep ":" | sed -f extract2.sed` \
    --smart --normalize \
    -s -o manual.epub
       



echo DONE DONE
