# Installation

This document helps you download and prepare your machine for building the Antescofo Documentation in its various forms.

We specifically use [mkdocs](http://www.mkdocs.org/), [Pygments](http://pygments.org/) and [pandoc](http://pandoc.org/) with Python installed.


### Get the Antescofo Documentation Repository

You need a Forge Account and then:
[https://forge-2.ircam.fr/p/AntescofoDoc/source/help/](https://forge-2.ircam.fr/p/AntescofoDoc/source/help/)


Once the repository is cloned, you have a new directory called `AntescofoDoc` which is the root of the project. 


### Mkdocs

* <code>brew install mkdocs</code>

Alternatively

* Install Python on your machine if not done yet!
* Open a terminal
* Install *pip3*: <code>sudo -H easy_install pip3</code>
* Install MkDocs: <code>sudo -H pip3 install mkdocs</code> The installation of
  mkdocs using pip3 is optional: you can installit with brew and the package with
  pip3. If makdocs is not working after package upgarde, try to reinstall mkdocs. 

Then install the Python libraries that are needed:

* Install the Markdown Extension *markdown_include*: <code>sudo -H pip3
  install markdown-include</code>

* Install the markdown Extension *pymdown* (to have inline syntax
  highlighting): <code>sudo -H pip3 install pymdown-extensions</code>

* Install the [*material*
  style](https://squidfunk.github.io/mkdocs-material/) (if needed):
  <code>sudo -H pip3 install mkdocs-material</code>

* Install the *cinder* style (if needed): <code>sudo -H pip3 install
  mkdocs-cinder</code>

* Install the *bootswatch* style (cerulean, amelia, etc., if needed):
  <code>sudo -H pip3 install mkdocs-bootswatch</code>

* Install font-awesome
  [http://bwmarrin.github.io/MkDocsPlus/fontawesome/] <code>sudo -H pip3
  install fontawesome_markdown</code> and then add the url of the
  javascript file distribution in the extra_javascript item in mkdoc.yml

You can now locally see the documentation by going to the *mkdocs.yml*
file and doing <code>mkdocs serve</code>. The documentation is then
visible on a web page: open in your browser the URL
`http://127.0.0.1:8000`

To upgrade the various packages:  <code>sudo -H pip3 install --upgrade package</code>

In summary (order may matter):
<code>
    pip3 install --upgrade --force-reinstall  markdown-include
    pip3 install --upgrade --force-reinstall  pymdown-extensions
    pip3 install --upgrade --force-reinstall  mkdocs-material
    pip3 install --upgrade --force-reinstall  mkdocs-cinder
    pip3 install --upgrade --force-reinstall  mkdocs-bootstrap
    pip3 install --upgrade --force-reinstall  mkdocs-bootswatch
    pip3 install --upgrade --force-reinstall  fontawesome_markdown
    pip3 install  --upgrade --force-reinstall  mkdocs-autolinks-plugin

    pip3 install --upgrade --force-reinstall  mkdocs

</code>
If something goes wrong in library code, try to uninstall and to reinstall them 
(sometimes the change of version leaves spurious dependances and the installation
goes wrong). 



### Pygment

If you want to have syntax highlighting for Antescofo code, you should
install Pygment.  To install [Pygments](http://pygments.org/):
<code>pip3 install Pygments</code>:
- remove the pygmentize command installed by brew (if any): `brew uninstall pygments` 
- remove previous pip installation of pygmentize in `/usr/local/lib/python3.9/site-packages/Pygments-*`
(use `locate Pygments-" to find these sources directories)"
- try not to use `sudo -H` by removing the non-editable directory). 

Then:

1. source :

     - OLD OLD METHOD : Go to AntescofoDoc/PYGMENT/Pygments-2.2.0 or to the latest
       version. When you install a new version, copy
       `.../pygments/lexers/antesc.py` and
       `.../pygments/styles/jlgclassic.py` from the old modified pygment
       installation into the new one. Also adjust the PYTHONPATH (in
       your shell and in `.cshrc` for future uses): `setenv PYTHONPATH
       "/Users/giavitto/UTopIa/antescofoDOC/PYGMENT/Pygments-2.2.0"`
       
     - OLD METHOD: Go to AntescofoDoc/PYGMENT/pygments which is a git
       managed directory (clone of `git clone
       https://github.com/pygments/pygments`). When you upgrade, check
       that `...pygments/pygments/lexers/antesc.py` and
       `...pygments/pygments/styles/jlgclassic.py`. Adjust the PYTHONPATH
       (in your shell and in `.cshrc` for future uses): `setenv
       PYTHONPATH
       "/Users/giavitto/UTopIa/antescofoDOC/PYGMENT/pygments"`
       
    - NEW METHOD: now Pygment uses tox instead of Makefile:
      - install tox: `pip3 install tox`
      - get the source in `AntescofoDoc/PYGMENT/` : 
        `git clone https://github.com/pygments/pygments`
      - add the antescofo lexer and style that are in 
        `.../Pygment_antescofo_files/...` into:
          - `...pygments/pygments/lexers/antesc.py` 
          - and `...pygments/pygments/styles/jlgclassic.py`
    
2. `pip3 install -e .` (instead of `sudo -H python3 setup.py install`)
    Installation should go in `/usr/local/bin/pygmentize` 

3. Check taht the installed pygmenrtize command handle antescofo file with
   `pygmentize -L | grep antesc`
   
To test the pygment lexer directly, you can use the command:

     pygmentize -O full,style=jlgclassic -l antescofo -o /tmp/tt.html  /tmp/tt.antesc

Which will pygmentize the antescofo source code in `/tmp/tt.antesc` as
an html file in `/tmp/tmp.html` following the style `jlgclassic`.


### Pandoc

Pandoc can be installed using the distribution available on the 
[Pandoc Download Page](http://pandoc.org/installing.html)

### Installing on Linux

*mkdocs*, *pygment*, and *pandoc* should be in the repositories of your distribution.

For instance, for debian, do `sudo apt-get install mkdocs python-pip  pygment pandoc`. You are likely to have to install
`markown-include` with `pip`.
On Debian, mkdocs is installed to be used with Python 3 but `pip` installs for Python 2 by default. Install also `pip3` with
`sudo apt-get install python3-pip`and then invoke `pip3` instead of `pip`to install `markdown-include`.


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Building the Documentation

They are three ways to obtain the documentation:

1. (static) _HTML documentation_: Just go to the AntescofoDoc folder in
terminal and <code>mkdocs build</code>. This will build the HTML version
in the HTML folder. Use flag <code>-c</code> to remove the old files in
the destination directory. The root page `AntescofoDoc/HTML/index.html`
can then be visualized by opening it in a browser.

2. _web page_: Just go to the AntescofoDoc folder in terminal and
<code>mkdocs serve</code>. You can then open the URL
`http://127.0.0.1:8000` in a browser and you get the doc. The result is
 the same as the previous method but the page are genarated in
real-time, as soon as you change one of the documentation file.

Use <code>mkdocs serve --dirtyreload</code> durng development, to recompile
only the markdown file that have changed. This will speed-up the display but
some links will possiblu be broken. 

3. _latex document_: go to the AntescofoDoc folder in terminal and
<code>sh ./pandoc.sh</code>. A Latex file `manual.tex` is generated in
the folder `LATEX`. You can compile it, _e.g._ with `pdflatex
manual.tex`.

Various adjustement are made by the script `pandoc.sh` on the latex
source, using `sed` script `fix_latex_typo.sed`




- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Installing the documentation on support.ircam.fr

Compile the doc with `mkdocs build --clean` with the mkdocs.yaml option
`use_directory_urls: true`. Remove the `HTML/Figures/stuff*` and
`HTML/Figures/Supplement.pptx` files.  Then copy (scp) the `HTML`
directory into `support.ircam.fr:/var/www/html/docs/Antescofo` and
rename `HTML` into `manuals`.

Recompile with `use_directory_urls: false` and zip the directory into a
file `AntescofoDocHTML.zip`:

```
    mv HTML AntescofoDocHTML
    zip -r AntescofoDocHTML.zip AntescofoDocHTML
```    

and put the file in `support.ircam.fr:/var/www/html/docs/Antescofo/`

Compile the pdf file (cf. previous section) and copy it at
`support.ircam.fr:/var/www/html/docs/Antescofo/AntescofoDoc.pdf`

With SFTP:
The access policy is changing. An sftp access must be granted:
- use the comand line `sftp giavitto@support.ircam.fr`
- move to the right directory (if the machine has been jailed, you only see the directory 
  support and its subdirectories): just do `ls` and `cd`
- upload the `HTML` directory and rename it using `put -R`, `rm` and `rename`commands.


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# Writing the documentation


### Document organization

The sources of the docs are located in directory
`AntescofoDoc/docs`. The layout of the documentation is described in
`AntescofoDoc/mkdocs.yml`. The current organization splits the doc in four
parts:

- a User Manual
- a Reference Guide
- a Library
- Cookbook (Examples, HowTos and Code Snippets)

Each parts correspond to a directory in `AntescofoDoc/docs/`. These
directory are populated with the source files. These files are written
in **markdown**
[markdown](https://daringfireball.net/projects/markdown/)
(see section _Stylistic convention_ below).

The file `AntescofoDoc/mkdocs.yml` contains a description of the
documentation structure and links the chapters with the source
file. This desciption is written in `yaml` a format used to describe
document meta-data. Please, read the `mkdocs` documentation for an
explanation of the content of  `AntescofoDoc/mkdocs.yml`.  If you want
to add new chapter, section, pages, or source files to the
documentation, or change its layout or structure, you have to change
accordingly the `mkdocs.yml` description. 

The directory `docs/Figures` contains all figures in png or jpeg format. The
same figure can be given also in pdf, for a better rendering in latex.

The directory `docs/BNF_DIAGRAMS` contains teh javascript and the css
style used to draw _railroad diagrams_ used to illustrate the
_Antescofo_ grammar in the doc. The diagram itself are in
`docs/BNF_DIAGRAMS/bnf_xxx.html` and included in the doc file using the
`{! ... !}` construction.

The `pandoc.yaml`contains meta information used by pandoc to generate
the latex. The file `antescofoLatextemplate.tex` is the latex template
used by pandoc to generate the latex document. It can be modified to
improve the latex documentation.

The shell command `pandoc.sh` contains the command to generate the
documentation. This command gathers automatically the sources file by
analysing the `mkdocs.yml` description of the doc, with the help of the
`extract.sed` command.


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


### Changing or updating documentation pages

To change a documentation page, you-just have to edit the corresponding
source file (written in markdown format). You can visualize on-th-fly
the result, by launching the command `mkdocs serve` in directory
`AntescofoDoc`. The documentation is then visible on a web page: open in
your browser the URL `http://127.0.0.1:8000`. The command
<code>mkdocs serve --dirtyreload</code>
can be used to make the display incremental (only in _debug mode_). 


### Adding new pages

So, if you want to add documentation, you can add a file in
`AntescofoDoc/docs/...` and describe the new entry in `mkdocs.yml`. The
figure appearing in this page must be given in `AntescofoDoc/Figures`
and the references added to `AntescofoDoc/docs/references.ref`. That's
enough. 

To help the migration from latex document to markdown source, you can
use `pandoc` to obtain a first draft that is then easy to adjust by
hand. The command `pandoc -o tmp.md source.tex` will produce in file
`tmp.md` a traduction in markdown format of teh latex `source.tex`. A
number of problems are to be expected:

- all user defined commands disappear in the translation

- Antescofo code given in `lstlisting` environment is translated in
markdown `code block`. Please convert these code blocks in `fence code`,
_i.e._ put a ` ```antescofo`
on the first line and ` ``` `
at the last line. See examples of code in file
`AntescofoDoc/docs/Library/snipets.md`

- table and figure are incorrectly translated. 


- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -



### Stylistic convention


#### Links and References

To simplify the writing of the documentation, the file
`AntescofoDoc/docs/references.ref` contains the reference (link, URL,
etc.) in markdown syntax. This file can be included in any other source
file using the `markdown-include`extension, using the syntax

```
{!references.ref!}
```

It allows to mutualize the writing of references and links.

In `AntescofoDoc/Library/Functions/` they are several files that
contains references to functions and various list of functions:

- `functions.ref` contains a reference `[@f]` to the page describing
  function `@f`. When you add a new function, you have to add the
  corresponding ref in this page, in addition to the source file `f.md`
  that describe the function.

- `xxx_functions.list` contains a list of the functions related to
  domain `xxx` (_e.g._ predicate, math, system, etc.) Please update
  these lists with the new functions if needed. These list are used to
  provide the reader a bird view of the library.




#### Inclusion of Antescofo Code and Pygment improvement

Another extension is useful: the _code highlighting_, see documentation at
page
[CodeHilite](https://pythonhosted.org/Markdown/extensions/code_hilite.html).
The syntax is the following
<code>
  :::antecofo
  ; code
  ; here
</code>
See file `docs/Library/snipets.md`
for an example that combines code highlighting and file inclusion.

The highlighting relies on pygment. A lexer has been developped for
_Antescofo_: `PYGMENT/Pygments-2.0.2/pygments/lexers/antesc.py`. It can
be improved. If so, you have to rebuild pygment: follows the instruction
of section "Pygments" above. Install the new version only if it passes
the test.

Note that the generation of test is buggy or very sensitive to the
layout of the `antesc.py`file. If the test fails, changing the
indentation may be enough to make it works. For the same reason, avoid
tabulation in this file.

When the new version is installed, the mkdocs and pandoc tools will take
it into account automatically.

#### Latex maths

If you surround a Latex math formula by `$$` or `$` or use `\begin{align} \end{align}`
(or similar math environment such as `equation`), formulas will be rendered on the webpage
with mathjax thanks to the Python markdown extension *arithmatex* (already installed with
*pymdown-extensions*)



#### Adding other markdown extensions

Other extensions can be used: they must be declared in the
`AntescofoDoc/mkdocs.yml` file and installed using `pip`. See page
[markdown extension](https://pythonhosted.org/Markdown/extensions/) to
have a list of available features.

Such extension must also be handled by pandoc, to be propagated to
latex. See the pandoc documentation for the specification of the right
flags. The pandoc command is in `pandoc.sh`


#### Figures

To allows the translation of figure into proper latex figure, it is
better to include figure using the markdown syntax:

<code>
   ! [principe] (../Figures/principe.png)
</code>

(with no spurious space).



#### Railroad diagrams

Railroad diagrams are used to illustrate the grammatical construct in
the reference manual. they can be used for others purposes. They are
generated in SVG using a javascript program embedded in the HTML pages.
The program is borrowed from `https://github.com/tabatkins/railroad-diagrams`.

The script and the css style are in
`docs/BNF_DIAGRAMS/railroad-diagrams-gh-pages/`. The diagrams itself are
html file in `docs/BNF_DIAGRAMS/`. These files are included where needed. 

The construction of the diagram is by a term denoting a BNF form.
See the `README.md` file for a short introduction. 



#### Advanced features

Markdown accepts litteral HTML code. So you can center, change colors,
adjust bloc of texts and image, using the HTML feature. See markdown
description at page 
[markdown](https://daringfireball.net/projects/markdown/)


#### Changing the HTML page style

You can change the layout of the documentation page by changing the
style of the documentation in `mkdocs.yml`.

You can adjust the documentation style by changing the css style.

A default css style is defined for code highlighting in
`docs/jlgclassic.css`. This file as been generated from a dedicated
style developped for pygment using the command (in the
last `PYGMENT/Pygments-...` directory):

```
pygmentize -O full -S jlgclassic -f html > ../../docs/jlgclassic.css
```

This css give a style to the various kind of token recognized by
`antesc.py`. This syle complement teh defaults style. So it must be
adjusted if you want to overide completely the default documentation
style.


