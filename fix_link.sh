#!/bin/bash


echo process  $(ls *.md)


for f in $(ls workflow_ed*.md); do
    echo "Processing $f"
    sed \
	-f /Users/giavitto/UTopIa/Antescofo/Work/AntescofoDoc/replace.sed \
	< $f \
	> $f.tmp
    mv  $f _tmp_$f
    mv $f.tmp $f
done

echo DONE

